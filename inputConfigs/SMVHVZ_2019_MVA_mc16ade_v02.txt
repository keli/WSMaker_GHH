CoreRegions
ZeroLepton ZeroLep/r32-15_customCDI_20190820/LimitHistograms.VHbb.0Lep.13TeV.mc16ade.Oxford.32-15NewRegionsOldExtensions.root mvaFullRun2Default,MET,mBB,mvadiboson
OneLepton OneLep/r32-15_NewMVA/NoSTXS/LimitHistograms.VHbb.1Lep.13TeV.mc16ade.LAL.v06.NewRegionsH_NEW.root mvaFullRun2Default,pTV,mBB,mvadiboson
TwoLepton TwoLep/r32-15_customCDI_20190814/v2/fullSysts/LimitHistograms.VHbb.2Lep.13TeV.mc16ade.Kyoto.r32-15_customCDI_20190814.v2.root mvaPolVars,pTV,mBB,mvadiboson
