#!/usr/bin/env python

import sys
import os
import AnalysisMgr_GHHRun2 as Mgr
import BatchMgr as Batch

InputVersion = "GHH_2l_0629"

def add_config(mass, lumi, unc = "StatOnly", vtype="Boosted", var2l= 'mV', signal = "GHH600X", one_bin=False,mcstats=True, 
               ssTwoLep=True, osTwoLep=False, ThreeLep=False, debug=False, doEMu=True):
    conf = Mgr.WorkspaceConfig_GHHRun2(unc=unc, vtype=vtype, InputVersion=InputVersion, signal=signal, MassPoint=mass, OneBin=one_bin, UseStatSystematics=mcstats, Debug=debug, DoShapePlots=True, DeleteNormFiles=False, DoLumirescale=lumi,  LogScalePlots=False)

    conf.set_regions(signal=signal, var2l=var2l, vtype=vtype, sstwo_lepton=ssTwoLep, ostwo_lepton=osTwoLep, three_lepton=ThreeLep,  doemu=doEMu)

    fullversion = "GHHRun2_13TeV_" + vtype + "_" + outversion + "_"  + signal + "_" + str(lumi) + "_" +unc + "_" + "ss2l" + str(ssTwoLep) + "_os2l" + str(osTwoLep) + "_3l" + str(ThreeLep) + "_" + var2l+ "_EMu" + str(doEMu)

    return conf.write_configs(fullversion)

if __name__ == "__main__":

    if len(sys.argv) is not 2:
        print "Usage: Analysis_GHHRun2.py <outversion>"
        exit(0)

    outversion = sys.argv[1]


    # For running on existing workspaces (-k option):
    #WSlink = "/afs/cern.ch/user/g/gwilliam/workspace/WSMaker/workspaces/MonoH0708152p.180815_MonoH_13TeV_180815_Systs_Scalar_2000/combined/2000.root"
    #os.system("ls -lh " + WSlink)

    ########## create config files

    print "Adding config files..."
    configfiles_stat = []
    configfiles = []
    mass = [300,600,900]
    vtype = ["Boosted","Medium","Resolved"]
    vs2l= ['mV']
    lumi = [1]
    sstwoLep = True
    ostwoLep = False
    threeLep = False
    doemu = False
    syst_type="StatOnly"


    for l in lumi:
        for m in mass:
            for vs in vs2l:
                for vt in vtype:
                     print "Adding config file for " 
                     configfiles += add_config(str(m), l, unc=syst_type, vtype=vt, signal="GHH"+str(m)+"X", var2l=vs, one_bin=False, mcstats=True, 
                                              ssTwoLep=sstwoLep, osTwoLep=ostwoLep, ThreeLep=threeLep, 
                                              debug=True, doEMu=doemu)
    for fi in configfiles_stat:
        print fi
    for fi in configfiles:
        print fi

    ########## define tasks

    tasks = ["-w"]             # create workspace (do always)
    # tasks += ["-k", WSlink]
    tasks += ["-l", 1]         # limit
#    tasks += ["-s", 1]         # significance
#    tasks += ["--fcc", "1,2@{MassPoint}"]  # fitCrossChecks
#    tasks += ["-u", 1]         # breakdown of muhat errors stat/syst/theory
    tasks += ["-p", "0,2,3@{MassPoint}"]
    tasks += ["-t", "0,1@{MassPoint}"]
    # # requires fitCrossChecks:
    # tasks += ["-m", "4,10"]     # reduced diag plots (quick)
    # # tasks += ["-b", "2"]       # unfolded b-tag plots (quick)
    # the '!' signifies the reference workspace, & signifies to do sum plots
    # tasks += ["-p", "0,1@{MassPoint}"]       # post-fit plots (CPU intense)
    # # tasks += ["-p", "0,1;@{MassPoint};blah,blah"]       # post-fit plots (CPU intense)
    # # tasks += ["-r", "{MassPoint}", "-n", "{MassPoint}"]   # NP ranking
    # tasks += ["-t", "0,1@{MassPoint}"]       # yield tables (CPU intense)
    # tasks += ["-a", "current;p,4,10"]       # yield ratio tables

    ########## submit jobs

    # in case you want to run something locally
    #Batch.run_local_batch(configfiles, outversion, tasks)
    #Batch.run_local_batch(configfiles, outversion, ["-w", "-l", "0,{MassPoint}"])
#    Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-l", 1,"-s",1], '8nh')
    Batch.run_local_batch(configfiles, outversion, tasks)  
    # Batch.run_lxplus_batch(configfiles, outversion, tasks, '8nh')

    # adjust to recover failed ranking subjobs
    #redoSubJob = -1

    #if redoSubJob < 0:
        #print "Submitting stat only jobs..."
        #Batch.run_lxplus_batch(configfiles_stat, outversion, ["-w", "-l", 1,"-s",1], '2nd')
        #print "Submitting systs jobs..."
        #Batch.run_lxplus_batch(configfiles, outversion, tasks, '2nd')

    #print "Submitting rankings..."
    #for m in mass:
    #WSlink = "/afs/cern.ch/user/n/nmorange/Hbb/WSMaker/workspaces/21013.testJERsamplesFine_AZh_13TeV_testJERsamplesFine_Systs_500/combined/500.root"
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "-r", "{MassPoint}"], '8nh', jobs=20, subJob=redoSubJob)
    #Batch.run_local_batch(configfiles, outversion, ["-w", "-k", WSlink, "-l", "0,{MassPoint}"])
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "-r", "{0}".format(m)], '1nd', jobs=20, subJob=redoSubJob)
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-r", "{MassPoint}"], '2nd', jobs=20, subJob=redoSubJob)
    #Batch.run_local_batch(configfiles, outversion, ["-w", "-k", WSlink, "-n", "{MassPoint}"])

    ########## non-default stuff. Warning: don't submit rankings / toys / NLL scans with the same outversion and nJobs!
    #print "Submitting toys..."
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "--fcc", "6"], '2nd', jobs=50, subJob=redoSubJob)
    #print "Submitting NLL scans..."
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "--fcc", "7,2,doNLLscan"], '2nd', jobs=50, subJob=redoSubJob)
