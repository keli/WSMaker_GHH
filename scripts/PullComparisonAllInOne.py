#!/usr/bin/env python
#from ROOT import *

import os
alg = 9
'''
2: unconditional ( start with mu=1 )
4: conditional mu = 0
5: conditional mu = 1
6: run Asimov mu = 1 toys: randomize Poisson term
7: unconditional fit to asimov where asimov is built with mu=1
8: unconditional fit to asimov where asimov is built with mu=0
9: conditional fit to asimov where asimov is built with mu=1
10: conditional fit to asimov where asimov is built with mu=0
'''

FitList = [
  'SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2_STXS.Test_fullRes_VHbb_Test_0_mc16ad_Systs_mva',
  'SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2_STXS.Test_fullRes_VHbb_Test_1_mc16ad_Systs_mva',
  'SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2_STXS.Test_fullRes_VHbb_Test_2_mc16ad_Systs_mva',
  'SMVHVZ_Summer18_MVA_mc16a_v07_fixed2_STXS.Test_fullRes_VHbb_Test_0_mc16a_Systs_mva',
  'SMVHVZ_Summer18_MVA_mc16a_v07_fixed2_STXS.Test_fullRes_VHbb_Test_1_mc16a_Systs_mva',
  'SMVHVZ_Summer18_MVA_mc16a_v07_fixed2_STXS.Test_fullRes_VHbb_Test_2_mc16a_Systs_mva',
  'SMVHVZ_Summer18_MVA_mc16d_v07_fixed2_STXS.Test_fullRes_VHbb_Test_0_mc16d_Systs_mva',
  'SMVHVZ_Summer18_MVA_mc16d_v07_fixed2_STXS.Test_fullRes_VHbb_Test_1_mc16d_Systs_mva',
  'SMVHVZ_Summer18_MVA_mc16d_v07_fixed2_STXS.Test_fullRes_VHbb_Test_2_mc16d_Systs_mva',
]

for fit in FitList:
  fit_stxs = fit+'_STXS_FitScheme_1'
  Lepton =  fit.split('_')[10]
  MCType =  fit.split('_')[11]
  os.system("ls output/%s"%fit)
  os.system('python WSMakerCore/scripts/comparePulls.py -n -a %d -w %s %s -p PullComparisons.WithAlg.%d/%s.L%s '%(alg,fit,fit_stxs,alg,MCType,Lepton))
