#!/usr/bin/env python

# FOR BOOSTED ANALYSIS

import os
import sys
import subprocess
import utility as U

def main(var="mBB",version="None") :
  
  channels  = ["OneLepton"]
  njet_list = ["2jet","3jet"]
  ptv_list  = ["75_150ptv","150_200ptv", "200ptv"]
  
  # WhfCR + SR
  print "variable :",var

  for nChan in channels : 

    for njet in njet_list : 
     
      for ptv in ptv_list:
        print "Merging WhfCR and SR regions : for %s, %s, %s" % (nChan, njet, ptv)
        
        filename1  = "13TeV_%s_2tag%s_%s_WhfSR_%s.root" % (nChan, njet, ptv, var)
        filename2  = "13TeV_%s_2tag%s_%s_WhfCR_%s.root" % (nChan, njet, ptv, var)

        if version is not 'None':
          filename1  = 'inputs/'+version+'/'+filename1
          filename2  = 'inputs/'+version+'/'+filename2
          tempname = 'inputs/'+version+'/temp.root'
          
        if os.path.isfile(filename1) and os.path.isfile(filename2):
          os.system("hadd "+tempname+" "+filename1+" "+filename2)
          os.system("mv "+tempname+" "+filename1)
        else :
          U.Error("Some files %s and %s are missing..." % (filename1, filename2))
            
if __name__ == "__main__":

  version = sys.argv[1]

  var_list = [ "mBB" ]
  for var in var_list : 
      main(var,version)
      



