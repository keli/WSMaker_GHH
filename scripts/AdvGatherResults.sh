###################################
# you need modify the below part

# YMVA XCB XDB
Ver=MAVHMVA
LEP=0

AutoRanking=true
AutoConvert=true
Unblinded=false

####################################
mkdir Results_"$Ver"_"$LEP"

# copy prefit and postfit plots
# check if you only get one result with:
# ls plots/*_fullRes_VHbbRun2_13TeV_"$Ver."01_"$LEP"_125_Systs_use1tagFalse_*
# replace "$Ver." and "$LEP" with your fitting
echo "collecte plots!"
sleep 1s
cp -r plots/*_fullRes_VHbbRun2_13TeV_"$Ver."01_"$LEP"_125_Systs_use1tagFalse_*/* Results_"$Ver"_"$LEP"/
if $Unblinded; then
    mkdir Results_"$Ver"_"$LEP"/postfit_unblinded/
    cp -r plots/*_fullRes_VHbbRun2_13TeV_"$Ver."04_"$LEP"_125_Systs_use1tagFalse_*/postfit/* Results_"$Ver"_"$LEP"/postfit_unblinded/
    mkdir Results_"$Ver"_"$LEP"/fcc_unblinded/
    cp -r plots/*_fullRes_VHbbRun2_13TeV_"$Ver."04_"$LEP"_125_Systs_use1tagFalse_*/fcc/* Results_"$Ver"_"$LEP"/fcc_unblinded/
fi
echo "done!"

# collect workspace
echo "collecte workspace!"
sleep 1s
mkdir -p Results_"$Ver"_"$LEP"/workspaces
cp -r workspaces/*_fullRes_VHbbRun2_13TeV_"$Ver."0[1,2,3]_"$LEP"_125_Systs_use1tagFalse_* Results_"$Ver"_"$LEP"/workspaces
if $Unblinded; then
    mkdir -p Results_"$Ver"_"$LEP"/workspaces_unblinded/
    cp -r workspaces/*_fullRes_VHbbRun2_13TeV_"$Ver."04_"$LEP"_125_Systs_use1tagFalse_* Results_"$Ver"_"$LEP"/workspaces_unblinded/
fi
echo "done!"

# collect fccs
echo "collecte fccs!"
sleep 1s
mkdir -p Results_"$Ver"_"$LEP"/fccs
cp -r fccs/*_fullRes_VHbbRun2_13TeV_"$Ver."01_"$LEP"_125_Systs_use1tagFalse_* Results_"$Ver"_"$LEP"/fccs
if $Unblinded; then
    mkdir -p Results_"$Ver"_"$LEP"/fccs_unblinded
    cp -r fccs/*_fullRes_VHbbRun2_13TeV_"$Ver."04_"$LEP"_125_Systs_use1tagFalse_* Results_"$Ver"_"$LEP"/fccs_unblinded
fi
echo "done!"


# create the prefit yields table
echo "collecte yields!"
sleep 1s
mkdir -p Results_"$Ver"_"$LEP"/Yields
mv Results_"$Ver"_"$LEP"/events.txt Results_"$Ver"_"$LEP"/Yields/
echo "done!"

# collect the breakdown
echo "collecte breakdown!"
sleep 1s
mv Results_"$Ver"_"$LEP"/breakdown/ Results_"$Ver"_"$LEP"/breakdown_asimov/
if $Unblinded; then
    cp -r plots/*_fullRes_VHbbRun2_13TeV_"$Ver."04_"$LEP"_125_Systs_use1tagFalse_*/breakdown Results_"$Ver"_"$LEP"/breakdown_unblinded/
fi

echo "done!"

# collect significance
echo "collecte significance!"
sleep 1s
mkdir -p Results_"$Ver"_"$LEP"/Significance/
echo "There should be 5 lines in this file (this line included), otherwise ask producer!" > Results_"$Ver"_"$LEP"/Significance/Sig.txt
echo "Exp. Significance prefit asimov:" >> Results_"$Ver"_"$LEP"/Significance/Sig.txt
grep "Median significance" logs/*_fullRes_VHbbRun2_13TeV_"$Ver."01_"$LEP"_*getSig_125.log >> Results_"$Ver"_"$LEP"/Significance/Sig.txt
echo "Exp. Significance postfit asimov:" >> Results_"$Ver"_"$LEP"/Significance/Sig.txt
grep "Median significance" logs/*_fullRes_VHbbRun2_13TeV_"$Ver."02_"$LEP"_*getSig_125.log >> Results_"$Ver"_"$LEP"/Significance/Sig.txt
echo "Obs. Significance:" >> Results_"$Ver"_"$LEP"/Significance/Sig.txt
grep "Observed significance" logs/*_fullRes_VHbbRun2_13TeV_"$Ver."04_"$LEP"_*getSig_125.log >> Results_"$Ver"_"$LEP"/Significance/Sig.txt
echo "done!"

# collect the rankings
echo "collecte rankings!"
sleep 1s
mkdir -p Results_"$Ver"_"$LEP"/Rankings_Asimov_postfit_SF

if $AutoRanking; then
  wsName_01=`ls workspaces/ |grep "$Ver."01 |grep _"$LEP"_`
  echo "$wsName_01"
  python scripts/makeNPrankPlots.py $wsName_01
  
  wsName_03=`ls workspaces/ |grep "$Ver."03 |grep _"$LEP"_`
  echo "$wsName_03"
  python scripts/makeNPrankPlots.py $wsName_03

  if $Unblinded; then
      wsName_04=`ls workspaces/ |grep "$Ver."04 |grep _"$LEP"_`
      echo "$wsName_04"
      python scripts/makeNPrankPlots.py $wsName_04
  fi
fi


cp pdf-files/*"$Ver."01_"$LEP"_*pulls_125.pdf Results_"$Ver"_"$LEP"/Rankings_Asimov_postfit_SF/
cp png-files/*"$Ver."01_"$LEP"_*pulls_125.png Results_"$Ver"_"$LEP"/Rankings_Asimov_postfit_SF/
cp eps-files/*"$Ver."01_"$LEP"_*pulls_125.eps Results_"$Ver"_"$LEP"/Rankings_Asimov_postfit_SF/

mkdir -p Results_"$Ver"_"$LEP"/Rankings_Asimov_prefit_SF

cp pdf-files/*"$Ver."03_"$LEP"_*pulls_125.pdf Results_"$Ver"_"$LEP"/Rankings_Asimov_prefit_SF/
cp png-files/*"$Ver."03_"$LEP"_*pulls_125.png Results_"$Ver"_"$LEP"/Rankings_Asimov_prefit_SF/
cp eps-files/*"$Ver."03_"$LEP"_*pulls_125.eps Results_"$Ver"_"$LEP"/Rankings_Asimov_prefit_SF/

if $Unblinded; then
    mkdir -p Results_"$Ver"_"$LEP"/Rankings_unblinded
    cp pdf-files/*"$Ver."04_"$LEP"_*pulls_125.pdf Results_"$Ver"_"$LEP"/Rankings_unblinded/
    cp png-files/*"$Ver."04_"$LEP"_*pulls_125.png Results_"$Ver"_"$LEP"/Rankings_unblinded/
    cp eps-files/*"$Ver."04_"$LEP"_*pulls_125.eps Results_"$Ver"_"$LEP"/Rankings_unblinded/
fi


echo "done!"

# convert eps into png/pdf, needed to create the webpage
if $AutoConvert; then
  echo "convert the eps to png/pdf for shapes and systs"
  echo "please DO NOT interrupt!"
  sleep 3s
  cd $WORKDIR/Results_"$Ver"_"$LEP"/shapes
  source $WORKDIR/macros/webpage/convertepstopngpdf.sh 
  cd $WORKDIR/Results_"$Ver"_"$LEP"/systs
  source $WORKDIR/macros/webpage/convertepstopngpdf.sh 
  cd $WORKDIR
  echo "done!"
fi

# pack it up
tar zcf Results_"$Ver"_"$LEP".tar.gz Results_"$Ver"_"$LEP"

echo "The results collection is done!"
#echo "You need add NP comparison manually!"
