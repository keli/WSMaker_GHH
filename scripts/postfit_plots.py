import subprocess,argparse,os,shutil

"""
SCRIPT to create plots from workspace using WSMakerCore/scripts/doPlotFromWS.py

"""


fcc_version='SMVHVZ_Summer18_MVA_mc16ad_v06' # this is the 'InputVersion' (following the convention of launch_default_jobs.py and inputConfigs) of the FCC we use as the fit result in making the postfit plots
fcc_var='mva' # mva or mBB (discriminant variable used in the FCC)
dist_tag='AsimovR21' # this is the 'outversion' (following the convention of launch_default_jobs.py) attached to the workspaces with distributions used as the basis of the postfit plots (i.e. the prefit distributions)
dist_version='SMVHVZ_Summer18_MVA_mc16ad_v06' # 'InputVersion' of the distribution workspaces
mctype='mc16ad'

ws_pattern='{0}.{3}_fullRes_VHbb_{3}_{1}_{4}_Systs_{2}' #version, lep, var, wsouttag, mctype

#vars={0:['mBB','MET'],1:['mBB','mTW','pTV'],2:['mBB','mLL','pTV']}
vars={'012':['mvadiboson']}
vars={'0':['mBB'],'1':['mBB'],'2':['mBB']}
sums_hash   = {'0':['mBB'],'1':['mBB'],'2':['mBB'],'012':['mBB']}

master_hash = {'0':['mva','mBB','MET','pTB1','pTB2','dRBB','dEtaBB','dPhiVBB','MEff','mBBJ','pTJ3','MEff3'],
               '1':['mva','mBB','MET','pTV','mTW','pTB1','PTB2','dYWH','Mtop','dPhiLBmin','dPhiVBB','dRBB','mBBJ','pTJ3'],
               '2':['mva','mBB','MET','pTV','mLL','pTB1','pTB2','dRBB','dEtaVBB','dPhiVBB','mBBJ','pTJ3'],
               '012':['mva','mBB','pTV','MET','pTB1','PTB2','dPhiLBmin','dPhiVBB','dRBB','dEtaVBB','dPhiVBB','MEff','mTW','dYWH','Mtop','mLL','mBBJ','pTJ3']}

main_hash  = {'0':['mBB','MET'],
              '1':['mBB','pTV','mTW'],
              '2':['mBB','pTV','mLL'],
              '012':['mBB','mva']}

back_hash  = {'0':['pTB1','pTB2','dRBB','dEtaBB','dPhiVBB','pTJ3','mBBJ','MEff','MEff3'],
              '1':['pTB2','dYWH','Mtop','pTB1','dPhiLBmin','dPhiVBB','dRBB','MET'],
              '2':['pTB1','pTB2','dRBB','dEtaBB','dPhiVBB'],
              '012':['mBB','mva']}
bucket=vars
leps=vars
parser = argparse.ArgumentParser(description='Postfit derp')
parser.add_argument('-f','--full',help='Use the full hash',action='store_true')
parser.add_argument('-m','--main',help='Use the main hash',action='store_true')
parser.add_argument('-b','--back',help='Use the back hash',action='store_true')
parser.add_argument('-l','--lep',help='Which lep case to do',default='yogurt')
parser.add_argument('-o','--override',help='Overwrite plots',action='store_true')
parser.add_argument('-s','--sums',help='Just do the special S/B sub mBB plots',action='store_true')
args=parser.parse_args()

if args.lep is 'yogurt':
    leps=['0','1','2']
elif args.lep is '-1':
    leps=['0','1','2','012']
else:
    leps=[args.lep]
if args.sums:
    bucket=sums_hash
elif args.main:
    bucket=main_hash
elif args.back:
    bucket=back_hash
elif args.full:
    bucket=master_hash

ftype = {'0':'Prefit','2':'GlobalFit_unconditional_mu1','3':'GlobalFit_conditional_mu1'}

for v in leps:
    fcc=ws_pattern.format(dist_version,v,fcc_var,dist_tag,mctype)
    if not os.path.exists('output/{0}/workspaces/combined/125.root'.format(fcc)):                           
        print 'THE WORKSPACE output/{0}/workspaces/combined/125.root DOES NOT EXIST--SKIPPING!'.format(fcc)                                                              
        continue
    for var in bucket[v]:
        for p in ['3']: #the plot types you want (hashed in the ftype variable above
            cmd = ['python','WSMakerCore/scripts/doPlotFromWS.py','-p',p,'-f',fcc]
            Blinding = os.getenv("IS_BLINDED")
            if Blinding and p=='2':
                print ' You are running with BLINDING {0} and using FIT TYPE {1}'.format(Blinding,ftype['2'])
                exit()
            #if (var is 'pTV' and (v is '2' or v is 2)) or (var is 'mBB' and args.sums):
            #    cmd.append('-s')
            ws=ws_pattern.format(dist_version,v,var,dist_tag,mctype) #'camilla'
            if not os.path.exists('output/{0}/workspaces/combined/125.root'.format(ws)):
                print 'THE WORKSPACE output/{0}/workspaces/combined/125.root DOES NOT EXIST--SKIPPING!'.format(ws)
                continue
            cmd.append(ws)
            pdir='output/{0}/plots/{1}'.format(ws,'prefit' if p is '0' else 'postfit')
            if os.path.isdir(pdir):
                if args.override:
                    shutil.rmtree(pdir)
                elif os.path.exists('output/{0}/plots/{1}/plotojbs_{2}.yik'.format(ws,'prefit' if p is '0' else 'postfit',ftype[p])):
                    print 'Skipping '+cmd
                    continue
            log = 'output/{0}/logs/plot{1}{2}lep{3}.log'.format(ws,var,v,p)
            print ' '.join(cmd)
            yup=open(log,'w')
            ok=subprocess.Popen(cmd,stderr=yup,stdout=yup)
            ok.wait()
            yup.close()
            print 'Complete....'
