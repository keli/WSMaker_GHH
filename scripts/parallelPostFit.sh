#!/bin/bash
# put the name of the workspace name here, which is regarded as the reference of the postfit
FitSource=SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mva
ConditionCode=T13Run
CollectionDir=PostFit_$ConditionCode
doCollection=true
for i in `ls workspaces/ |grep $ConditionCode`
do
{
    python scripts/doPlotFromWS.py -p 2 -f $FitSource "$i" >"$i".log 2>&1 
    if [[ "${i##*_}" = "pTV" ]];then
      echo "for pTV, we doing sum plots"
      python scripts/doPlotFromWS.py -p 2 -s -f $FitSource "$i" >"$i".log 2>&1 
      rm -r plots/$i/postfit/*Higgsweighted*
    fi
    echo "$i finished!"
}&
done
wait
echo "done the plotting!"

if $doCollection ;then
  mkdir $CollectionDir
  for i in `ls plots/ |grep $ConditionCode`;do mkdir PostFit_$ConditionCode/${i##*_};cp -r plots/$i/postfit/Region* PostFit_$ConditionCode/${i##*_};done
  echo "Plots collection has been done. Find them in $CollectionDir"
fi
