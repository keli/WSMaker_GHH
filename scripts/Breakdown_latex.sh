#!/bin/sh

file_input=output/SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2.VH_unblinded_fullRes_VHbb_VH_unblinded_012_mc16ad_Systs_mva/plots/breakdown/muHatTable_mode10_Asimov0_SigXsecOverSM.txt
file_output=Breakdown_paper.tex
rm $file_output

echo "\begin{tabular}{ l | l  c }" >>$file_output
echo "\toprule" >>$file_output
echo "\multicolumn{2}{l}{Source of uncertainty} & $\sigma_\mu$ \\\\" >>$file_output
echo "\midrule" >>$file_output
echo "\multicolumn{2}{l}{Total} &" `awk /Total/'{print $7}' $file_input` "\\\\" >>$file_output
echo "\multicolumn{2}{l}{Statistical} &" `(awk /DataStat/'{print $7;exit}' $file_input)` "\\\\" >>$file_output
echo "\multicolumn{2}{l}{Systematic} &" `(awk /FullSyst/'{print $7;exit}' $file_input)` "\\\\" >>$file_output
echo "\midrule" >>$file_output
echo "\multicolumn{3}{l}{Experimental uncertainties}\\\\" >>$file_output
echo "\midrule" >>$file_output
echo "\multicolumn{2}{l}{Jets} & " `(awk /JET/'{print $8;exit}' $file_input)` "\\\\" >>$file_output
echo "\multicolumn{2}{l}{\met} &" `(awk /MET/'{print $8;exit}' $file_input)` "\\\\" >>$file_output
echo "\multicolumn{2}{l}{Leptons} &" `(awk /lepton/'{print $8;exit}' $file_input)` "\\\\" >>$file_output
echo "\multirow{3}{*}{\$b\$-tagging~~~} & \$b\$-jets &" `(awk /b-jet/'{print $9;exit}' $file_input)` "\\\\" >>$file_output
echo "& \$c\$-jets & " `(awk /c-jet/'{print $9;exit}' $file_input)` "\\\\" >>$file_output
echo "& light-flavour jets &" `(awk /l-jet/'{print $9;exit}' $file_input)` "\\\\" >>$file_output
echo "& extrapolation &" `(awk /extrap/'{print $9;exit}' $file_input)` "\\\\" >>$file_output
echo "\multicolumn{2}{l}{Pile-up} &" `(awk /PU/'{print $8;exit}' $file_input)` "\\\\" >>$file_output
echo "\multicolumn{2}{l}{Luminosity} &" `(awk /Lumi/'{print $7;exit}' $file_input)` "\\\\" >>$file_output
echo "\midrule" >>$file_output
echo "\multicolumn{3}{l}{Theoretical and modelling uncertainties}\\\\" >>$file_output
echo "\midrule" >>$file_output
echo "\multicolumn{2}{l}{Signal} &" `(awk /"Modelling: VH"/'{print $8;exit}' $file_input)` "\\\\" >>$file_output
echo "\multicolumn{3}{l}{} \\\\" >>$file_output
echo "\multicolumn{2}{l}{Floating normalisations} &" `(awk /Floating/'{print $8;exit}' $file_input)` "\\\\" >>$file_output
echo "\multicolumn{2}{l}{\$Z\$~+~jets} &" `(awk /"Modelling: Z\+jets"/'{print $8;exit}' $file_input)` "\\\\" >>$file_output
echo "\multicolumn{2}{l}{\$W\$~+~jets} &" `(awk /"Modelling: W\+jets"/'{print $8;exit}' $file_input)` "\\\\" >>$file_output
echo "\multicolumn{2}{l}{\ttb} &" `(awk /ttbar/'{print $8;exit}' $file_input)` "\\\\" >>$file_output
echo "\multicolumn{2}{l}{Single top quark} &" `(awk /single/'{print $9;exit}' $file_input)` "\\\\" >>$file_output
echo "\multicolumn{2}{l}{Diboson} &" `(awk /Diboson/'{print $8;exit}' $file_input)` "\\\\" >>$file_output
echo "\multicolumn{2}{l}{Multijet} &" `(awk /"Multi Jet"/'{print $8;exit}' $file_input)` "\\\\" >>$file_output
echo "\multicolumn{3}{l}{} \\\\" >>$file_output
echo "\multicolumn{2}{l}{MC statistical} &" `(awk "/MC stat/"'{print $8;exit}' $file_input)` "\\\\" >>$file_output
echo "\bottomrule" >>$file_output
echo "\end{tabular}" >>$file_output

