"""
Author: Philipp Windischhofer
Date:   December 2019
Email:  philipp.windischhofer@cern.ch

Script to post-process WSMaker fit inputs. At the moment, it is set up
to remove the normalisation impact of W and ttbar pTV shape uncertainties
across various analysis regions to avoid correlating them with the respective
normalisation factors. Additionally, VH and VV mBB uncertainties are processed
in a similar way to avoid correlating them with the signal normalisation.

The code should be flexible enough to easily do other things as well.

usage: RemoveNormImpact.py [-h] [--indir INDIR] [--outdir OUTDIR]

post process VHbb fit inputs

optional arguments:
  -h, --help       show this help message and exit
  --indir INDIR    path to directory containing the original fit inputs
  --outdir OUTDIR  path to directory containing the processed fit inputs
  --all_vars       flag to enable postprocessing for all BDT input variables
"""

import os, re, glob, itertools, warnings
from distutils.dir_util import copy_tree
from argparse import ArgumentParser
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

# check if the ROOT version is new enough
major, minor = map(float, ROOT.gROOT.GetVersion().split('/')[0].split('.'))
if major < 6 or minor < 18:
    raise ImportError('Error: this requires at least ROOT v6.18 to work around some strange behaviour of ROOT! Please do lsetup "ROOT 6.18.04" or similar.')

class TFileHandler(object):
    
    def __init__(self, fileobj, mode = 'UPDATE'):
        self.fileobj = fileobj
        self.need_to_close = False
        if isinstance(self.fileobj, str):
            if mode == 'READ':
                self.fileobj = ROOT.TFile.Open(self.fileobj)
            else:
                self.fileobj = ROOT.TFile(self.fileobj, mode)

            ROOT.SetOwnership(self.fileobj, False)
            self.need_to_close = True

    def close(self):
        if self.need_to_close:
            self.fileobj.Close()

    def get_obj_list(self, obj_type, directory = '/'):
        self.fileobj.cd(directory)
        obj_list = []

        for cur_key in ROOT.gDirectory.GetListOfKeys():
            if cur_key.GetClassName() == obj_type:
                obj_list.append(cur_key.GetName())
        
        return obj_list

    def get_obj(self, obj_name):
        obj = self.fileobj.Get(obj_name)
        return obj

    def update(self, obj, directory):
        self.fileobj.cd(directory)
        obj.Write()

    def get_available_samples(self):
        return self.get_obj_list(obj_type = "TH1F", directory = '/')

    def get_nom_sample(self, sample):
        return self.get_obj(sample)

    def get_syst_sample(self, sample, syst):
        return self.get_obj(os.path.join("Systematics", "{}_{}".format(sample, syst)))

    def scale_syst_sample(self, sample, syst, SF):
        syst_sample = self.get_syst_sample(sample, syst)
        if syst_sample:
            syst_sample.Scale(SF)
            self.update(syst_sample, directory = "Systematics")
        else:
            print("Requested histogram does not exist, skipping rescaling operation!")

def get_yields(handlers, samples, yield_loader):
    yields = 0
    for handler in handlers:
        for sample in samples:
            yields += yield_loader(handler, sample)

    return yields    

def remove_normalisation_effect(workdir, region, samples, syst):
    print("-----------------------")
    print("starting to remove normalisation impact of {} on {} for region '{}'.".format(syst, ' '.join(samples), region))

    # find all files that are affected
    region_files = filter(re.compile(region).match, glob.glob(os.path.join(workdir, '*')))

    if len(region_files) > 0:
        print("this region affects the following files:")
        print("\n".join(region_files))
    else:
        warnings.showwarning("You are trying to normalise across an empty set of signal regions - are you sure you know what you are doing?", SyntaxWarning, "RemoveNormImpact.py", 108)

    handlers = [TFileHandler(cur_file, mode = 'UPDATE') for cur_file in region_files]

    # compute the nominal yields
    def nominal_yield_loader(handler, sample):
        hist = handler.get_nom_sample(sample)
        if not hist:
            print("Nominal histogram does not exist, assuming zero yield.")
            return 0.0
        return hist.Integral(0, hist.GetNbinsX() + 1)

    nominal_yields = get_yields(handlers, samples, yield_loader = nominal_yield_loader)

    # compute the systematic yields
    def syst_yield_loader(handler, sample):
        hist = handler.get_syst_sample(sample, syst)
        if not hist:
            print("Systematic histogram does not exist, assuming zero yield.")
            return 0.0
        return hist.Integral(0, hist.GetNbinsX() + 1)

    syst_yields = get_yields(handlers, samples, yield_loader = syst_yield_loader)

    print("loaded the following yields:")
    print("nom_yield [syst_yield] = {} [{}]".format(nominal_yields, syst_yields))
    print("- - - - - - - - - - - -")

    # compute the scale factors for the systematic samples
    if syst_yields > 0:
        SF = nominal_yields / syst_yields
    else:
        SF = 1.0 # can not do anything else that would make sense
    print("will scale with {}".format(SF))

    # apply the SFs to all the histograms in all participating files ...
    for handler in handlers:
        for sample in samples:
            handler.scale_syst_sample(sample, syst, SF)

    # ... and close the files
    for handler in handlers:
        handler.close()

    print("-----------------------")    

def remove_normalisation_effects(workdir, variables, analysis_regions, systs, samples):
    for variable, analysis_region, syst, cur_samples in itertools.product(variables, analysis_regions, systs, samples):
        region = ".*{}.*{}.root".format(analysis_region, variable)
        remove_normalisation_effect(workdir, region = region, samples = cur_samples, syst = syst)

def assemble_STXS_bins():
    STXS_pTV_regions = ["PTVx0x75", "PTVx75x150", "PTVx150x250", "PTVx250x400", "PTVxGT400"]
    STXS_jet_regions = ["0J", "1J", "GE2J"]
    STXS_production_modes = ["QQ2HLNU", "QQ2HNUNU", "QQ2HLL", "GG2HNUNU", "GG2HLL"]
    STXS_special_bins = ["FWDH"]

    STXS_binlist = []
        
    # assemble the bins in the central region
    for cur_STXS_bin in itertools.product(STXS_production_modes, STXS_pTV_regions, STXS_jet_regions):
        STXS_binlist.append("x".join(cur_STXS_bin))
            
    # assemble the special bins: those for the forward region and those for all remaining ("unknown") events
    for cur_STXS_bin in itertools.product(STXS_production_modes, STXS_special_bins):
        STXS_binlist.append("x".join(cur_STXS_bin))

    return STXS_binlist

def RemoveNormImpact(indir, outdir, all_vars):
    # prepare the output directory
    copy_tree(indir, outdir)
    workdir = outdir # from now on operate only in the output directory

    # common settings
    variables_2lep = ["mva", "mvadiboson", "mBB", "pTV"]
    variables_1lep = ["mva", "mvadiboson", "mBB", "pTV"]
    variables_0lep = ["mva", "mvadiboson", "mBB", "MET"]

    if all_vars:
        # make sure to postprocess all variables for which postfit plots are made
        variables_2lep = ["mva", "mvadiboson", "mBB", "pTV", "dEtaVBB", "dPhiVBB", "dRBB", "mLL", "METSig", "cosThetaLep", "pTB1", "pTB2", "mBBJ", "pTJ3", "binMV2c10B1", "binMV2c10B2", "pTL1", "pTL2"]
        variables_1lep = ["mva", "pTV", "mBB", "mvadiboson", "MET", "pTB1", "pTB2", "dRBB", "dPhiVBB", "dPhiLBmin", "mTW", "dYWH", "Mtop", "pTJ3", "mBBJ", "binMV2c10B1", "binMV2c10B2", "binMV2c10B1B2"]
        variables_0lep = ["mva", "MET", "mBB", "mvadiboson", "pTB1", "pTB2", "softMET", "dRBB", "dEtaBB", "HT", "dPhiVBB", "pTJ3", "mBBJ", "binMV2c10B1", "binMV2c10B2", "binMV2c10B1B2"]

    # ----------------------------------------
    # remove normalisation impact from signal mBB uncertainties
    # ----------------------------------------
    # do it separately for each (nJ, pTV) bin
    analysis_regions_2lep_mBB = ["TwoLepton_2tag2jet_75_150ptv_(SR|CRHigh|CRLow)", "TwoLepton_2tag3pjet_75_150ptv_(SR|CRHigh|CRLow)",
                                 "TwoLepton_2tag2jet_150_250ptv_(SR|CRHigh|CRLow)", "TwoLepton_2tag3pjet_150_250ptv_(SR|CRHigh|CRLow)",
                                 "TwoLepton_2tag2jet_250ptv_(SR|CRHigh|CRLow)", "TwoLepton_2tag3pjet_250ptv_(SR|CRHigh|CRLow)"]

    analysis_regions_1lep_mBB = ["OneLepton_2tag2jet_75_150ptv_(SR|CRHigh|CRLow)", "OneLepton_2tag3jet_75_150ptv_(SR|CRHigh|CRLow)",
                                 "OneLepton_2tag2jet_150_250ptv_(SR|CRHigh|CRLow)", "OneLepton_2tag3jet_150_250ptv_(SR|CRHigh|CRLow)",
                                 "OneLepton_2tag2jet_250ptv_(SR|CRHigh|CRLow)", "OneLepton_2tag3jet_250ptv_(SR|CRHigh|CRLow)"]

    analysis_regions_0lep_mBB = ["ZeroLepton_2tag2jet_150_250ptv_(SR|CRHigh|CRLow)", "ZeroLepton_2tag3jet_150_250ptv_(SR|CRHigh|CRLow)",
                                 "ZeroLepton_2tag2jet_250ptv_(SR|CRHigh|CRLow)", "ZeroLepton_2tag3jet_250ptv_(SR|CRHigh|CRLow)"]
    
    # ----------------------------------------
    # treat signal
    # ----------------------------------------
    samples = [["WZ"], ["ZZ"]]
    systs_mBB = [
        "SysVVMbbPSUE__1up", "SysVVMbbPSUE__1down", "SysVVMbbME__1up", "SysVVMbbME__1down"
    ]
    remove_normalisation_effects(workdir, variables_2lep, analysis_regions_2lep_mBB, systs_mBB, samples)
    remove_normalisation_effects(workdir, variables_1lep, analysis_regions_1lep_mBB, systs_mBB, samples)
    remove_normalisation_effects(workdir, variables_0lep, analysis_regions_0lep_mBB, systs_mBB, samples)

    STXS_bins = assemble_STXS_bins()
    samples = [[cur_bin] for cur_bin in STXS_bins] # remove the norm impact on each signal component separately
    systs_mBB = [
        "SysVHUEPSMbb__1up", "SysVHUEPSMbb__1down", "SysVHQCDscaleMbb_ggZH__1up", "SysVHQCDscaleMbb_ggZH__1down", "SysVHQCDscaleMbb__1up", "SysVHQCDscaleMbb__1down"
    ]
    remove_normalisation_effects(workdir, variables_2lep, analysis_regions_2lep_mBB, systs_mBB, samples)
    remove_normalisation_effects(workdir, variables_1lep, analysis_regions_1lep_mBB, systs_mBB, samples)
    remove_normalisation_effects(workdir, variables_0lep, analysis_regions_0lep_mBB, systs_mBB, samples)

    # ----------------------------------------
    # treat ttbar
    # ----------------------------------------
    # BDTr ME and PS Systematic 0-lepton
    analysis_regions_0lep_ME_PS = ["ZeroLepton_2tag2jet_(150_250|250)ptv_(SR|CRHigh|CRLow)","ZeroLepton_2tag3jet_(150_250|250)ptv_(SR|CRHigh|CRLow)"]
    samples = [["ttbar"]]
    systs_BDTr = [
        "SysBDTr_ttbar_ME__1up", "SysBDTr_ttbar_PS__1up"
    ]
    remove_normalisation_effects(workdir, variables_0lep, analysis_regions_0lep_ME_PS, systs_BDTr, samples)

    # BDTr ME and PS Systematic 1-lepton
    analysis_regions_1lep_ME_PS = ["OneLepton_2tag2jet_(150_250|250)ptv_(SR|CRHigh|CRLow)","OneLepton_2tag3jet_(150_250|250)ptv_(SR|CRHigh|CRLow)"]
    samples = [["ttbar"]]
    systs_BDTr = [
        "SysBDTr_ttbar_ME__1up", "SysBDTr_ttbar_PS__1up"
    ]
    remove_normalisation_effects(workdir, variables_1lep, analysis_regions_1lep_ME_PS, systs_BDTr, samples)

    # ----------------------------------------
    # remove normalisation impact from pTV uncertainties
    # ----------------------------------------
    # rescale pTV-like uncertainties such that they are less correlated with the normalisations in the respective regions
    analysis_regions_2lep_ptv = ["TwoLepton_2tag2jet_75_150ptv_(SR|CRHigh|CRLow)", "TwoLepton_2tag3pjet_75_150ptv_(SR|CRHigh|CRLow)",
                                 "TwoLepton_2tag2jet_(150_250|250)ptv_(SR|CRHigh|CRLow)", "TwoLepton_2tag3pjet_(150_250|250)ptv_(SR|CRHigh|CRLow)"]
    analysis_regions_1lep_ptv = ["OneLepton_2tag2jet_75_150ptv_(SR|CRHigh|CRLow)", "OneLepton_2tag3jet_75_150ptv_(SR|CRHigh|CRLow)",
                                 "OneLepton_2tag2jet_(150_250|250)ptv_(SR|CRHigh|CRLow)", "OneLepton_2tag3jet_(150_250|250)ptv_(SR|CRHigh|CRLow)"]
    analysis_regions_0lep_ptv = ["ZeroLepton_2tag2jet_(150_250|250)ptv_(SR|CRHigh|CRLow)", "ZeroLepton_2tag3jet_(150_250|250)ptv_(SR|CRHigh|CRLow)"]

    # ----------------------------------------
    # treat ttbar
    # ----------------------------------------
    samples = [["ttbar"]]
    systs_ptv = [
        "SysTTbarPTV__1down", "SysTTbarPTV__1up", 
        "SysTTbarPtV_BDTr__1down", "SysTTbarPtV_BDTr__1up", 
    ]
    remove_normalisation_effects(workdir, variables_1lep, analysis_regions_1lep_ptv, systs_ptv, samples)
    remove_normalisation_effects(workdir, variables_0lep, analysis_regions_0lep_ptv, systs_ptv, samples)

    # ----------------------------------------
    # treat Wbb
    # ----------------------------------------
    # ICHEP-style
    systs_ptv = [
        "SysWPtV__1up", "SysWPtV__1down",
    ]
    samples = [["Wbb"], ["Wbc"], ["Wbl"], ["Wcc"], ["Wcl"], ["Wl"]]
    remove_normalisation_effects(workdir, variables_1lep, analysis_regions_1lep_ptv, systs_ptv, samples)
    remove_normalisation_effects(workdir, variables_0lep, analysis_regions_0lep_ptv, systs_ptv, samples)

    # BDTr
    systs_ptv = [
        "SysWPtV_BDTr__1up", "SysWPtV_BDTr__1down", 
    ]
    samples = [["Wbb"], ["Wbc"], ["Wbl"], ["Wcc"]]
    remove_normalisation_effects(workdir, variables_1lep, analysis_regions_1lep_ptv, systs_ptv, samples)
    remove_normalisation_effects(workdir, variables_0lep, analysis_regions_0lep_ptv, systs_ptv, samples)

    # ----------------------------------------
    # treat Zbb
    # ----------------------------------------
    # ICHEP-style
    systs_ptv = [
        "SysZPtV__1up", "SysZPtV__1down",
    ]
    samples = [["Zbb"], ["Zbc"], ["Zbl"], ["Zcc"], ["Zcl"], ["Zl"]]
    remove_normalisation_effects(workdir, variables_0lep, analysis_regions_0lep_ptv, systs_ptv, samples)
    remove_normalisation_effects(workdir, variables_2lep, analysis_regions_2lep_ptv, systs_ptv, samples)

if __name__ == "__main__":
    parser = ArgumentParser(description = "post process VHbb fit inputs")
    parser.add_argument("--indir", action = "store", dest = "indir", help = "path to directory containing the original fit inputs")
    parser.add_argument("--outdir", action = "store", dest = "outdir", help = "path to directory containing the processed fit inputs")
    parser.add_argument("--all_vars", action = "store_const", const = True, default = False, dest = "all_vars", help = "flag to enable postprocessing for all BDT input variables")
    args = vars(parser.parse_args())

    RemoveNormImpact(**args)
