#!/usr/bin/env python

# FOR BOOSTED ANALYSIS

# Instructions: before splitting check which variables to be considered as default.

import os
import sys
import subprocess
import utility as U

channels_vec      = [["ssTwoLepton"],["osTwoLepton"],["ThreeLepton"]]
regions_vec       = [ ["SR","CRHigh","CRLow"            ],  # 0lep
                      ["SR","CRHigh","CRLow"            ],  # 1lep
                      ["SR","CRHigh","CRLow","topemucr" ] ] # 2lep


def main(nlep=1,var_org='mvaR21',var_tar='mva',version='None') :
  
  # channels  = ["ZeroLepton"]
  channels  = channels_vec[nlep]
  nTagnJet_list = ["Boosted","Medium","Resolved"]
  if nlep == 0 :
    ptv_list  = ["ee","emu","mue","mumu","Inc"]
  if nlep == 1 :
    ptv_list  = ["Inc"]
  if nlep == 2 :
    ptv_list  = ["eeeReg1","eemuReg1","mumu2Reg1","mumumuReg1","eeeReg2","eemuReg2","mumu2Reg2","mumumuReg2","eeeReg3","eemuReg3","mumu2Reg3","mumumuReg3","eeeReg4","eemuReg4","mumu2Reg4","mumumuReg4"]
  
  #
  print "variable :",var_org,"to",var_tar
  print "regions  :",reg

  for nChan in channels : 

    for nTag in nTagnJet_list : 

      for ptv in ptv_list : 
     
        print "Rename variables : in %s, %s, %s %s" % (nChan, nTag, ptv, reg)
       
        original  = "13TeV_%s_%s_%s_%s_%s.root"  % (nChan, nTag, ptv, reg, var_org)
        target    = "13TeV_%s_%s_%s_%s_%s.root"  % (nChan, nTag, ptv, reg, var_tar)

        if version is not 'None':
          original  = 'inputs/'+version+'/'+original
          target    = 'inputs/'+version+'/'+target
     
        if os.path.isfile(original):
          os.system("mv "+original+" "+target)
        else :
          U.Error("%s is missing..." % (original))

if __name__ == "__main__":
  if len(sys.argv) is not 2:
    print "You are now running the RenameVar.py for the current inputs"
    print "TO run for the inputs in inputs/version"
    print "Usage: RenameVar.py <version>"
    version = 'None'
  else:
    version = sys.argv[1]
    
  # Better not to have a default here  
  # var_org   = 'mva28' # original name of the variable in the split inputs
  # var_tar   = 'mva'   # renamed variable
  # regions  = [ "SR" ]
  # for reg in regions :
  #   main(var_org,var_tar,version)
  #   main('mvadiboson28','mvadiboson',version)

  # mdacunha: even better would be to make a cycle also in the region

  regions = regions_vec [0]
  for reg in regions :
    main(0,"mV","mV",version)
    # Not needed anymore
    # main(0,"mBBMVA","mBB",version)
    # main(0,"mva28","mva",version)
    # main(0,'mvadiboson28','mvadiboson',version)

#  regions = regions_vec [1]
#  for reg in regions :
#    main(1,"mvaFullRun2Default","mva",version)
#    # Not needed anymore
#    # main(1,"mBBRedo","mBB",version)
#    # main(1,"mvaR21","mva",version)
#    # main(1,'mvadibosonR21','mvadiboson',version)
#
#  regions = regions_vec [2]
#  for reg in regions :
#    main(2,"mvaPolVars","mva",version)
#    # Not needed anymore
#    # main(2,"mvaR21","mva",version)
#    # main(2,'mvadibosonR21','mvadiboson',version)
