#!/usr/bin/env python

import os, sys, re, glob
import ROOT
import math


debug = False

def main():
    version = sys.argv[1].rstrip('/')
    CRFileList = glob.glob('inputs/'+version+'/13TeV_TwoLepton_*topemucr*.root')
    for CRFileName in CRFileList:
        SRFileName = ''
        if re.match('.*topemucr(High|Low).*', CRFileName):
            SRFileName = re.sub('topemucr', 'CR', CRFileName)
        elif re.match('.*topemucr.*', CRFileName):
            SRFileName = re.sub('topemucr', 'SR', CRFileName)
        else:
            continue

        region_str = os.path.basename(CRFileName)
        region_str = re.sub('^13TeV_TwoLepton_', '', region_str)
        region_str = re.sub('\.root$', '', region_str)

        if os.path.exists(SRFileName) and os.path.exists(CRFileName):
            process_file(SRFileName,CRFileName, region_str)
        else:
            if debug: sys.stdout.write( 'MakeDataDivenTTbarHistos:   Files for (%s, %s, %s) don\'t exist\n' %(tagjet, ptv, var) )


def process_file(SRfilestr, CRfilestr, region_str):

    print 'MakeDataDivenTTbarHistos: Processing %s and %s...' %(SRfilestr, CRfilestr)
    CRfile = ROOT.TFile(CRfilestr, 'READ')
    SRfile = ROOT.TFile(SRfilestr, 'UPDATE')
    
    if CRfile.IsZombie(): return
    if SRfile.IsZombie(): return


    keyList = []
    keyList.extend(CRfile.GetDirectory('').GetListOfKeys())
    data_hist = None
    sumMC_hist = None

    for key in keyList:
        name = key.GetName()
        if debug: sys.stdout.write( 'MakeDataDivenTTbarHistos:   '+key.GetClassName()+' '+name+'\n' )
        if key.GetClassName().find('TH1')>-1:
            h = CRfile.Get(name)
            if name == 'data':
                data_hist = h.Clone('emuCRData')
                data_hist.SetTitle('emuCRData_'+region_str)

            elif not re.match('^(QQ2HLL|GG2HLL|QQ2HLNU|.*_.*)', name):
                if sumMC_hist == None:
                    sumMC_hist = h.Clone('emuCRSumMC')
                    sumMC_hist.SetTitle('emuCRSumMC_'+region_str)
                    if debug: sys.stdout.write( 'MakeDataDivenTTbarHistos:   Initialized the sumMC histogram with %s.\n'%h.GetName())
                else:
                    sumMC_hist.Add(h)
                    if debug: sys.stdout.write( 'MakeDataDivenTTbarHistos:   Added %s to the sumMC histogram.\n'%h.GetName())

    setDataStatError(sumMC_hist)
    if data_hist is not None and data_hist.Integral()>0.5:
        applyExtrapolationFactor(data_hist, region_str)
    else:
        epsilon = 1e-8
        # larger than the ignoring threshold 1e-9 in Category::tryAddSample
        # less than 1e-4 used in Category::computeMCStatUncertHist
        data_hist.SetBinContent(1, epsilon)
        data_hist.SetBinError(1, 0)
    applyExtrapolationFactor(sumMC_hist, region_str)

    SRfile.cd()
    if data_hist is not None and data_hist.Integral()>1e-9:
        data_hist.Write('emuCRData')
    sumMC_hist.Write('emuCRSumMC')

    SRfile.Close()
    CRfile.Close()

def applyExtrapolationFactor(h, region_str):
    if '2jet' in region_str:
        h.Scale(0.995)
    if '3pjet' in region_str:
        h.Scale(1.011)



def setDataStatError(hist):
    nbins = hist.GetNbinsX()
    for ibin in range(0,nbins+1):
        cont = hist.GetBinContent(ibin)
        if cont <= 0 :
            hist.SetBinError(ibin, 0)
        else:
            hist.SetBinError(ibin, math.sqrt(cont))


if __name__ == '__main__':
    main()
