from AnalysisMgr import WorkspaceConfig

class WorkspaceConfig_GHHRun2(WorkspaceConfig):
    def __init__(self, *args, **kwargs):
        kwargs["Analysis"] = "GHHRun2"
        super(WorkspaceConfig_GHHRun2, self).__init__(*args, **kwargs)
        self["Regions"] = []
        return


    def set_regions(self, signal="GHH600X", vtype="Boosted",  var2l='mV', sstwo_lepton=True, ostwo_lepton=False, three_lepton=False,  doemu=False):
        self["Regions"] = [ ]
        self.append_regions(signal, var2l, vtype, sstwo_lepton, ostwo_lepton, three_lepton, doemu )

    def append_regions(self, signal="GHH600X", var2l='mV', vtype="Boosted", sstwo_lepton=True, ostwo_lepton=False, three_lepton=False, doemu=False,  mctype=''):

        var=var2l
        energy = "13TeV_ssTwoLepton_"+vtype
        if sstwo_lepton:
            # PTV BINNING
            if doemu:
                ptvs = ["ee","emu","mue","mumu" ]
                for p in ptvs:
                    self["Regions"].append(energy+"_ss2l_SR_{0}_{1}".format( p, var2l))
	    else:
                self["Regions"].append(energy+"_ss2l_SR_Inc_{0}".format( var2l))

        self.check_regions()
