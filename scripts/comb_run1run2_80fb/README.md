This is combination code is taken from Stefan Gadatsch and Andrea Gabrielli. 
Small modification have been made to ensure that guassian constraints for MC stats are not overwritten with default mean of 1 and 0
This is needed for ttH and VBF workspaces.

For this, we structured the combination in separate steps with each one feeding the step to the next one. 
The idea behind this was to maintain simpler scripts for faster turnaround and faster debugging, but at the same time lead to multiple scripts that contain similar code for correlating systematics. 

This combination was particularly difficult from a technical standpoint. Some issues we had to solve:
1 - Run 1 ws are made with Root 5. Hence, only certain root versions allowed combination to happen
2 - Run 1 VBF workspace had missing global observables in the ModelConfig. This lead to numerous instability where the scans on data would fail. These vars were injected into the workspace using the script workspaces/run1/VBFSurgery.cxx
3 - Run 2 ttHbb has BSM signal model inject in the PDF. The corresponding POIs had to be set to zero for the combination to work
4 - ttHbb has multiple POI to take into account different final states. In the combination, the POI for only ttHbb and tHbb were combined. All other final states have been held to SM values

After checking out the code, the following are the instructions to produce the workspaces. 

setup the root version and compile the code. Copy the workspaces over

    cd CombinationTool
    source setup.sh
    make
    cp -r /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/ICHEP2018Results/CombinationWorkspaces/workspaces ./

Inside the workspaces folder there is a readme that explains what each ws is and what it was used for.

If you want to perform the Run 1 VBF surgery to fix global observable issue. You need to do this to reproduce paper results:
    cd workspaces/run1
    root -l -q -b VBFsurgery.cxx

Run 2 ttH workspaces come split into lep+jet and all had. Initial combination needs to be performed to make a Run2 ttH workspace. This is based on Stefan Gadatsch’s script for the ttH observation combination
    python scripts/combine_ttHbb_rel20p7.py

Run 1 combination. This combines the Run 1 VH, VBF, ttHbb workspaces. 
    python scripts/combineRun1_VH_VBF_ttHbb.py

    By default, this script will produce a combined ws with correlated NP and 1 combined POI
    
    For this script, there are three input options:
    --doDecorrelatedPOI -> This option will create 3 POIs. One for each channel
    --doDecorrelatedNP -> This option will de-correlated all NPs
    --removeVBF -> This option remove VBF from the combination. At one point, there was idea to remove VBF from the combination. Hence this option

Run 2 combination. This combines the Run 2 VH, VBF, ttHbb workspaces. 
    python scripts/combineRun2_VH_VBF_ttHbb.py
    
    By default, this script will produce a combined ws with correlated NP and 1 combined POI
    
    For this script, there are 4 input options:
    --doDecorrelatedPOI -> This option will create 3 POIs. One for each channel
    --doDecorrelatedNP -> This option will de-correlated all NPs
    --removeVBF -> This option remove VBF from the combination. At one point, there was idea to remove VBF from the combination. Hence this option
    --doEPS -> This option will cause Run 2 EPS VHbb workspace to be used in the combination



Run 1+2 combination. This combines the Run 1 and Run 2 workspaces. Require the output from the 2 scripts before. Moreover, for example, if you want decorrelated POI, you will need to run decorrelated POI ws for Run 1 and Run2 combination
    python scripts/combineRun12_VH_VBF_ttHbb.py
    
    By default, this script will produce a combined ws with correlated NP and 1 combined POI
    
    For this script, there are 6 input options:
    --doDecorrelatedPOI -> This option will create 6 POIs. One for each channel for each year. Requires Run1/2 comb ws that ran with doDecorrelatedPOI option
    --doDecorrelatedNP -> This option will de-correlated all NPs
    --removeVBF -> This option will cause the script to use Run 1/2 comb workspaces that don’t have VBF in this
    --doEPS -> This option will cause Run 2 EPS combined workspace to be used in the combination
    --threeMu -> This option will create 3 POIs. One for each channel combined across years. Requires Run1/2 comb ws that ran with doDecorrelatedPOI option
    --twoMu -> This option will create 2 POIs. One for each year, combined across channels



Run 1+2 VBF combination.
    python scripts/combineRun1Run2_VBF.py
    
    By default, this script will produce a combined ws with correlated NP and 1 combined POI
    
    For this script, there are 2 input options:
    --doDecorrelatedPOI -> This option will create 2 POIs. One for each year
    --doDecorrelatedNP -> This option will de-correlated all NPs


Run 1+2 ttHbb combination.
    python scripts/combineRun1Run2_ttHbb.py
    
    By default, this script will produce a combined ws with correlated NP and 1 combined POI
    
    For this script, there are 2 input options:
    --doDecorrelatedPOI -> This option will create 2 POIs. One for each year
    --doDecorrelatedNP -> This option will de-correlated all NPs


Run 1+2 VHbb combination.
    python scripts/run12-combtool.py
    
    See the combination_VHbb.pdf for all the options in this script





All the scripts above, the combination is performed using the observed data. Asimov needs to be added to the workspace. 
Our asimov is generated with systematics profiled to data. To allows us to recover the injected value, the global observable are shifted. Hence, to run on data, the combined workspaces can be used out of the box. However, to run on asimov, please add this line to your scan script:

    ws->loadSnapshot(“conditionalGlobs_1");



To add asimov in your workspace, use the ‘asimovWS.C’ script in addAsimov folder. The input arguments are mostly info about the input workspace. Two important options are:
doConditional - if True, this will create asimov conditional to data. if False, this will correspond to MC asimov.
domuFixFirst - if True, mu is held at 1, for the conditional fit. if false, the mu is floated in the workspace

The output from this script is stored in workspaces_withAsimov folder. Hopefully, if the input naming convention is followed, in nicely organized folder.

To help with running thisasimov, which can take a long time, we made some scripts to submit on lxplus batch. They are in “batchSubmit” folder. Job definition is in “workspaceNames.py” script and actual submission happens in “submitAsimov.py” script


The final scans were performed using wsScans. The VHbb branch can be found at https://gitlab.cern.ch/sabidi/wsScans/tree/VHbb




For any questions, please contact
Bianca Ciungu - bianca.ciungu@cern.ch
Haider Abidi  - sabidi@cern.ch
