#!/usr/bin/env python
import os
import math
from ROOT import *
import workspaceNames


import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--queue",          type = str, default = "1nd",        help = "Queue to submit to.")
parser.add_argument("--inDir",          type = str, default = "/afs/cern.ch/user/b/bciungu/work/Hbb/hcomb_2018/addAsimov/workspaces/" , help = "main folder for output dir")
parser.add_argument("--dryRun",         default = False, action='store_true',   help = "does not submit")



args = parser.parse_args()
WORKSPACEDIR = args.inDir

listToProcess = [
    workspaceNames.List_Uncorrelated,
    workspaceNames.List_Correlated_Check,
    workspaceNames.List_Correlated
]  

def main(): 



    for jList in listToProcess:
        for job in jList:

            jobListBaseDir  = "jobList/"
            bsubBaseDir     = "bsub/" 

            # get the job command list
            cmdToRun = getJobDef(job)

            print cmdToRun


            bsubCmd = getBsubDef(job, cmdToRun)
            # write this out to a file
            bsubFile = job['workspace'] 
            bsubFile = bsubFile.replace(".root", "_")
            bsubFile = bsubFile + job['conditional'] + ".sh"
            bsubFile = bsubBaseDir + "/" + bsubFile

            print bsubFile
            fileObj=open(bsubFile, 'w')
            fileObj.write(bsubCmd)
            fileObj.close() 

            if args.dryRun == True:
                continue

            os.system("chmod -R 775 " + bsubFile)
            command = "bsub < "+ bsubFile
            print command
            os.system(command)
            



def getJobDef(currJob):
    if not os.path.isfile( WORKSPACEDIR + currJob['workspace']):
        print 'error: DNE ',WORKSPACEDIR + currJob['workspace']
        exit(1)
    cmd =  'root -l -b -q \'asimovWS.C+('''
    cmd += '"%s",' %(WORKSPACEDIR + currJob['workspace'])
    cmd += '"%s",' %(currJob['RooDataSet'])
    cmd += '-99,'
    cmd += '%s,' %(currJob['conditional'])
    if ('fixPOI' in currJob):
        cmd += '%s,' %(currJob['fixPOI'])
    else:
        cmd += 'true,'
    cmd += '"%s")\'' %(currJob['workspaceName'])
    #print cmd
    #exit()
    return cmd


def getBsubDef(currJob, cmdToRun):

    bsubOutFileName = currJob["workspace"]
    bsubOutFileName = "stdout_" + bsubOutFileName.replace(".root", "_") + currJob["conditional"] + ".txt"

    text = """
#!/bin/bash

"""
    text += """
#BSUB -J %s """ %(currJob["workspace"])
    text += """
#BSUB -o ../bsub/outputs/%s""" %bsubOutFileName

    text += """
#BSUB -q %s
#BSUB -u bianca.ciungu@cern.ch

""" %(args.queue)

    text += """
WORKDIR=$TMPDIR/LSF_$LSB_JOBID
HOMEDIR=%s
OUTDIR=$HOMEDIR
FOLDER=addAsimov/

""" %(os.getcwd()) 

    text += """
stagein()
{
    cd $HOMEDIR
    cd $FOLDER
    ls

    currentDir=$PWD

    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    lsetup "root 6.10.04-x86_64-slc6-gcc62-opt"



    cd $currentDir
    ls
}

runcode()
{
"""
        
    text += cmdToRun
    text += """
  pwd
    """ 
    text += """
  ls
    """ 
    text += """
}

stageout()
{
    rm -rf core.*
    
}

stagein
runcode
stageout

exit
"""
    return text




# run the main function
if __name__ == "__main__":
    main()

