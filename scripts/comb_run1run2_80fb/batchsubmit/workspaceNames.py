List_Correlated = [

# {'workspace':'Run1_VH_VBF_ttHbb_correlated.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run1_VH_VBF_ttHbb_correlated.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_UPESUpdate.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_UPESUpdate.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_EPS_VH_VBF_ttHbb_correlated.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_EPS_VH_VBF_ttHbb_correlated.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

#{'workspace':'Run12_VH_VBF_ttHbb_correlated.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run12_VH_VBF_ttHbb_correlated_UPESUpdate.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_VH_VBF_ttHbb_correlated_UPESUpdate.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run12_VH_VBF_ttHbb_correlated_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_VH_VBF_ttHbb_correlated_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run12_EPS_VH_VBF_ttHbb_correlated.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_EPS_VH_VBF_ttHbb_correlated.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run12_125-r1_combData-r2_obsData-normal_partial.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_125-r1_combData-r2_obsData-normal_partial.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'}

# {'workspace':'Run12_125-r1_combData-r2_obsData-normal_partial_UPESUpdate.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_125-r1_combData-r2_obsData-normal_partial_UPESUpdate.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run12_125-r1_combData-r2_obsData-normal_partial_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_125-r1_combData-r2_obsData-normal_partial_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run12_EPS_VH.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_EPS_VH.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run12_ttHbb_correlated.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_ttHbb_correlated.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run12_VBF_correlated.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_VBF_correlated.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'}

# {'workspace':'run2/Run2_VHbb_125.root','RooDataSet':'obsData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'run2/Run2_VHbb_125.root','RooDataSet':'obsData', 'conditional':'false', 'workspaceName':'combined'}

# {'workspace':'run1/Run1_VHbbWHZH_125.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'run1/Run1_VHbbWHZH_125.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},
# {'workspace':'run2/Run2_VHWHZH_PSUE_ggV_125.root','RooDataSet':'obsData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'run2/Run2_VHWHZH_PSUE_ggV_125.root','RooDataSet':'obsData', 'conditional':'false', 'workspaceName':'combined'},
# {'workspace':'Run12_125-r1_combData-r2_obsData-poi-whzh-normal_partial_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_125-r1_combData-r2_obsData-poi-whzh-normal_partial_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'run1/Run1_VHbb012lep_125.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'run1/Run1_VHbb012lep_125.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},
# {'workspace':'run2/Run2_VH012lep_PSUE_ggV_125.root','RooDataSet':'obsData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'run2/Run2_VH012lep_PSUE_ggV_125.root','RooDataSet':'obsData', 'conditional':'false', 'workspaceName':'combined'},
# {'workspace':'Run12_125-r1_combData-r2_obsData-poi-012lep-normal_partial_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_125-r1_combData-r2_obsData-poi-012lep-normal_partial_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},


# {'workspace':'Run12_125-r1_combData-r2_obsData-poi-run12split-normal_partial_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_125-r1_combData-r2_obsData-poi-run12split-normal_partial_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run12_125-r1_combData-r2_obsData-poi-012lep-normal_full_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_125-r1_combData-r2_obsData-poi-012lep-normal_full_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},




]

List_Correlated_Check = [

#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B0_decoVH8TeV.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B0_decoVH8TeV.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},
##### NP correlation check
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_alpha_JET_EffectiveNP_1.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_alpha_JET_EffectiveNP_1.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},
#
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_alpha_JET_EtaIntercalibration_Modelling.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_alpha_JET_EtaIntercalibration_Modelling.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},
#
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B0.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B0.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},
#
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B3.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B3.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},
#
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B4.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B4.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},
#
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_C0.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_C0.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},
#
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_L0.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_L0.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},
#
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_L11.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_L11.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},
#
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_alpha_SysJET_JER_SINGLE_NP.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_alpha_SysJET_JER_SINGLE_NP.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},
#
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B1.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B1.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},
#
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_alpha_SysJET_Pileup_RhoTopology.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_comb_alpha_SysJET_Pileup_RhoTopology.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run12_VH_VBF_ttHbb_decorrlatedPOI_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'true', 'fixPOI':'false', 'workspaceName':'combined'},
# {'workspace':'Run12_125-r1_combData-r2_obsData-poi-run12split-normal_partial_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'true', 'fixPOI':'false', 'workspaceName':'combined'},


# ##### NP correlation check
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_alpha_JET_EffectiveNP_1.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_alpha_JET_EffectiveNP_1.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_alpha_JET_EtaIntercalibration_Modelling.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_alpha_JET_EtaIntercalibration_Modelling.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B0_decoVH8TeV.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B0_decoVH8TeV.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B3.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B3.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B4.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B4.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_C0.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_C0.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_L0.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_L0.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_L11.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_L11.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_alpha_SysJET_JER_SINGLE_NP.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_alpha_SysJET_JER_SINGLE_NP.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B1.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_ATLAS_FTAG_B1.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_alpha_SysJET_Pileup_RhoTopology.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_comb_alpha_SysJET_Pileup_RhoTopology.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},




## mcStat update Jul 23

{'workspace':'Run12_125-r1_combData-r2_obsData-normal_partial_mcStatUpdate.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
{'workspace':'Run12_125-r1_combData-r2_obsData-normal_partial_mcStatUpdate.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},











]







List_Uncorrelated = [

#Run 1
# {'workspace':'Run1_VH_VBF_ttHbb_correlated_decorrlatedNP.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run1_VH_VBF_ttHbb_correlated_decorrlatedNP.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},


# Run 2 
# {'workspace':'Run2_EPS_VH_VBF_ttHbb_correlated_decorrlatedNP.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_EPS_VH_VBF_ttHbb_correlated_decorrlatedNP.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_decorrlatedNP.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_decorrlatedNP.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# {'workspace':'Run2_VH_VBF_ttHbb_correlated_decorrlatedNP_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run2_VH_VBF_ttHbb_correlated_decorrlatedNP_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

# Run 1 + 2 
# {'workspace':'Run12_EPS_VH_VBF_ttHbb_correlated_decorrlatedNP.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_EPS_VH_VBF_ttHbb_correlated_decorrlatedNP.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},

#{'workspace':'Run12_VH_VBF_ttHbb_correlated_decorrlatedNP.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
#{'workspace':'Run12_VH_VBF_ttHbb_correlated_decorrlatedNP.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'}

# {'workspace':'Run12_VH_VBF_ttHbb_correlated_decorrlatedNP_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'true', 'workspaceName':'combined'},
# {'workspace':'Run12_VH_VBF_ttHbb_correlated_decorrlatedNP_PSUE_ggV.root','RooDataSet':'combData', 'conditional':'false', 'workspaceName':'combined'},


]
