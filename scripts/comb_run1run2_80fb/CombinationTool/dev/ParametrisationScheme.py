#!/usr/bin/env python
""" ParametrisationScheme.

Collection of multiple workspace editing operations.
"""

from __future__ import absolute_import

import logging


__author__ = "Stefan Gadatsch"
__credits__ = ["Stefan Gadatsch", "Andrea Gabrielli"]
__version__ = "0.1"
__maintainer__ = "Stefan Gadatsch"
__email__ = "stefan.gadatsch@cern.ch"

__all__ = [
    'ParametrisationScheme'
]


FORMAT = '%(asctime)s - %(processName)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
existing_logger = logging.getLogger('x')


class ParametrisationScheme(object):
    def __init__(self, name):
        self._name = name
        self._expressions = []
        self._nuis = []
        self._globs = []

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def expressions(self):
        return self._expressions

    @expressions.setter
    def expressions(self, expressions):
        self._expressions = expressions

    @property
    def nuisances(self):
        return self._nuis

    @nuisances.setter
    def nuisances(self, nuisances):
        self._nuis = nuisances

    @property
    def globs(self):
        return self._globs

    @globs.setter
    def globs(self, globs):
        self._globs = globs

    def AddExpression(self, expression):
        self._expressions.append(expression)

    def AddNewNuisanceParameters(self, nuisance):
        self._nuis.append(nuisance)

    def AddNewGlobalObservable(self, glob):
        self._globs.append(glob)


if __name__ == '__main__':
    pass
