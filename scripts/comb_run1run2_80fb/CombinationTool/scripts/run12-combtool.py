#!/usr/bin/env python
import ROOT,sys,os,argparse,socket
import matrix_manipulator as mm
npn='23NP' #19NP
harvard='harvard' in socket.gethostname()
ROOT.PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem
#Figure out how to do linear combinations Maybe use RooFormulaVar's and clone/re-write workspaces as appropriate?


def asimovDataName(mu_asimov=1,isConditional=True):
    # mu values of less than 0 represent (for now anyway) not doing Asimov
    if mu_asimov<0:
        return 'obsData'
    number = str(int(mu_asimov)) if mu_asimov%1==0  else str(mu_asimov).rstrip('0')
    return 'asimovData_cnd{0}_{1}'.format(int(isConditional),number)

def unfolded_jes_nps(strong):
    datdir='/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct/'if harvard else '/afs/cern.ch/work/s/stchan/public/hcomb/'
    fr1map="{0}/ProjectionCoefficientFile_{1}_test_withAmplitudes_April29.root".format(datdir,'Final2012')
    enps_r1=list('alpha_SysJetNP{0}_Y2012_8TeV'.format(i+1) for i in range(4)) #5 isn't in there for some reason even though 6 is the "rest" one

    fr2map="{0}/ProjectionCoefficientFile_{1}_test_withAmplitudes_April29.root".format(datdir,'Moriond2017')
    enps_r2=list('alpha_SysJET_21NP_JET_EffectiveNP_{0}'.format(i+1) for i in range(7))

    nps_r1=mm.parse_JES_map(mapfile=fr1map,effective_nps=enps_r1,qrun2=False)[1]
    nps_r2=mm.parse_JES_map(mapfile=fr2map,effective_nps=enps_r2,qrun2=True)[1]

    tidus=[]
    for one in nps_r1:
        ok=[one]
        for two in nps_r2:
            if mm.ismatch(one,two,noemu=False,jes_strong=strong):
                ok.append(two)
                tidus.append(ok)
                break
    #print '\n'.join(list(' '.join(t) for t in tidus))
    return tidus

#do a CombinationTool job--the Run2 dataset name (to allow for Asimov), correlation scheme ('btag','jes', and uncorr), extent of correlations (full or partial), and Run2 WS path can be specified
def do_combination(dataset13TeV='obsData',dataset78TeV='combData',corrType='normal',full_corr=True,run2path="Run2WS-20170421/combined/125_wAsimov.root",run1path="Run1WS/combined/125_wAsimov.root",dodb=False): #ICHEP2016WS/combined/
    # A note on NP naming: the Run 1 workspace is already a combined worksapce, so the NP's will sometimes have a _7TeV or _8TeV after their names to specify
    # So you might get an NP that looks like Blah_blah_7TeV_78TeV


    # Figure out what your input Run 1 and Run 2 workspace files are.  There is a standard naming scheme
    # jes: effective JES NP's full and partial according to JetEtMiss correlation recommendations
    # jesu: unfolded JES NP's full and partial as above
    # jes8: leave 7 TeV out of it; full and partial as above, but only correlate 8 and 13 TeV
    # btag:  b, c, l correlate only B, C, and L NP's.  bl mens correlate B and L only (charm tagging really changed between Run 1 and 2).  nf means the 8 TeV leading NP is \emph{not} flipped (historical; this is wrong).
    #     If no label, all flavors correlated.  Partial is just the leading NP(s); full is up to the highest index in all datasets.
    # poi-whzh: WH and ZH signal strengths treated as separate.  Full is a 4 POI fit (Run 1 WH/ZH, Run 2 WH/ZH).  Partial is a 2 POI fit (combined WH/ZH).  (I'm sorry.  I know this is counterintuitive; this part was coded late at night at some point)
    # poi-012lep: As with WH/ZH except for 0, 1, and 2 lep signal strengths separate, so full is a 6 POI fit, and partial is 3 POI (consistently counterintuitive).
    # poi-comp: Run1/Run2 POI's

    # PART I DETERMINING I/O LOCATIONS
    # Some bookeeping; find out where the workspaces live 
    # Note that if the dataset name is hard-coded in the conditionals below
    # it's because I never made Asimov for those cases, not for any mysterious physical reason
    # CHANGE!!!
    corrType=corrType.lower()
    fulljes=(full_corr and 'jesu' in corrType) 
    wsdir='/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct/workspaces/'if harvard else '/afs/cern.ch/user/b/bciungu/work/Hbb/Jul22CombinedVH/'
    print "run1path", run1path
    print "run2path", run2path
    f78TeVfile = ('' if run1path[0]=='/' else wsdir)+run1path
    f13TeVfile = ('' if run2path[0]=='/' else wsdir)+run2path
    if corrType=='jesu':
        f78TeVfile = run1path[0:run1path.rfind('/')]+'/jes-unfold.root'
        f13TeVfile = run2path[0:run2path.rfind('/')]+'/jes-unfold.root'
    if 'btag' in corrType and corrType!='btag-nf':
        f78TeVfile = run1path[0:run1path.rfind('/')]+'/btag-unfold.root'
    # if 'poi' in corrType and 'lep' in corrType: 
    #     nlep=corrType[corrType.find('-')+1]
    #     f78TeVfile = run1path[0:run1path.rfind('/')]+'/VH3poi_MVA.root'
    #     f13TeVfile = run2path[0:run2path.rfind('/')]+'/VH3poi_MVA.root'
        # EXPAND!
        # dataset78TeV='combData'
        # dataset13TeV='obsData'
    # if corrType=='poi-whzh':
    #     f78TeVfile = run1path[0:run1path.rfind('/')]+'/VH2poi_MVA.root'
    #     f13TeVfile = run2path[0:run2path.rfind('/')]+'/125.root'
    #     # EXPAND!
    #     dataset78TeV='combData'
    #     dataset13TeV='obsData'

    # Standard output file naming convention
    OutfileName =  "{0}/125-r1_{1}-r2_{2}-{3}_{4}_mcStatUpdate.root".format(wsdir,dataset78TeV,dataset13TeV,corrType,'full' if full_corr else 'partial')
    # If we're doing diboson fits, just set everything to the only supported options 
    # EXPAND!
    if dodb:
        f78TeVfile = run1path[0:run1path.rfind('/')]+'/VZ1poi_MVA.root'
        f13TeVfile = run2path[0:run2path.rfind('/')]+'/VZ1poi_MVA.root'
        OutfileName =  "{0}Run1Run2Comb/combined/125-db.root".format(wsdir)
        dataset78TeV='combData'
        dataset13TeV='obsData'
        corrType='jes'
        full_corr=False

    print 'OutfileName = ', OutfileName
    print 'f78TeVfile = ', f78TeVfile
    print 'f13TeVfile = ', f13TeVfile

    # PART Ia: EXTRACT WORKSPACES

    combined = ROOT.CombinedMeasurement("combined_master")
 
    # specify the Run1 measurement; (for now) this won't change
    m78TeV = ROOT.Measurement("78TeV") ##78TeV #carlo
    m78TeV.SetSnapshotName("nominalNuis") #snapshot_paramsVals_initial _GlobalFit
    m78TeV.SetFileName(f78TeVfile)
    m78TeV.SetWorkspaceName("combined")
    m78TeV.SetModelConfigName("ModelConfig")
    m78TeV.SetDataName(dataset78TeV)
    combined.AddMeasurement(m78TeV) #carlo
    
    # and now the Run2
    m13TeV = ROOT.Measurement("13TeV")
    m13TeV.SetSnapshotName("snapshot_paramsVals_initial") #snapshot_paramsVals_initial
    m13TeV.SetFileName(f13TeVfile)
    m13TeV.SetWorkspaceName("combined")
    m13TeV.SetModelConfigName("ModelConfig")
    m13TeV.SetDataName(dataset13TeV)
    combined.AddMeasurement(m13TeV)

    # PART II: DEFINE CORRELATIONS
    correlation = ROOT.CorrelationScheme("CorrelationScheme") #carlo

    # PART IIa: Parameter Of Interest Correlations
    # If 'poi' is not in the correlation type, we just want the usual, single POI fit, so correlate the signal strengths
    if 'poi' not in corrType:
        correlation.SetParametersOfInterest("SigXsecOverSM") #carlo 
        correlation.CorrelateParameter("78TeV::SigXsecOverSM,13TeV::SigXsecOverSM", "SigXsecOverSM")
    else:
        if corrType=='poi-012lep':
            # Seperate 0, 1, and 2 lepton POI's (3 and 6 POI fits)
            # FULL is 6 POI's
            # PARTIAL is 3 POI's
            # CHANGE?
            if full_corr:
                correlation.SetParametersOfInterest("SigXsecOverSM_L0_78TeV,SigXsecOverSM_L1_78TeV,SigXsecOverSM_L2_78TeV,SigXsecOverSM_L0_13TeV,SigXsecOverSM_L1_13TeV,SigXsecOverSM_L2_13TeV") #stephen 
            else: #yeah, I know--it's flipped, but we arbitrarily chose partial for the poi whzh/012lep before we realized we'd need extra shit
                correlation.SetParametersOfInterest("SigXsecOverSM_L0,SigXsecOverSM_L1,SigXsecOverSM_L2") #stephen 
                correlation.CorrelateParameter("78TeV::SigXsecOverSM_L0,13TeV::SigXsecOverSM_L0", "SigXsecOverSM_L0")
                correlation.CorrelateParameter("78TeV::SigXsecOverSM_L1,13TeV::SigXsecOverSM_L1", "SigXsecOverSM_L1")
                correlation.CorrelateParameter("78TeV::SigXsecOverSM_L2,13TeV::SigXsecOverSM_L2", "SigXsecOverSM_L2")
        elif corrType=='poi-whzh':
            # Separate WH/ZH POI's (2 and 4 POI fits)
            # FULL is 4 POI's
            # PARTIAL is 2 POI's
            if full_corr:
                correlation.SetParametersOfInterest("SigXsecOverSMWH_78TeV,SigXsecOverSMZH_78TeV,SigXsecOverSMWH_13TeV,SigXsecOverSMZH_13TeV") #stephen 
            else: #yeah, I know--it's flipped, but we arbitrarily chose partial for the poi whzh/012lep before we realized we'd need extra shit
                correlation.SetParametersOfInterest("SigXsecOverSMWH,SigXsecOverSMZH") #stephen 
                correlation.CorrelateParameter("78TeV::SigXsecOverSMWH,13TeV::SigXsecOverSMWH", "SigXsecOverSMWH")
                correlation.CorrelateParameter("78TeV::SigXsecOverSMZH,13TeV::SigXsecOverSMZH", "SigXsecOverSMZH")
        else:
            # 2 POI fits (Run1/Run2 independent)
            correlation.SetParametersOfInterest("SigXsecOverSM_78TeV,SigXsecOverSM_13TeV") #carlo 

    # PART IIb: DEFAULT CORRELATIONS
    # non-trivial correlations
    # start with signal systematics that are always correlated (for now)
    correlation.CorrelateParameter("78TeV::ATLAS_BR_bb,13TeV::alpha_SysTheoryBRbb","alpha_SysTheoryBRbb")
    correlation.CorrelateParameter("78TeV::alpha_SysTheoryQCDscale_ggZH,13TeV::alpha_SysTheoryQCDscale_ggZH","alpha_SysTheoryQCDscale_ggZH")
    correlation.CorrelateParameter("78TeV::alpha_SysTheoryQCDscale_qqVH,13TeV::alpha_SysTheoryQCDscale_qqVH","alpha_SysTheoryQCDscale_qqVH")
    correlation.CorrelateParameter("78TeV::alpha_SysTheoryPDF_ggZH_8TeV,13TeV::alpha_SysTheoryPDF_ggZH","alpha_SysTheoryPDF_ggZH")
    correlation.CorrelateParameter("78TeV::alpha_SysTheoryPDF_qqVH_8TeV,13TeV::alpha_SysTheoryPDF_qqVH","alpha_SysTheoryPDF_qqVH")
    correlation.CorrelateParameter("78TeV::alpha_SysTheoryVHPt_8TeV,13TeV::alpha_SysVHNLOEWK","alpha_SysVHNLOEWK")
    # BJES is also always correlated, but historically, we have checked not including 7 TeV in JES correlations, so if we're looking at 'jes8', this is handled specially
    # This is the JES weak/partial scheme; hence, a correlation type of 'jes' and fullcorr set to False actually does nothing special
    # CHANGE?
    if corrType!='jes8':
        correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_BJES_Response,78TeV::alpha_SysJetFlavB_7TeV,78TeV::alpha_SysJetFlavB_8TeV","alpha_SysJET_BJES_Response") # the only unfolded thing you correlate in the JES "weak" scheme

    # old test: testing signal acceptance in addition to the usual theory ones
    if corrType=='sigacc':
        correlation.CorrelateParameter("78TeV::alpha_SysTheoryAcc_J2_ggZH_8TeV,13TeV::alpha_SysTheoryAcc_J2_ggZH","alpha_SysTheoryAcc_J2_ggZH")
        correlation.CorrelateParameter("78TeV::alpha_SysTheoryAcc_J3_ggZH_8TeV,13TeV::alpha_SysTheoryAcc_J3_ggZH","alpha_SysTheoryAcc_J3_ggZH")
        correlation.CorrelateParameter("78TeV::alpha_SysTheoryAcc_J2_qqVH_8TeV,13TeV::alpha_SysTheoryAcc_J2_qqVH","alpha_SysTheoryAcc_J2_qqZH")
        correlation.CorrelateParameter("78TeV::alpha_SysTheoryAcc_J3_qqVH_8TeV,13TeV::alpha_SysTheoryAcc_J3_qqVH","alpha_SysTheoryAcc_J3_qqZH")
        correlation.CorrelateParameter("78TeV::alpha_SysTheoryAccPDF_qqVH_8TeV,13TeV::alpha_SysTheoryPDFAcc_qqVH","alpha_SysTheoryPDFAcc_qqVH")
        correlation.CorrelateParameter("78TeV::alpha_SysTheoryAccPDF_ggZH_8TeV,13TeV::alpha_SysTheoryPDFAcc_ggZH","alpha_SysTheoryPDFAcc_ggZH")
        correlation.CorrelateParameter("78TeV::alpha_SysTheoryAccPS_8TeV,13TeV::alpha_SysTheoryUEPSAcc_J3,13TeV::alpha_SysTheoryUEPSAcc","alpha_SysTheoryUEPSAcc")
        
    if 'btag' in corrType:
        # PART IIc FLAVOR TAGGING
        all = corrType=='btag'
        if all or corrType in ['btag-b','btag-bl','btag-nf']:
            if corrType=='btag-nf':
                correlation.CorrelateParameter("78TeV::alpha_SysBTagB0Effic_7TeV,78TeV::alpha_SysBTagB0Effic_Y2012_8TeV,13TeV::alpha_SysFT_EFF_Eigen_B_0", "alpha_SysFT_EFF_Eigen_B_0")
            else:
                # Requires an edited workspace with the flipped 0th 8 TeV B NP
                correlation.CorrelateParameter("78TeV::alpha_SysBTagB0Effic_7TeV,78TeV::alpha_SysFT_EFF_Eigen_B_0_8TeV,13TeV::alpha_SysFT_EFF_Eigen_B_0", "alpha_SysFT_EFF_Eigen_B_0")
            if full_corr:
                for i in range(1,3):
                    correlation.CorrelateParameter("78TeV::alpha_SysBTagB{0}Effic_7TeV,78TeV::alpha_SysBTagB{0}Effic_Y2012_8TeV,13TeV::alpha_SysFT_EFF_Eigen_B_{0}".format(i), "alpha_SysFT_EFF_Eigen_B_{0}".format(i))
        if all or corrType=='btag-c':
            correlation.CorrelateParameter("78TeV::alpha_SysBTagC0Effic_7TeV,78TeV::alpha_SysBTagC0Effic_Y2012_8TeV,13TeV::alpha_SysFT_EFF_Eigen_C_0", "alpha_SysFT_EFF_Eigen_C_0")
            if full_corr:
                for i in range(1,4):
                    correlation.CorrelateParameter("78TeV::alpha_SysBTagC{0}Effic_7TeV,78TeV::alpha_SysBTagC{0}Effic_Y2012_8TeV,13TeV::alpha_SysFT_EFF_Eigen_C_{0}".format(i), "alpha_SysFT_EFF_Eigen_C_{0}".format(i))
        if all or corrType in ['btag-l','btag-bl']:
            correlation.CorrelateParameter("78TeV::alpha_SysBTagLEffic_7TeV,78TeV::alpha_SysBTagL0Effic_Y2012_8TeV,13TeV::alpha_SysFT_EFF_Eigen_Light_0", "alpha_SysFT_EFF_Eigen_Light_0")
            if full_corr:
                for i in range(1,4):
                    correlation.CorrelateParameter("78TeV::alpha_SysBTagL{0}Effic_Y2012_8TeV,13TeV::alpha_SysFT_EFF_Eigen_Light_{0}".format(i), "alpha_SysFT_EFF_Eigen_Light_{0}".format(i))
    elif corrType in ['jes','jesu']:
        if corrType=='jesu':
            # The unfolded JES NP scheme code is dealt with elsewhere
            for np in unfolded_jes_nps(strong=full_corr):
                correlation.CorrelateParameter("78TeV::{0},13TeV::{1}".format(np[0],np[1]),np[1])
        if full_corr:
            # In the full/strong scheme, also matched all of the named NP's
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_RhoTopology,78TeV::alpha_SysJetPileRho_Y2012_8TeV","alpha_SysJET_Pileup_RhoTopology")  	 #no 7 TeV
            #correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Response,78TeV::alpha_SysJetFlavResp_(Wjets|Zjets|Diboson|Top)_[78]TeV_78TeV")  
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_OffsetMu,78TeV::alpha_SysJetMu_7TeV,78TeV::alpha_SysJetMu_8TeV","alpha_SysJET_Pileup_OffsetMu")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_OffsetNPV,78TeV::alpha_SysJetNPV_7TeV,78TeV::alpha_SysJetNPV_8TeV","alpha_SysJET_Pileup_OffsetNPV")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_PtTerm,78TeV::alpha_SysJetPilePt_Y2012_8TeV","alpha_SysJET_Pileup_PtTerm")	 #no 7 TeV
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_EtaIntercalibration_Modelling,78TeV::alpha_SysJetEtaModel_7TeV,78TeV::alpha_SysJetEtaModel_8TeV","alpha_SysJET_EtaIntercalibration_Modelling")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_EtaIntercalibration_TotalStat,78TeV::alpha_SysJetEtaStat_7TeV,78TeV::alpha_SysJetEtaStat_Y2012_8TeV","alpha_SysJET_EtaIntercalibration_TotalStat")
            #correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_EtaIntercalibration_NonClosure,78TeV::alpha_SysJetNonClos_7TeV,78TeV::alpha_SysJetNonClos_8TeV","alpha_SysJET_EtaIntercalibration_NonClosure")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_Top,13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_ttbar_L2,78TeV::alpha_SysJetFlavComp_Top_7TeV,78TeV::alpha_SysJetFlavComp_Top_8TeV","alpha_SysJET_Flavor_Composition_Top")   
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_Vjets,78TeV::alpha_SysJetFlavComp_Wjets_7TeV,78TeV::alpha_SysJetFlavComp_Wjets_8TeV,78TeV::alpha_SysJetFlavComp_Zjets_7TeV,78TeV::alpha_SysJetFlavComp_Zjets_8TeV","alpha_SysJET_Flavor_Composition_Zjets")   # W/Z+jets serparate in Run1, not Run2
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition,13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_VV,78TeV::alpha_SysJetFlavComp_Diboson_7TeV,78TeV::alpha_SysJetFlavComp_VHVV_8TeV","alpha_SysJET_Flavor_Composition_VHVV")   # VHVV separate in Run2, not Run1
            #correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_Wjets,78TeV::alpha_SysJetFlavComp_Wjets_7TeV,78TeV::alpha_SysJetFlavComp_Wjets_8TeV","alpha_SysJET_Flavor_Composition_Wjets")   
            #correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_Zjets,78TeV::alpha_SysJetFlavComp_Zjets_7TeV,78TeV::alpha_SysJetFlavComp_Zjets_8TeV","alpha_SysJET_Flavor_Composition_Zjets")   
            #correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition,78TeV::alpha_SysJetFlavComp_Diboson_7TeV,78TeV::alpha_SysJetFlavComp_VHVV_8TeV","alpha_SysJET_Flavor_Composition_VHVV")   
    elif corrType=='jes8':
        # only correlate 8 TeV and 13 TeV; ignor 7 TeV
        correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_BJES_Response,78TeV::alpha_SysJetFlavB_8TeV","alpha_SysJET_BJES_Response")
        if full_corr:
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_EtaIntercalibration_Modelling,78TeV::alpha_SysJetEtaModel_8TeV","alpha_SysJET_EtaIntercalibration_Modelling")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_EtaIntercalibration_TotalStat,78TeV::alpha_SysJetEtaStat_Y2012_8TeV","alpha_SysJET_EtaIntercalibration_TotalStat")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_EtaIntercalibration_NonClosure,78TeV::alpha_SysJetNonClos_8TeV","alpha_SysJET_EtaIntercalibration_NonClosure")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_OffsetMu,78TeV::alpha_SysJetMu_8TeV","alpha_SysJET_Pileup_OffsetMu")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_OffsetNPV,78TeV::alpha_SysJetNPV_8TeV","alpha_SysJET_Pileup_OffsetNPV")
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_PtTerm,78TeV::alpha_SysJetPilePt_Y2012_8TeV","alpha_SysJET_Pileup_PtTerm")	 #no 7 TeV
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Pileup_RhoTopology,78TeV::alpha_SysJetPileRho_Y2012_8TeV","alpha_SysJET_Pileup_RhoTopology")  	 #no 7 TeV
            #correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Response,78TeV::alpha_SysJetFlavResp_(Wjets|Zjets|Diboson|Top)_[78]TeV_78TeV")   
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_Top,13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_ttbar_L2,78TeV::alpha_SysJetFlavComp_Top_8TeV","alpha_SysJET_Flavor_Composition_Top") 
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_Vjets,78TeV::alpha_SysJetFlavComp_Wjets_8TeV,78TeV::alpha_SysJetFlavComp_Zjets_7TeV,78TeV::alpha_SysJetFlavComp_Zjets_8TeV","alpha_SysJET_Flavor_Composition_Zjets")   # W/Z+jets serparate in Run1, not Run2
            correlation.CorrelateParameter("13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition,13TeV::alpha_SysJET_"+npn+"_JET_Flavor_Composition_VV,78TeV::alpha_SysJetFlavComp_VHVV_8TeV","alpha_SysJET_Flavor_Composition_VHVV")   # VHVV separate in Run2, not Run1
            

    # PART III wrap up
    combined.SetCorrelationScheme(correlation) #carlo
    combined.CollectMeasurements()
    print '~*~*~*~*~COLLECTED MEASUREMENTS!~*~*~*~*~'
    combined.CombineMeasurements()
    #combined.MakeAsimovData(True)
    combined.writeToFile(OutfileName)
    #combined.MakeSnapshots(ROOT.CombinedMeasurement.nominal)
    #combined.writeToFile(OutfileName)
    combined.Print()

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Parse options for automated CombinationTool job launching')
  parser.add_argument('-c','--corrtype',help='Type of correlation scheme to use',default='normal')
  parser.add_argument('-o','--obs',help="Run1 observed data",action='store_true')
  parser.add_argument('-u','--mu',help='Mu value for injected mu <0 --> obsData not Asimov',default=-99,type=float)
  parser.add_argument('-f','--fullcorr',help='Do full correlation schemes (if applicable)',action='store_true')
  #parser.add_argument('-r','--run2path',help='Points to the Run2 WS (should contain the Asimov)',default='/afs/cern.ch/user/b/bciungu/work/Hbb/WSMaker_VHbb_V2/output/Haider_SMVHVZ_Summer18_MVA_mc16a_v05.mc16a_Run2_IncMu_VHbb_mc16a_Run2_IncMu_012_Systs_mva/workspaces/combined/125.root')
 # parser.add_argument('-r','--run2path',help='Points to the Run2 WS (should contain the Asimov)',default='/afs/cern.ch/work/r/rcostaba/public/VHbbWSpaces/SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2.Asimov_fullRes_VHbb_Asimov_012_mc16ad_Systs_mva/workspaces/combined/125.root')  
  
  # ICHEP final 1 poi
  #parser.add_argument('-r','--run2path',help='Points to the Run2 WS (should contain the Asimov)',default='/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/ICHEP2018Results/VHWSforCombination_UPDATED__PSUE-ggV__Higgs/SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2.Observed_fullRes_VHbb_Observed_012_mc16ad_Systs_mva/workspaces/combined/125.root') #andy WS PSUE ggV i

  #post ICHEP with updated mcStats
  parser.add_argument('-r','--run2path',help='Points to the Run2 WS (should contain the Asimov)',default='/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/ICHEP2018Results/VHWSforCombination_UPDATED__PSUE-ggV__Higgs/SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2_extension.UncondData_fullRes_VHbb_UncondData_012_mc16ad_Systs_mva/workspaces/combined/125.root') 

  parser.add_argument('-1','--run1path',help='Points to the Run1 WS (should contain the Asimov)',default='/afs/cern.ch/user/b/bciungu/work/Hbb/hcomb/workspaces/Run1WS/combined/125.root')
  
  # ICHEP final WH/ZH
  # parser.add_argument('-r','--run2path',default='/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/ICHEP2018Results/VHWSforCombination_UPDATED__PSUE-ggV__Higgs/SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2.Observed_2PoI_fullRes_VHbb_Observed_2PoI_012_mc16ad_Systs_mva/workspaces/combined/125.root') #andy WS PSUE ggV 2poi
  # parser.add_argument('-1','--run1path',help='Points to the Run1 WS (should contain the Asimov)',default='/afs/cern.ch/user/b/bciungu/work/Hbb/hcomb/workspaces/Run1WS/combined/VH2poi_MVA.root')  
  
  # ICHEP 3 poi/ 6 poi 
  # parser.add_argument('-r','--run2path',default='/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/ICHEP2018Results/VHWSforCombination_UPDATED__PSUE-ggV__Higgs/SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2.Observed_3PoI_fullRes_VHbb_Observed_3PoI_012_mc16ad_Systs_mva/workspaces/combined/125.root') #andy WS PSUE ggV 3poi
  # parser.add_argument('-1','--run1path',help='Points to the Run1 WS (should contain the Asimov)',default='/afs/cern.ch/user/b/bciungu/work/Hbb/hcomb/workspaces/Run1WS/combined/VH3poi_MVA.root')  




  parser.add_argument('--cond',help='Look at conditional Asimov data',action='store_true')
  parser.add_argument('--db',help='Do diboson combo; supersedes most shit',action='store_true')
  args=parser.parse_args()

  try:
      loadcmd="{0}lib/libCombinationTool.so".format('/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct/'if harvard else '/afs/cern.ch/work/s/stchan/vhbb/combtools/wsm/')
      loadcmd="lib/libCombinationTool.so"
      ROOT.gSystem.Load(loadcmd)
      print 'Successfully loaded',loadcmd
  except:
      print ("Could not load library. Make sure that it was compiled correctly.")

  #do_combination(dataset78TeV=('combData' if args.obs else asimovDataName(0.51,True)),dataset13TeV="asimovData_1",corrType=args.corrtype,full_corr=args.fullcorr,run2path=args.run2path, run1path=args.run1path,dodb=args.db)

  do_combination(dataset78TeV="combData",dataset13TeV="obsData",corrType=args.corrtype,full_corr=args.fullcorr,run2path=args.run2path, run1path=args.run1path,dodb=args.db)
