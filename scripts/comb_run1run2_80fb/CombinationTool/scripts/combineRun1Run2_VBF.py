#!/usr/bin/env python

# Load the shared library. To compile it, follow the instructions given in the
# README.
import ROOT, sys
ROOT.PyConfig.IgnoreCommandLineOptions = True

from ROOT import gSystem

try:
    gSystem.Load("lib/libCombinationTool.so")
except:
    print ("Could not load library. Make sure that it was compiled correctly.")

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--doDecorrelatedPOI", action='store_true', help="Do De-correlated POI")
parser.add_argument("--doDecorrelatedNP",  action='store_true', help="Do De-correlated NP")

args = parser.parse_args()


# Location of run 2 input workspaces
file_run1="./workspaces/run1/Run1_VBF_surgery.root"
file_run2="./workspaces/run2/Run2_VBF_combined.root"

# Define
combined = ROOT.CombinedMeasurement("combined_master")

run1 = ROOT.Measurement("Run1")
run1.SetFileName(file_run1)
run1.SetWorkspaceName("combinedn")
run1.SetModelConfigName("ModelConfig")
run1.SetDataName("obsData")
combined.AddMeasurement(run1)


run2 = ROOT.Measurement("Run2")
run2.SetFileName(file_run2)
run2.SetWorkspaceName("combined")
run2.SetModelConfigName("ModelConfig")
run2.SetDataName("combData")
combined.AddMeasurement(run2)


# Define a correlation scheme that should be used when combining the specified
# measurements. Parameters in the scheme that are not present in the
# measurements will be ignored.
correlation = ROOT.CorrelationScheme("CorrelationScheme")

if(args.doDecorrelatedPOI):
	# Define parameters of interest for the combined measurement. Only parameters
	# present in the final workspace will be considered.
	correlation.SetParametersOfInterest("SigXsecOverSM_run2_VBF,SigXsecOverSM_run1_VBF")

	# Universal signal strength, not needed anymore as re-parametrisation and correction
	# for tautau contamination takes care of it.
	#correlation.CorrelateParameter("HZZ::mH", "mH")

	correlation.CorrelateParameter("Run1::mu",		"SigXsecOverSM_run1_VBF")

	correlation.CorrelateParameter("Run2::mu",		"SigXsecOverSM_run2_VBF")
else:
	# Define parameters of interest for the combined measurement. Only parameters
	# present in the final workspace will be considered.

	correlation.SetParametersOfInterest("SigXsecOverSM")

	# Universal signal strength, not needed anymore as re-parametrisation and correction
	# for tautau contamination takes care of it.
	#correlation.CorrelateParameter("HZZ::mH", "mH")

	correlation.CorrelateParameter("Run1::mu",		"SigXsecOverSM")
	correlation.CorrelateParameter("Run2::mu",		"SigXsecOverSM")



if(args.doDecorrelatedNP):
    ########################
    # VBF Run 1
    ########################
    correlation.CorrelateParameter("Run1::alpha_SysBTagSF","ATLAS_SysBTagSF_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysEW","ATLAS_SysEW_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF1_R1_Var0","ATLAS_SysF1_R1_Var0_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF1_R1_Var1","ATLAS_SysF1_R1_Var1_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF1_R1_Var2","ATLAS_SysF1_R1_Var2_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF1_R2_Var0","ATLAS_SysF1_R2_Var0_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF1_R2_Var1","ATLAS_SysF1_R2_Var1_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF1_R2_Var2","ATLAS_SysF1_R2_Var2_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF1_R3_Var0","ATLAS_SysF1_R3_Var0_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF1_R3_Var1","ATLAS_SysF1_R3_Var1_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF1_R3_Var2","ATLAS_SysF1_R3_Var2_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF1_R4_Var0","ATLAS_SysF1_R4_Var0_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF1_R4_Var1","ATLAS_SysF1_R4_Var1_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF1_R4_Var2","ATLAS_SysF1_R4_Var2_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF1_R4_Var3","ATLAS_SysF1_R4_Var3_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R1_Var0","ATLAS_SysF2_R1_Var0_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R1_Var1","ATLAS_SysF2_R1_Var1_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R2_Var0","ATLAS_SysF2_R2_Var0_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R2_Var1","ATLAS_SysF2_R2_Var1_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R2_Var2","ATLAS_SysF2_R2_Var2_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R2_Var3","ATLAS_SysF2_R2_Var3_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R3_Var0","ATLAS_SysF2_R3_Var0_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R3_Var1","ATLAS_SysF2_R3_Var1_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R3_Var2","ATLAS_SysF2_R3_Var2_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R3_Var3","ATLAS_SysF2_R3_Var3_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R4_Var0","ATLAS_SysF2_R4_Var0_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R4_Var1","ATLAS_SysF2_R4_Var1_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R4_Var2","ATLAS_SysF2_R4_Var2_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysF2_R4_Var3","ATLAS_SysF2_R4_Var3_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJER","ATLAS_SysJER_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_0","ATLAS_SysJES_0_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_1","ATLAS_SysJES_1_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_10","ATLAS_SysJES_10_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_11","ATLAS_SysJES_11_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_12","ATLAS_SysJES_12_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_13","ATLAS_SysJES_13_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_14","ATLAS_SysJES_14_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_15","ATLAS_SysJES_15_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_16","ATLAS_SysJES_16_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_2","ATLAS_SysJES_2_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_3","ATLAS_SysJES_3_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_4","ATLAS_SysJES_4_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_5","ATLAS_SysJES_5_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_6","ATLAS_SysJES_6_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_7","ATLAS_SysJES_7_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_8","ATLAS_SysJES_8_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysJES_9","ATLAS_SysJES_9_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysPDF","ATLAS_SysPDF_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysQCD","ATLAS_SysQCD_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysQGR","ATLAS_SysQGR_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_SysTrigSF","ATLAS_SysTrigSF_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_h1","unconst_h1_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_h2","unconst_h2_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_h3","unconst_h3_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_h4","unconst_h4_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_z1","unconst_z1_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_z2","unconst_z2_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_z3","unconst_z3_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_z4","unconst_z4_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_lumi","ATLAS_lumi_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_mcstat","gamma_mcstat_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_mcsigH","gamma_mcsigH_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_mcsigZ_z_1","gamma_mcsigZ_z_1_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_mcsigZ_z_2","gamma_mcsigZ_z_2_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_mcsigZ_z_3","gamma_mcsigZ_z_3_run1VBF")
    correlation.CorrelateParameter("Run1::alpha_mcsigZ_z_4","gamma_mcsigZ_z_4_run1VBF")
    correlation.CorrelateParameter("Run1::muZ","unconst_muZ_run1VBF")
    correlation.CorrelateParameter("Run1::nb1","unconst_nb1_run1VBF")
    correlation.CorrelateParameter("Run1::nb2","unconst_nb2_run1VBF")
    correlation.CorrelateParameter("Run1::nb3","unconst_nb3_run1VBF")
    correlation.CorrelateParameter("Run1::nb4","unconst_nb4_run1VBF")

    ########################
    # VBF
    ########################
    
    correlation.CorrelateParameter("Run2::alpha_ATLAS_lumi_combined","ATLAS_LUMI_RUN2_CORR")
    
    correlation.CorrelateParameter("Run2::alpha_JET_21NP_JET_BJES_Response_combined","ATLAS_JES_BJES_Response_Rel20p7")
    correlation.CorrelateParameter("Run2::alpha_JET_21NP_JET_EffectiveNP1_combined","ATLAS_JES_EffectiveNP_1_Rel20p7")
    correlation.CorrelateParameter("Run2::alpha_JET_21NP_JET_EffectiveNP2_combined","ATLAS_JES_EffectiveNP_2_Rel20p7")
    correlation.CorrelateParameter("Run2::alpha_JET_21NP_JET_EtaIntercalibration_Modelling_combined","ATLAS_JES_EtaInter_Model_Rel20p7")
    correlation.CorrelateParameter("Run2::alpha_JET_21NP_JET_Flavor_Composition_combined","ATLAS_JES_Flavor_Comp_Rel20p7")
    correlation.CorrelateParameter("Run2::alpha_JET_21NP_JET_Flavor_Response_combined","ATLAS_JES_Flavor_Resp_Rel20p7")
    correlation.CorrelateParameter("Run2::alpha_JET_21NP_JET_Pileup_PtTerm_combined","ATLAS_JES_PU_PtTerm_Rel20p7")
    correlation.CorrelateParameter("Run2::alpha_JET_21NP_JET_Pileup_RhoTopology_combined","ATLAS_JES_PU_Rho_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_21NP_JET_EffectiveNP_3_vbfincl","ATLAS_JES_EffectiveNP_3_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_21NP_JET_EffectiveNP_4_vbfincl","ATLAS_JES_EffectiveNP_4_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_21NP_JET_EffectiveNP_5_vbfincl","ATLAS_JES_EffectiveNP_5_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_21NP_JET_EffectiveNP_6_vbfincl","ATLAS_JES_EffectiveNP_6_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_21NP_JET_EffectiveNP_7_vbfincl","ATLAS_JES_EffectiveNP_7_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_21NP_JET_EffectiveNP_8restTerm_vbfincl","ATLAS_JES_EffectiveNP_8restTerm_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_21NP_JET_EtaIntercalibration_NonClosure_vbfincl","ATLAS_JES_EtaInter_NonClosure_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_21NP_JET_EtaIntercalibration_TotalStat_vbfincl","ATLAS_JES_EtaInter_Stat_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_21NP_JET_Pileup_OffsetMu_vbfincl","ATLAS_JES_PU_OffsetMu_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_21NP_JET_Pileup_OffsetNPV_vbfincl","ATLAS_JES_PU_OffsetNPV_Rel20p7")
    
    correlation.CorrelateParameter("Run2::auto_alpha_JVT_vbfg","ATLAS_JVT_Rel20p7")
    
    correlation.CorrelateParameter("Run2::alpha_JET_JER_SINGLE_NP_combined","ATLAS_JER_SINGLE_NP_Rel20p7")
    
    correlation.CorrelateParameter("Run2::alpha_PRW_DATASF_combined","ATLAS_PRW_DATASF_Rel20p7")
    
    correlation.CorrelateParameter("Run2::alpha_QCDScale_VBF_combined","ATLAS_QCDScale_VBF_combined_Rel20p7")
    correlation.CorrelateParameter("Run2::alpha_TH_PS_combined","ATLAS_TH_PS_combined_Rel20p7")
    correlation.CorrelateParameter("Run2::alpha_ttH_combined","ATLAS_ttH_combined_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_TH_PDFVar_vbfincl","ATLAS_TH_PDFVar_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_TH_alpha_s_vbfincl","ATLAS_TH_alpha_s_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_TH_QCDScaleggF_mig01_vbfincl","ATLAS_TH_QCDScaleggF_mig01_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_TH_QCDScaleggF_mig12_vbfincl","ATLAS_TH_QCDScaleggF_mig12_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_TH_QCDScaleggF_mu_vbfincl","ATLAS_TH_QCDScaleggF_mu_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_TH_QCDScaleggF_pTH120_vbfincl","ATLAS_TH_QCDScaleggF_pTH120_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_TH_QCDScaleggF_pTH60_vbfincl","ATLAS_TH_QCDScaleggF_pTH60_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_TH_QCDScaleggF_qmt_vbfincl","ATLAS_TH_QCDScaleggF_qmt_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_TH_QCDScaleggF_res_vbfincl","ATLAS_TH_QCDScaleggF_res_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_TH_QCDScaleggF_vbf2j_vbfincl","ATLAS_TH_QCDScaleggF_vbf2j_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_TH_QCDScaleggF_vbf3j_vbfincl","ATLAS_TH_QCDScaleggF_vbf3j_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_HbbBR_vbfg","alpha_SysTheoryBRbb")
    correlation.CorrelateParameter("Run2::auto_alpha_VH_vbfg","ATLAS_VH_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_VH_vbfincl","ATLAS_VH_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ZgQCDscale_vbfg","ATLAS_ZgQCDscale_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ggF_vbfg","ATLAS_ggF_vbfg_Rel20p7")
    
    correlation.CorrelateParameter("Run2::alpha_bjetTrigEta_combined","ATLAS_bjetTrigEta_combined_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_BTrig_J1_SF_2cen_vbfincl","ATLAS_BTrig_J1_SF_2cen_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_BTrig_J1_SF_4cen_vbfincl","ATLAS_BTrig_J1_SF_4cen_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_BTrig_J2_SF_2cen_vbfincl","ATLAS_BTrig_J2_SF_2cen_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_BTrig_J2_SF_4cen_vbfincl","ATLAS_BTrig_J2_SF_4cen_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_bjetTrigPtSF_vbfg","ATLAS_bjetTrigPtSF_vbfg_Rel20p7")
    
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_Eigen_B_0_70WP_vbfincl","ATLAS_Eigen_B_0_70WP_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_Eigen_B_0_85WP_vbfincl","ATLAS_Eigen_B_0_85WP_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_Eigen_B_1_70WP_vbfincl","ATLAS_Eigen_B_1_70WP_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_Eigen_B_1_85WP_vbfincl","ATLAS_Eigen_B_1_85WP_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_Eigen_B_2_70WP_vbfincl","ATLAS_Eigen_B_2_70WP_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_FT_EFF_Eigen_B_0_vbfg","ATLAS_FT_EFF_Eigen_B_0_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_FT_EFF_Eigen_B_1_vbfg","ATLAS_FT_EFF_Eigen_B_1_vbfg_Rel20p7")
    
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_QG_nchargedME_vbfincl","ATLAS_JET_QG_nchargedME_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_QG_nchargedPDF_vbfincl","ATLAS_JET_QG_nchargedPDF_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_QG_trackEfficiency_vbfincl","ATLAS_JET_QG_trackEfficiency_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_ATLAS_JET_QG_trackFakes_vbfincl","ATLAS_JET_QG_trackFakes_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::alpha_JET_QG_nchargedExp_combined","ATLAS_JET_QG_nchargedExp_combined_Rel20p7")
    
    correlation.CorrelateParameter("Run2::auto_alpha_PhIDeff_vbfg","ATLAS_PhIDeff_vbfg_Rel20p7")
    
    correlation.CorrelateParameter("Run2::auto_sp_2cen_SRII_vbfincl","unconst_sp_2cen_SRII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_sp_2cen_SRI_vbfincl","unconst_sp_2cen_SRI_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_sp_4cen_SRIII_vbfincl","unconst_sp_4cen_SRIII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_sp_4cen_SRII_vbfincl","unconst_sp_4cen_SRII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_sp_4cen_SRIV_vbfincl","unconst_sp_4cen_SRIV_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_sp_4cen_SRI_vbfincl","unconst_sp_4cen_SRI_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::lp1_vbfg","uncons_lp1_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::lp2_vbfg","uncons_lp2_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::mp1_vbfg","uncons_mp1_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::mp2_vbfg","uncons_mp2_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::muZhigh_vbfg","uncons_muZhigh_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::muZlow_vbfg","uncons_muZlow_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::muZmedium_vbfg","uncons_muZmedium_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::nBkghighBDT_vbfg","uncons_nBkghighBDT_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::nBkglowBDT_vbfg","uncons_nBkglowBDT_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::nBkgmediumBDT_vbfg","uncons_nBkgmediumBDT_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::nbkg_SRIII_4cen_vbfincl","uncons_nbkg_SRIII_4cen_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::nbkg_SRII_2cen_vbfincl","uncons_nbkg_SRII_2cen_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::nbkg_SRII_4cen_vbfincl","uncons_nbkg_SRII_4cen_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::nbkg_SRIV_4cen_vbfincl","uncons_nbkg_SRIV_4cen_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::nbkg_SRI_2cen_vbfincl","uncons_nbkg_SRI_2cen_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::nbkg_SRI_4cen_vbfincl","uncons_nbkg_SRI_4cen_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::p1_vbfg","uncons_p1_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::p2_vbfg","uncons_p2_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::sf_z_2cen_SRII_vbfincl","uncons_sf_z_2cen_SRII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::sf_z_2cen_SRI_vbfincl","uncons_sf_z_2cen_SRI_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::sf_z_4cen_SRIII_vbfincl","uncons_sf_z_4cen_SRIII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::sf_z_4cen_SRII_vbfincl","uncons_sf_z_4cen_SRII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::sf_z_4cen_SRIV_vbfincl","uncons_sf_z_4cen_SRIV_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::sf_z_4cen_SRI_vbfincl","uncons_sf_z_4cen_SRI_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a1_2cenSRII_vbfincl","uncons_a1_2cenSRII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a1_2cenSRI_vbfincl","uncons_a1_2cenSRI_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a1_4cenSRIII_vbfincl","uncons_a1_4cenSRIII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a1_4cenSRII_vbfincl","uncons_a1_4cenSRII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a1_4cenSRIV_vbfincl","uncons_a1_4cenSRIV_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a1_4cenSRI_vbfincl","uncons_a1_4cenSRI_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a2_2cenSRII_vbfincl","uncons_a2_2cenSRII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a2_2cenSRI_vbfincl","uncons_a2_2cenSRI_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a2_4cenSRIII_vbfincl","uncons_a2_4cenSRIII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a2_4cenSRII_vbfincl","uncons_a2_4cenSRII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a2_4cenSRIV_vbfincl","uncons_a2_4cenSRIV_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a2_4cenSRI_vbfincl","uncons_a2_4cenSRI_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a3_2cenSRII_vbfincl","uncons_a3_2cenSRII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a3_2cenSRI_vbfincl","uncons_a3_2cenSRI_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a3_4cenSRIII_vbfincl","uncons_a3_4cenSRIII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a3_4cenSRII_vbfincl","uncons_a3_4cenSRII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a3_4cenSRIV_vbfincl","uncons_a3_4cenSRIV_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a3_4cenSRI_vbfincl","uncons_a3_4cenSRI_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a4_2cenSRII_vbfincl","uncons_a4_2cenSRII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a4_2cenSRI_vbfincl","uncons_a4_2cenSRI_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a4_4cenSRIII_vbfincl","uncons_a4_4cenSRIII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a4_4cenSRII_vbfincl","uncons_a4_4cenSRII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a4_4cenSRIV_vbfincl","uncons_a4_4cenSRIV_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a4_4cenSRI_vbfincl","uncons_a4_4cenSRI_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a5_4cenSRII_vbfincl","uncons_a5_4cenSRII_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::a5_4cenSRIV_vbfincl","uncons_a5_4cenSRIV_vbfincl_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_SPUR_HIGHBDT_vbfg","unconst_SPUR_HIGHBDT_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_SPUR_LOWBDT_vbfg","unconst_SPUR_LOWBDT_vbfg_Rel20p7")
    correlation.CorrelateParameter("Run2::auto_alpha_SPUR_MEDIUMBDT_vbfg","unconst_SPUR_MEDIUMBDT_vbfg_Rel20p7")



combined.SetCorrelationScheme(correlation)

combined.CollectMeasurements()
combined.CombineMeasurements()


# Add common signal strength parameter for all signal samples
#signalstrength = ROOT.ParametrisationScheme("signalstrength")
#signalstrength.AddExpression("sigma=expr::XS_prime('@0*1000',sigmatot)")



# Combine the different parametrisations
#parametrisation = ROOT.ParametrisationSequence("parametrisation")
#parametrisation.AddScheme(signalstrength)
#parametrisation.SetParametersOfInterest("sigmatot")

# Carry out the re-parametrisation
#combined.SetParametrisationSequence(parametrisation)
#combined.ParametriseMeasurements()



# Generate Asimov data (NP measured in unconditional fit, generated for mu = 1)
#combined.MakeAsimovData(ROOT.kTRUE, ROOT.CombinedMeasurement.ucmles, ROOT.CombinedMeasurement.nominal)


baseName="workspaces/Run12_VBF_doDecorrelatedPOI_doDecorrelatedNP.root"

# Save the combined workspace
if(args.doDecorrelatedPOI):
	baseName=baseName.replace("_doDecorrelatedPOI", "_decorrlatedPOI")
else:
	baseName=baseName.replace("_doDecorrelatedPOI", "_correlated")

# Save the combined workspace
if(args.doDecorrelatedNP):
	baseName=baseName.replace("_doDecorrelatedNP", "_decorrlatedNP")
else:
	baseName=baseName.replace("_doDecorrelatedNP", "")

combined.writeToFile(baseName)

# Print useful information like the correlation scheme, re-namings, etc.
combined.Print()

