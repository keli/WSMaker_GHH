#include "TInterpreter.h"

#include "../inc/Channel.h"
#include "../inc/Measurement.h"
#include "../inc/CombinedMeasurement.h"
#include "../inc/CorrelationScheme.h"
#include "../inc/ParametrisationScheme.h"
#include "../inc/ParametrisationSequence.h"

using namespace std;

// Main function to run the combination code
int main()
{ 
  gInterpreter->ProcessLineSynch("using namespace RooStats::HistFactory");

  // Define the name of the combined workspace
  std::string OutfileName = "combWS.root";

  // Create a new combined measurement
  CombinedMeasurement* combined = new CombinedMeasurement("combined_master");

  // Add measurement for lvlv 2011 to combined measurement
  // The nominal snapshot to load initally is called "nominalNuis".
  // The workspace "combined" is stored in the file "lvlv2011_125.5.root".
  // "ModelConfig" holds all information about nuisance paramters, PDF, etc.
  // The dataset to be added in the combination is called "obsData".
  Measurement* lvlv2012 = new Measurement("lvlv2012");
  lvlv2012->SetSnapshotName("nominalNuis");
  lvlv2012->SetFileName("lvlv2012_125.5.root");
  lvlv2012->SetWorkspaceName("combined");
  lvlv2012->SetModelConfigName("ModelConfig");
  lvlv2012->SetDataName("obsData");
  combined->AddMeasurement(*lvlv2012);

  // Add measurement for lvlv 2012 to combined measurement
  // The nominal snapshot to load initally is called "nominalNuis".
  // The workspace "combined" is stored in the file "lvlv2012_125.5.root".
  // "ModelConfig" holds all information about nuisance paramters, PDF, etc.
  // The dataset to be added in the combination is called "obsData".
  Measurement* lvlv2011 = new Measurement("lvlv2011");
  lvlv2011->SetSnapshotName("nominalNuis");
  lvlv2011->SetFileName("lvlv2011_125.5.root");
  lvlv2011->SetWorkspaceName("combined");
  lvlv2011->SetModelConfigName("ModelConfig");
  lvlv2011->SetDataName("obsData");
  combined->AddMeasurement(*lvlv2011);

  // Define a correlation schme that should be used when combining the specified
  // measurements. Parameters in the scheme that are not present in the
  // measurements will be ignored. In this example only a few correlations for
  // demonstration purposes are added.
  CorrelationScheme* correlation = new CorrelationScheme("CorrelationScheme");

  // Define parameters of interest for the combined measurement. Only parameters
  // present in the final workspace will be considered.
  correlation->SetParametersOfInterest("mu_XS7_ggF,mu_XS8_ggF,mu_XS7_VBF,mu_XS8_VBF,mu_XS7_WH,mu_XS8_WH,mu_XS7_ZH,mu_XS8_ZH,mu_BR_WW");

  // Correlate the common signal strength, but give it a dummy name, since it is
  // not needed in this example and the parameter can be recovered by using the
  // post-combination re-parametrisation capabilities.
  correlation->CorrelateParameter("lvlv2012::SigXsecOverSM_HWW,lvlv2011::SigXsecOverSM_HWW", "dummy[1.0]");

  // Rename unconstrained parameters, typically scale factors like signal
  // strength for various production modes. The parameters will be renamed and
  // set constant to 1. To make them float, RooFit factory syntax can be used.
  correlation->RenameParameter("lvlv2011", "ATLAS_sampleNorm_ggf125.5", "mu_XS7_ggF[1.0]");
  correlation->RenameParameter("lvlv2011", "ATLAS_sampleNorm_vbf125.5", "mu_XS7_VBF[1.0]");
  correlation->RenameParameter("lvlv2011", "ATLAS_sampleNorm_wh125.5",  "mu_XS7_WH[1.0]");
  correlation->RenameParameter("lvlv2011", "ATLAS_sampleNorm_zh125.5",  "mu_XS7_ZH[1.0]");

  correlation->RenameParameter("lvlv2012", "ATLAS_sampleNorm_ggf125.5", "mu_XS8_ggF[1.0]");
  correlation->RenameParameter("lvlv2012", "ATLAS_sampleNorm_vbf125.5", "mu_XS8_VBF[1.0]");
  correlation->RenameParameter("lvlv2012", "ATLAS_sampleNorm_wh125.5",  "mu_XS8_WH[1.0]");
  correlation->RenameParameter("lvlv2012", "ATLAS_sampleNorm_zh125.5",  "mu_XS8_ZH[1.0]");

  // Correlate an unconstrained parameter and set it constant in the combined
  // workspace.
  correlation->CorrelateParameter("lvlv2011::mu_BR_WW,lvlv2012::mu_BR_WW", "mu_BR_WW[1.0]");

  // Example of renaming a constrained nuisance parameter from from different
  // measurements, without correlating it. By default all parameters will be
  // decorrelated between the different measurements.
  correlation->RenameParameter("lvlv2011", "alpha_ATLAS_BTag_CEFF", "ATLAS_BTag_CEFF_2011");
  correlation->RenameParameter("lvlv2012", "alpha_ATLAS_BTag_CEFF", "ATLAS_BTag_CEFF_2012");

  // Example of correlating constrained nuisance parameter between different
  // measurements. By default all parameters will be decorrelated between the
  // different measurements. The combination tool determines the necessary
  // attributes automatically, but instead constraint term, nuisance
  // parameter and global observable can be specified explicitely as
  // Measurement::ConstraintTerm(NuisanceParameter,GlobalObservable)
  correlation->CorrelateParameter("lvlv2012::alpha_ATLAS_BTag_BEFF,lvlv2011::alpha_ATLAS_BTag_BEFF", "ATLAS_BTag_BEFF");

  // Use the correlation scheme for the combined measurement.
  combined->SetCorrelationScheme(correlation);

  // Run the combination code. First all measurements are regularised, i.e. the
  // structure of the PDF will be unified, parameters and constraint terms renamed
  // according to a common convention, etc. Then a new simultaneous PDF and
  // dataset is build. The combined and regularised measurement is saved to a ROOT
  // file.
  combined->CollectMeasurements();
  combined->CombineMeasurements();
  combined->writeToFile(OutfileName.c_str());

  // Define a sequence of re-parametrisations to demonstrate post-regularisation
  // and combination adjustments such as for example expressing a scale factor as
  // a product of multiple parameters, linear transformations, etc.
  ParametrisationSequence* sequence = new ParametrisationSequence("sequence");

  // Define a simple scheme which introduces again a common signal strength
  // factor, which is multiplied to all individual signal strength factors
  ParametrisationScheme* scheme = new ParametrisationScheme("scheme");
  scheme->AddExpression("mu[1.0,-10.0,10.0]");
  scheme->AddExpression("mu_XS7_ggF=expr::mu_XS7_ggF_prime('@0*@1',mu,mu_XS7_ggF)");
  scheme->AddExpression("mu_XS8_ggF=expr::mu_XS8_ggF_prime('@0*@1',mu,mu_XS8_ggF)");
  scheme->AddExpression("mu_XS7_VBF=expr::mu_XS7_VBF_prime('@0*@1',mu,mu_XS7_VBF)");
  scheme->AddExpression("mu_XS8_VBF=expr::mu_XS8_VBF_prime('@0*@1',mu,mu_XS8_VBF)");
  scheme->AddExpression("mu_XS7_WH=expr::mu_XS7_WH_prime('@0*@1',mu,mu_XS7_WH)");
  scheme->AddExpression("mu_XS8_WH=expr::mu_XS8_WH_prime('@0*@1',mu,mu_XS8_WH)");
  scheme->AddExpression("mu_XS7_ZH=expr::mu_XS7_ZH_prime('@0*@1',mu,mu_XS7_ZH)");
  scheme->AddExpression("mu_XS8_ZH=expr::mu_XS8_ZH_prime('@0*@1',mu,mu_XS8_ZH)");
  sequence->AddScheme(*scheme);

  // Generate Asimov data for the combined workspace. It is possible to generate
  // it condtional and uncoditional.
  combined->MakeAsimovData(kTRUE);

  // Re-parametrise the combined measurement according to the previously defined
  // sequence of schemes.
  combined->SetParametrisationSequence(sequence);
  combined->ParametriseMeasurements();

  // Add snapshots to the combined workspace. For different choices, see the
  // class documentation in the README. Here a nominal snapshot is added, i.e.
  // scale factors are set to the nominal values from the input workspaces and
  // Gaussian constrained parameters are set 0, Poisson terms to 1.
  combined->MakeSnapshots(CombinedMeasurement::nominal);

  // Save the re-parametrised measurement to a new ROOT file for later usage.
  TString tmpOutfileName = OutfileName;
  tmpOutfileName.ReplaceAll(".root", "_parametrised.root");
  combined->writeToFile(tmpOutfileName);

  // Print useful information like the correlation scheme, re-namings, etc.
  combined->Print();

  // Clean up
  delete combined;
  delete lvlv2012;
  delete lvlv2011;

  return 0;
}
