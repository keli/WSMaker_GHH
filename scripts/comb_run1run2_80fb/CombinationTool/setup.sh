# GCC and Python
. "/afs/cern.ch/atlas/project/HSG7/root/root_v6-04-16/x86_64-slc6-gcc49/setup_build_afs.sh"

# Graphviz dependency for TGraphStruct (libGviz.so)
LD_LIBRARY_PATH="/afs/cern.ch/sw/lcg/external/graphviz/2.28.0/x86_64-slc6-gcc48-opt/lib:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH

# ROOT
if [ "x${BASH_ARGV[0]}" != "x" ]; then
  . "/afs/cern.ch/atlas/project/HSG7/root/root_v6-04-16/x86_64-slc6-gcc49/bin/thisroot.sh"
else
  # non-bash Bourne-style shell (eg. zsh) needs another way
  root_oldpwd="`pwd`"
  cd "/afs/cern.ch/atlas/project/HSG7/root/root_v6-04-16/x86_64-slc6-gcc49"
  . "bin/thisroot.sh"
  cd "$root_oldpwd"
  unset root_oldpwd
fi

# increase stack size - needed for large workspaces
ulimit -S -s unlimited
