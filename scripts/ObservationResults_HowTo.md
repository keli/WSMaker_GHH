## Setup:

In case you do not come from [VHbbMiniTutorial.md]{https://gitlab.cern.ch/atlas-physics/higgs/hbb/WSMaker_VHbb/blob/master/VHbbMiniTutorial.md}, the master does not support 2018 analysis anymore, please use tag-00-02-03:
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-physics/higgs/hbb/WSMaker_VHbb.git WSMaker_ICHEP
cd WSMaker_ICHEP
git checkout tags/00-02-03
git submodule update
```

## Official Inputs and HowTo run MVA Fit:

official inputs: **SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2.txt**

1) be sure that in setup.sh you have `IS_BLINDED = 0` then:
`````
source setup.sh
cd build && cmake ..
make -j4
``````

2) Split your inputs with:
``````
SplitInputs -r Run2 -v SMVHVZ_Summer18_MVA_mc16ad_v07_fixed2
``````

3) run `launch_default_jobs.py` with the following options:

```````
version="v07_fixed2"
GlobalRun = False
doPostFit = False
doCutBase = False
doDiboson = False

doSTXS = False
doExtra = False

# What to run
createSimpleWorkspace = True
runPulls = True
runBreakdown = True
runRanks = True (just if you need it, it takes time! consider to run on the batch instead.. more instructions later)
runLimits = False
runP0 = True
runToyStudy = False
````````

4) look here for more instructions about ranking and pulls: https://nmorange.web.cern.ch/nmorange/WSMaker/html/HowTo.html

## Run post-fit plots for extra kinematic variables:
0) add the kinematic variables you want to the related inputConfig file and Split the histograms.

1) run `launch_default_jobs.py` with the same settings as before, just changing:
``````
doPostFit = True
...
# What to run
createSimpleWorkspace = True
runPulls = False
runBreakdown = False
runRanks = False
runLimits = False
runP0 = False
runToyStudy = False
````````
2) run the following script:
```````
#!/bin/bash
WS=< name of the MVA WS you made with doPostFit = False >
WS2= < name of the WS of your kinematic variable you made with doPostFit = True >
python WSMakerCore/scripts/doPlotFromWS.py -m 125 -p 2 -f ${WS} ${WS2}
```````
the results will be in the plot folder of your WS2.

# Specific instructions for MVA plots:

- To put an arrow in the ratio plot of ptv variable for 1 lep 2 jets W+HF CR go in this file `WSMakerCore/scripts/plotMaker.py` uncomment these lines:
``````
#props = self.properties                                                       
#if ((props["L"] == "1") and (props["J"] == "2") and (props["D"] == "WhfCR") and (props["dist"] == "pTV") and (props['BMin'] == "150")):
``````
	#final_range = 0.5


- To remove the yellow band in the ranking plot, go in this file `WSMakerCore/macros/drawPlot_pulls.C` and put false the boolean variable drawPrefitImpactBand (bool drawPrefitImpactBand  = false;)


#  MVA_VZ (mvadiboson) analysis:
1) you follow all the MVA steps just changing:
``````
doDiboson = True
````
remember to add `mvadiboson` to the list of variables to split in your inputConfig file before splitting the inputs.

2) **IMPORTANT**: for postfit plots you need to modify by hand `scripts/analysisPlottingConfig.py` putting:
``````
vh_fit=False
``````
3) To produce the SoB plot for diboson, uncomment the lines that begins with `###For VZ` in `WSMakerCore/scripts/plotSoB.py`.


#  Cutbased (CBA) analysis:
Official inputs: **SMVHVZ_Summer18_CUT_mc16ad_v07_fixed2.txt**

1) run as for MVA fit just changing:
``````
doCutBase = True
````


# How to run on the batch:
run with:
``````
run_on_batch = True
`````
then the results are written to: `/afs/cern.ch/work/analysis/statistics/batch`.

and you can collect them with:
`````
python WSMakerCore/scripts/getResults.py get --ranking --fccs --NPs --workspaces --breakdown --plots --logs wsName
`````
then they will be written to your ouput directory.

everything else from there is the same, except the ranking, here you do:
``````
python WSMakerCore/scripts/makeNPrankPlots.py   wsname[=...job10of10...] >  log
````
if you have jobs wsname[=...job0of10...]  to wsname[=...job9of10...]


- To make the stitched ptZ plots for the 2-lepton (Figure 2b) , we need to use the '-s' option when make the plots:
python WSMakerCore/scripts/doPlotFromWS.py -s -p 2 -f $FCC SWS
FCC is FitCrossChecks folder present in the output directory from the VH MVA analysis
WS is the workspace created with the variable ptV in 2 leptons

- To make the "big"-money plot (Fig. 10a)  change in file scripts/analysisPlottingConfig.py the flag:
 moneyPlot = False --> True
Should be sufficent.
Note: I think this can be used to make the "small"-money plot (Figure 10b) --> Needs confirmation

- To make the cut based mbb plots (Figures 7, 8 and 9 ) and to get correct fccs:
run with:
``````
doCutBase = True
doCutBasePlots = True
`````
Then one needs to change the file scripts/analysisPlottingConfig.py:
* set the flag vh_cba to True, the mu value will then get fixed to 1.06

- To make the PR plot (Figures 4 and 10b)
run with:
``````
doCutBasePlots = True
do_mbb_plot = True
`````
Then one needs to change the file scripts/analysisPlottingConfig.py:
* set the flag vh_cba to True, the mu value will then get fixed to 1.06
