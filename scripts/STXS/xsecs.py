"""
Script to generate a nice plot of fitted STXS values. 

Author: Philipp Windischhofer
Date:   November 2019
Email:  philipp.windischhofer@cern.ch

usage: xsecs.py [-h] [--bd_indir BD_INDIR] [--outfile OUTFILE]

visualise STXS signal strenghts

optional arguments:
  -h, --help           show this help message and exit
  --bd_indir BD_INDIR  path to directory containing the uncertainty breakdown,
                       usually in 'plots/breakdown'
  --outfile OUTFILE    path to generated plot
  --year YEAR           data-taking periods used to make this plot, following
                        WSMaker convention. Default: 6051, i.e. full Run 2
"""

from argparse import ArgumentParser
import glob, os
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

from BreakdownParser import BreakdownParser
from STXSPlotter import STXSPlotter

from analysisPlottingConfig import Config

def drawSignalStrenghts(plotdata_WH, plotdata_ZH, outfile, inner_label = "", legend_header = ""):
    # unpack all the information and pass it to the general-purpose plotting routine
    def unpack(plotdata):
        labels = [cur["label"] for cur in plotdata]
        central_values = [cur["central_value"] * cur["xsec"] for cur in plotdata]
        total_unc_hi = [cur["total_unc"]["unc_hi"] * cur["xsec"] if "total_unc" in cur else 0.0 for cur in plotdata]
        total_unc_lo = [cur["total_unc"]["unc_lo"] * cur["xsec"] if "total_unc" in cur else 0.0 for cur in plotdata]
        stat_unc_hi = [cur["stat_unc"]["unc_hi"] * cur["xsec"] if "stat_unc" in cur else 0.0 for cur in plotdata]
        stat_unc_lo = [cur["stat_unc"]["unc_lo"] * cur["xsec"] if "stat_unc" in cur else 0.0  for cur in plotdata]
        theory_values = [cur["xsec"] for cur in plotdata]
        theory_unc = [cur["xsec"] * cur["xsec_rel_err"] for cur in plotdata]

        return {"labels": labels, "central_values": central_values, "total_unc_hi": total_unc_hi,
                "total_unc_lo": total_unc_lo, "stat_unc_hi": stat_unc_hi, "stat_unc_lo": stat_unc_lo,
                "theory_values": theory_values, "theory_unc": theory_unc}

    plotdata_WH = unpack(plotdata_WH)
    plotdata_ZH = unpack(plotdata_ZH)

    STXSPlotter.plot_STXS_xsec(plotdata_WH, plotdata_ZH, outfile, inner_label = inner_label, legend_header = legend_header)

def PlotSignalStrenghts(bd_indir, outfile, year):
    # fit parameters to extract and show
    pars_WH = [("SigXsecOverSMWHx150x250PTV", "150 < p_{T}^{W,t} < 250 GeV"),
               ("SigXsecOverSMWHxGT250PTV", "p_{T}^{W,t} > 250 GeV")]

    pars_ZH = [("SigXsecOverSMZHx75x150PTV", "75 < p_{T}^{Z,t} < 150 GeV"),
               ("SigXsecOverSMZHx150x250PTV", "150 < p_{T}^{Z,t} < 250 GeV"),
               ("SigXsecOverSMZHxGT250PTV", "p_{T}^{Z,t} > 250 GeV")]

    theory_input = {"SigXsecOverSMWHx150x250PTV": {"xsec": 24.00, "xsec_rel_err": 0.0442},
                    "SigXsecOverSMWHxGT250PTV": {"xsec": 7.08, "xsec_rel_err": 0.0476},
                    "SigXsecOverSMZHx75x150PTV": {"xsec": 50.61, "xsec_rel_err": 0.0808},
                    "SigXsecOverSMZHx150x250PTV": {"xsec": 18.80, "xsec_rel_err": 0.1262},
                    "SigXsecOverSMZHxGT250PTV": {"xsec": 4.85, "xsec_rel_err": 0.1025},
    }

    # load the results from the uncertainty breakdown
    bd_files = glob.glob(os.path.join(bd_indir, "*.txt"))

    print("have the following uncertainty breakdowns available:")
    print("\n".join(bd_files))

    unc_bds = {}
    for bd_file in bd_files:
        cur_POI = BreakdownParser.get_POI_name(bd_file)
        cur_POI_value = BreakdownParser.get_POI_value(bd_file)
        cur_stat_unc = BreakdownParser.get_unc(bd_file, "DataStat")
        cur_total_unc = BreakdownParser.get_unc(bd_file, "Total")
        unc_bds[cur_POI] = {"stat_unc": cur_stat_unc, "total_unc": cur_total_unc, "central_value": cur_POI_value}

    # assemble the complete plotting information
    plotdata_WH = []
    plotdata_ZH = []

    for par_name, par_label in pars_WH:
        parval = unc_bds[par_name]
        parval['label'] = par_label
        if par_name in theory_input:
            parval.update(theory_input[par_name])
        plotdata_WH.append(parval)

    for par_name, par_label in pars_ZH:
        parval = unc_bds[par_name]
        parval['label'] = par_label
        if par_name in theory_input:
            parval.update(theory_input[par_name])
        plotdata_ZH.append(parval)

    print("Using the following WH fit result for plotting")
    print(plotdata_WH)

    print("Using the following ZH fit result for plotting")
    print(plotdata_ZH)

    plotconf = Config([])
    plotconf._year = year
    runinfo = plotconf.get_run_info()
    lumi, energy = runinfo.values()[0]

    drawSignalStrenghts(plotdata_WH, plotdata_ZH, outfile, inner_label = "#sqrt{{s}}={} TeV, {} fb^{{-1}}".format(energy, lumi),
                        legend_header = "VH, H #rightarrow bb, V #rightarrow leptons cross-sections:")

if __name__ == "__main__":
    ROOT.gROOT.SetBatch(True)

    parser = ArgumentParser(description = "visualise STXS signal strenghts")
    parser.add_argument("--bd_indir", action = "store", dest = "bd_indir", help = "path to directory containing the uncertainty breakdown, usually in 'plots/breakdown'")
    parser.add_argument("--outfile", action = "store", dest = "outfile", help = "path to generated plot")
    parser.add_argument("--year", action = "store", dest = "year", default = "6051", help = "data-taking periods used to make this plot, following WSMaker convention. Default: 6051, i.e. full Run 2")
    args = vars(parser.parse_args())

    PlotSignalStrenghts(**args)
