"""
Script to generate a nice plot of the fitted signal strenghts in a multi-POI fit. 

Author: Philipp Windischhofer
Date:   November 2019
Email:  philipp.windischhofer@cern.ch

usage: signalstrengths.py [-h] [--bd_indir BD_INDIR] [--outfile OUTFILE]

generate simpleMu plot

optional arguments:
  -h, --help           show this help message and exit
  --bd_indir BD_INDIR  path to directory containing the uncertainty breakdown,
                       usually in 'plots/breakdown'
  --outfile OUTFILE    path to generated plot
  --year YEAR           data-taking periods used to make this plot, following
                        WSMaker convention. Default: 6051, i.e. full Run 2
"""

from argparse import ArgumentParser
import glob, os
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

from BreakdownParser import BreakdownParser
from STXSPlotter import STXSPlotter

from analysisPlottingConfig import Config

def PlotSignalStrenghts(bd_indir, outfile, year):
    # define some global settings for the plots
    plot_order = ["SigXsecOverSMWHx150x250PTV", "SigXsecOverSMWHxGT250PTV", "SigXsecOverSMZHx75x150PTV", "SigXsecOverSMZHx150x250PTV", "SigXsecOverSMZHxGT250PTV"]
    plot_order.reverse()

    label_generator = {"SigXsecOverSMWHx150x250PTV": "WH, 150 < p_{T}^{W,t} < 250 GeV",
                       "SigXsecOverSMWHxGT250PTV": "WH, p_{T}^{W,t} > 250 GeV",
                       "SigXsecOverSMZHx75x150PTV": "ZH, 75 < p_{T}^{Z,t} < 150 GeV",
                       "SigXsecOverSMZHx150x250PTV": "ZH, 150 < p_{T}^{Z,t} < 250 GeV",
                       "SigXsecOverSMZHxGT250PTV": "ZH, p_{T}^{Z,t} > 250 GeV"}

    theory_input = {"SigXsecOverSMWHx150x250PTV": {"mu": 1.0, "rel_err": 0.0442},
                    "SigXsecOverSMWHxGT250PTV": {"mu": 1.0, "rel_err": 0.0476},
                    "SigXsecOverSMZHx75x150PTV": {"mu": 1.0, "rel_err": 0.0808},
                    "SigXsecOverSMZHx150x250PTV": {"mu": 1.0, "rel_err": 0.1262},
                    "SigXsecOverSMZHxGT250PTV": {"mu": 1.0, "rel_err": 0.1025}}

    labels = []
    central_values = []
    stat_uncs_hi = []
    stat_uncs_lo = []
    total_uncs_hi = []
    total_uncs_lo = []
    theory_central_values = []
    theory_uncs = []

    # load the results from the uncertainty breakdown
    bd_files = glob.glob(os.path.join(bd_indir, "*.txt"))

    print("have the following uncertainty breakdowns available:")
    print("\n".join(bd_files))

    bd_data = {}
    for bd_file in bd_files:
        cur_POI = BreakdownParser.get_POI_name(bd_file)
        cur_POI_value = BreakdownParser.get_POI_value(bd_file)
        cur_stat_unc = BreakdownParser.get_unc(bd_file, "DataStat")
        cur_total_unc = BreakdownParser.get_unc(bd_file, "Total")
        bd_data[cur_POI] = {'central_value': cur_POI_value, 'stat_unc': cur_stat_unc, 'total_unc': cur_total_unc}

    # unpack everything in the correct order and pass it on to the plotting routine
    for cur_POI in plot_order:
        cur_label = label_generator[cur_POI]
        cur_central_value = bd_data[cur_POI]['central_value']
        cur_stat_unc_hi = bd_data[cur_POI]['stat_unc']['unc_hi']
        cur_stat_unc_lo = bd_data[cur_POI]['stat_unc']['unc_lo']
        cur_total_unc_hi = bd_data[cur_POI]['total_unc']['unc_hi']
        cur_total_unc_lo = bd_data[cur_POI]['total_unc']['unc_lo']
        cur_theory_central_value = theory_input[cur_POI]['mu']
        cur_theory_unc = theory_input[cur_POI]['rel_err'] * theory_input[cur_POI]['mu']

        labels.append(cur_label)
        central_values.append(cur_central_value)
        stat_uncs_hi.append(cur_stat_unc_hi)
        stat_uncs_lo.append(cur_stat_unc_lo)
        total_uncs_hi.append(cur_total_unc_hi)
        total_uncs_lo.append(cur_total_unc_lo)
        theory_central_values.append(cur_theory_central_value)
        theory_uncs.append(cur_theory_unc)

    plotdata = {'central_values': central_values, 'stat_uncs_hi': stat_uncs_hi, 'stat_uncs_lo': stat_uncs_lo, 'total_uncs_lo': total_uncs_lo, 'total_uncs_hi': total_uncs_hi, 'theory_central_values': theory_central_values, 'theory_uncs': theory_uncs, 'labels': labels}

    plotconf = Config([])
    plotconf._year = year
    runinfo = plotconf.get_run_info()
    lumi, energy = runinfo.values()[0]

    STXSPlotter.plot_mu(plotdata, outfile, xlabel = "#sigma #times #it{B} normalised to SM", ylabel = "", inner_label = "VH, H #rightarrow bb    #sqrt{{s}}={} TeV, {} fb^{{-1}}".format(energy, lumi))

if __name__ == "__main__":
    ROOT.gROOT.SetBatch(True)

    parser = ArgumentParser(description = "generate simpleMu plot")
    parser.add_argument("--bd_indir", action = "store", dest = "bd_indir", help = "path to directory containing the uncertainty breakdown, usually in 'plots/breakdown'")
    parser.add_argument("--outfile", action = "store", dest = "outfile", help = "path to generated plot")
    parser.add_argument("--year", action = "store", dest = "year", default = "6051", help = "data-taking periods used to make this plot, following WSMaker convention. Default: 6051, i.e. full Run 2")
    args = vars(parser.parse_args())

    PlotSignalStrenghts(**args)
