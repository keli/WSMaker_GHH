#!/usr/bin/env python

import sys
import os
import AnalysisMgr_GHHRun2 as Mgr
import BatchMgr as Batch
import time,copy

# Inputs
#InputVersion = "SMVHVZ_LHCP17_MVA_v06"
#InputVersion = "SMVH_STXS_v07" # download it from /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/ICHEP2016/STXS_Split/
#version="v07_fixed2" #"v07_fixed2_STXS.fixVHUE" #"v07_fixed2_STXS"
version = "GHH_2l_0629"
GlobalRun = False # caution: when true, it will change the following settings based on the sys.argv[1]
doPostFit = False # needed when produce the post-fit plots for the variables used in BDT training, if you don't know it, please turn it off

doCutBase = True
doCutBasePlots = False # switch to get convergence on the fit for plots
doDiboson = False
Add1LMedium = False
mrgPtV = False # switch to remove split at 250 GeV # if r32-07+ require "hadd *_150ptv_* *_150_250ptv_* *_250ptv_*" on splitinputs
doTTbarDataDriven2L = False
do_mbb_plot = False
dopTVin3bins=False
PCBTInputs = False# turn on when running inputs produced in PCBT mode

# Options for STXS, for the default run, keep doSTXS as False
# If you don't know ask Changqiao/Milene/Giulia/Ilaria/Philipp
doSTXS = False
### used STXS cases : 1=1POI; 2=WH/ZH; 3=5POI; 5=3POI; 9=6POI
FitSTXS_Scheme = 1
### switch to true for XS measurement - currently impl for mode 3 and 5
doXSWS = True

doICHEPStyle = False
doNewRegions = False

# Options to postprocess the STXS workspace and re-express the signal strengths
# in terms of Wilson coefficients of dim-6 operators
doEFT = False # main switch to enable EFT-reinterpretations. See further below for more detailed configuration options.

# Options for HL-LHC, for the default run, keep doExtra as False
# If you don't know ask Changqiao
doExtra = False
Extra_MCStatErrSF = 0.0
Extra_Lumi = 3000.0


# Channels
# channels = ["0", "1", "2", "012"]
channels = ["012"]
# MCTypes = ["mc16ade","mc16e","mc16ad","mc16d","mc16a"]
MCTypes = ["mc16ade"]

# Syst or not
syst_type = [ "MCStat" ] # "FloatOnly", "StatOnly", "Systs", "MCStat"]

#Blinding / injection
signalInjection = 0 # DEFAULT 0: no injection
doExp = "1" # "0" to run observed, "1" to run expected only

# How to run
run_on_batch = False

# What to run
createSimpleWorkspace = True
runPulls = False
runBreakdown = False
runRanks = False
runLimits = False
runP0 = False
runToyStudy = False

# Turn on additional debug plots
doplots = False

# Lesser used flags:
anaType = "CUT" #overwrite for cut-based
masses = [125]
doptvbin = True if doCutBase and not doNewRegions else False # decided w.r.t docutbase
if not doPostFit:
  if doCutBase:
    vs2tag = ['mV']
    anaType = "CUT"
#else:
#  vs2tag = ['pTV','MET','pTB1','pTB2','mBB','dRBB','dEtaBB','dPhiVBB','dEtaVBB','MEff','MEff3','dPhiLBmin','mTW','mLL','dYWH','Mtop','pTJ3','mBBJ','mBBJ3','METSig']
#  # vs2tag = ['mBB']
#  if ("2" in channels or "012" in channels) and not doCutBase:
#    vs2tag.append('mva')
#  runPulls = False
#  runBreakdown = False
#  runRanks = False
#  runLimits = False
#  runP0 = False
#  runToyStudy = False
#  doplots = False
vs1tag = ['dRBB']
use1tag = False
runRankingOnPostfitAsimov = False
do_special_2year_fit = False

if GlobalRun:
  if sys.argv[1].endswith('.01'):
    print( 'run set 1')
    doExp = "1"
    runPulls = True
    runBreakdown = True
    runRanks = True
    runLimits = False
    runP0 = True
    doplots = True
    runRankingOnPostfitAsimov = True
  elif sys.argv[1].endswith('.02'):
    print( 'run set 2')
    doExp = "0"
    runPulls = False
    runBreakdown = False
    runRanks = False
    runLimits = False
    runP0 = True
    doplots = False
  elif sys.argv[1].endswith('.03'):
    print( 'run set 3')
    doExp = "1"
    runPulls = False
    runBreakdown = False
    runRanks = True
    runLimits = False
    runP0 = False
    doplots = False
    runRankingOnPostfitAsimov = False
  elif sys.argv[1].endswith('.04'):
    print( 'run set 4')
    doExp = "0"
    runPulls = True
    runBreakdown = True
    runRanks = True
    runLimits = False
    runP0 = True
    doplots = False
    runRankingOnPostfitAsimov = False

#the argument for ConfigFlags; delimited and camel-cased for readability (cf. scripts/AnalysisMgr.py, parse_config_flags for full options):
baseline_configs = {'Fitss2l':True,'Fitos2l':False,"Fit3ll":False}
#baseline_configs = {'UseJet21NP':True,'CutBase':False,"CutBasePostfit":False,"UseGaussianConstraintTypeMCStat":True,'DoDiboson':False,'FitWHZH':False,
#                    'FitVHVZ':False,'DoSystsPlots':False,"DoShapePlots":False,
#                    'StatThreshold':0.0, 'PlotsFormat':"png",
#                    'CreateROOTfiles':False,'PostFit':False,"DefaultFitConfig":False,
#                    'FitWZZZ':False, 'Add1LMedium':False, 'PCBTInputs':False,
#                    'DataDrivenTTbar2L': False, 'MbbPlot':False
#                    #'DecorrPOI':'L',
#                    }

# --------------------------------------
# configuration for EFT fits
# --------------------------------------
# paths to individual parametrisation files, each of which represents a polynomial in the new POIs (expected to be found in "inputs/")
polynomial_defs = {"production": "param_order_1.xml"}

# the final expression that will be used to reparametrise each STXS bin, can involve any combination of the polynomials defined above
param_expr = "production"

# list of Wilson coefficients that should be simultaneously active (and will therefore
# get simultaneously constrained by the fit). The parameters set here will end up as the
# "POIs" of the resulting workspace.
active_POIs = ["cHq3"]

POI_ranges = [0.2] # range in which the the PNLL scans should be carried out.
                   # e.g. here, cHq3 is swept over the symmetric interval [-0.2, 0.2]

# STXS bins to which the parametrisation should be applied
active_bins = ["SigXsecOverSMWHx150x250PTV", "SigXsecOverSMWHxGT250PTV",
               "SigXsecOverSMQQZHx75x150PTV", "SigXsecOverSMQQZHx150x250PTV", 
               "SigXsecOverSMQQZHxGT250PTV"]


# - - - - - - - - - - - - - - - - - - -
# configuration for fitting eigenoperators
# - - - - - - - - - - - - - - - - - - -
doEigenoperators = False

# path to the file defining the eigenoperators
eigenoperator_defs = "eigenop_defs.xml"

# list of eigenoperators to be fit simultaneously (for N STXS bins, usually the
# top N eigenoperators)
active_eigenoperators = ["E0", "E1", "E2", "E3", "E4"]

# numerical ranges of the corresponding coefficients
eigen_POI_ranges = [40.0, 40.0, 40.0, 40.0, 40.0]

# if asked to fit eigenoperators, need to enable all
# operators that are available in the parametrisation
if doEigenoperators:
  print("enabled eigenoperator fitting")

  import xml.etree.ElementTree as ET
  import itertools
  active_POIs = list(set(itertools.chain.from_iterable([ET.parse(os.path.join("inputs", cur_file)).getroot().findall("operators")[0].get("operators").split(',') for cur_file in polynomial_defs.values()])))
  POI_ranges = [40.0 for POI in active_POIs]

  print("active_POIs reset to: {}".format(active_POIs))
  print("POI_ranges reset to: {}".format(POI_ranges))
# - - - - - - - - - - - - - - - - - - -

# whether or not to generate scans of the resulting NLL
runLikelihoodLandscape = False

# whether or not to generate plots of these scans
runLikelihoodPlots = False

# collates all EFT-related configuration settings
eft_config = {"doEFT": doEFT, "polynomialNames": ",".join(polynomial_defs.keys()), "polynomialDefs": ",".join(polynomial_defs.values()),
              "paramExpr": param_expr, "activePOIs": ",".join(active_POIs), "POIRanges": ",".join(map(str, POI_ranges)),
              "activeBins": ",".join(active_bins), "doEigenoperators": doEigenoperators,
              "EigenoperatorDefsFile": eigenoperator_defs, "activeEigenoperators": ",".join(active_eigenoperators),
              "EigenPOIRanges": ",".join(map(str, eigen_POI_ranges))}

baseline_configs.update(eft_config)
# --------------------------------------

baseline_configs.update({"PruneAfterMerge":True})
baseline_configs.update({"SmoothAfterMerge":True})
if doCutBase:
    baseline_configs.update({"CutBase":True})
    if doCutBasePlots:
        baseline_configs.update({"CutBasePostfit":True})
        baseline_configs.update({"UseGaussianConstraintTypeMCStat":True}) # Used for the convergence of the hessian matrix
if doDiboson:
    baseline_configs.update({"DoDiboson":True})
if doPostFit:
    baseline_configs.update({"PostFit":True})
    baseline_configs.update({"DefaultFitConfig":True})
if Add1LMedium:
    baseline_configs.update({"Add1LMedium":True})
if PCBTInputs:
    baseline_configs.update({"PCBTInputs":True}) 
if doTTbarDataDriven2L:
    baseline_configs.update({"DataDrivenTTbar2L":True})
if do_mbb_plot:
    baseline_configs.update({"MbbPlot":True})
if do_special_2year_fit:
    baseline_configs.update({"FitMC16aMC16d":True})
    MCTypes = ["XXXX"]
if doSTXS:
    baseline_configs.update({"DoSTXS":True})
    baseline_configs.update({"FitSTXSScheme":FitSTXS_Scheme})
if doXSWS:
    baseline_configs.update({"DoXSWS":True})
if doExtra:
    baseline_configs.update({"DoExtrapolation":True})
    baseline_configs.update({"ExtrapolationMCStatErrSF":Extra_MCStatErrSF})
    baseline_configs.update({"ExtrapolationLuminosity":Extra_Lumi})
if doNewRegions:
    baseline_configs.update({"NewRegions":True})
if doICHEPStyle:
    baseline_configs.update({"ICHEPStyle":True})
if not dopTVin3bins:
    baseline_configs.update({"pTVin3bins":False})


baseline_configs_SimpleWS = copy.deepcopy(baseline_configs)
if createSimpleWorkspace and doplots:
    baseline_configs_SimpleWS.update({'DoSystsPlots':True,"DoShapePlots":True, 'SmoothingControlPlots':True, "ShowMCStatBandInSysPlot": True})


def add_config(config, mass, syst='Systs', channel='0', var2tag='mBB', var1tag='mBB',
        use1tag=False, doptvbin=True, doPostFit=False, mctype="mc16ad", newregions=False):
    config.update({'InputVersion':InputVersion,'MassPoint':mass,'DoInjection':signalInjection,'SkipUnkownSysts':True})
    conf = Mgr.WorkspaceConfig_VHbbRun2(syst, **config)

    localmctype = [mctype]
    if mctype == "XXXX":
        localmctype = ["mc16a", "mc16d"]

    for mc in localmctype:
        if "0" in channel:
            conf.append_regions(var2tag, var1tag, True, False, False,
                                use1tag, False, doptvbin, mrgPtV, doPostFit, mctype=mc, newregions=newregions)
        if "1" in channel:
            conf.append_regions(var2tag, var1tag, False, True, False,
                                use1tag, False, doptvbin, mrgPtV, doPostFit, Add1LMedium, mctype=mc, newregions=newregions)
        if "2" in channel:
            conf.append_regions(var2tag, var1tag, False, False, True,
                                use1tag, False, doptvbin, mrgPtV, doPostFit, False, mctype=mc, newregions=newregions)

    # NM 18-05-04: simplify naming for the most common case. Can revert if we re-study 1tag, other masses... at some point
    fullversion = "VHbb_" + outversion + "_" + channel + "_" + mctype + "_" + syst + "_" + var2tag
    if doSTXS:
      stxs_info = '_STXS' + '_FitScheme_%d'%FitSTXS_Scheme + ( '_DOXS' if doXSWS else '')
      fullversion = fullversion + stxs_info
    if doExtra:
      #Extrapolation_info = '_Extra' + '_MCStatErrSF_%.1f'%Extra_MCStatErrSF + '_Lumi_%.1f'%Extra_Lumi
      Extrapolation_info = '_Extra' + '_MCStatErrSF_%f'%Extra_MCStatErrSF + '_Lumi_%f'%Extra_Lumi
      fullversion = fullversion + Extrapolation_info
    #if (var2tag is not var1tag) and (use1tag is True) :
    #    fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + syst + "_" + "use1tag" + str(use1tag) + "_" + var2tag + "2tag_" + var1tag + "1tag"
    #else:
    #    fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + syst + "_" + "use1tag" + str(use1tag) + "_" + var2tag

    return conf.write_configs(fullversion)


if __name__ == "__main__":

    if len(sys.argv) is not 2:
        print( "Usage: launch_default_jobs.py <outversion>")
        exit(0)

    outversion = sys.argv[1]
    """
    print "WARNING: if you haven't touched the base configuration, prepare to"
    print "see your lxbatch quota die quickly"

    print "I'll give you 5 seconds to abort..."
    time.sleep(5)
    """

    print( "Adding config files...")

    print( "Let's start by a few jobs where we run simple fits to get pulls")
    print( "and other things when they are ready")
    configfiles = []
    configfiles_simpleWS = []
    if signalInjection is not 0:
        doExp="1"

    for sys in syst_type:
        for c in channels:
            for m in masses:
                for MCType in MCTypes:
                    for var2tag in vs2tag:
                        for var1tag in vs1tag:
                            InputVersion = "SMVHVZ_2019_MVA_"+MCType+"_"+version
                            #InputVersion = "SMVHVZ_2019_"+anaType+"_"+MCType+"_"+version
                            configfiles += add_config(baseline_configs, str(m), syst=sys, channel
                                    = c, var2tag=var2tag, var1tag=var1tag, use1tag=use1tag,
                                    doptvbin=doptvbin, doPostFit=doPostFit, mctype=MCType, newregions=doNewRegions)
                            configfiles_simpleWS += add_config(baseline_configs_SimpleWS, str(m),
                                    syst=sys, channel = c, var2tag=var2tag, var1tag=var1tag,
                                    use1tag=use1tag, doptvbin=doptvbin, doPostFit=doPostFit,
                                    mctype=MCType, newregions=doNewRegions)
    for fi in configfiles:
        print( fi)

    dataForRanking = "obsData"
    if doExp == '1':
        if runRankingOnPostfitAsimov:
            dataForRanking = "asimovData_paramsVals_GlobalFit_mu1.00,conditionalGlobs_1"
        else:
            dataForRanking = "asimovData"

    Blinding = os.getenv("IS_BLINDED")
    print(' You are running with blinding: %s ' % Blinding)

    if run_on_batch:

        if createSimpleWorkspace:
            print( 'creating simple ws named %s' % outversion)
            Batch.run_lxplus_batch(configfiles_simpleWS, outversion+"_simple", ["-w"], '1000')

        # hopefully queue 8nh is enough for now.
        if runPulls:
            if outversion.endswith('.04') and GlobalRun:
                if Blinding == "0":
                    Batch.run_lxplus_batch(configfiles, outversion+"_fullRes",
                                           ["-w", "--fcc", "2,5,7,9@{MassPoint}", "-m", "2,5,7,9", "-p", "0,2@{MassPoint}", "-t", "0,3@{MassPoint}", "-c", "7!-n"],
                                           "5000")  
                                     
                else:
                    print( 'You want to run Global .04 while blinding is applied, please setup the framework correctly')
                    exit(0)
            elif Blinding == "0" :
                if do_mbb_plot:
                    Batch.run_lxplus_batch(configfiles, outversion+"_fullRes",
                                           ["-w", "--fcc", "2,5,7,9@{MassPoint}", "-m", "2,5,7,9", "-p", "0,2@{MassPoint}&", "-t", "0,2@{MassPoint}", "-c", "7!-n"],
                                           "5000")
                else:
                    Batch.run_lxplus_batch(configfiles, outversion+"_fullRes",
                                           ["-w", "--fcc", "2,5,7,9@{MassPoint}", "-m", "2,5,7,9", "-p", "0,2@{MassPoint}", "-t", "0,2@{MassPoint}", "-c", "7!-n"],
                                           "5000")  
            elif do_mbb_plot:
                Batch.run_lxplus_batch(configfiles, outversion+"_fullRes",
                                       ["-w", "--fcc", "5,7,9@{MassPoint}", "-m", "5,7,9", "-p", "0,3@{MassPoint}&", "-t", "0,3@{MassPoint}", "-c", "7!-n"],
                                       "5000")
            else:
                Batch.run_lxplus_batch(configfiles, outversion+"_fullRes",
                                       ["-w", "--fcc", "5,7,9@{MassPoint}", "-m", "5,7,9", "-p", "0,3@{MassPoint}", "-t", "0,3@{MassPoint}", "-c", "7!-n"],
                                       "5000")

        if doEFT and runLikelihoodLandscape:
          if Blinding == "1":
            cmd = ["-w", "--nll_landscape", "Asimov@{MassPoint}"]
          else:
            cmd = ["-w", "--nll_landscape", "data,Asimov@{MassPoint}"]

          if runLikelihoodPlots:
            cmd.extend(["--nll_plots"])
            
          Batch.run_lxplus_batch(configfiles, outversion + "_fullRes",
                                 cmd, "10000")

        if runBreakdown:
            if Blinding == "0" or (Blinding == "1" and doExp == "1") :
                Batch.run_lxplus_batch(configfiles, outversion+"_breakdown",
                                   ["-w", "-u", doExp+",{MassPoint},11"],
                                   "5000")

            else:
                print( 'You want to run Breakdown and fit to data while blinding , please setup the framework correctly')
                exit(0)
        if runLimits:
            Batch.run_lxplus_batch(configfiles, outversion+"_limits",
                                   ["-w", "-l", doExp+",{MassPoint}"],
                                   "10000")
        if runP0:
            Batch.run_lxplus_batch(configfiles, outversion+"_p0",
                                   ["-w", "-s", doExp+",{MassPoint}"],
                                   "2000")
        if runRanks:
            # create workspaces locally
            commands = ["-w"]
            if doExp == "1" and runRankingOnPostfitAsimov:
                commands.extend(["--fcc", "5s@{MassPoint}"])
            #Batch.run_local_batch(configfiles, outversion+"_ranking", commands)
            # Then run the rankings on batch
            #Batch.run_slurm_batch(configs, out+"_fullRes",cmd_opts,jobtype='_rank',mem='16384',jobs=10)
            if (doSTXS and FitSTXS_Scheme==3):
              Batch.run_lxplus_batch(configfiles, outversion+"_ranking",['-w', '-r','{MassPoint},'+dataForRanking],jobs=100, timelimit="72000")
            else:
              Batch.run_lxplus_batch(configfiles, outversion+"_ranking",['-w', '-r','{MassPoint},'+dataForRanking],jobs=10, timelimit="10000" )
        if runToyStudy:
            # create workspaces locally
            Batch.run_local_batch(configfiles, outversion+"_toys", ["-w"])
            # Then run the toys on batch
            Batch.run_lxplus_batch(configfiles, outversion+"_toys",['-w', '-k', "jobs", "--fcc", "6"],
                                   jobs=50, timelimit="72000")

    else: # local running
        commands = ["-w"]
        if runPulls:
            if outversion.endswith('.04') and GlobalRun:
                if Blinding == "0":
                    commands.extend(["--fcc", "2,5,7,9@{MassPoint}", "-m", "2,5,7,9", "-p", "0,2@{MassPoint}", "-t", "0,3@{MassPoint}", "-c", "7!-n"])
                else:
                    print( 'You want to run Global .04 while blinding is applied, please setup the framework correctly')
                    exit(0)
            elif Blinding == "0" :
                if do_mbb_plot :
                    commands.extend(["--fcc", "2,5,7,9@{MassPoint}", "-m", "2,5,7,9", "-p", "0,2@{MassPoint}&", "-t", "0,2@{MassPoint}", "-c", "7!-n"])
                elif doEFT:
                    commands.extend(["--fcc", "2,8@{MassPoint}", "-m", "2,8", "-p", "0,2@{MassPoint}", "-t", "0,2@{MassPoint}", "-c", "7!-n"])
                else :
                    commands.extend(["--fcc", "2,5,7,9@{MassPoint}", "-m", "2,5,7,9", "-p", "0,2@{MassPoint}", "-t", "0,2@{MassPoint}", "-c", "7!-n"])
            elif do_mbb_plot:
                commands.extend(["--fcc", "5,7,9@{MassPoint}", "-m", "5,7,9", "-p", "0,3@{MassPoint}&", "-t", "0,2@{MassPoint}", "-c", "7!-n"])
            else:
              if doEFT:
                commands.extend(["--fcc", "2,8@{MassPoint}", "-m", "2,8", "-p", "0,2@{MassPoint}", "-t", "0,2@{MassPoint}", "-c", "7!-n"])
              else:
                commands.extend(["--fcc", "7,5,9@{MassPoint}", "-m", "7,5,9", "-p", "0,3@{MassPoint}", "-t", "0,2@{MassPoint}", "-c", "7!-n"])
                #commands.extend(["-p", "0@{MassPoint}"])
        if doEFT and runLikelihoodLandscape:
          if Blinding == "1":
            commands.extend(["--nll_landscape", "Asimov@{MassPoint}"])
          else:
            commands.extend(["--nll_landscape", "data,Asimov@{MassPoint}"])
        if doEFT and runLikelihoodPlots:
                commands.extend(["--nll_plots"])
        if runLimits or runBreakdown :
                commands.extend(["-l", doExp+",{MassPoint}"])
        if runBreakdown:
            if Blinding == "0" or (Blinding == "1" and doExp == "1") :
                commands.extend(["-u", doExp+",{MassPoint},11"])
            else:
                print( 'You want to run breakdown and fit to data while blinding , please setup the framework correctly')
                exit(0)
        if runP0:
            commands.extend(["-s", doExp+",{MassPoint}"])
        if runRanks:
            if Blinding == "0" or (Blinding == "1" and doExp == "1") :
                commands.extend(['-r','{MassPoint},'+dataForRanking, '-n', '{MassPoint}'])
            else:
                print( 'You want to run Ranks and fit to data while blinding , please setup the framework correctly')
                exit(0)
        if runToyStudy:
            commands.extend(["--fcc", "6@{MassPoint}"])
        Batch.run_local_batch(configfiles_simpleWS, outversion+"_fullRes", commands)
