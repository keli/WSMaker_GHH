#!/usr/bin/env python

# FOR BOOSTED ANALYSIS

import os
import sys
import subprocess
import utility as U

def main(var="BDT",reg="SR",version="None") :
  
  channels  = ["TwoLepton"]
  #nTag_list = ["0tag", "1tag", "2tag"]
  ptv_list  = ["0_75ptv", "75_150ptv", "0_150ptv", "150ptv", "150_200ptv", "200ptv"]
  nTag_list = ["2tag"]
  #ptv_list  = ["0_75ptv", "75_150ptv", "150ptv",]
  
  # high+low mBB cr
  print "variable :",var
  print "regions  :",reg

  for nChan in channels : 

    for nTag in nTag_list : 

      for ptv in ptv_list : 
     
        print "Merging 4+ jet regions : for %s, %s, %s %s" % (nChan, nTag, ptv, reg)
       
        filename1  = "13TeV_%s_%s3jet_%s_%s_%s.root"  % (nChan, nTag, ptv, reg, var)
        filename2  = "13TeV_%s_%s4pjet_%s_%s_%s.root" % (nChan, nTag, ptv, reg, var)
        targetname = "13TeV_%s_%s3pjet_%s_%s_%s.root" % (nChan, nTag, ptv, reg, var)
        if version is not 'None':
          filename1  = 'inputs/'+version+'/'+filename1
          filename2  = 'inputs/'+version+'/'+filename2
          targetname = 'inputs/'+version+'/'+targetname
     
        if os.path.isfile(filename1) and os.path.isfile(filename2):
          if os.path.isfile(targetname):
            U.Warn("%s already exists!"%targetname)
          else :
            os.system("hadd "+targetname+" "+filename1+" "+filename2)
        else :
          U.Error("Some files %s and %s are missing..." % (filename1, filename2))

if __name__ == "__main__":
  if len(sys.argv) is not 2:
    print "You are now running the Mergenjetregions.py for the current inputs"
    print "TO run for the inputs in inputs/version"
    print "Usage: Mergenjetregions.py <version>"
    version = 'None'
  else:
    version = sys.argv[1]
    
  
  #var_list = [ "mva", "mvadiboson", "mBB", "mBBMVA", "pTV"]
  var_list = [ "mva", "mvadiboson", "mBB", "mBBMVA", "pTV", "MET", "pTB1", "pTB2", "dRBB", "dPhiVBB", "dEtaVBB", "mLL", "pTJ3", "mBBJ3" ]
  regions  = [ "SR", "topemucr" ]
  for var in var_list : 
      for reg in regions :
          main(var,reg,version)
