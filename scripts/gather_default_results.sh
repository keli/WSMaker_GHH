#!/bin/bash

# $1: inputs version
# $2: output tag from launch_default_results command line

mass=125
version=$1.$2
versionTxt=$1_$2
cd /tmp/$USER
mkdir results_$versionTxt
finalResDir=results_$versionTxt
mkdir -vp $finalResDir/limits
mkdir -vp $finalResDir/pullComparisons
mkdir -vp $finalResDir/plots

mkdir tmpRes_$versionTxt
cd tmpRes_$versionTxt

echo  Start with limit plots
getResults.py get --ranking --restrict-to limits $version
for chan in 0; # 2 02;
do
  wsname=${version}_limits_AZh_13TeV_${2}_${chan}_hasMerged_True_hasRes_True
  root -l -b -q ${WORKDIR}'/scripts/AZh/plotLimit.C(1, "'$chan'", "'$wsname'",0,"00")'
  cp plots/${wsname}_00/final/* ../$finalResDir/limits
done

echo get the breakdown
getResults.py get --plots --restrict-to breakdown $version

echo get the plots
getResults.py get --plots --tables --fccs --NPs --restrict-to fullRes $version

for chan in 0 2 02;
do
  echo copy the breakdown in the fullRes
  cp -r plots/${version}_breakdown_AZh_13TeV_${2}_${chan}_hasMerged_True_hasRes_True_${mass}_00/breakdown plots/${version}_fullRes_AZh_13TeV_${2}_${chan}_hasMerged_True_hasRes_True_${mass}_00
  echo do data/MC pull comparisons
  comparePulls.py 1 1,0 ../$finalResDir/pullComparisons/compareDataMC_${chan}lep 0 "" ${version}_fullRes_AZh_13TeV_${2}_${chan}_hasMerged_True_hasRes_True_${mass}_00
done

echo pull comparisons in data
comparePulls.py 1 0,0 ../$finalResDir/pullComparisons/compareData_0202lep 0 "" ${version}_fullRes_AZh_13TeV_${2}_0_hasMerged_True_hasRes_True_${mass}_00 ${version}_fullRes_AZh_13TeV_${2}_2_hasMerged_True_hasRes_True_${mass}_00 ${version}_fullRes_AZh_13TeV_${2}_02_hasMerged_True_hasRes_True_${mass}_00
echo "Black: 0 lepton" > ../$finalResDir/pullComparisons/compareData_0202lep/legend.txt
echo "Red: 2 lepton" >> ../$finalResDir/pullComparisons/compareData_0202lep/legend.txt
echo "Blue: 0+2 lepton" >> ../$finalResDir/pullComparisons/compareData_0202lep/legend.txt

echo copy the plots
cp -r plots/${version}_fullRes* ../$finalResDir/plots
cp -r tables ../$finalResDir
cd ../$finalResDir/

echo do a little bit of cleaning
find plots -name "*.yik" -delete
find plots -name "*sob.eps" -delete
find plots -name "*stats.eps" -delete
find plots -name "NP_*.eps" -delete
for chan in 0 2;
do
  rm -rf plots/${version}_fullRes_AZh_13TeV_${2}_${chan}_hasMerged_True_hasRes_True_${mass}_00/mass
done
echo remove empty dirs
find . -type d -empty -exec rmdir {} \;

echo Convert files and add html structure
$WORKDIR/macros/webpage/convertepstopngpdf.sh
python $WORKDIR/macros/webpage/createHtmlOverview.py

echo okay, time to archive
cd ..
tar zcvf results_${versionTxt}.tgz results_${versionTxt}
mkdir -vp ~/public/for_editors/
mv results_${versionTxt}.tgz ~/public/for_editors
cd $WORKDIR
echo TADAAAAAAAA

# Publish to www. Nicolas-specific
today=$(date +%y-%m-%d)
mkdir -vp /afs/cern.ch/user/n/nmorange/www/HbbPlots/StatAnalysis/${today}
cd /afs/cern.ch/user/n/nmorange/www/HbbPlots/StatAnalysis/${today}
tar zxvf ~/public/for_editors/results_${versionTxt}.tgz
cd ..
python $WORKDIR/macros/webpage/createHtmlOverview.py
