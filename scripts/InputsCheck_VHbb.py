#!/usr/bin/env python
""" A small script to check the difference between two sets of histograms

Author: Changqiao LI
Date:   2018-04-13
Email:  changqiao.li@cern.ch

Description:
    Once the new inputs is coming, you can run this script to check the yields difference between new and old.
    And it can also be used to check if the expected updates are in place or not by plotting the shape comparison,
    and this will call CompareShape.py

"""

import argparse
import logging
import time
from ROOT import *
import InputsCheck as IC

from fnmatch import fnmatch


# settings
#eos_dir = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/ICHEP2016/'
#IC.input_ref  = eos_dir + 'LAL/LimitHistograms.VH.vvbb.13TeV.LAL.v30.root' #0lep
#IC.input_test = eos_dir + 'LAL/BUG_LimitHistograms.VH.vvbb.13TeV.LAL.v30.root' #0lep
eos_dir='/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/inputs/'
IC.input_ref  = eos_dir+'ZeroLep/v3/Non-STXS/LimitHistograms.VH.0Lep.13TeV.mc16ade.Oxford.v02.r32-07_customCDI.root'
IC.input_test = eos_dir+'ZeroLep/v5/Non-STXS/LimitHistograms.VHbb.0Lep.13TeV.mc16ade.Oxford.r32-15v5.root'
# IC.input_ref  = eos_dir+'OneLep/r32-07_customCDI/Non-STXS/LimitHistograms.VHbb.1Lep.13TeV.mc16ade.LAL.v01.NoSTXS.r32-07_customCDI.root'
# IC.input_test = eos_dir+'OneLep/r32-15_customCDI/no_STXS/LimitHistograms.VHbb.1Lep.13TeV.mc16ade.LAL.v01.NoSTXS.r32-15_customCDI.root'
# IC.input_ref  = eos_dir+'TwoLep/r32-07_customCDI/LimitHistograms.VHbb.2Lep.13TeV.mc16ade.Kyoto.v01.r32-07_customCDI.root'
# IC.input_test = eos_dir+'TwoLep/r32-15_customCDI/v2/LimitHistograms.VHbb.2Lep.13TeV.mc16ade.Kyoto.r32-15_customCDI_v2.root'

IC.lep = 'L2'

IC.exp_diff = [
  '*_2tag2jet_150ptv_WhfSR_mva',
]


# basic structure for inputs
IC.Processes = [
'data',
# signal
# 1 lepton
'qqWlvH125','QQ2HLNUxFWDH',
'QQ2HLNUxPTVx0x75x0J','QQ2HLNUxPTVx75x150x0J','QQ2HLNUxPTVx150x250x0J','QQ2HLNUxPTVx250x400x0J','QQ2HLNUxPTVxGT400x0J',
'QQ2HLNUxPTVx0x75x1J','QQ2HLNUxPTVx75x150x1J','QQ2HLNUxPTVx150x250x1J','QQ2HLNUxPTVx250x400x1J','QQ2HLNUxPTVxGT400x1J',
'QQ2HLNUxPTVx0x75xGE2J','QQ2HLNUxPTVx75x150xGE2J','QQ2HLNUxPTVx150x250xGE2J','QQ2HLNUxPTVx250x400xGE2J','QQ2HLNUxPTVxGT400xGE2J',
#0 lepton:
'qqZvvH125', 'ggZvvH125','QQ2HNUNUxFWDH','GG2HNUNUxFWDH',
'QQ2HNUNUxPTVx0x75x0J','QQ2HNUNUxPTVx75x150x0J','QQ2HNUNUxPTVx150x250x0J','QQ2HNUNUxPTVx250x400x0J','QQ2HNUNUxPTVxGT400x0J',
'QQ2HNUNUxPTVx0x75x1J','QQ2HNUNUxPTVx75x150x1J','QQ2HNUNUxPTVx150x250x1J','QQ2HNUNUxPTVx250x400x1J','QQ2HNUNUxPTVxGT400x1J',
'QQ2HNUNUxPTVx0x75xGE2J','QQ2HNUNUxPTVx75x150xGE2J','QQ2HNUNUxPTVx150x250xGE2J','QQ2HNUNUxPTVx250x400xGE2J','QQ2HNUNUxPTVxGT400xGE2J',
'GG2HNUNUxPTVx0x75x0J','GG2HNUNUxPTVx75x150x0J','GG2HNUNUxPTVx150x250x0J','GG2HNUNUxPTVx250x400x0J','GG2HNUNUxPTVxGT400x0J',
'GG2HNUNUxPTVx0x75x1J','GG2HNUNUxPTVx75x150x1J','GG2HNUNUxPTVx150x250x1J','GG2HNUNUxPTVx250x400x1J','GG2HNUNUxPTVxGT400x1J',
'GG2HNUNUxPTVx0x75xGE2J','GG2HNUNUxPTVx75x150xGE2J','GG2HNUNUxPTVx150x250xGE2J','GG2HNUNUxPTVx250x400xGE2J','GG2HNUNUxPTVxGT400xGE2J',
#2 lepton:
'qqZllH125','ggZllH125','QQ2HLLxFWDH','GG2HLLxFWDH',
'QQ2HLLxPTVx0x75x0J','QQ2HLLxPTVx75x150x0J','QQ2HLLxPTVx150x250x0J','QQ2HLLxPTVx250x400x0J','QQ2HLLxPTVxGT400x0J',
'QQ2HLLxPTVx0x75x1J','QQ2HLLxPTVx75x150x1J','QQ2HLLxPTVx150x250x1J','QQ2HLLxPTVx250x400x1J','QQ2HLLxPTVxGT400x1J',
'QQ2HLLxPTVx0x75xGE2J','QQ2HLLxPTVx75x150xGE2J','QQ2HLLxPTVx150x250xGE2J','QQ2HLLxPTVx250x400xGE2J','QQ2HLLxPTVxGT400xGE2J',
'GG2HLLxPTVx0x75x0J','GG2HLLxPTVx75x150x0J','GG2HLLxPTVx150x250x0J','GG2HLLxPTVx250x400x0J','GG2HLLxPTVxGT400x0J',
'GG2HLLxPTVx0x75x1J','GG2HLLxPTVx75x150x1J','GG2HLLxPTVx150x250x1J','GG2HLLxPTVx250x400x1J','GG2HLLxPTVxGT400x1J',
'GG2HLLxPTVx0x75xGE2J','GG2HLLxPTVx75x150xGE2J','GG2HLLxPTVx150x250xGE2J','GG2HLLxPTVx250x400xGE2J','GG2HLLxPTVxGT400xGE2J',
# W+jets
'Wbb','Wbc','Wbl','Wcc','Wcl','Wl',
# Z+jets
'Zbb','Zbc','Zbl','Zcc','Zcl','Zl',
# top
'ttbar','stopWt','stops','stopt',
# diboson
'WW','WZ','ZZ',
# Multijet
'multijetEl','multijetMu'
]

IC.Vars = {
'L0':['mvaFullRun2Default'],
'L1':['mvaFullRun2Default'],
'L2':['mvaPolVars'],
#'mva28', # 0 lep
#'mBB',
}

IC.Regions = {
'L0':[
  '2tag2jet_150_250ptv_SR',
  '2tag2jet_250ptv_SR',
  '2tag3jet_150_250ptv_SR',
  '2tag3jet_250ptv_SR',
  '2tag2jet_150_250ptv_CRLow',
  '2tag2jet_250ptv_CRLow',
  '2tag3jet_150_250ptv_CRLow',
  '2tag3jet_250ptv_CRLow',
  '2tag2jet_150_250ptv_CRHigh',
  '2tag2jet_250ptv_CRHigh',
  '2tag3jet_150_250ptv_CRHigh',
  '2tag3jet_250ptv_CRHigh',
],

'L1':[
  '2tag2jet_150_250ptv_SR',
  '2tag2jet_250ptv_SR',
  '2tag3jet_150_250ptv_SR',
  '2tag3jet_250ptv_SR',
  '2tag2jet_150_250ptv_CRLow',
  '2tag2jet_250ptv_CRLow',
  '2tag3jet_150_250ptv_CRLow',
  '2tag3jet_250ptv_CRLow',
  '2tag2jet_150_250ptv_CRHigh',
  '2tag2jet_250ptv_CRHigh',
  '2tag3jet_150_250ptv_CRHigh',
  '2tag3jet_250ptv_CRHigh',
],

'L2':[
  '2tag2jet_75_150ptv_SR',
  '2tag2jet_150_250ptv_SR',
  '2tag2jet_250ptv_SR',
  '2tag3pjet_75_150ptv_SR',
  '2tag3pjet_150_250ptv_SR',
  '2tag3pjet_250ptv_SR',
  '2tag2jet_75_150ptv_CRLow',
  '2tag2jet_150_250ptv_CRLow',
  '2tag2jet_250ptv_CRLow',
  '2tag3pjet_75_150ptv_CRLow',
  '2tag3pjet_150_250ptv_CRLow',
  '2tag3pjet_250ptv_CRLow',
  '2tag2jet_75_150ptv_CRHigh',
  '2tag2jet_150_250ptv_CRHigh',
  '2tag2jet_250ptv_CRHigh',
  '2tag3pjet_75_150ptv_CRHigh',
  '2tag3pjet_150_250ptv_CRHigh',
  '2tag3pjet_250ptv_CRHigh',
## 2lep CR, please check mBBMVA instead of mva
#'2tag2jet_0_75ptv_topemucr',
#'2tag2jet_75_150ptv_topemucr',
#'2tag2jet_150ptv_topemucr',
#'2tag3jet_0_75ptv_topemucr',
#'2tag3jet_75_150ptv_topemucr',
#'2tag3jet_150ptv_topemucr',
],
}

IC.Systs = [
#{'Name':'blablabla','OneSide':False,'Region':[],'Process':[]},
# two side general sys
{'Name':'SysEG_RESOLUTION_ALL','OneSide':False},
{'Name':'SysEG_SCALE_ALL','OneSide':False},
{'Name':'SysEL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR','OneSide':False},
{'Name':'SysEL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR','OneSide':False},
{'Name':'SysEL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR','OneSide':False},
{'Name':'SysEL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_0_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_1_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_2_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_3_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_4_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_5_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_6_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_7_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_8_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_C_0_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_C_1_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_C_2_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_C_3_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_0_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_1_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_2_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_3_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_4_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_extrapolation_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysJET_CR_JET_BJES_Response','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Detector1','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Detector2','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Modelling1','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Modelling2','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Modelling3','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Modelling4','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Statistical1','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Statistical2','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Statistical3','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Statistical4','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Statistical5','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Statistical6','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Mixed1','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Mixed2','OneSide':False},
{'Name':'SysJET_CR_JET_EffectiveNP_Mixed3','OneSide':False},
{'Name':'SysJET_CR_JET_EtaIntercalibration_Modelling','OneSide':False},
{'Name':'SysJET_CR_JET_EtaIntercalibration_NonClosure_highE','OneSide':False},
{'Name':'SysJET_CR_JET_EtaIntercalibration_NonClosure_negEta','OneSide':False},
{'Name':'SysJET_CR_JET_EtaIntercalibration_NonClosure_posEta','OneSide':False},
{'Name':'SysJET_CR_JET_EtaIntercalibration_TotalStat','OneSide':False},
{'Name':'SysJET_CR_JET_Flavor_Composition','OneSide':False},
{'Name':'SysJET_CR_JET_Flavor_Response','OneSide':False},
{'Name':'SysJET_CR_JET_Pileup_OffsetMu','OneSide':False},
{'Name':'SysJET_CR_JET_Pileup_OffsetNPV','OneSide':False},
{'Name':'SysJET_CR_JET_Pileup_PtTerm','OneSide':False},
{'Name':'SysJET_CR_JET_Pileup_RhoTopology','OneSide':False},
{'Name':'SysJET_CR_JET_PunchThrough_MC16','OneSide':False},
{'Name':'SysJET_CR_JET_SingleParticle_HighPt','OneSide':False},
{'Name':'SysJET_JvtEfficiency','OneSide':False},
##{'Name':'SysJET_SR1_JET_GroupedNP_1','OneSide':False}, # the reduced set of NPs for JES which we don't use
##{'Name':'SysJET_SR1_JET_GroupedNP_2','OneSide':False},
##{'Name':'SysJET_SR1_JET_GroupedNP_3','OneSide':False},
{'Name':'SysMET_JetTrk_Scale','OneSide':False},
{'Name':'SysMET_SoftTrk_Scale','OneSide':False},
{'Name':'SysMETTrigStat','OneSide':False},
{'Name':'SysMETTrigTop','OneSide':False},
{'Name':'SysMUON_EFF_RECO_STAT','OneSide':False},
{'Name':'SysMUON_EFF_RECO_STAT_LOWPT','OneSide':False},
{'Name':'SysMUON_EFF_RECO_SYS','OneSide':False},
{'Name':'SysMUON_EFF_RECO_SYS_LOWPT','OneSide':False},
{'Name':'SysMUON_EFF_TrigStatUncertainty','OneSide':False},
{'Name':'SysMUON_EFF_TrigSystUncertainty','OneSide':False},
{'Name':'SysMUON_ID','OneSide':False},
{'Name':'SysMUON_EFF_ISO_STAT','OneSide':False},
{'Name':'SysMUON_EFF_ISO_SYS','OneSide':False},
{'Name':'SysMUON_MS','OneSide':False},
{'Name':'SysMUON_SAGITTA_RESBIAS','OneSide':False},
{'Name':'SysMUON_SAGITTA_RHO','OneSide':False},
{'Name':'SysMUON_SCALE','OneSide':False},
{'Name':'SysMUON_EFF_TTVA_STAT','OneSide':False},
{'Name':'SysMUON_EFF_TTVA_SYS','OneSide':False},
{'Name':'SysPRW_DATASF','OneSide':False},
{'Name':'SysTAUS_TRUEHADTAU_SME_TES_MODEL','OneSide':False},
{'Name':'SysTAUS_TRUEHADTAU_SME_TES_INSITU','OneSide':False},
{'Name':'SysTAUS_TRUEHADTAU_SME_TES_DETECTOR','OneSide':False},

## one side booked here
{'Name':'SysJET_CR_JET_JER_EffectiveNP_1','OneSide':True},
{'Name':'SysJET_CR_JET_JER_EffectiveNP_2','OneSide':True},
{'Name':'SysJET_CR_JET_JER_EffectiveNP_3','OneSide':True},
{'Name':'SysJET_CR_JET_JER_EffectiveNP_4','OneSide':True},
{'Name':'SysJET_CR_JET_JER_EffectiveNP_5','OneSide':True},
{'Name':'SysJET_CR_JET_JER_EffectiveNP_6','OneSide':True},
{'Name':'SysJET_CR_JET_JER_EffectiveNP_7restTerm','OneSide':True},
{'Name':'SysMET_SoftTrk_ResoPara','OneSide':True},
{'Name':'SysMET_SoftTrk_ResoPerp','OneSide':True},

## some special syst. Only check for the process in 'Process'
{'Name':'SysTTbarMBB','OneSide':False,'Process':['ttbar']},
{'Name':'SysTTbarPTV','OneSide':False,'Process':['ttbar']},
{'Name':'SysTTbarPTVMBB','OneSide':False,'Process':['ttbar']},

{'Name':'SysStopWtbbACC','OneSide':False,'Process':['stopWt']},
{'Name':'SysStopWtothACC','OneSide':False,'Process':['stopWt']},
{'Name':'SysStopWtMTOP','OneSide':False,'Process':['stopWt']},
{'Name':'SysStopWtMBB','OneSide':False,'Process':['stopWt']},
{'Name':'SysStopWtPTV','OneSide':False,'Process':['stopWt']},

{'Name':'SysStoptMBB','OneSide':False,'Process':['stopt']},
{'Name':'SysStoptPTV','OneSide':False,'Process':['stopt']},

{'Name':'SysWMbb','OneSide':False,'Process':['Wbb','Wbc','Wbl','Wcc','Wcl']},
{'Name':'SysWPtV','OneSide':False,'Process':['Wbb','Wbc','Wbl','Wcc','Wcl']},
{'Name':'SysWPtVMbb','OneSide':False,'Process':['Wbb','Wbc','Wbl','Wcc','Wcl']},

{'Name':'SysZMbb','OneSide':False,'Process':['Zbb','Zbc','Zbl','Zcc','Zcl']},
{'Name':'SysZPtV','OneSide':False,'Process':['Zbb','Zbc','Zbl','Zcc','Zcl']},
{'Name':'SysZPtVMbb','OneSide':False,'Process':['Zbb','Zbc','Zbl','Zcc','Zcl']},

{'Name':'SysVVMbbME','OneSide':False,'Process':['WZ','ZZ']},
{'Name':'SysVVMbbPSUE','OneSide':False,'Process':['WZ','ZZ']},
{'Name':'SysVVPTVME','OneSide':False,'Process':['WZ','ZZ']},
{'Name':'SysVVPTVPSUE','OneSide':False,'Process':['WZ','ZZ']},
{'Name':'SysVVMbbVPSUE','OneSide':False,'Process':['WZ','ZZ']},
#signal systs have to be update for STXS
{'Name':'SysVHQCDscalePTV','OneSide':False,'Process':['qqWlvH125','qqZvvH125','ggZvvH125','qqZllH125','ggZllH125']},
{'Name':'SysVHQCDscaleMbb','OneSide':False,'Process':['qqWlvH125','qqZvvH125','ggZvvH125','qqZllH125','ggZllH125']},

{'Name':'SysVHPDFPTV','OneSide':False,'Process':['qqWlvH125','qqZvvH125','ggZvvH125','qqZllH125','ggZllH125']},
{'Name':'SysVHQCDscalePTV_ggZH','OneSide':False,'Process':['ggZvvH125','ggZllH125']},
{'Name':'SysVHQCDscaleMbb_ggZH','OneSide':False,'Process':['ggZvvH125','ggZllH125']},
{'Name':'SysVHPDFPTV_ggZH','OneSide':False,'Process':['ggZvvH125','ggZllH125']},
{'Name':'SysVHPDFPTV','OneSide':False,'Process':['qqWlvH125','qqZvvH125','ggZvvH125','qqZllH125','ggZllH125']},
{'Name':'SysVHUEPSMbb','OneSide':False,'Process':['qqWlvH125','qqZvvH125','ggZvvH125','qqZllH125','ggZllH125']},
{'Name':'SysVHNLOEWK','OneSide':False,'Process':['qqWlvH125','qqZvvH125','ggZvvH125','qqZllH125','ggZllH125']},




]

start_time = time.time()
IC.main()
print("--- %s seconds for this job---" % round(time.time() - start_time, 2))
