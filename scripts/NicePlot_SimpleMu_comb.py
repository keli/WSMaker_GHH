##########################################################################
#
# this ia actually an effective macro to make the fancy plot!!!!
#
##########################################################################
import glob, os, sys
import commands
from ROOT import *
from glob import *
import time
import math
##########################################################################

gROOT.LoadMacro("macros/AtlasStyle.C")
gROOT.LoadMacro("macros/AtlasUtils.C")
SetAtlasStyle()
gStyle.SetEndErrorSize(7.0)
gROOT.SetBatch(True)

#########################################################################
outDir="Results/"
os.system("mkdir -p "+outDir)
mainFolder=""
noInputs  =5

upperLim= 4
lowerLim= 0


def myTextBold(x,y,color,text, tsize):
  l=TLatex();
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(62);
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);

####################################################################################################
mode="4"
doVH=True
value=[]

mode=sys.argv[1]
###if sys.argv[2]=="0":
###    doVH=False

if doVH:
    upperLim=  7
    lowerLim=  0

    value=[
        ["Comb."   ,0, 1.008, 0.201, 0.195, 0.125, 0.124, "",1],
        ["VH"      ,1, 0.983, 0.221, 0.209, 0.142, 0.140, "",1],
        ["ttH"     ,2, 1.000, 0.558, 0.538, 0.276, 0.272, "",1],
        ["VBF+ggF" ,3, 1.681, 1.159, 1.122, 1.01, 1.00, "",1],
    ]

    if mode=="-1":
        upperLim=  5.0
        lowerLim=  0.0
        value=[
            ["Comb." ,0, 0.984, 0.221, 0.209, 0.142, 0.140, "",1],
            ["2L  "  ,1, 1.284, 0.460, 0.423, 0.342, 0.332, "",1],
            ["1L  "  ,2, 1.071, 0.369, 0.347, 0.235, 0.229, "",1],
            ["0L  "  ,3, 0.730, 0.292, 0.282, 0.222, 0.217, "",1],
        ]

    if mode=="-2":
        upperLim=  5.0
        lowerLim=  0.0
        value=[
            ["Comb."                    ,0, 1.1349, 0.2402 , 0.2253 , 0.1536, 0.1511, "",1],
            ["H#rightarrow b#bar{b}"    ,1, 1.1653, 0.2692 , 0.2500 , 0.1628, 0.1601 , "",1],
            #["H#rightarrow #gamma#gamma",2, 1.0653, 0.5956 , 0.5448 , 0.5313, 0.4947 , "",1],
             ["H#rightarrow #gamma#gamma",2, 1.0288, 0.5994 , 0.5448 , 0.5311, 0.4963 , "",1],
            #["H#rightarrow ZZ^{*}"      ,3, 0.8731, 1.1856 , 0.7446 , 1.1683, 0.746, "",1],
             ["H#rightarrow ZZ^{*}"      ,3, 0.9445, 1.3039 , 0.8666 , 1.2649, 0.8544, "",1],
        ]
            
    if mode=="1":
        upperLim=  5.0
        lowerLim=  0.0
        value=[
            ["Comb." ,0, 0.984, 0.221, 0.209, 0.142, 0.140, "",1],
            ["ZH  "  ,1, 0.923, 0.279, 0.267, 0.206, 0.203, "",1],
            ["WH  "  ,2, 1.085, 0.375, 0.353, 0.237, 0.232, "",1],
        ]
    
    if mode=="2":
        upperLim=  14.0
        lowerLim=  -4.0
        value=[
            ["Comb."         ,0,  1.008, 0.201, 0.195, 0.125, 0.124, "",1],
            ["VH Run2"       ,1,  1.153, 0.268, 0.248, 0.161, 0.160, "",1],
            ["VH Run1"       ,1,  0.507, 0.400, 0.372, 0.311, 0.299, "",1],
            ["ttH Run2"      ,2,  0.852, 0.630, 0.607, 0.298, 0.293, "",1],
            ["ttH Run1"      ,1,  1.496, 1.223, 1.138, 0.727, 0.708, "",1],
            ["VBF+ggF Run2"  ,3,  2.467, 1.383, 1.313, 1.305, 1.291, "",1],
            ["VBF+ggF Run1"  ,1, -0.778, 2.261, 2.275, 1.594, 1.577, "",1],
        ]

    if mode=="3":
        value=[
            ["Comb: MVA" ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["Comb: CBA" ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVH/1POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VH_1POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VH_1POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L: MVA"   ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
            ["2L: CBA"   ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVH/3POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VH_3POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VH_3POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L: MVA"   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
            ["1L: CBA"   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVH/3POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VH_3POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VH_3POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L: MVA"   ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/3POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed3POI_fullRes_VHbbRun2_13TeV_Observed3POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
            ["0L: CBA"   ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/CBAVH/3POI/muHatTable_SMVH_LHCP17_CUT_v08.Observed_CBA_VH_3POI_fullRes_VHbbRun2_13TeV_Observed_CBA_VH_3POI_012_125_Systs_use1tagFalse_mBB_mode1_Asimov0_SigXsecOverSM_L0.txt",1],
        ]


    if mode=="4":
        value=[
            ["Comb." ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/1POI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_fullRes_VHbbRun2_13TeV_Observed_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM.txt",1],
            ["2L: 2j high" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L2_J2_BMin150.txt",1],
            ["2L: 2j low"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L2_J2_BMin75.txt",1],
            ["2L: 3j high" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L2_J3_BMin150.txt",1],
            ["2L: 3j low"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L2_J3_BMin75.txt",1],
            ["1L: 2j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L1_J2_BMin150.txt",1],
            ["1L: 3j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L1_J3_BMin150.txt",1],
            ["0L: 2j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L0_J2_BMin150.txt",1],
            ["0L: 3j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "NewResult/MVAVH/8OI/muHatTable_SMVHVZ_LHCP17_MVA_v07.Observed_8POI_fullRes_VHbbRun2_13TeV_Observed_8POI_012_125_Systs_use1tagFalse_mva_mode1_Asimov0_SigXsecOverSM_L0_J3_BMin150.txt",1],
        ]
        upperLim=  10
        lowerLim= -2


        
Nobj=len(value)

####################################################################################################
           
for obj in value:
    if "txt" not in obj[7]: continue
    theF=file(obj[7])
    lines=theF.readlines()
    index=obj[8]
    incremental=0
    mu=1
    errT_u=0
    errT_d=0
    errS_u=0
    errS_d=0
    readThis=False
    for line in lines:
        if "muhat" in line:
            incremental+=1
            if incremental==index: readThis=True
            else                 : readThis=False
            ##print line
            if readThis: mu=float(line.split(":")[1])
        elif "Total" in line and readThis:
            ##print line.split()
            errT_u=float(line.split()[2])
            errT_d=float(line.split()[4])
        elif "DataStat" in line and errS_u==0 and readThis:
            errS_u=float(line.split()[2])
            errS_d=float(line.split()[4])
    theF.close()
    obj[2]=mu
    obj[3]=errT_u
    obj[4]=errT_d
    obj[5]=errS_u
    obj[6]=errS_d

####################################################################################################################
    
for obj in value:
    print obj
    
print "info on final fit: "
print " Mu val: "+str( value[0][2] )
print " Stat Err:  UP= "+str( value[0][5] )+"   DO= "+str( value[0][6] )
print " Sys  Err:  UP= "+str( math.sqrt( value[0][3]*value[0][3]-value[0][5]*value[0][5]) )+"   DO= "+str( math.sqrt(value[0][4]*value[0][4] - value[0][6]*value[0][6]) )
print " TOT  Err:  UP= "+str( value[0][3] )+"   DO= "+str( value[0][4] )

    
#####################################################################################################################
value.append( ["Fake", 0, 0, 0, 0, 0, 0, 0, 0 ] )
if Nobj>4: value.append( ["Fake", 0, 0, 0, 0, 0, 0, 0, 0 ] )

theHisto2=TH2D("plot2","plot2",20,lowerLim,upperLim,len(value),-0.5,len(value) )  ##0.5)
theHisto2.GetYaxis().SetTickSize(0)
theHisto2.GetYaxis().SetTickLength(0)
#if doVH: theHisto2.GetXaxis().SetTitle("#mu_{VH}^{b#bar{b}}")   ##"best fit #mu_{VH}^{b#bar{b}}"
#else   : theHisto2.GetXaxis().SetTitle("#mu_{VZ}^{b#bar{b}}")

#if doVH: theHisto2.GetXaxis().SetTitle("Best fit #mu_{VH}^{b#bar{b}} for m_{H}=125 GeV")
#else   : theHisto2.GetXaxis().SetTitle("Best fit #mu_{VZ}^{b#bar{b}}")

if doVH:
    if mode=="1" or mode=="-1": theHisto2.GetXaxis().SetTitle("#mu_{VH}^{bb}")
    elif mode=="-2"           : theHisto2.GetXaxis().SetTitle("#mu_{VH}")
    else                      : theHisto2.GetXaxis().SetTitle("#mu_{H#rightarrowbb}")
        
#else   : theHisto2.GetXaxis().SetTitle("Best fit #mu_{VZ}^{b#bar{b}}")

##best fit #mu=#sigma^{t#bar{t}H}/#sigma^{t#bar{t}H}_{SM} for m_{H}=125 GeV")
theHisto2.GetYaxis().SetLabelSize(0.06)
theHisto2.GetXaxis().SetTitleOffset(1.0)
theHisto2.GetXaxis().SetTitleSize(0.06)
theHisto2.GetYaxis().SetLabelSize(0.07)
count=0
for obj in value:
    if "Fake" in obj[0]: continue
    count+=1
    ##print "setting: "+str(obj[1])
    theHisto2.GetYaxis().SetBinLabel(count,obj[0])

c2 = TCanvas("test2","test2",800,600)
##c2 = TCanvas("test2","test2",600,600)
if mode=="1" or mode=="0" or mode=="-1" or mode=="-2": gPad.SetLeftMargin(0.18)
else        : gPad.SetLeftMargin(0.23)
gPad.SetTopMargin(0.05)
theHisto2.Draw()

tmp5=TH1D("tmp5", "tmp5", 1,1,2)
tmp6=TH1D("tmp6", "tmp6", 1,1,2)
tmp5.SetLineColor(1)
tmp6.SetLineWidth(4)
tmp5.SetLineWidth(3)
colorS=8
if not doVH  : colorS=kAzure+7##4
if mode=="-2": colorS=kRed
tmp6.SetLineColor(colorS)


g = TGraphAsymmErrors()
g.SetLineWidth(4)
g.SetMarkerColor(1)
g.SetMarkerStyle(20)
g.SetMarkerSize(1.6)

g2 = TGraphAsymmErrors()
g2.SetLineWidth(3)
g2.SetMarkerColor(1)
g2.SetMarkerStyle(20)
g2.SetMarkerSize(1.6)
g2.SetLineColor(colorS)

count=-1.0
for obj in value:
    count+=1.
    if "Fake" in obj[0]: continue
    scale=1.15 ## was 1.1
    if Nobj==4: scale=1.09  #was 1.05
    if Nobj>=5: scale=1.05
    g.SetPoint( int(count)     , obj[2]    , float(count)*scale )
    g.SetPointEXhigh( int(count), obj[3])
    g.SetPointEXlow(  int(count), obj[4])
    g.SetPointEYhigh( int(count), float(0) )
    g.SetPointEYlow(  int(count), float(0) )
    g2.SetPoint( int(count)     , obj[2]    , float(count)*scale )
    g2.SetPointEYhigh( int(count), float(0) )
    g2.SetPointEYlow(  int(count), float(0) )
    g2.SetPointEXhigh( int(count), obj[5] )
    g2.SetPointEXlow(  int(count), obj[6] )
    print " --> Setting point: "+str(obj[2])+"  +/- "+str(obj[3])
    sysUp=0
    if obj[3]*obj[3]-obj[5]*obj[5]>0:
        sysUp= math.sqrt(obj[3]*obj[3]-obj[5]*obj[5])
    sysDo=0
    if obj[4]*obj[4]-obj[6]*obj[6]>0:
        sysDo= math.sqrt(obj[4]*obj[4]-obj[6]*obj[6])

    mystring1 = "%.2f  {}^{+%.2f}_{#minus%.2f}" % (obj[2], obj[3], obj[4])
    #mystring2 ="{}^{+%.1f}_{#minus%.1f}  , {}^{+%.1f}_{#minus%.1f}            " % (obj[5],obj[6], sysUp, sysDo) ## add a
    mystring2 = "                    {}^{+%.2f}_{#minus%.2f} , " % (obj[5],obj[6]) ## add a
    mystring3 = "                             {}^{+%.2f}_{#minus%.2f}           " % (sysUp, sysDo) ## add a
    mystring4 = "                   (                 )         "

    textsize=0.051
    if mode=="3": textsize=0.044 
    offset=0
    startX=0.57
    slope=0.157 ## was0.16
    if Nobj==3: slope=0.21 ##was 0.2
    startY=0.22+slope*count+offset
    if Nobj>4: startY=0.18+0.095*count+offset
   
    if Nobj==8: startY=0.18+0.082*count+offset
    if Nobj==9: startY=0.18+0.075*count+offset
    if mode=="3": startY=0.19+0.079*count+offset

    if mode=="2": startY=0.19+0.088*count+offset
        
    myTextBold(startX     ,startY,1, mystring1, textsize)  # 0.144
    myText(    startX+0.01,startY,1, mystring2, textsize)  
    myText(    startX+0.01,startY,1, mystring3, textsize)
    myText(    startX+0.01,startY,1, mystring4, textsize)

slope=0.12
if Nobj==3: slope=0.14
if Nobj>4 : slope=0.10
if mode=="3": slope=0.055
yValLabel=0.32+(len(value)-1)*slope
if mode!="1": yValLabel=yValLabel-0.03 ##last was not there
if mode!="3":
    textsize=0.045
    myTextBold(0.65, yValLabel , 1, "( Tot. )"        , textsize) ##was 0.66 for %.1
    myText(    0.76, yValLabel , 1, "( Stat., Syst. )", textsize-0.001) ##84
else:
    textsize=0.040
    myTextBold(0.64, yValLabel , 1, "( Tot. )"        , textsize) ##was 0.66 for %.1
    myText(    0.74, yValLabel , 1, "( Stat., Syst. )", textsize-0.001) ##84
    
legend4=TLegend(0.25,0.77,0.57,0.87)
legend4.SetTextFont(42)
legend4.SetTextSize(0.047)
legend4.SetFillColor(0)
legend4.SetLineColor(0)
legend4.SetFillStyle(0)
legend4.SetBorderSize(0)
legend4.SetNColumns(2)
legend4.AddEntry(tmp5,"Total" ,"l")
legend4.AddEntry(tmp6,"Stat.","l")
legend4.Draw("SAME")

subtract=+1.25
if Nobj>4: subtract+=1
line0=TLine(0., -0.5, 0., len(value)-subtract)
line0.SetLineWidth(3)
line0.SetLineStyle(3)
line0.SetLineColor(kGray+1)
if lowerLim<0: line0.Draw("SAMEL") 

line1=TLine(1., -0.5, 1., len(value)-subtract)
line1.SetLineWidth(2)
line1.SetLineColor(kBlack)
line1.Draw("SAMEL")

yVal=0.5
if mode=="3": yVal=1.5
lineC=None
if mode=="3":
    lineC=TLine(theHisto2.GetXaxis().GetXmin(), yVal+0.05,theHisto2.GetXaxis().GetXmax(), yVal+0.05)
else:
    lineC=TLine(theHisto2.GetXaxis().GetXmin(), yVal,theHisto2.GetXaxis().GetXmax(), yVal)
lineC.SetLineWidth(2)
lineC.SetLineStyle(2)
lineC.SetLineColor(kBlack)
lineC.Draw("SAMEL")

#lall.Draw("SAME")
g.Draw("SAMEP")
g2.Draw("SAMEP")


#myText(0.15,0.65,1,"(13.3 fb^{-1} )",0.03)
#myText(0.15,0.50,1,"(13.2 fb^{-1} )",0.03)
#myText(0.15,0.34,1,"(13.2 fb^{-1} )",0.03)

if mode=="1"  :
    ATLAS_LABEL(0.20,0.88,1)
    #myText(     0.33,0.88,1,"Internal",0.035)
    myText(  0.39+0.02,0.88,1,"VH,H#rightarrowb#bar{b}",0.048)
elif mode=="2":
    ATLAS_LABEL(0.24,0.88,1)
    #myText(     0.358,0.88,1,"Internal",0.035)
    myText(  0.42+0.03,0.88,1,"H#rightarrowb#bar{b}",0.048)
elif mode=="0":
    ATLAS_LABEL(0.22,0.88,1)
    #myText(     0.34,0.88,1,"Internal",0.035)
    myText(  0.39+0.04,0.88,1,"H#rightarrowb#bar{b}",0.048)
else:
    ATLAS_LABEL(0.22,0.88,1)
    #myText(     0.35,0.88,1,"Internal",0.035)
    myText(  0.39+0.08,0.88,1,"VH",0.048)
###myText(     0.37,0.88,1,"Preliminary",0.05)

##ATLAS_LABEL(0.22,0.88,1)
#if mode=="1":
#    if doVH: myText(  0.45,0.73,1,"VH, H#rightarrowbb",0.048)
#    else   : myText(  0.45,0.88,1,"VZ, Z(bb)",0.048)
#if mode=="-1":
#    if doVH: myText(  0.45,0.75,1,"VH, H(bb)",0.048)
#if mode=="1" or mode=="-1": myText(  0.39+0.02,0.88,1,"VH,H#rightarrowb#bar{b}",0.048)
##else                      : myText(  0.42+0.02,0.88,1,"H#rightarrowb#bar{b}",0.048)
    
##myText(     0.60,0.93,1,"#sqrt{s}=13 TeV, #scale[0.6]{#int}L dt=3.2 fb^{-1}")
########myText(     0.65,0.88,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}",0.045)
if mode!="-2" : myText(     0.590,0.89,1,"#sqrt{s}= 7 TeV, 8 TeV, and 13 TeV"                             , 0.0382)
if mode=="1" or mode=="-1": myText(     0.550,0.84,1,"      4.7 fb^{-1}, 20.3 fb^{-1}, and 79.8 fb^{-1}", 0.0382)
elif mode=="-2"           : myText(     0.62 ,0.88,1,"#sqrt{s}=13 TeV, 79.8 fb^{-1}" ,0.045)
else                      : myText(     0.510,0.84,1,"      4.7 fb^{-1}, 20.3 fb^{-1}, and 24.5-79.8 fb^{-1}", 0.0382)
##8.3 fb^{-1}")

c2.Update()
name="Plot12_mu_"+str(mode)
if doVH: name+="_VH"
else   : name+="_VZ"
c2.Print( outDir+name+".pdf" )
###os.system( "open "+outDir+name+".pdf &" )

#############################################################################
#############################################################################

