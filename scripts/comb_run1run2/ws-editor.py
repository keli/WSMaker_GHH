#!/usr/bin/env python
from ROOT import *
from commands import *
from collections import OrderedDict as odict
import time,sys,math,socket,os,copy
import matrix_manipulator as mm

"""
Notes: You have a RooCategory called "master_measurement" in Run1
    ---it has "[SnapShot_ExtRefClone]" but otherwise is identical modulo regions
    ---master_measurement shows up in the "OBSERVABLES" vomit at end instead of beginning
"""

def skip_this_np(name,newnps):
    for thing in newnps:
        if thing in name:
            #print '{} is in {}'.format(thing,name)
            return True
    return False
isrun2=len(sys.argv)==1 #the default put some blah blah after to make it run1
# because of signs, some b-tagging NP's need to be flipped
# signs of ttbar normalization changes for leading NPs for 7,8,13 TeV: B: -+-; C:*+-; L: +--
# flip 8 TeV BC, 7 TeV L
# right now, we are only concerned with flipping the leading B
btag=True
poisplit=False
asimov=False
debug=False
wstag = 'Run2WS-20170601' if isrun2 else 'Run1WS'
wstag = 'SMVHVZ_LHCP17_MVA_v05' if isrun2 else 'Run1WS'
datdir = '/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct' if 'harvard' in socket.gethostname() else '/afs/cern.ch/work/s/stchan/public/hcomb'
new_pois=['muRun1','deltaMu'] if poisplit else []
if not btag:
    if isrun2:
        eff_nps=list('alpha_SysJET_21NP_JET_EffectiveNP_{0}'.format(i+1) for i in range(7))
        #eff_nps.append('alpha_SysJET_21NP_JET_EffectiveNP_8restTerm')
    else:
        eff_nps=list('alpha_SysJetNP{0}_Y2012_8TeV'.format(i+1) for i in range(4)) #5 isn't in there for some reason even though 6 is the "rest" one
        #eff_nps.append('alpha_SysJetNP6_rest_Y2012_8TeV')
    skip_list=list(s for s in eff_nps)
    if not isrun2:
        skip_list+=list(s.replace('Y2012','Y2012Constraint') for s in eff_nps)
    if debug:
        print 'SKIP LIST'
        print ' '.join(skip_list)
    mfile="{0}/ProjectionCoefficientFile_{1}_test_withAmplitudes_April29.root".format(datdir,'Moriond2017' if isrun2 else 'Final2012')
    coeffs,newconstrains,noldnps=mm.parse_JES_map(mapfile=mfile,effective_nps=eff_nps,qrun2=isrun2)
    if debug:
        print 'Unfolded JES NPs:'
        print '\n'.join(newconstrains)
elif poisplit:
    eff_nps=['SigXsecOverSM']
    skip_list=eff_nps
    coeffs,newconstrains,noldnps={'SigXsecOverSM':[1,1]},new_pois,1
else:
    if not isrun2:
        # if we're doing b-tagging (for now), we just flip the 0th 8 TeV B NP
        eff_nps=['alpha_SysBTagB0Effic_Y2012_8TeV']
        skip_list=eff_nps+['alpha_SysBTagB0Effic_Y2012Constraint_8TeV']
        coeffs,newconstrains,noldnps={'alpha_SysBTagB0Effic_Y2012_8TeV':[-1]},['alpha_SysFT_EFF_Eigen_B_0_8TeV'],1
"""
if not isrun2:
    f=TFile('{1}/workspaces/{0}/combined/125_asimovData_cnd1_0.51.root'.format(wstag,datdir))
else:
    f=TFile('{1}/workspaces/{0}/combined/125_wAsimov.root'.format(wstag,datdir))
"""
f=TFile('{1}/workspaces/{0}/combined/125.root'.format(wstag,datdir))
ws      = f.Get("combined")

data    = ws.data(mm.ws_datname(isrun2,asimov))
mc      = ws.obj("ModelConfig");
pois    = mc.GetParametersOfInterest()
np      = mc.GetNuisanceParameters()
globObs = mc.GetGlobalObservables()
Obs     = mc.GetObservables()
PDF     = mc.GetPdf()
#PDF.Print("v")
#print PDF.GetName(), PDF.GetTitle(), PDF.ClassName()

#---------
#Creating the new set of NP, Observables and Global Observables
np_new=RooArgSet()
Obs_new=RooArgSet()
globObs_new=RooArgSet()
pois_new=RooArgSet()
#---------

#---------
#Preparing the new 75 gaussian constrains
#Adding the new 75 NP to the list of the new NP
#Adding the new 75 nominal and sigma values to the new global observables
ws_new=RooWorkspace("Combined");
ws_new.autoImportClassCode(1);
newconstrainList=[]
for iconstrain in newconstrains:
    if poisplit:
        ws_new.factory("{0}[-40,40]".format(iconstrain))
    else:
        ws_new.factory("Gaussian::"+iconstrain+"Constraint("+iconstrain+"[-5,5],nom_"+iconstrain+"[0,-5,5],1)");
        newconstrainList.append(ws_new.obj(iconstrain+"Constraint"));
#---------
#print newconstrainList

#---------
#Preparing the new RooCategory, incordpotaring all the categories in the original PDF
roocatname = mm.ws_roocat(isrun2)
Cat=RooCategory(roocatname,roocatname)
iterPDF=PDF.serverIterator();
iPDF=iterPDF.Next()
kittens=[]

while iPDF:#Loop over the categories
    if "RooCategory" not in iPDF.ClassName():
        cat=mm.catname(iPDF.GetName())
        if debug:
            print 'Adding {0} called {1} as {2}'.format(iPDF.ClassName(),iPDF.GetName(),cat.Data())
        Cat.defineType(cat.Data())
        kittens.append(cat)
    elif debug:
        print 'Skipped a RooCategory called',iPDF.GetName()
    iPDF=iterPDF.Next()

#---------

#---------
#Creating the new PDF
TotalPDF=RooSimultaneous("simPDF","",Cat);
#---------


iterPDF=PDF.serverIterator();
iPDF=iterPDF.Next()
categories={}
while iPDF:#Loop over the categories
    if debug: print iPDF.GetName(),iPDF.GetTitle(),iPDF.ClassName()
    if "RooCategory" not in iPDF.ClassName():
        newlist=RooArgList()
        subpdf=iPDF.pdfList().createIterator()
        isubpdf=subpdf.Next()
        while isubpdf: #Loop over the PDF blocks for each categories
            if debug: 
                print isubpdf.GetName(),isubpdf.GetTitle(),isubpdf.ClassName()
            if not skip_this_np(isubpdf.GetName(),skip_list): #if "JET_EffectiveNP" not in isubpdf.GetName():
                newlist.add(isubpdf)
            elif debug or True:
                print 'Skipping {0} in PDF category {1}'.format(isubpdf.GetName(),iPDF.GetName())
            if "RooRealSumPdf" in isubpdf.ClassName() and debug:
                print 'Looping through sub pdf...'
                isubpdf.Print("v")
                functions=isubpdf.funcList().createIterator()
                ifunc=functions.Next()
                while ifunc:
                    print ifunc.GetName(),ifunc.GetTitle()
                    ifunc.Print("v")
                    ifunc=functions.Next()
                coef=isubpdf.coefList().createIterator()
                icoef=coef.Next()
                while icoef:
                    print icoef.GetName(),icoef.GetTitle()
                    icoef=coef.Next()
            isubpdf=subpdf.Next()
        cat=TString()
        cat.Form(iPDF.GetName())
        name=cat+TString("_new")
        title=TString()
        title.Form(iPDF.GetTitle())
        title=title+TString("_new")
        cat=mm.catname(cat.Data())
        if debug: 
            print name, cat
        if isrun2 or (not isrun2 and '8TeV' in iPDF.GetName()): # don't add these 8 TeV JES parameters to 7 TeV regions...will cause problems
            for iNewConstr in newconstrainList:
                newlist.add(iNewConstr)
        if cat not in kittens:
            print 'MEOW, {0} not in our category list {1}'.format(cat.Data(),'\n'.join(list(str(k) for k in kittens)))
        categories[cat]=RooProdPdf(name.Data(),title.Data(),newlist) ;
        if debug: print categories[cat].Print("V")
    elif debug:
        print 'Okay, skip the RooCategory...'
        iPDF.Print("v")
    iPDF=iterPDF.Next()
    if debug: print "-----------"
print "\n\n\n\n"

for c in categories.keys():
    TotalPDF.addPdf(categories[c],c.Data())
if debug:
    print 'CHECKCHECK'
    TotalPDF.Print("v")
    iterPDF=TotalPDF.serverIterator();
    iPDF=iterPDF.Next()
    while iPDF:#Loop over the categories
        print 'PDF:',iPDF.GetName(),iPDF.GetTitle(),iPDF.ClassName()
        iPDF.Print("v")
        iPDF=iterPDF.Next()
getattr(ws_new,'import')(TotalPDF);
getattr(ws_new,'import')(data);

for inp in coeffs:
    # linear combination syntax: '(0.5)*(@0)+(0.5)*(@1)'
    if debug:
        print "expr::{0}decomp('{1}',{2})".format(inp,'+'.join(list('({0})*(@{1})'.format(coeffs[inp][i],i) for i in range(noldnps))),','.join(newconstrains))
        print "EDIT::simPDF(simPDF, {0}={0}decomp)".format(inp)
    ws_new.factory("expr::{0}decomp('{1}',{2})".format(inp,'+'.join(list('({0})*(@{1})'.format(coeffs[inp][i],i) for i in range(noldnps))),','.join(newconstrains)))
    ws_new.factory("EDIT::simPDF(simPDF, {0}={0}decomp)".format(inp))


iter_vars=np.createIterator()
var_vars = iter_vars.Next()
while var_vars :
    if not skip_this_np(var_vars.GetName(),skip_list):
        np_new.add(var_vars)
    var_vars = iter_vars.Next()

#normalizations
iter_vars=globObs.createIterator()
var_vars = iter_vars.Next()
while var_vars :
    if not skip_this_np(var_vars.GetName(),skip_list):
        globObs_new.add(var_vars)
    var_vars = iter_vars.Next()

#the actual fit distributions (mBB/MVA)
iter_vars=Obs.createIterator()
var_vars = iter_vars.Next()
while var_vars :
    #print var_vars.GetName()
    if not skip_this_np(var_vars.GetName(),skip_list):
        Obs_new.add(var_vars)
    var_vars = iter_vars.Next()

all_vars=ws_new.allVars()
iter_vars=all_vars.createIterator()
var_vars = iter_vars.Next()
while var_vars :
    if "alpha_SysJET_Complete" in var_vars.GetName() and not "nom" in var_vars.GetName():
        np_new.add(var_vars)
    if "alpha_SysJET_Complete" in var_vars.GetName() and "nom" in var_vars.GetName():
        var_vars.setConstant(kTRUE)
        globObs_new.add(var_vars)
    var_vars = iter_vars.Next()
all_vars=ws_new.allVars()
iter_vars=all_vars.createIterator()
var_vars = iter_vars.Next()
while var_vars :
    nom = var_vars.GetName()
    if nom in new_pois:
        print 'We found',nom
        pois_new.add(var_vars)
    var_vars = iter_vars.Next()


mc_new =RooStats.ModelConfig("ModelConfiG", ws_new);
mc_new.SetPdf(ws_new.obj("simPDF"));
mc_new.SetParametersOfInterest(pois_new if poisplit else pois);
mc_new.SetNuisanceParameters(np_new);
mc_new.SetObservables(Obs_new);
mc_new.SetGlobalObservables(globObs_new);
getattr(ws_new,'import')(mc_new);

print '-------------------POIS-------------------'
pois.Print("v")
print '-------------------NUISANCE PARAMETERS-------------------'
np_new.Print("v");
print '-------------------OBSERVABLES-------------------'
Obs_new.Print("v");
print '-------------------GLOBAL OBSERVABLES-------------------'
globObs_new.Print("v");
ws_new.SaveAs('{1}/workspaces/{0}/combined/copy.root'.format(wstag,datdir))
