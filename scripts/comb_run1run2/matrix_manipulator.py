#!/usr/bin/env python
from ROOT import *
from commands import *
from collections import OrderedDict as odict
import time,sys,math,socket,os,copy

gROOT.LoadMacro("$WORKDIR/macros/AtlasStyle.C")
SetAtlasStyle()
gROOT.SetBatch(True)

def catname(fine):
    start,end = fine.find("_")+1,max(fine.find('_7TeV'),fine.find('_8TeV'))
    return TString(fine[start:end if end>0 else len(fine)])

def ws_datname(r2,asimov=False):
    if asimov:
        return 'asimovData_cnd1_{0}'.format('1' if r2 else '0.51')
    else:
        return 'obsData' if r2 else 'combData'

def ws_pdf(r2):
    return "simPdf" if r2 else 'combPdf'

def ws_roocat(r2):
    return "channelCat" if r2 else "master_measurement"

def print_maps(coefficients,mapfile,enplabels,unplabels,justprint=True):
    maps=["Coeffs_2DHisto_original","Coeffs_2DHisto_complem","Coeffs_2DHisto_zero"]
    f=TFile(mapfile)
    pdir = mapfile.replace('.root','_plots/')
    if not os.path.exists(pdir):
        os.makedirs(pdir)

    # for starters, vomit the histograms in our map file
    portrait = TCanvas(mapfile+'v',mapfile+'v',200,600)
    for m in maps:
        h=f.Get(m)
        h.Draw("colz text")
        portrait.Print('{0}{1}.pdf'.format(pdir,m))

    # for readability, since screens are landscape, effective NPs will be Y and unfolded are X; numbers are squished....whoops
        # landscape = TCanvas(mapfile+'p',mapfile+'p',600,200)
    morty=list(coefficients[l] for l in enplabels)
    plot_matrix(morty,enplabels,unplabels,'Coeffs_2DHisto_scaled',200,600,pdir)
    if justprint: sys.exit()
    return morty

def paolo_corrfull(run1nps,run2nps,noemu,jes_strong):
    if len(run1nps)!=len(run2nps):
        print 'You have different numbers of NPs from Run1 and Run2....'
    npbucket = run1nps+run2nps
    ntot,nr1 = len(npbucket),len(run1nps)

    # initialize ntot x ntot matrix
    corrfull = list(list(0.0 for x in range(ntot)) for y in range(ntot)) 
    for x in range(ntot):
        xisr1 = x<nr1
        for y in range(ntot):
            yisr1 = y<nr1
            if x==y:
                corrfull[x][y] = 1.0
            elif (xisr1 and not yisr1) and ismatch(npbucket[x],npbucket[y],noemu,jes_strong):
                corrfull[x][y] = 1.0
            elif (not xisr1 and yisr1) and ismatch(npbucket[y],npbucket[x],noemu,jes_strong):
                corrfull[x][y] = 1.0
    return corrfull

def paolo_map(run1map,run2map):
    nr1eff,nr1unfold = len(run1map),len(run1map[0])
    nr2eff,nr2unfold = len(run2map),len(run2map[0])
    run1half = list(one + list(0.0 for i in range(nr2unfold)) for one in run1map)
    run2half = list(list(0.0 for i in range(nr1unfold)) + two for two in run2map)
    map = run1half+run2half
    return map

def paolo_effeffmap(corrfull,effunfmap,nr2=7):
    # effeff_kl = Sum_i Sum_j MAP_ki CORR_FULL_ij MAP_lj
    nr1 = len(effunfmap)-nr2
    neff,nunf = len(effunfmap),len(corrfull)
    effeff = list(list(0.0 for q in range(neff)) for p in range(neff))
    for k in range(neff):
        for l in range(neff):
            for i in range(nunf):
                for j in range(nunf):
                    effeff[k][l] += effunfmap[k][i] * corrfull[i][j] * effunfmap[l][j]
    r1r1 = list(x[0:nr1] for x in effeff[0:nr1])
    r1r2 = list(x[nr1:] for x in effeff[0:nr1])
    r2r1 = list(x[0:nr1] for x in effeff[nr1:])
    r2r2 = list(x[nr1:] for x in effeff[nr1:])
    return effeff,r1r2

def plot_matrix(matrix,xlabels,ylabels,ourname,lenx=-99,leny=-99,pdir='./',r01=False):
    nx,ny,xl,yl = len(xlabels),len(ylabels),lenx,leny
    if lenx==-99:
        xl,yl=600,600.*ny/nx
    canvas = TCanvas(ourname+'c',ourname+'c',xl,int(yl))
    mine = TH2D(ourname+'h',ourname+'h',nx,-0.5,nx-0.5,ny,-0.5,ny-0.5)
    mine.GetXaxis().SetTickLength(0.0)
    mine.GetYaxis().SetTickLength(0.0)
    if r01: mine.GetZaxis().SetRangeUser(-1.0, 1.0);
    for y in range(ny): mine.GetYaxis().SetBinLabel(y+1,label_wipe(ylabels[y]))
    for x in range(nx): mine.GetXaxis().SetBinLabel(x+1,label_wipe(xlabels[x]))
    for x in range(nx):
        for y in range(ny):
            #print x,y,coefficients[enplabels[x]][y]
            content = matrix[x][y]
            mine.Fill(x,y,content)
    mine.Draw('colz text')
    canvas.Print('{0}{1}.pdf'.format(pdir,ourname))

def print_matrix(matrix):
    for row in matrix:
        print ' '.join(list('{:15.8}'.format(r) for r in row))
        #print ''.join(list('{:1}'.format(int(r)) for r in row))

def label_wipe(raw):
    return raw.replace("JET_","").replace("alpha_Sys","").replace("alpha_SysJET_Complete_JET_",'').replace('Jet','').replace('Effective','Eff')

def ismatch(run1,run2,noemu,jes_strong):
    match = False
    translation_hash = {'r1':'r2','_8TeV':''} #hash Run1 to Run2 equivalents; can be full names or fragments
    blacklist = ['_Stat']
    if noemu: blacklist+=['_Mu','_ESZee']
    if jes_strong:
        translation_hash.update({'Zjet_JVF':'LAr_Jvt'})
    else:
        blacklist+=['Zjet_Veto','Gjet_Veto','MJB_Beta','MJB_Threshold']
    r1 = copy.copy(run1)
    for thing in translation_hash:
        r1=r1.replace(thing,translation_hash[thing])
    match = r1==run2
    for b in blacklist:
        if b in run2:
            match = False
    return match

def JES_map_amplitudes(mapfile,amphist):
    f = TFile(mapfile)
    h = f.Get(amphist)
    nps = list(h.GetXaxis().GetBinLabel(i+1) for i in range(h.GetNbinsX()))
    amps = list(h.GetBinContent(i+1) for i in range(h.GetNbinsX()))
    tot=0
    for i in range(h.GetNbinsX()): tot+=amps[i]*amps[i]
    #for i in range(h.GetNbinsX()): print i+1,nps[i],amps[i],amps[i]/tot**0.5
    return nps,amps

def parse_JES_map(mapfile,effective_nps,maphistname="Coeffs_2DHisto_complem",qrun2=True,ampfile='',amphist='AllNPs_amplitude'): #original
    newconstraints=[]
    fprojections=TFile(mapfile)
    histo=fprojections.Get(maphistname)
    mapnps = list(histo.GetYaxis().GetBinLabel(i+1) for i in range(histo.GetNbinsY()))
    if fprojections.Get(amphist)==None:
        amplitudes = parse_JES_amplitudes(ampfile)
    else:
        ampnps,amplitudes = JES_map_amplitudes(mapfile,amphist)
        if ampnps != mapnps:
            print 'In file {0}: Amplitude histogram nps....'.format(mapfile)
            print '\n'.join(ampnps)
            print 'Do not match map histogram nps....'
            print '\n'.join(mapnps)
            print 'Figure this out....'
            sys.exit()

    # now that we've double checked the NP names, save them in the usual format
    newconstraints=list("alpha_SysJET_Complete_"+name.split("AntiKt4EMTopo_JET_")[1]+ ('' if qrun2 else '_8TeV') for name in mapnps)
    
    coefficients = dict()
    nold,nnew=histo.GetNbinsY(),len(effective_nps)
    if len(amplitudes)!=nold:
        print 'You will run into trouble because your JES map in {0} does not have the same number of NPs as your amplitude information file {1}.  Fix this.'.format(mapfile,ampfile) 
        sys.exit()
    if nnew!=histo.GetNbinsX():
        print 'WARNING! You have loaded a map with {0} effective NPs but have hardcoded in {1}'.format(histo.GetNbinsX(),len(effective_nps))
    print ''
    for i in range(nnew):
        #print ' '.join(list(str('{:7.3}'.format(histo.GetBinContent(i+1,ibin+1))) for ibin in range(4)))

        if "restTerm" in effective_nps[i]:
            raw_row=[]
            # assuming the last row isn't actully a bunch of zeros for the "restTerm" NP but actually 1 - quad_sum_others 
            for ibin in range(nold):
                derp=0
                for wut in range(nnew-1):
                    derp+=histo.GetBinContent(wut+1,ibin+1)*histo.GetBinContent(wut+1,ibin+1)
                raw_row.append(math.sqrt(1-derp))
            coefficients.update({effective_nps[i]:list(raw_row[ibin]*amplitudes[ibin] for ibin in range(nold))}) 
                #print str('{:7.3}'.format(derp)),
                #print 'coefficient',ibin,derp,'*',float(amplitudes[ibin][-2]),float(amplitudes[ibin][-1])
        else:
            coefficients.update({effective_nps[i]:list(histo.GetBinContent(i+1,ibin+1)*amplitudes[ibin] for ibin in range(nold))}) 
            #print ' '.join(list(str('{:7.3}'.format(histo.GetBinContent(i+1,ibin+1))) for ibin in range(4)))

    # calculat and divide by a coefficient divisor for proper normalization
    for c in coefficients:
        tot=0
        for n in coefficients[c]: 
            tot+=n*n
        divisor = math.sqrt(tot)
        for rick in range(len(coefficients[c])): 
            coefficients[c][rick]=coefficients[c][rick]/divisor

    fprojections.Close();
    return coefficients,newconstraints,nold



def parse_JES_amplitudes(fname='outcomes.txt'):
    stagione = file(fname,'r').read().split('\n')
    """
    #File looks like:
    -------------
    JESComponent.3.SubComp: ['Gjet_Jvt', 'Gjet_Jvt_AntiKt4Topo_EMJES_1D', 0.2967599999999978]
    JESComponent.3.SubComp: ['Zjet_Jvt', 'Zjet_Jvt_AntiKt4Topo_EMJES_1D', 0.35584499999999625]
    JESComponent.3.SubComp: 0.652605
    JESComponent.3.SubComp: 0.034254582886
    -------------
    JESComponent.4.Name: ['Zjet_KTerm', 'Zjet_KTerm_AntiKt4Topo_EMJES_1D', 2.340529]
    JESComponent.4.Name: 2.340529
    JESComponent.4.Name: 0.122852023242
    -------------
    """
    primavera = dict()
    for s in stagione:
        if s[0]!='-':
            estate = s.split(':')
            autunno = int(estate[0].split('.')[1])-1 #the NP number with proper array indexing so this behaves like an array
            if autunno in primavera:
                primavera[autunno]+=estate[1:]
            else:
                primavera[autunno]=estate[1:]
    return list(float(p[-2]) for p in primavera) # we don't care about relative amplitudes

if __name__ == "__main__":
    datdir = '/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct' if 'harvard' in socket.gethostname() else '/afs/cern.ch/work/s/stchan/public/hcomb'
    
    # run1 and run2 nps and map files
    enps_r1,enps_r2 = list('alpha_SysJetNP{0}_Y2012_8TeV'.format(i+1) for i in range(5)), list('alpha_SysJET_21NP_JET_EffectiveNP_{0}'.format(i+1) for i in range(7))
    fr1map,fr2map='{0}/ProjectionCoefficientFile_Final2012_test_withAmplitudes_April29.root'.format(datdir),'{0}/ProjectionCoefficientFile_Moriond2017_test_withAmplitudes_April29.root'.format(datdir)

    # pull information from the map file
    coeffs_r1,newconstrains_r1,noldnps_r1=parse_JES_map(mapfile=fr1map,effective_nps=enps_r1,qrun2=False)
    coeffs_r2,newconstrains_r2,noldnps_r2=parse_JES_map(mapfile=fr2map,effective_nps=enps_r2,qrun2=True)

    mm_r1=print_maps(coeffs_r1,fr1map,enps_r1,newconstrains_r1,justprint=False)
    mm_r2=print_maps(coeffs_r2,fr2map,enps_r2,newconstrains_r2,justprint=False)

    for strong in [True,False]:
        for noemu in [True,False]:
            descriptor = ('jes-strong' if strong else 'jes-weak') + ('_noemu'  if noemu else '_emu')
            big,small=paolo_effeffmap(paolo_corrfull(newconstrains_r1,newconstrains_r2,noemu=noemu,jes_strong=strong),paolo_map(mm_r1,mm_r2),nr2=len(enps_r2))
            plot_matrix(big,enps_r1+enps_r2,enps_r1+enps_r2,'Coeffs_2DHisto_effeff_'+descriptor,pdir='/afs/cern.ch/work/s/stchan/public/hcomb/ProjectionCoefficientFile_plots/',lenx=600,leny=300,r01=True)
            plot_matrix(small,enps_r1,enps_r2,'Coeffs_2DHisto_r1effr2eff_'+descriptor,pdir='/afs/cern.ch/work/s/stchan/public/hcomb/ProjectionCoefficientFile_plots/',r01=True)
