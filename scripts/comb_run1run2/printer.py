from ROOT import *
from commands import *
import time,sys,socket
import matrix_manipulator as mm
debug=True
isrun2=len(sys.argv)==1 #the default put some blah blah after to make it run1
wstag = 'Run2WS-20170421' if isrun2 else 'Run1WS'
datdir = '/n/atlasfs/atlascode/backedup/stchan/vhbb/maker_ws/wsmct' if 'harvard' in socket.gethostname() else '/afs/cern.ch/work/s/stchan/public/hcomb'

f=TFile('{1}/workspaces/{0}/combined/jes-unfold.root'.format(wstag,datdir))
print 'Showing File details for',f.GetName()
ws      = f.Get("combined")
data    = ws.data(mm.ws_datname(isrun2))
mc      = ws.obj("ModelConfig");
pois    = mc.GetParametersOfInterest()
np      = mc.GetNuisanceParameters()
globObs = mc.GetGlobalObservables()
Obs     = mc.GetObservables()
PDF     = mc.GetPdf()

iterPDF=PDF.serverIterator();
iPDF=iterPDF.Next()
categories={}
while iPDF:#Loop over the categories
    if debug: print iPDF.GetName(),iPDF.GetTitle(),iPDF.ClassName()
    if "RooCategory" not in iPDF.ClassName():
        newlist=RooArgList()
        subpdf=iPDF.pdfList().createIterator()
        isubpdf=subpdf.Next()
        while isubpdf: #Loop over the PDF blocks for each categories
            if debug: print isubpdf.GetName(),isubpdf.GetTitle(),isubpdf.ClassName()
            if "RooRealSumPdf" in isubpdf.ClassName() and debug:
                print 'Looping through sub pdf...'
                isubpdf.Print("v")
                functions=isubpdf.funcList().createIterator()
                ifunc=functions.Next()
                while ifunc:
                    print ifunc.GetName(),ifunc.GetTitle()
                    ifunc.Print("v")
                    ifunc=functions.Next()
                coef=isubpdf.coefList().createIterator()
                icoef=coef.Next()
                while icoef:
                    print icoef.GetName(),icoef.GetTitle()
                    icoef=coef.Next()
            isubpdf=subpdf.Next()
    elif debug:
        print 'Okay, skip the RooCategory...'
        iPDF.Print("v")
    iPDF=iterPDF.Next()
    if debug: print "-----------"
print "\n\n\n\n"
pois.Print("v")
globObs.Print("v");
Obs.Print("v")
