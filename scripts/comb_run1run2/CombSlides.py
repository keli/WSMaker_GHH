import combCC,NicePlot_SimpleMu as valerio
import SlideMaker,os,argparse,subprocess,shutil,glob,sys
from ROOT import *
from collections import OrderedDict

"""
For the correlation bullshit you somehow can only use ROOT and not numpy for
and for some reason only works in cint becuase TMatrixD makes ROOT in python shit itself:

you have a script root_macro.C
replace text at run time in two functions

ONE which is a vector of strings (so "mode9" in the python script you killed) that are keys for...
TWO a map to vector<double> that are the numbers you would have fed in anyway
     --for this you want to just write
     --object["keyname"]=std::vector<double>{first,second};
     --over and over
"""
class CombSlides(OrderedDict):
    def __init__(self, *args, **kwargs):
        super(CombSlides,self).__init__()
        self.update(r1_outv='r1',r2_outv='run2',comb_outv='delta',directory='./results-summaries/',combauxkeys=[],r2key='run2',run1a=False,test_outv='test',x_outv='x',obs=False)
        
        #set defaults
        self.update(kwargs)
        if self['directory'][len(self['directory'])-1] != '/':
            self['directory']+='/'
        self['directory']+=('-'.join([self['r1_outv'],self['r2_outv'],self['comb_outv'],self['test_outv'],self['x_outv']])+'/')
        if not os.path.exists(self['directory']):
            os.makedirs(self['directory'])
        if self['r2_outv']=='ichep': 
            self.r2key='ichep' 

        self.slides = SlideMaker.SlideMaker(directory=self['directory'],file_name='r1_{0}-r2_{1}-comb_{2}-test_{3}-x_{4}.tex'.format(self['r1_outv'],self['r2_outv'],self['comb_outv'],self['test_outv'],self['x_outv']))
        self.results=dict()
        self.bdcats={'run1':dict(),'run2':dict(),'combined':dict(),'test':dict(),'x':dict()}
        self.bdnums={'run1':dict(),'run2':dict(),'combined':dict(),'test':dict(),'x':dict()}
        self.import_results()
        self.slides.front_matter('comb-fit-results')

    def mutype_h(self,mutype):
        if mutype=='':
            return 'Combination'
        return mutype.replace('_L0','0 lep').replace('_L1','1 lep').replace('_L2','2 lep').replace('_78TeV',' Run1').replace('_13TeV',' Run2')

    def valerio_value(self,rtype,inv,outv,basekey):
        value=[['Combination',0, 1.0, 0.0, 0.0, 0.0, 0.0, self.bd_files(basekey)[0],1]]
        for mu in self.poi_mus(inv):
            value.append([self.mutype_h(mu),0, 1.0, 0.0, 0.0, 0.0, 0.0, self.bd_files2(rtype,inv,outv,mu)[0],1])
        return value

    def valerio_nice_plot(self,rtype,inv,outv,baseinv,tag='',quick=False):
        if not quick:valerio.valerio_nice_plot(self.valerio_value(rtype,inv,outv,self.key(rtype,baseinv,outv,'breakdown')),self['directory'],tag)
        return './Plot_mu_'+tag+'.pdf'

    def is_number(self,string):
        try:
            float(string)
            return True
        except ValueError as yup:
            return False

    def poi_mus(self,inv):
        mutypes=['']
        if 'poi' in inv:
            mutypes = ['_78TeV','_13TeV']
            if 'poi-012lep' in inv:
                if 'partial' in inv:
                    mutypes = ['_L0','_L1','_L2']
                else:
                    mutypes = ['_L0_78TeV','_L1_78TeV','_L2_78TeV','_L0_13TeV','_L1_13TeV','_L2_13TeV']
            elif 'poi-whzh' in inv:
                mutypes = ['WH','ZH']
            elif 'poi-0lep' in inv:
                mutypes = ['_L0']
            elif 'poi-1lep' in inv:
                mutypes = ['_L1']
            elif 'poi-2lep' in inv:
                mutypes = ['_L2']
        return mutypes

    def import_results(self):
        files=[self['directory']+'../run1_{0}-results.txt'.format(self['r1_outv']),
               self['directory']+'../{1}_{0}-results.txt'.format(self['r2_outv'],self['r2key']),
               self['directory']+'../test_{0}-results.txt'.format(self['test_outv']),
               self['directory']+'../x_{0}-results.txt'.format(self['x_outv']),
               self['directory']+'../combined_{0}-results.txt'.format(self['comb_outv'])]
        if len(self['combauxkeys'])>0:
            files+=list(self['directory']+'../combined_{0}-results.txt'.format(k) for k in self['combauxkeys'])
        for f in files:
            if not os.path.exists(f):
                print 'No results file ',f
                continue
            ok=file(f,'r')
            lines=ok.read().split('\n')
            for l in lines:
                words = l.split(' ')
                # end cases--this means nothing interesting
                if len(words)<2:
                    continue
                else:
                    key=words[0]
                    if 'combined_' in f:
                        ckey=f.split('combined_')[-1].split('-results')[0]
                        if ckey in self['combauxkeys']:
                            print key,'will replace/supplement default result from outv',self['comb_outv']
                            key=key.replace(ckey,self['comb_outv'])
                    self.results[key]=words[1:]
            ok.close()
        # now fetch breakdown results for each of the cases
        # starting with Run1
        for inv in combCC.in_versions(combCC.types['run1'],allobs=self['obs'],runv='run1'):
            self.bd_hash('run1',inv,self['r1_outv'])
        for inv in combCC.in_versions(combCC.types['test'],allobs=self['obs'],runv='test'):
            self.bd_hash('test',inv,self['test_outv'])
        for inv in combCC.in_versions(combCC.types['x'],allobs=self['obs'],runv='x'):
            self.bd_hash('x',inv,self['x_outv'])
        for inv in combCC.in_versions(combCC.types[self['r2key']],allobs=self['obs'],runv=self['r2key']):
            self.bd_hash(self['r2key'],inv,self['r2_outv'])
        for inv in combCC.in_versions(combCC.types['combined'],allobs=self['obs']):
            self.bd_hash('combined',inv,self['comb_outv'])

    def intype(self,scheme,full,rtype='combined'):
        return combCC.input_version(-1 if self['obs'] else 1,scheme,full_corr=full,mur1=-1 if self['obs'] else -0.51,runtype=rtype)

    def key(self,rtype,inv,outv,result,suffix=''):
        return rtype+'-'+inv+'.'+outv+'-'+result+('' if suffix == '' else '-')+suffix

    def limit_tex(self,rtype,inv,outv,ltype):
        turn = self.key(rtype,inv,outv,'limit',ltype)
        if turn not in self.results:
            #if ltype=='obs': return self.limit_tex(rtype,inv,outv,ltype='exp')
            return 'N/A'
        ok=self.results[turn]
        return '${0:.3}^{{+{1:.3}}}_{{-{2:.3}}}$'.format(float(ok[0]),float(ok[1]),abs(float(ok[2]))) if ltype=='exp' else '{0:.3}'.format(float(ok[0]))

    def sig_tex(self,rtype,inv,outv,stype,isp0=False):
        turn = self.key(rtype,inv,outv,'sig',stype)
        if turn not in self.results:
            if stype=='exp':
                turn = self.key(rtype,inv,outv,'sig','obs') #return self.sig_tex(rtype,inv,outv,stype='exp')
            if turn not in self.results:
                return 'N/A'
        # contents are (observed,expected,injected) x (sig,p0)
        ok=self.results[turn]
        if isp0:
            return ok[(3 if stype=='obs' else 4)]
        else:
            return '{0:.4}'.format(float(ok[(0 if stype=='obs' else 1)]))

    def rank_plots(self,rtype,inv,outv):
        turn = self.key(rtype,inv,outv,'ranks')
        if turn not in self.results:
            return []
        # contents are workspace; failed jobs
        plots,cable,w,x23 = self.results[turn][1:],[],self.results[turn][0],['there-is-no-prefit.pdf','there-is-no-postfit.pdf']
        w2=w.replace('.','-')
        postfit,prefit=w2+'_pulls_125',w2+'_pulls_prefit_125'
        for p in plots:
            paper=p[p.rfind('/')+1:]
            clip=paper.replace('.','-',paper.count('.')-1)#replace all periods except the final one (file extension) so beamer doesn't complain
            shutil.copy2(p,self['directory']+clip)
            cable.append(clip)
        #for end in ['.png','.eps','.pdf']:
        for end in ['.eps','.pdf']:
            if prefit+end in cable:
                x23[0]=prefit+end
            if postfit+end in cable:
                x23[1]=postfit+end
        return x23

    def bd_files2(self,rtype,inv,outv,mutype):
        turn = self.key(rtype,inv,outv,'breakdown')
        if not mutype in self.poi_mus(inv):
            print '{0} is not coded to exist for {1}'.format(mutype,inv)
            return 'N/A','N/A'
        return self.bd_files(turn,mutype)

    def bd_files(self,turn,mutype=''):
        #turn = self.key(rtype,inv,outv,'breakdown')
        if turn not in self.results:
            return 'N/A','N/A'
        txt,tex=self.results[turn][0],self.results[turn][1]
        txtf=txt[0:txt.rfind('SigXsecOverSM')]+'SigXsecOverSM{0}.txt'.format(mutype)
        texf=tex[0:tex.rfind('SigXsecOverSM')]+'SigXsecOverSM{0}.tex'.format(mutype)
        if not os.path.exists(txtf):
            modestr=txtf[txtf.find('_mode'):txtf.find('_',txtf.find('_mode')+1)]
            goodenough=False
            for moder in combCC.bd_mode_precedence('poi'):
                newmode='_mode{0}'.format(moder)
                if os.path.exists(txtf.replace(modestr,newmode)):
                    txtf=txtf(modestr,newmode)
                    texf=texf(modestr,newmode)
                    goodenough=True
                    break
            if not goodenough:
                return 'N/A','N/A'
        return [txtf,texf]

    def bd_hash(self,rtype,inv,outv):
        turn = self.key(rtype,inv,outv,'breakdown')
        if turn not in self.results:
            return 'N/A'

        # okay, if it's a poi thing, there are multiple breakdowns
        mutypes=self.poi_mus(inv)
        for mew in mutypes:
            invo = inv+mew
            txtf,texf = self.bd_files(turn,mew)
            if txtf=='N/A':
                continue
            # entry is text file, tex file, missing result workspace names
            # first look to see if a muhat value is calculated (would be stored in text file)
            proc = subprocess.Popen(['grep','-a',"muhat",txtf], stdout=subprocess.PIPE) # -a lets grep read a file it thinks is binary
            proc.wait()
            buffer = proc.stdout.read()
            if 'muhat:' in buffer:
                mu = float(buffer.split(':')[1])
                self.bdcats[rtype][invo+'muhat']=mu
                self.bdnums[rtype][invo+'muhat']=[float(mu)]
        
            # now let's see if an nll_val_true values is there
            proc = subprocess.Popen(['grep','-a',"nll_val_true",txtf], stdout=subprocess.PIPE) # -a lets grep read a file it thinks is binary
            proc.wait()
            buffer = proc.stdout.read()
            if 'nll_val_true:' in buffer:
                nll = float(buffer.split(':')[1])
                self.bdcats[rtype][invo+'nll_val_true']=nll
                self.bdnums[rtype][invo+'nll_val_true']=[float(nll)]
            readme=file(texf,'r')
            lines,i=readme.read().split('\n'),0
            while i<len(lines) and lines[i]!='*' and lines[i]!='\\end{tabular}':
                words = lines[i].split('&')
                i+=1
                if len(words)<2:
                    continue
                else:
                    # lines in tex files look like (cat) & ($table_entry$) \\
                        # strip numbers from here instead of txt file because guaranteed precision here (text files clip for readability)
                    old = '$' in lines[i]
                    cat=words[0].strip(' ')
                    if old:
                        tex,cat=words[1][:words[1].rfind('$')+1]
                    else:
                        #then it looks like "CAT & +NUM / -NUM \\
                        tex=words[1][:words[1].rfind('\\')]
                    if tex=='' or 'Set of nuisance' in cat or 'parameters' in cat:
                        continue
                    self.bdcats[rtype][invo+cat]=tex.replace('\\','').replace('hline','')
                    if old:
                        plus,minus=tex.rfind('+')+1,tex.rfind('-')+1
                        plue,minue=tex.find('}',plus),tex.find('}',minus)
                        self.bdnums[rtype][invo+cat]=[float(tex[plus:plue]),float(tex[minus:minue])]
                    else:
                        self.bdnums[rtype][invo+cat]=list(999 if ('nan' in bowl or 'inf' in bowl) else float(bowl.replace(' ','').replace('\\','').replace('hline','')) for bowl in tex.split('/'))
            readme.close()
        return None

    def scale_to_corr(self,qzero,rho,r1,r2):
        return (qzero*qzero - 2*rho*r1*r2)**0.5

    def pull_ws(self,rtype,inv,outv):
        turn = self.key(rtype,inv,outv,'pulls')
        if turn not in self.results:
            return 'N/A'
        elif len(self.results[turn])<1:
            return 'N/A'
        return self.results[turn][0]

    def finish(self):
        print 'Look for your results in ',self['directory']
        self.slides.back_matter()

    def syst_cats(self):
        return ['muhat','FullSyst','DataStat','Jets','MET','BTag b','BTag c','BTag light','Leptons','Luminosity','Diboson','Model Zjets','Zjets flt. norm.','Model Wjets','Wjets flt. norm.','Model ttbar','ttbar flt. norm.','Model Single Top','Model MultiJet','Signal Systematics','MC stat']

    def model_plot_hash(self):
        # format 'human readable name':[ [Run1 tags], [Run2 tags] ]
        return {'Diboson': [['SysZZMbb','SysWZMbb'],['mBB__ZZ','mBB__WZ']],
                'V+jets Mbb': [['SysZMbb','SysWMbb','SysWbbPtW'], ['SysZMbb','SysWMbb','SysWPtV']],
                'ttbar MBB': [['SysTtbarMBB'],['SysTTbarMBB_']],
                'ttbar PtV': [['SysTtbarPtV'],['SysTTbarPTV_']],
                'Single Top': [['SysTopMbb','SysWtPt'],['SysStopWtMBB','SysStoptMBB','SysStopWtPTV']]}

    def model_syst_cats(self):
        return ['Diboson','Model Zjets','Zjets flt. norm.','Model Wjets','Wjets flt. norm.','Model ttbar','ttbar flt. norm.','Model Single Top','Model MultiJet','Signal Systematics']

    def nps_in_ws(self,npfile='./509sorted_nps.txt'):
        f=file(npfile,'r')
        lines = f.split('\n')
        chammoix = {'7TeV':[],'8TeV':[],'13TeV':[]}
        for l in lines:
            last_score = l.rfind('_')
            np,e = l[0:last_score],l[last_score+1:]
            if e in chammoix:
                chammoix[e].append(np)
            else:
                print 'Weird energy',e
        return chammoix

    def trim_cas_name(self,nom):
        return nom.replace('.pdf','').replace('f_','').replace('m_','').replace('_fit','').replace('_','')

    def modeling_plot_slides2(self):
        pdir_fmt='/afs/cern.ch/work/s/stchan/public/hcomb/cas_dump/plots_TwoLep_{0}TeV/'
        pd1,pd2=pdir_fmt.format('8'),pdir_fmt.format('13')
        plots1,plots2=os.listdir(pd1),os.listdir(pd2)
        dest_fmt=self['directory']+'CS_TwoLep_{0}TeV/'
        dd1,dd2=dest_fmt.format('8'),dest_fmt.format('13')
        if not os.path.exists(dd1):
            os.makedirs(dd1)
        if not os.path.exists(dd2):
            os.makedirs(dd2)
        cipher=self.model_plot_hash()
        for c in cipher:
            chop,stick=[],[]
            for p in plots1:
                for key in cipher[c][0]:
                    if key in p:
                        # FIX ME!
                        shutil.copy2(pd1+p,dd1+p)
                        chop.append([self.trim_cas_name(p),dd1.replace(self['directory'],'./')+p])
            for p in plots2:
                for key in cipher[c][1]:
                    if key in p:
                        shutil.copy2(pd2+p,dd2+p)
                        stick.append([self.trim_cas_name(p),dd2.replace(self['directory'],'./')+p])
            self.slides.slides_for_images(images=chop, tag='{0}, Run1'.format(c))
            self.slides.slides_for_images(images=stick,tag='{0}, Run2'.format(c))

    def modeling_plot_slides(self,topdir='/afs/cern.ch/work/s/stchan/public/hcomb/cas_dump/'):
        interesting_types = ['Jets','MET','BTag b','BTag c','BTag light','Leptons','Luminosity','Diboson','Model Zjets','Zjets flt. norm.','Model Wjets','Wjets flt. norm.','Model ttbar','ttbar flt. norm.','Model Single Top','Model MultiJet','Signal Systematics','MC stat']
        interesting_types = ['Model Wjets','Model Zjets']
        es,ls=['8TeV','13TeV'],['TwoLep']
        for e in es:
            for l in ls:
                sourcedir = topdir+'plots_{0}_{1}/'.format(l,e)
                writedir  = self['directory']+'CS_{0}_{1}/'.format(l,e)
                if not os.path.exists(writedir):
                    os.makedirs(writedir)
                plot_hash = self.sort_plots(os.listdir(sourcedir),interesting_types,energy=e)
                for t in plot_hash:
                    if len(plot_hash[t])==0:
                        continue
                    # copy plots to our usual tardir
                    for p in plot_hash[t]:
                        shutil.copy2(sourcedir+p,writedir+p)
                    # now make the slides
                    self.slides.slides_for_images(images=list([p[0:p.rfind('.')].replace('_',' '),'./CS_{0}_{1}/{2}'.format(l,e,p)] for p in plot_hash[t]),tag='{0}, {1}: {2}'.format(e,l,t))
        
    def sort_plots(self,plots,cats,energy=''):
        bucket = dict(list((c,[])for c in cats))
        for p in plots:
            type = self.syst_type(p)
            if type in cats and 'Sys' in p:
                bucket[type].append(p)
        killme=[]
        for b in bucket:
            if len(b)==0: killme.append(b)
        for k in killme:
            del bucket[k]
        return bucket

    def syst_type(self,syst):
        # taking the mode 9 logic and only taking the exclusive (i.e. non-composite) NP categories
        cat_hash=dict()
        cat_hash["Jets"]=["SysJET","SysJet","JVF","SysFATJET","SysJER"]
        cat_hash["MET"]=["SysMET"]
        cat_hash["BTag b"]=["SysFT_EFF_Eigen_B","BTagB"]
        cat_hash["BTag c"]=["SysFT_EFF_Eigen_C","BTagC","SysFT_EFF_extrapolation_from_charm","TruthTagDR"]
        cat_hash["BTag light"]=["SysFT_EFF_Eigen_Light","BTagL","SysFT_EFF_extrapolation_1"]
        cat_hash["Leptons"]=["SysEG","SysEL","SysMUON","Elec","Muon","LepVeto"]
        cat_hash["Luminosity"]=["LUMI"]
        cat_hash["Diboson"]=["SysVV","WW","ZZ","WZ"]
        cat_hash["Model Zjets"]=["ZDPhi","ZcDPhi","ZbDPhi","sZMbb","ZPt","Zcl","sZbb","Zl","Zbc","Zbl","Zhf","Zcc"]
        cat_hash["Zjets flt. norm."]=["norm_Z"]
        cat_hash["Model Wjets"]=["WJet","WDPhi","WMbb","WbbMbb","WPt","sWbb","Wbc","Wbl","Whf","Wcc","WclNorm","Wl"]
        cat_hash["Wjets flt. norm."]=["norm_W"]
        cat_hash["Model ttbar"]=["TtBar","Ttbar","ttBarHigh","TopPt","TTbarPTV","TTbarMBB","TopMBB","sttbar"]
        cat_hash["ttbar flt. norm."]=["norm_ttbar"]
        cat_hash["Model Single Top"]=["stop","Stop","SysWt"]
        cat_hash["Model Multi Jet"]=["MJ","Multijet"]
        cat_hash["Signal Systematics"]=["Theory","SysVH","BR"]
        cat_hash["MC stat"]=["gamma"]

        for c in cat_hash:
            for dog in cat_hash[c]:
                if dog in syst:
                    return c
        return 

    def chi_script_syst_names_numbers(self,r1,i1,r2,i2):
        numbers,names='',[]
        one,two=self.bd_numbers(r1,i1),self.bd_numbers(r2,i2)
        for syst in self.syst_cats():
            if syst in one and syst in two:
                numbers+='  claude["{0}"]={{{1},{2}}};\n'.format(syst,one[syst],two[syst])
                if syst not in ['muhat','FullSyst','DataStat']:
                    names.append(syst)
        # if there is no muhat calculated, just assume the standard asimov values (Run1 result for Run1, 1.0 for Run2)
        if 'muhat' not in one or 'muhat' not in two:
                numbers+='  claude["muhat"]={0.51,1.0};\n'
        return '"{0}"'.format('","'.join(names)),numbers

    def generate_chi_script(self,r1,i1,r2,i2,template='./scripts/VHbbRun2/comb_run1run2/template_macro.C'):
        text = file(template,'r').read()
        nom,num = self.chi_script_syst_names_numbers(r1,i1,r2,i2)
        text=text.replace('!!!',nom)
        text=text.replace('***',num)
        tag = '{0}_{1}_{2}_{3}'.format(r1,i1,r2,i2)
        tag=tag.replace('.','').replace('-','')
        text=text.replace('&&&',self['directory']+tag)
        text=text.replace('@@@',tag+'Combination')
        filename=self['directory']+tag+'Combination.C'
        nova = file(filename,'w')
        nova.write(text)
        nova.close()
        return filename,self['directory']+tag+'_tables.txt'

    def generate_chi_tables(self,r1,i1,r2,i2,template='./scripts/VHbbRun2/comb_run1run2/template_macro.C',quick=False,body=True):
        run,tbls = self.generate_chi_script(r1,i1,r2,i2,template)
        if not os.path.exists(tbls) or not quick:
            gROOT.ProcessLine('.x '+run)
        tables = file(tbls,'r').read().split('~~~')
        collabels = ['Comb $\\mu$',  '$\\sigma_{\\mu}$', '$\\chi^2$']
        collabelr = ['Comb $\\left|\\Delta\\mu\\right|$',  '$\\left|\\Delta\\sigma_{\\mu}\\right|$', '$\\left|\\Delta\\chi^2\\right|$']
        for t in tables:
            rows = t.split('\n')
            rows.remove('')
            if len(rows)<3:
                continue
            cat = rows[0]
            if cat not in self.model_syst_cats():
                continue
            tablerows = list(r.split()[1:] for r in rows[2:])
            rowlabels = list('$\\rho$={0}'.format(r.split()[0]) for r in rows[2:])
            #self.slides.table_slide(tablerows,collabels,rowlabels,'{0}+{1} {2} Correlation Projections'.format(r1,r2,cat),scrub=False)
            tablehash = dict(list((r.split()[0],list(float(q) for q in r.split()[1:])) for r in rows[2:]))
            relrows=[]
            for r in rowlabels:
                key=r[r.find('=')+1:]
                if key=='0':
                    relrows.append(list('---' for c in collabels))
                else:
                    relrows.append(list('{0:.3} ({1:.3}\\%)'.format(abs(tablehash[key][c]-tablehash['0'][c]),100.*abs(tablehash[key][c]-tablehash['0'][c])/abs(tablehash['0'][c])) for c in range(3)))
            #self.slides.table_slide(relrows,collabelr,rowlabels,'{0}+{1} {2} Correlation Projections (Relative)'.format(r1,r2,cat),scrub=False)
            #custom tables
            # delta mu (no % ) | sigma | delta sigma (with %) | chi2 |
            colcustom = ['$\\left|\\Delta\\mu\\right|$', '$\\sigma$', '$\\left|\\Delta\\sigma\\right|$', '$\\chi^2$']
            rowcustom = []
            for r in rowlabels:
                key=r[r.find('=')+1:]
                if key=='0':
                    rowcustom.append(['---',
                                     tablehash[key][1],
                                     '---',
                                     tablehash[key][2] ])
                else:
                    
                    rowcustom.append(['{0:.3}'.format(abs(tablehash[key][0]-tablehash['0'][0])),
                                     tablehash[key][1],
                                     '{0:.3} ({1:.3}\\%)'.format(abs(tablehash[key][1]-tablehash['0'][1]),100.*abs(tablehash[key][1]-tablehash['0'][1])/abs(tablehash['0'][1])),
                                     tablehash[key][2] ])
            self.slides.table_slide(rowcustom,colcustom,rowlabels,'{0}+{1} {2} Correlation Projections'.format(r1,r2,cat),scrub=False,body=body)

    def bd_numbers(self,run,inv):
        numeros=dict()
        for kind in self.bdnums[run]:
            if kind.find(inv)==0:
                shaw,root=0,len(self.bdnums[run][kind])
                for n in self.bdnums[run][kind]:shaw+=abs(n)
                numeros[kind.split(inv)[1]] = shaw*1.0/root
        return numeros

    def bd_str(self,run,cat):
        if run not in self.bdcats:
            return 'N/A'
        elif cat not in self.bdcats[run]:
            return 'N/A'
        else:
            return self.bdcats[run][cat]

    def bd_floats(self,run,cat):
        if run not in self.bdnums:
            if 'muhat' in cat or 'nll_val_true' in cat:
                return [-1]
            else:
                return [5,5]
        elif cat not in self.bdnums[run]:
            if 'muhat' in cat or 'nll_val_true' in cat:
                return [-1]
            else:
                return [5,5]
        else:
            return self.bdnums[run][cat]

    def nll_compatibility(self,singLLR,otherLLRs):
        nDGF,tot,npts = len(otherLLRs)-1,0,len(otherLLRs)
        for n in otherLLRs:
            if self.is_number(n):
                tot+=n
            else:
                npts-=1
        multLLR=tot/float(len(otherLLRs))
        #print multLLR
        compatibility=TMath.Prob(2*(singLLR-multLLR),nDGF)
        return compatibility


    def poi_slides(self,quick):
        base=combCC.input_version(-2,'jes',False,mur1=-9)
        r12_key=combCC.input_version(-2,'poi-comp',False,mur1=-9)
        lep_key=combCC.input_version(-4,'poi-012lep',False,mur1=-9)
        lepf_key=combCC.input_version(-4,'poi-012lep',True,mur1=-9)
        wzh_key=combCC.input_version(-3,'poi-whzh',False,mur1=-9)
        outv=self['comb_outv']

        descriptions=[[wzh_key,'WH/ZH: {0:.2f}\\%','WHZH'],[lep_key,'Leptons: {0:.2f}\\%','channels'],[r12_key,'Run1/Run2: {0:.2f}\\%','r1r2'],[lepf_key,'Run1/Run2 leptons: {0:.2f}\\%','r1r2channels']]

        # nll/muhat comp
        for i,bucket in  enumerate(list([typ[0],self.poi_mus(typ[0])] for typ in descriptions)):
            columnss = ['combined']+list(s[s.find('_')+1:] for s in bucket[1])
            rowss = ['nll val true','$\\hat{\\mu}$']
            nums = list(list(self.bd_str('combined',s+val) for s in [base]+list(bucket[0]+b for b in bucket[1])) for val in ['nll_val_true','muhat']) 
            compatability = self.nll_compatibility(nums[0][0],nums[0][1:])
            descriptions[i][1]=descriptions[i][1].format(compatability*100)
            self.slides.table_slide(nums,columnss,rowss,"NLL's {0} Compatability: {1:.3f}".format(bucket[0][bucket[0].find('poi-')+4:bucket[0].find('_partial')],compatability),smaller=1 if len(columnss)>4 else 0)

        #now for the plots
        plots = list([d[1],self.valerio_nice_plot('combined',d[0],outv,base,outv+'_'+d[2],quick)] for d in descriptions)

        self.slides.image_slide([plots[0],plots[1]],'Muhat Nice Plots 2/3 POI')
        self.slides.image_slide([plots[2],plots[3]],'Muhat Nice Plots R1/R2')
        #self.slides.image_slide([['WH/ZH','./Muhat_MVA-7-8-13TeV_{0}_WHZH.pdf'.format(outv)]],'Muhat Nice Plots',width=0.47)
        return

    def nice_plot_str(self,run,cat):
        #need: muhat, tot+, tot-, stat+, stat-, syst+, syst-
        nums=[]
        for n in ['muhat','Total','DataStat']: #full syst is not necessary because the script calculates it with subtraction in quadrature
            nums+=list(str(abs(s)) for s in self.bd_floats(run,cat+n))
        return ','.join(nums)

    def summary_table(self,cols,bdcats=[],quick=False):
        # table arrangement:
        # rows: stats (so sigs, limits, breakdown categories)
        # columns: different cases (e.g. mu's or correlation schemes)

        # for automatic versions, the first entries will always be:
        # Run1, Run2, Run1+Run2 comb, uncorr (R1+(R2,cnd,mu=1))
        #colkeys={'Run1':['run1',self.intype('normal',True),self['r1_outv']], # Run1 take as observed
         #        'Run2':[self['r2key'],self.intype('normal',True),self['r2_outv']]} # Run2 and combined take as conditional mu=1
          
        # for this round, we'll have mu (normal) and mu+JES (full) as the starters
        #colkeys=[['Comb-nocorr','combined',self.intype('normal',-1),self['comb_outv']],
        colkeys=[]#[['No Corr','combined',self.intype('normal',True),self['comb_outv']]]
        mucomp_row=[]

        stitle,pcslides=cols,[]
        if isinstance(cols,list):
            # manually defined
            colkeys=cols
            stitle=';'.join(cols)
        elif cols=='mus':
            # look at combined
            colkeys.append(['JES Full','combined',self.intype('jes',True),self['comb_outv']])
            for mu in combCC.types['combined']['normal']:
                colkeys.append(['Comb-mu='+str(mu),'combined',combCC.input_version(mu,'normal',True),self['comb_outv']])
        elif cols=='jes':
            bdcats.append([['muhat','Total','DataStat','FullSyst','Jets','BTag'],''])
            colkeys=[['R1 Unfold','x',self.intype('normal',True,'x'),self['x_outv']]]
            colkeys.append(['R1 Eff','run1',self.intype('normal',True,'run1'),self['r1_outv']])
            colkeys.append(['R2 Unfold','test',self.intype('normal',True,'test'),self['test_outv']]) # Run2 and combined take as conditional mu=1
            colkeys.append(['R2 Eff',self['r2key'],self.intype('normal',True,'run2'),self['r2_outv']])
            colkeys.append(['Comb Unfold','combined',self.intype('jesu',False),self['comb_outv']])
            colkeys.append(['Comb Eff','combined',self.intype('jes',False),self['comb_outv']])
            muhats = list(float(self.bd_str(c[1],c[2]+'muhat') if self.bd_str(c[1],c[2]+'muhat')!='N/A' else -99) for c in colkeys)
            mucomp_row = [str(abs(muhats[0]-muhats[1])),str(abs(muhats[2]-muhats[3])),str(abs(muhats[4]-muhats[5]))]
            pcslides=[]
        elif cols=='jesu':
            bdcats=[]#.append([['muhat','Total','DataStat','FullSyst','Jets','BTag'],''])
            colkeys.append(['Comb Unfold','combined',self.intype('jesu',False),self['comb_outv']])
            colkeys.append(['Comb Eff','combined',self.intype('jes',False),self['comb_outv']])
            #colkeys.append(['Strong Unfold','combined',self.intype('jesu',True),self['comb_outv']])
            #colkeys.append(['Strong Eff','combined',self.intype('jes',True),self['comb_outv']])
            #muhats = list(float(self.bd_str(c[1],c[2]+'muhat') if self.bd_str(c[1],c[2]+'muhat')!='N/A' else -99) for c in colkeys)
            #mucomp_row = [str(abs(muhats[0]-muhats[1])),str(abs(muhats[2]-muhats[3]))]
            pcslides=['Jet','JetMatched','JetEff','JetUnfold']
        elif cols=='btag':
            bdcats.append([['muhat','Total','DataStat','FullSyst','Jets','BTag'],''])
            colkeys.append(['JES Full','combined',self.intype('jes',True),self['comb_outv']])
            colkeys.append(['BTag Partial','combined',self.intype('btag',False),self['comb_outv']])
            colkeys.append(['BTag Full','combined',self.intype('btag',True),self['comb_outv']])
            pcslides=['BTagB','BTagC','BTagL']
        elif cols=='btag-b':
            bdcats=[[['muhat','Total','DataStat','FullSyst','BTag','BTag b'],'']]
            colkeys=[['Comb Eff','combined',self.intype('jes',False),self['comb_outv']]]
            colkeys.append(['BTag B0','combined',self.intype('btag-b',False),self['comb_outv']])
            colkeys.append(['B0 8TeV Not Flipped','combined',self.intype('btag-nf',False),self['comb_outv']])
            #colkeys.append(['BTag BCL0','combined',self.intype('btag',False),self['comb_outv']])
            muhats = list(float(self.bd_str(c[1],c[2]+'muhat') if self.bd_str(c[1],c[2]+'muhat')!='N/A' else -99) for c in colkeys)
            mucomp_row=list(('---' if i==0 else abs(muhats[0]-muhats[i])) for i in range(len(colkeys)))
            pcslides=['BTagB0','BTagB']
        elif cols=='btag-c':
            bdcats.append([['muhat','Total','DataStat','FullSyst','BTag','BTag c'],''])
            colkeys.append(['BTag C0','combined',self.intype('btag-c',False),self['comb_outv']])
            colkeys.append(['BTag BCL0','combined',self.intype('btag',False),self['comb_outv']])
            pcslides=['BTagC-R1','BTagC-R2-C','BTagC0','BTagCL0']
        elif cols=='btag-l':
            bdcats.append([['muhat','Total','DataStat','FullSyst','BTag','BTag light'],''])
            colkeys.append(['BTag L0','combined',self.intype('btag-l',False),self['comb_outv']])
            colkeys.append(['BTag BCL0','combined',self.intype('btag',False),self['comb_outv']])
            pcslides=['BTagL-R1','BTagL-R2-C','BTagL0']
        elif cols=='btag-bl':
            bdcats.append([['muhat','Total','DataStat','FullSyst','BTag','BTag light'],''])
            colkeys.append(['BTag BL0','combined',self.intype('btag-bl',False),self['comb_outv']])
            colkeys.append(['BTag BCL0','combined',self.intype('btag',False),self['comb_outv']])
            pcslides=['BTagBL0']
        elif cols=='btag-all':
            bdcats.append([['muhat','Total','DataStat','FullSyst','BTag','BTag b','BTag c','BTag light'],''])
            colkeys.append(['BTag B0','combined',self.intype('btag-b',False),self['comb_outv']])
            colkeys.append(['BTag C0','combined',self.intype('btag-c',False),self['comb_outv']])
            colkeys.append(['BTag L0','combined',self.intype('btag-l',False),self['comb_outv']])
            colkeys.append(['BTag BL0','combined',self.intype('btag-bl',False),self['comb_outv']])
            colkeys.append(['BTag BCL0','combined',self.intype('btag',False),self['comb_outv']])
            pcslides=[]#'BTagB-R1','BTagB-R2-C','BTagC-R1','BTagC-R2-C','BTagL-R1','BTagL-R2-C']
        elif cols=='rcomp':
            bdcats.append([['muhat','Total','DataStat','FullSyst','Jets','BTag'],''])
            bdcats.append([['Model Zjets','Zjets flt. norm.','Model Wjets','Wjets flt. norm.','Model ttbar','ttbar flt. norm.','Model Single Top','Model Multi Jet'],'Modeling'])
            pcslides=["allExceptGammas","all","BTag","Diboson","FloatNorm","GammasL0","GammasL1","GammasL2","Gammas","Jet","Lepton","LUMI","MET","MJ","Norm","Suspicious","Top","Wjets","Zjets"]
            colkeys.append(['JES Full','combined',self.intype('jes',True),self['comb_outv']])
            colkeys.append(['Run1','run1',self.intype('normal',True),self['r1_outv']]), # Run1 take as observed
            colkeys.append(['Run2',self['r2key'],self.intype('normal',True),self['r2_outv']]) # Run2 and combined take as conditional mu=1
        elif cols=='xcomp':
            bdcats.append([['muhat','Total','DataStat','FullSyst','Jets','BTag'],''])
            pcslides=['Jet','JetMatched','JetEff','JetUnfold']
            #colkeys.append(['JES Full','combined',self.intype('jes',True),self['comb_outv']])
            #colkeys.append(['Run1','run1',self.intype('normal',True),self['r1_outv']]), # Run1 take as observed
            colkeys=[['R1 Unfold','x',self.intype('normal',True),self['x_outv']]]
            colkeys.append(['R1 Eff','run1',self.intype('normal',True),self['r1_outv']])
        elif cols=='tcomp':
            bdcats.append([['muhat','Total','DataStat','FullSyst','Jets','BTag'],''])
            pcslides=['Jet','JetMatched','JetEff','JetUnfold']
            #colkeys.append(['JES Full','combined',self.intype('jes',True),self['comb_outv']])
            #colkeys.append(['Run1','run1',self.intype('normal',True),self['r1_outv']]), # Run1 take as observed
            colkeys=[['R2 Unfold','test',self.intype('normal',True),self['test_outv']]]
            colkeys.append(['R2 Eff',self['r2key'],self.intype('normal',True),self['r2_outv']])
        elif cols=='comb':
            bdcats=[[['muhat','Total','DataStat','FullSyst','Floating normalizations','All normalizations','All but normalizations','Jets, MET','Jets','MET','BTag','BTag b','BTag c','BTag light'],'I'],
                    [['Leptons','Luminosity','Diboson','Model Zjets','Zjets flt. norm.','Model Wjets','Wjets flt. norm.','Model ttbar','Model Single Top','Model Multi Jet','Signal Systematics','MC stat'],'II']]
            pcslides=['allExceptGammas','Jet','BTag']
            colkeys=[['Run1+Run2','combined',self.intype('jes',False),self['comb_outv']]]
            
        else:
            print 'You defined a weird column object for a summary table: ',cols,' exiting...'
            return

        collabels = list(k[0] for k in colkeys)
        rowlabels,number_bucket = ['Exp. Sig.','Exp. p0','Exp. Limit'],[list(self.sig_tex(*k[1:],stype='exp') for k in colkeys),list(self.sig_tex(*k[1:],stype='exp',isp0=True) for k in colkeys),list(self.limit_tex(*k[1:],ltype='exp') for k in colkeys)]
        self.slides.table_slide(number_bucket,collabels,rowlabels,'Expected Sensitivities: '+stitle)
        rowlabels,number_bucket = ['Obs. Sig.','Obs. p0','Obs. Limit'],[list(self.sig_tex(*k[1:],stype='obs') for k in colkeys),list(self.sig_tex(*k[1:],stype='obs',isp0=True) for k in colkeys),list(self.limit_tex(*k[1:],ltype='obs') for k in colkeys)]
        self.slides.table_slide(number_bucket,collabels,rowlabels,'Observed Sensitivities: '+stitle)
        for b in bdcats:
            if len(b)>0:
                if len(b[0])>0:
                    rowlabels,number_bucket = [],[]
                    if (len(colkeys)%len(mucomp_row)==0 if len(mucomp_row)>0 else False):
                        rowlabels.append('$\left|\\Delta\\hat{\\mu}\right|$')
                        number_bucket.append(mucomp_row)
                    for cat in b[0]:
                        number_bucket.append(list(self.bd_str(k[1],k[2]+cat) for k in colkeys))
                        rowlabels.append(cat)
        
                    self.slides.table_slide(number_bucket,collabels,rowlabels,'Breakdowns: '+stitle+' '+b[1],smaller=1 if len(collabels)>4 else 0)

        # ranks--start by gathering the plots
        prefit,postfit=[],[]
        for key in colkeys:
            rank = self.rank_plots(key[1],key[2],key[3])
            if len(rank)==2:
                prefit.append([key[0],rank[0]])
                postfit.append([key[0],rank[1]])
        if len(postfit)==1 and len(prefit)==1:self.slides.image_slide(prefit+postfit,'Ranks: '+stitle,one_row=True,width=0.32)
        if len(postfit)>1:self.slides.image_slide(postfit,'Ranks: '+stitle,one_row=True,width=0.32 if len(postfit)<3 else -1)
        if len(prefit)>1: self.slides.image_slide(prefit, 'Prefit Ranks: '+stitle,one_row=True,width=0.323 if len(postfit)<3 else -1)

        # now that the summary table is done, assemble the compare pulls command
        # Example of include just an image in a beamer talk:
        # \frame[plain]{\includegraphics[page=7,width=0.8\textwidth]{figures/2016-04-06-SGM-Sector-deformations-v1-1.pdf}}
        # we can only have four pull comparisons in a pull comparison plot
        if len(pcslides)==0:return # don't bother doing time consuming pull comparisons if we're not actually interested in looking at any of them
        goodpulls_ws,goodpulls_names,lookedat,colorcode,colors=[],[],0,'',['black','red','blue','magenta']

        while len(goodpulls_ws)<4 and lookedat<len(colkeys):
            pullws = self.pull_ws(*colkeys[lookedat][1:])
            if pullws!='N/A':
                goodpulls_ws.append(pullws)
                goodpulls_names.append(colkeys[lookedat][0])
                colorcode+='{2}\\textcolor{{{0}}}{{{1}}}'.format(colors[lookedat],colkeys[lookedat][0],''if colorcode=='' else ', ')
            else:
                print 'Could not find FCC for {0}: {1} ({2})'.format(pullws,colkeys[lookedat][0],colkeys[lookedat][2])
            lookedat+=1

            
        if len(goodpulls_ws)==1:
            pullws=goodpulls_ws[0]
            showme='nope'
            # find pull plots
            for pulltype in 'GlobalFit_unconditionnal_mu1 GlobalFit_conditionnal_mu1 AsimovFit_unconditionnal_mu1 AsimovFit_conditionnal_mu1'.split(' '):
                canddir='./plots/{0}/fcc/{1}/'.format(pullws,pulltype)
                if os.path.exists(canddir+'NP_all.pdf'):
                    showme=canddir
                    break
            pdir = self['directory']+'pulls-'+stitle+'/'
            if not os.path.exists(pdir):
                os.makedirs(pdir)
            for plot in pcslides:
                ppdf='NP_{0}.pdf'.format(plot)
                shutil.copy2('{0}NP_{1}.pdf'.format(showme,plot),pdir+ppdf)
                self.slides.image_slide([[plot,'pulls-'+stitle+'/'+ppdf]],'Pulls: '+stitle+'---'+plot,width=0.67,lines=colorcode)
            #put all extant plots in backup
            for pulltype in 'GlobalFit_unconditionnal_mu1 GlobalFit_conditionnal_mu1 AsimovFit_unconditionnal_mu1 AsimovFit_conditionnal_mu1'.split(' '):
                canddir='./plots/{0}/fcc/{1}/'.format(pullws,pulltype)
                if os.path.exists(canddir+'NP_all.pdf'):
                    plots = list(self.plot_copy(src=g,subdir=pulltype) for g in glob.glob(canddir+'NP_*pdf'))
                    self.slides.slides_for_images(plots,nperslide=1,w=0.67,body=False,tag='Pulls: '+pulltype)
        elif len(goodpulls_ws)>1:
            pdir = self['directory']+'pullcomp-'
            lname=stitle
            if cols!=stitle:
                lname = '_'.join(goodpulls_names)
            pdir+=lname
            if not os.path.exists(pdir):
                os.makedirs(pdir)
            else:
                print "FYI: It looks like you've done",pdir," before."
            # the first command arguments are isConditional isAsimov,mu_asimov
            if not quick:
                cmd = ['python','scripts/comparePulls.py','0','0,1',pdir,'0','None']+goodpulls_ws
                subprocess.Popen(cmd).wait()
            for comp in pcslides:
                self.slides.image_slide([[comp,'pullcomp-{0}/NP_{1}.pdf'.format(lname,comp)]],'Pull Comparisons: '+stitle+'---'+comp,width=0.67,lines=colorcode)
        else:
            print 'COL DESIGNATION--',stitle,': Not printing a pull comp plot because none of the requisite results exist.'

    def plot_copy(self,src,subdir='',plot_description=''):
        if not os.path.exists(src):
            print 'Source file {} does not exist.  Skipping copy'.format(src)
            return ['nope','./nice_try_buddy.pdf']
        if subdir!='':
            if subdir[-1]!='/':
                subdir+='/'
            if not os.path.exists(self['directory']+subdir):
                os.makedirs(self['directory']+subdir)
        shutil.copy2(src,self['directory']+subdir)
        return [self.slides.rm_tex_spec(src[src.rfind('/')+1:src.rfind('.')]) if plot_description=='' else plot_description,subdir+src[src.rfind('/')+1:]]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run1+Run2 combination WSMaker job checking/steering')
    parser.add_argument('-a','--run1a',help="Run1 Asimov data",action='store_true')
    parser.add_argument('-c','--comb',help='Combination outversion',default='eps02')
    parser.add_argument('-1','--run1',help='Run1 outversion',default='613r1')
    parser.add_argument('-2','--run2',help='Run2 outversion',default='613r2')
    parser.add_argument('-t','--test',help='test (R2x) outversion',default='jesu')
    parser.add_argument('-x','--x',help='x (R1x) outversion',default='jesu1')
    parser.add_argument('-o','--obs',help='Observed data (for the results getting...probably does not matter but whatever',action='store_true')
    parser.add_argument('--quick',help='Skip plotting',action='store_true')
    parser.add_argument('--dontget',help='Pass a dontget argument to the results fetcher script',action='store_true')
    parser.add_argument('--aux',help='Combination auxilliary outversions, comma delimited',default='')#eps02')
    parser.add_argument('--wiwijet',help='Skip getting. If you have no new results since last running, speeds things up a bit.  If you do, they will not be found.',action='store_true')
    args=parser.parse_args()
    
    gROOT.LoadMacro("$WORKDIR/macros/AtlasStyle.C")
    gROOT.LoadMacro("$WORKDIR/macros/AtlasUtils.C")
    SetAtlasStyle()
    gStyle.SetEndErrorSize(7.0)
    gROOT.SetBatch(True)
    auxkeys=args.aux.split(',')
    if '' in auxkeys:
        auxkeys.remove('')

    if not args.wiwijet:
        pescado = ['python','./scripts/VHbbRun2/comb_run1run2/combCC.py','--debug']
        if args.dontget:
            pescado.append('--dontget')
        if args.obs:
            pescado.append('-o')
        subprocess.Popen(pescado+['-r','run1',args.run1]).wait()
        subprocess.Popen(pescado+['-r','run2',args.run2]).wait()
        subprocess.Popen(pescado+['-r','test',args.test]).wait()
        subprocess.Popen(pescado+['-r','x',args.x]).wait()
        subprocess.Popen(pescado+[args.comb]).wait()
        for a in auxkeys: 
            subprocess.Popen(pescado+[a]).wait()

    summary = CombSlides(r1_outv=args.run1,r2_outv=args.run2,comb_outv=args.comb,combauxkeys=auxkeys,run1a=args.run1a,test_outv=args.test,x_outv=args.x,obs=args.obs)
    summary.summary_table('comb',quick=args.quick)
    summary.poi_slides(quick=args.quick)
    """
    summary.generate_chi_tables('run1',summary.intype('normal',True,rtype='run1'),'run2',summary.intype('normal',True,rtype='run2'),quick=args.quick,body=False)
    summary.summary_table('jes',quick=args.quick)
    summary.summary_table('jesu',quick=args.quick)
    summary.summary_table('btag-b',quick=args.quick)
    summary.summary_table('tcomp',quick=args.quick)
    summary.summary_table('xcomp',quick=args.quick)
    summary.modeling_plot_slides2()
    summary.summary_table('btag-all',quick=args.quick)
    summary.summary_table('btag-b',quick=args.quick)
    summary.summary_table('btag-c',quick=args.quick)
    summary.summary_table('btag-l',quick=args.quick)
    summary.summary_table('btag-bl',quick=args.quick)
    """
    summary.finish()
