import subprocess,math
from collections import OrderedDict

#odict for ordered dictionary, which preserves order of insertion, allows for *args in additon to **kwargs (I think)
class SlideMaker(OrderedDict):
    def __init__(self, *args, **kwargs):
        super(SlideMaker,self).__init__()
        self.backup=False
        self.update(file_name='auto_slides.tex',directory='.')
        self['backup_slides']=''
        self['backup_buffer']=''

        #set defaults
        self.update(kwargs)

        if self['directory'][len(self['directory'])-1] != '/':
            self['directory']+='/'
        if str(self['file_name']).find('.tex') != len(self['file_name'])-4:
            self['file_name']+='.tex'

        self.file=open(self['directory']+self['file_name'],'w')

    def front_matter(self,title="Auto Generated Slides",author="S K Chan"):
        #packages
#        self.file.write("\\documentclass[aspectratio=169]{beamer}\n\\usepackage{graphicx}\n\\usepackage{hyperref}\n\\usepackage{cancel}\n\\usepackage{pdflscape}\n\\usepackage{multirow}\n\\usepackage{verbatim}\n\\usepackage{atlasphysics}\n\usepackage{amssymb,amsmath,amsfonts,mathrsfs}\n\\usepackage{caption}\n\\usepackage[font+=scriptsize]{subcaption}\n\\usepackage{textpos}\n\\usepackage{xcolor}\n\\usepackage[T1]{fontenc}\n\\usepackage[sfdefault]{AlegreyaSans}\n\\renewcommand*\\oldstylenums[1]{{\\AlegreyaSansOsF #1}}\n");
        #self.file.write("\\documentclass[aspectratio=169]{beamer}\n\\usepackage{graphicx}\n\\usepackage{hyperref}\n\\usepackage{cancel}\n\\usepackage{pdflscape}\n\\usepackage{multirow}\n\\usepackage{verbatim}\n\\usepackage{atlasphysics}\n\usepackage{amssymb,amsmath,amsfonts,mathrsfs}\n\\usepackage[font=tiny]{caption}\n\\usepackage[font+=scriptsize]{subcaption}\n\\usepackage{textpos}\n\\usepackage{xcolor}\n\\usepackage[light,math]{iwona} \n\\usepackage[T1]{fontenc}\n");
        self.file.write("\\documentclass[aspectratio=169]{beamer}\n\\usepackage{graphicx}\n\\usepackage{hyperref}\n\\usepackage{cancel}\n\\usepackage{pdflscape}\n\\usepackage{multirow}\n\\usepackage{verbatim}\n\usepackage{amssymb,amsmath,amsfonts,mathrsfs}\n\\usepackage[font=tiny]{caption}\n\\usepackage[font+=scriptsize]{subcaption}\n\\usepackage{textpos}\n\\usepackage{xcolor}\n\\usepackage[light,math]{iwona} \n\\usepackage[T1]{fontenc}\n");
            
        #author and title
        self.file.write("\\title[%s]{%s}\n" % (title,title))
        self.file.write("\\author[%s]{%s}\n\\institute[Harvard]{Harvard University}\n" % (author,author))
        
        #my standard theme setup
        #colors
        self.file.write("\\usetheme{Madrid}\n\\makeatletter\n\\definecolor{deepnavy}{RGB}{0,0,65}\n\\definecolor{cblue}{RGB}{196,216,226}\n\\definecolor{deepcrimson}{RGB}{90,5,10}\n\\definecolor{hcrimson}{RGB}{165,28,48}\n\\setbeamercolor{alerted text}{fg=hcrimson}\n\\setbeamercolor{structure}{fg=deepcrimson}\n\\setbeamercolor{background canvas}{parent=normal text}\n\\setbeamercolor{background}{parent=background canvas}\n\\setbeamercolor{palette primary}{fg=white,bg=deepnavy}\n\\setbeamercolor{palette secondary}{fg=white,bg=black}\n\\setbeamercolor{palette tertiary}{fg=white,bg=deepnavy}\n\\setbeamertemplate{title page}[default][colsep=-4bp,rounded=true]\n")
        self.file.write("\\setbeamertemplate{frametitle}[default][center]\n\\setbeamertemplate{footline}%three blocks at the bottom with short author (inst), title, date p#/total\n{\n  \leavevmode%\n  \\hbox{%\n  \\begin{beamercolorbox}[wd=.333333\\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%\n    \\usebeamerfont{author in head/foot}\insertshortauthor~~\\beamer@ifempty{\insertshortinstitute}{}{}{(\insertshortinstitute)}\n  \\end{beamercolorbox}%\n  \\begin{beamercolorbox}[wd=.333333\\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%\n    \\usebeamerfont{title in head/foot}\\insertshorttitle\n  \\end{beamercolorbox}%\n  \\begin{beamercolorbox}[wd=.333333\\paperwidth,ht=2.25ex,dp=1ex,right]{date in head/foot}%\n    \\usebeamerfont{date in head/foot}\\insertshortdate{}\\hspace*{2em}\n    \\insertframenumber{} / \\inserttotalframenumber\\hspace*{2ex}\n   \\end{beamercolorbox}}%\n  \\vskip0pt%\n}\n")
        self.file.write("\\addtobeamertemplate{frametitle}{}{\n\\begin{textblock*}{100mm}(0.944\\textwidth,-1cm)\n\\pagecolor{white}\n\\includegraphics[height=1cm,width=1cm,keepaspectratio]{../HarvardWreath500wikipedia.png}\n\\end{textblock*}\n\\begin{textblock*}{100mm}(-0.025\\textwidth,-0.9cm)\n\\includegraphics[height=0.875cm,keepaspectratio]{../ATLAS-square-w-transp.png}\n\\end{textblock*}\n}\n\\makeatother\n\n\\captionsetup{skip=0pt,belowskip=0pt,font=scriptsize,labelfont=scriptsize}\n\\setbeamerfont{caption}{size=\\tiny}\n\\setbeamerfont{caption name}{size=\\tiny}\n\n\\setbeamertemplate{itemize items}[default]\n\\setbeamertemplate{enumerate items}[default]\n")
        self.file.write("\\begin{document}\n")
        self.title_slide()
        return

    def title_slide(self):
        #front matter
        self.file.write("\\begin{frame}\n\\titlepage\n\\begin{figure}\n  \\begin{center}\n")
        
        #atlas logo
        self.file.write("    \\begin{subfigure}{0.3\\textwidth}\n      \\begin{center}%trim lbrt\n        \\includegraphics[height=2cm,keepaspectratio]{../ATLAS-Logo-B&W-RGB.png}\n      \\end{center}\n    \\end{subfigure}\n")
        #harvard logo
        self.file.write("    \\begin{subfigure}{0.3\\textwidth}\n      \\begin{center}\n        \\includegraphics[height=2cm,keepaspectratio]{../HarvardWreath500wikipedia.png}\n")
        
        #back matter
        self.file.write("      \\end{center}\n    \\end{subfigure}\n  \\end{center}\n\\end{figure}\n\\end{frame}\n\n")
        return

    def tex_to_table_slide(self,tex,title='slide',key=''):
        lines=file(tex,'r').read().split('\n')
        self.file.write("\\begin{{frame}}\n  \\frametitle{{{0}}}\n  \\begin{{center}}".format(title))
        for l in lines:
            if '&' in l:
                if 'otal' in l or 'hline' in l or key in l:
                    self.file.write('{}\n'.format(l))
            else:
                self.file.write('{}\n'.format(l))
        self.file.write("  \\end{center}\n\\end{frame}\n")
        return

    def rm_tex_spec(self,raw_name):
        tex_spec,new_name = ["_","^",'%'],raw_name
        hack=True
        if hack:
            for thing in tex_spec:
                raw_name=raw_name.replace(thing,'\\'+thing)
            return raw_name
        for char in tex_spec:
            old_name=new_name
            pos=old_name.find(char)
            if pos<0: continue
            new_name=old_name[0:pos]+" " #"\\"+char
            end=''
            while pos>0:
                end=old_name[pos+1:]
                next_pos=old_name.find(char,pos+1)
                if next_pos>0: new_name+=(old_name[pos+1:next_pos]+" ") #"\\"+char)
                pos=next_pos
            new_name+=end
        return new_name

    def slides_for_images(self,images,nperslide=4,tag='Plots',scrub=True,w=-1,body=True):
        nslides = math.ceil(len(images)*1.0/nperslide)
        for i in range(int(nslides)):
            lo = i*nperslide
            hi = min(lo+nperslide,len(images))
            self.image_slide(images[lo:hi],title='{0} ({1} of {2})'.format(tag,i+1,int(nslides)),width = (0.45 if hi-lo==1 else -1) if w<0 else w,body=body)
        return

    def image_slide(self,images,title="Title",width=-1,lines='',scrub=True,one_row=False,bottom_caption=True,body=True):
        if scrub: title=self.rm_tex_spec(title)
        stuff = lines.replace('@','\\n\\item ')
        if len(stuff)>0:
            stuff = '\\item '+stuff+'\\n'
        self.slide_write("\\begin{frame}\n  \\frametitle{%s}\n  \\begin{itemize}\n %s \\end{itemize}\n  \\begin{figure}\n" % (title,stuff),body)
        if bottom_caption:
            self['backup_buffer']+="\\begin{figure}\n  \\label{fig:%s}\n" % (title.replace(' ','').replace(',','').replace('(','').replace(')',''))
        else:
            self['backup_buffer']+="\\begin{frame}\n  \\label{fig:%s}\n  \\caption{%s}\n  \\begin{figure}\n" % (title.replace(' ','').replace(',','').replace('(','').replace(')',''),title+' '+stuff)
        """
        we want to figure out how to arrange the plots in a nice fashion; three to a row 
        with the exception of four plots, which we want to do 2x2; should never have more
        than six plots
        """
        n_per_row=round(len(images)*0.5)#(2 if len(images)<3 or len(images)==4 else 3)
        if len(images)==1: n_per_row=1.1
        elif len(images)<5 or one_row: n_per_row=len(images)
        span=0.9/n_per_row
        if width>0:
            span=width
        for image in images:
            if len(image)==1: self.file_backup_write("    \\begin{subfigure}{%f\\textwidth}\\includegraphics[width=\\textwidth]{%s}\\end{subfigure}\n" % (span,image),body=body)
            else: self.file_backup_write("    \\begin{subfigure}{%f\\textwidth}\\caption{%s}\\includegraphics[width=\\textwidth]{%s}\\end{subfigure}\n" % (span,image[0],image[1]),body=body)
            #if len(image)==1: self.file_backup_write("    \\subfigure[]{\\includegraphics[width=%f\\textwidth]{%s}}\n" % (span,image),body=body)
            #else: self.file_backup_write("    \\subfigure[%s]{\\includegraphics[width=%f\\textwidth]{%s}}\n" % (image[0],span,image[1]),body=body)
        self.slide_write("  \end{figure}\n\\end{frame}\n",body)
        if bottom_caption:
            self['backup_buffer']+="  \\caption{%s}\n\\end{figure}\n\n" % (title+' '+stuff)
        else:
            self['backup_buffer']+="\\end{figure}\n"
        return

    def slide_write(self,text,body=True):
        if body:
            self.file.write(text)
        else:
            self['backup_slides']+=text

    def file_backup_write(self,text,replaces=[],body=True):
        if body:
            self.file.write(text)
        else:
            self['backup_slides']+=text
        for r in replaces:
            text=text.replace(r[0],r[1])
        self['backup_buffer']+=text


    def table_slide(self,entries,top_labels,side_labels,title='Title',flt_prec=4,scrub=True,colw=-1,row_title='',bottom_caption=True,body=True,smaller=0):
        if scrub: title=self.rm_tex_spec(title)
        self.slide_write("\\begin{frame}\n  \\frametitle{%s}\n" % title,body=body)
        self['backup_buffer']+='\\begin{table}[htbp]\n\\begin{center}\n\\label{tab:%s}\n'% (title.replace(' ','').replace(',','').replace('(','').replace(')',''))
        if not bottom_caption:
            self['backup_buffer']+="\\caption{%s}\n"% (title)
        self.slide_write('\\begin{table}[htbp]\n\\begin{center}\n',body=body) #table top
        self.slide_write("\\caption{%s}\n"%title,body=body) #for easier copy-pasta, should the day come
        if smaller==1:
            self.slide_write("{\\scriptsize",body=body)
        elif smaller==2:
            self.slide_write("{\\tiny",body=body)
        else:
            self.slide_write("{\\footnotesize",body=body)

        #entries have format[ [row1],...,[row m] ] for an m x n table
        columns = 0
        for row in entries:
            if len(row) > columns:
                columns = len(row)
        self.file_backup_write('\\begin{tabular}{|l|',[['|','']],body=body)
        for i in range(columns): self.file_backup_write('c|' if colw<0 else 'p{{{0}cm}}|'.format(colw),[['|','']],body=body)
        self.file_backup_write('}\n\\hline\n',[['\\hline','\\hline\\hline']],body=body)


        #now write the column labels
        self.file_backup_write(row_title,body=body)
        for i in range(columns): self.file_backup_write(' &{0}'.format((self.rm_tex_spec(top_labels[i]) if scrub else top_labels[i]) if i<len(top_labels) else 'label'),body=body)
        self.file_backup_write('\\\\\n\\hline\n',body=body)

        for i in range(len(entries)):
            self.file_backup_write(self.rm_tex_spec(side_labels[i]) if i<len(side_labels) else 'label',body=body)
            entries_per_column = float(columns/len(entries[i]))
            if entries_per_column <= 1: 
                #simple table filling
                for j in range(columns): self.file_backup_write(' & '+self.table_entry_str(entries[i][j],flt_prec),body=body)
            else: 
                #assume entries are merged uniformly; put extras columns with rightmost entry
                nper = int(entries_per_column)
                leftover = columns - len(entries[i])*nper
                for j in range(len(entries[i])): self.file_backup_write(' &  \multicolumn{{{0}}}{{|c|}}{{{1}}}'.format(nper if j<len(entries[i])-1 else nper+leftover,self.table_entry_str(entries[i][j],flt_prec)),body=body)
            self.file_backup_write('\\\\\n\\hline\n',body=body)

        self.slide_write('\\end{tabular}}\n\\end{center}\n\\end{table}\n\\end{frame}\n',body=body) #table bottom
        self['backup_buffer']+=('\\end{tabular}\n\\end{center}\n'+("\\caption{%s}\n"% (title) if bottom_caption else '')+'\\end{table}\n\n')
        return

    def is_number(self,string):
        try:
            float(string)
            return True
        except ValueError as yup:
            return False

    def table_entry_str(self,entry,flt_prec=4):
        if isinstance(entry,float):
            return str(round(entry,flt_prec))
        elif isinstance(entry,str):
            if self.is_number(entry):
                return str(round(float(entry),flt_prec))
        return str(entry) #it's a str or an int
        
    def backup_slide(self,text='Backup'):
        self.backup = self.backup or text=='Backup'
        if self.backup:self.file.write("\\appendix\n\\newcounter{finalframe}\n\\setcounter{finalframe}{\\value{framenumber}}\n")
        self.file.write("\n\\begin{frame}\n  \\frametitle{}\n  \\begin{center}\n  {\\Huge %s}\n  \\end{center}\n\\end{frame}\n" % (text))

    def back_matter(self):
        #end the document and include the infrastructure for backup slides with slide counters only including front material
        if not self.backup: self.backup_slide()
        self.file.write("\\setcounter{framenumber}{\\value{finalframe}}\n")
        self.file.write(self['backup_slides'])
        self.file.write("\\end{document}\n")
        self.file.write(self['backup_buffer'])
        self.file.close()

    def title_from_tags(self,tags):
        ret_me="%s pT---%s GeV"
        tag,pt="???","30"
        if len(tags)>0:
            scr0,jpt = tags[0].find("_"),tags[0].find("_jpt")
            tag=tags[0][0:scr0]
            if jpt>=0:
                scr1=tags[0].find("_",jpt+1)
                pt=tags[0][jpt+4:scr1]
        return ret_me % (tag,pt)

    def title_from_tag(self,tag):
        ret_me="Run %s, Pd %s"
        run,pd="???","-999"
        scr0=tag.find("_")
        pd=tag[0:scr0].upper()
        scr1=tag.find("_",scr0+1)
        run=tag[scr0+1:scr1]
        return ret_me % (run,pd)

    def caption_from_tag(self,tag):
        start=tag.find("_")+1
        end=tag.find("_",start)
        return tag[start:end]

    def ptv_str_conv(self,ptv,objstr='V'):
        yo="$p_T^{{{0}}} \in \left[".format(objstr)
        yo+=ptv.replace("ptv","").replace("_",",")
        if "," in yo:
            yo+="\\right]$"
        else:
            yo+=",\\infty\\right)$"
        yo+=" GeV"
        return yo

    def print_skimmed_plots(self,plot_dir,keys=[],plots_per_slide=5,width=-1):
        plots=skim_dir_for_plots(keys,plot_dir)
        for f in keys:
            self.backup_slide(self.rm_tex_spec(f))
            for i in range(int(math.ceil(len(plots[f])/plots_per_slide))):
                stitle,splots = '{}, {}'.format(f,i),[]
                for j in range(min(plots_per_slide,len(plots[f])-i*plots_per_slide)): 
                    plot=plots[f][i*plots_per_slide+j]
                    splots.append(['\\scalebox{0.25}{'+self.rm_tex_spec(plot[0:plot.rfind('.')])+'}',pdir+plot])
                self.image_slide(splots,stitle,width)
        return

    """
    skim a directory for files with extension 'ext' and any of the keywords in 'keys'
    what is returned is a dictionary of form {key:list_of_plots_with_key_and_ext}
    """
    def skim_dir_for_plots(self,keys=[],dir='./figures/',ext='.pdf',concat=True):
        plots=dict()
        if not os.path.exists(dir):
            return plots
        if ext[0] != '.': ext='.'+ext
        if keys==[]:keys.append('all')
        for k in keys:
            plots.update({k:[]})
        for p in os.listdir(dir):
            for k in keys:
                epos=p.find(k+ext if concat else ext) * (1 if concat or (k in p and e in p) else -1)
                if epos>0 and epos==len(p)-len(k+ext):
                    plots[k].append(p)
        return plots

    """
    spit out a bunch of image slides; plots will have a 'pattern' that looks like 'figure-direcotry/{kw0}-stuff-{kw1}(etc).pdf'
    the 'arguments' will be a list of arguments matching to keywords in 'keywords'
    the slide arguments are what specifically goes on each slide 
    (slide_args is a list of lists--at the bottom of the nested for loop, the final category can be broken up into pieces)
    """
    def plots_from_keys(self,pattern,keywords,arguments,slide_kw,slide_args,slide_title='',width=-1):
        if len(keywords)!=len(arguments):
            print 'In SlideMaker::plots_from_keys, you have {} keywords but {} buckets of entries--this is not going to work'.format(len(keywords),len(arguments))
            return
        keydex,plotkey,key,slides=0,{k:None for k in keywords},[0 for k in keywords],[]
        key[-1]=-1
        while self.toggle_key(key,arguments):
            for k in range(len(key)):plotkey.update({keywords[k]:arguments[k][key[k]]})
            for i in range(len(slide_args)):
                title,plots=slide_title,[]
                for k in keywords: title+=' {}: {}'.format(k,plotkey[k])
                title+='; v. {}-{}'.format(slide_kw,i)
                for arg in slide_args[i]:
                    plotkey.update({slide_kw:arg})
                    plots.append([self.rm_tex_spec(arg),pattern.format(**plotkey)])
                self.image_slide(plots,title,width)
        return
    def toggle_key(self,key,arguments):
        for iter in range(len(key)-1,-1,-1):
            if key[iter]==len(arguments[iter])-1:
                continue
            else:
                key[iter]+=1
                for i in range(iter+1,len(key)):
                    key[i]=0
                return True
        return False
            
