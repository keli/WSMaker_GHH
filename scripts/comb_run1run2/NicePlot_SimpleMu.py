##########################################################################
#
# this ia actually an effective macro to make the fancy plot!!!!
#
##########################################################################
import glob, os, sys
import commands
from ROOT import *
from glob import *
import time
import math
##########################################################################

#########################################################################

def myTextBold(x,y,color,text, tsize):
  l=TLatex();
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(62);
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);

####################################################################################################
  
def valerio_nice_plot(value=[],odir='',tag=''):
  upperLim=  8
  lowerLim= -1
  if odir=='':
    odir=outDir

  Nobj=len(value)

  for obj in value:
    if "txt" not in obj[7]: continue
    theF=file(obj[7])
    lines=theF.readlines()
    index=obj[8]
    incremental=0
    mu=1
    errT_u=0
    errT_d=0
    errS_u=0
    errS_d=0
    readThis=False
    for line in lines:
      if "muhat" in line:
        incremental+=1
        if incremental==index: readThis=True
        else                 : readThis=False
            ##print line
        if readThis: mu=float(line.split(":")[1])
      elif "Total" in line and readThis:
        ##print line.split()
        errT_u=float(line.split()[2])
        errT_d=float(line.split()[4])
      elif "DataStat" in line and errS_u==0 and readThis:
        errS_u=float(line.split()[2])
        errS_d=float(line.split()[4])
    theF.close()
    obj[2]=mu
    obj[3]=errT_u
    obj[4]=errT_d
    obj[5]=errS_u
    obj[6]=errS_d

    
  for obj in value:
    print obj
    
  print "info on final fit: "
  print " Mu val: "+str( value[0][2] )
  print " Stat Err:  UP= "+str( value[0][5] )+"   DO= "+str( value[0][6] )
  print " Sys  Err:  UP= "+str( math.sqrt( value[0][3]*value[0][3]-value[0][5]*value[0][5]) )+"   DO= "+str( math.sqrt(value[0][4]*value[0][4] - value[0][6]*value[0][6]) )
  print " TOT  Err:  UP= "+str( value[0][3] )+"   DO= "+str( value[0][4] )

    
  value.append( ["Fake", 0, 0, 0, 0, 0, 0, 0, 0 ] )
  if Nobj>4: value.append( ["Fake", 0, 0, 0, 0, 0, 0, 0, 0 ] )

  theHisto2=TH2D("plot2"+tag,"plot2"+tag,20,lowerLim,upperLim,len(value),-0.5,len(value) )  ##0.5)
  theHisto2.GetYaxis().SetTickSize(0)
  theHisto2.GetYaxis().SetTickLength(0)
  theHisto2.GetXaxis().SetTitle("best fit #mu_{VH}")
  #else   : theHisto2.GetXaxis().SetTitle("best fit #mu_{VZ}")
##best fit #mu=#sigma^{t#bar{t}H}/#sigma^{t#bar{t}H}_{SM} for m_{H}=125 GeV")
  theHisto2.GetYaxis().SetLabelSize(0.06)
  theHisto2.GetXaxis().SetTitleOffset(1.0)
  theHisto2.GetXaxis().SetTitleSize(0.06)
  theHisto2.GetYaxis().SetLabelSize(0.07)
  count=0
  for obj in value:
    if "Fake" in obj[0]: continue
    count+=1
    print "setting: "+str(obj[1])
    theHisto2.GetYaxis().SetBinLabel(count,obj[0])

  c2 = TCanvas("test2","test2",800,600)
  gPad.SetLeftMargin(0.20)
  gPad.SetTopMargin(0.05)
  theHisto2.Draw()

  tmp5=TH1D("tmp5", "tmp5", 1,1,2)
  tmp6=TH1D("tmp6", "tmp6", 1,1,2)
  tmp5.SetLineColor(1)
  tmp6.SetLineWidth(4)
  tmp5.SetLineWidth(3)
  tmp6.SetLineColor(8)


  g = TGraphAsymmErrors()
  g.SetLineWidth(4)
  g.SetMarkerColor(1)
  g.SetMarkerStyle(20)
  g.SetMarkerSize(1.6)

  g2 = TGraphAsymmErrors()
  g2.SetLineWidth(3)
  g2.SetMarkerColor(1)
  g2.SetMarkerStyle(20)
  g2.SetMarkerSize(1.6)
  g2.SetLineColor(8)
  
  count=-1.0
  for obj in value:
    count+=1.
    if "Fake" in obj[0]: continue
    scale=1.1
    if Nobj>=4: scale=1.05
    g.SetPoint( int(count)     , obj[2]    , float(count)*scale )
    g.SetPointEXhigh( int(count), obj[3])
    g.SetPointEXlow(  int(count), obj[4])
    g.SetPointEYhigh( int(count), float(0) )
    g.SetPointEYlow(  int(count), float(0) )
    g2.SetPoint( int(count)     , obj[2]    , float(count)*scale )
    g2.SetPointEYhigh( int(count), float(0) )
    g2.SetPointEYlow(  int(count), float(0) )
    g2.SetPointEXhigh( int(count), obj[5] )
    g2.SetPointEXlow(  int(count), obj[6] )
    print " --> Setting point: "+str(obj[2])+"  +/- "+str(obj[3])
    sysUp=0
    if obj[3]*obj[3]-obj[5]*obj[5]>0:
      sysUp= math.sqrt(obj[3]*obj[3]-obj[5]*obj[5])
    sysDo=0
    if obj[4]*obj[4]-obj[6]*obj[6]>0:
      sysDo= math.sqrt(obj[4]*obj[4]-obj[6]*obj[6])

    mystring1 = "%.2f  {}^{+%.2f}_{#minus%.2f}" % (obj[2], obj[3], obj[4])
    #mystring2 ="{}^{+%.1f}_{#minus%.1f}  , {}^{+%.1f}_{#minus%.1f}            " % (obj[5],obj[6], sysUp, sysDo) ## add a
    mystring2 = "                    {}^{+%.2f}_{#minus%.2f} , " % (obj[5],obj[6]) ## add a
    mystring3 = "                             {}^{+%.2f}_{#minus%.2f}           " % (sysUp, sysDo) ## add a
    mystring4 = "                   (                 )         "
    
    offset=0
    startX=0.57
    slope=0.16
    if Nobj==3: slope=0.20
    startY=0.22+slope*count+offset
    if Nobj>4: startY=0.18+0.095*count+offset
    if Nobj==8: startY=0.18+0.082*count+offset
    if Nobj==9: startY=0.18+0.075*count+offset
    myTextBold(startX     ,startY,1, mystring1, 0.051)  # 0.144
    myText(    startX+0.01,startY,1, mystring2, 0.051)  
    myText(    startX+0.01,startY,1, mystring3, 0.051)
    myText(    startX+0.01,startY,1, mystring4, 0.051)

  slope=0.12
  if Nobj==3: slope=0.14
  if Nobj>4 : slope=0.10
  yValLabel=0.32+(len(value)-1)*slope#-0.02
  myTextBold(0.65, yValLabel , 1, " ( tot. )"        , 0.045) ##was 0.66 for %.1
  myText(    0.76, yValLabel , 1, "( stat. , syst. )", 0.044) ##84
  
  legend4=TLegend(0.28,0.77,0.60,0.87)
  legend4.SetTextFont(42)
  legend4.SetTextSize(0.047)
  legend4.SetFillColor(0)
  legend4.SetLineColor(0)
  legend4.SetFillStyle(0)
  legend4.SetBorderSize(0)
  legend4.SetNColumns(2)
  legend4.AddEntry(tmp5,"total" ,"l")
  legend4.AddEntry(tmp6,"stat.","l")
  legend4.Draw("SAME")
  
  line0=TLine(0., -0.5, 0., len(value)-1.25)
  line0.SetLineWidth(3)
  line0.SetLineStyle(3)
  line0.SetLineColor(kGray+1)
  line0.Draw("SAMEL") 
  
  line1=TLine(1., -0.5, 1., len(value)-1.25)
  line1.SetLineWidth(2)
  line1.SetLineColor(kBlack)
  line1.Draw("SAMEL")
  
  lineC=TLine(theHisto2.GetXaxis().GetXmin(), 0.5,theHisto2.GetXaxis().GetXmax(), 0.5)
  lineC.SetLineWidth(2)
  lineC.SetLineStyle(2)
  lineC.SetLineColor(kBlack)
  lineC.Draw("SAMEL")
  
#lall.Draw("SAME")
  g.Draw("SAMEP")
  g2.Draw("SAMEP")
  

#myText(0.15,0.65,1,"(13.3 fb^{-1} )",0.03)
#myText(0.15,0.50,1,"(13.2 fb^{-1} )",0.03)
#myText(0.15,0.34,1,"(13.2 fb^{-1} )",0.03)

  ATLAS_LABEL(0.27,0.88,1)
  myText(     0.40,0.88,1,"Internal",0.05)
##myText(     0.60,0.93,1,"#sqrt{s}=13 TeV, #scale[0.6]{#int}L dt=3.2 fb^{-1}")
  myText(     0.650,0.89,1,"#sqrt{s}=7+8+13 TeV",0.04)
  myText(     0.650,0.845,1,"#scale[0.6]{#int}L dt=4.7+20.3+36.1 fb^{-1}",0.04)
##8.3 fb^{-1}")
  
  c2.Update()
  name="Plot_mu_"+tag#str(mode)
  c2.Print( odir+name+".pdf" )

#############################################################################
#############################################################################

