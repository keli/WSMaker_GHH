##########################################################################
#
# this ia actually an effective macro to make the fancy plot!!!!
#
##########################################################################
import glob, os, sys
import commands
from ROOT import *
from glob import *
import time
import math
##########################################################################

gROOT.LoadMacro("$WORKDIR/macros/AtlasStyle.C")
gROOT.LoadMacro("$WORKDIR/macros/AtlasUtils.C")
SetAtlasStyle()
gStyle.SetEndErrorSize(7.0)
gROOT.SetBatch(True)

#########################################################################
outDir="./Mu_Results/"
os.system("mkdir -p "+outDir)
mainFolder=""
noInputs  =5

upperLim= 4
lowerLim= 0

#if len(sys.argv)!=1:
#    print " USAGE:  python NicePlot_SimpleMu.py <mode>"
#    print " at the moment Mode needs to be expanded "
#    exit(-1)

#if   sys.argv[1]=="Asimov":
#    isObserved=False
#elif sys.argv[1]=="Data":
#    isObserved=True
#else:
#    print "Parameter: "+sys.argv[1]+" NOT recognised .... use either Asimov/Data"
#    exit(-1)

def ATLASLabel(x, y, space, text, color=1, tsize=0):
  l=TLatex() #text = 0.115 by default
  l.SetNDC()
  l.SetTextFont(72)
  l.SetTextColor(color)
  if tsize>0: l.SetTextSize(tsize) 

  delx = space*696*gPad.GetWh()/(472*gPad.GetWw())

  l.DrawLatex(x,y,"ATLAS")
  if text:
    p=TLatex()
    p.SetNDC()
    p.SetTextFont(42)
    if tsize>0: p.SetTextSize(tsize) 
    p.SetTextColor(color)
    p.DrawLatex(x+delx,y,text)

def myText(x,y,color,text, tsize):
  l=TLatex();
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(42);
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);

def myTextBold(x,y,color,text, tsize):
  l=TLatex();
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(62);
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);

####################################################################################################
mode="1"
doVH=True
value=[]

mode=sys.argv[1]
if sys.argv[2]=="0":
    doVH=False


if not doVH:
    ## in descending order
    ## please ignore the second argument, just set the title and the fileName
    ## change fileName with the appropriate EOS path to the corresponding file

    #mode 0 is 3POI fit
    if mode =="0":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vv-mva/breakdown/muHatTable_mode11_Asimov1_SigXsecOverSM.txt",1],
            ["2L "         ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vv-mva/L0-L1-L2_3POI/breakdown/muHatTable_mode15_Asimov1_SigXsecOverSM_L2.txt",1],
            ["1L "         ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vv-mva/L0-L1-L2_3POI/breakdown/muHatTable_mode15_Asimov1_SigXsecOverSM_L1.txt",1],
            ["0L "         ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vv-mva/L0-L1-L2_3POI/breakdown/muHatTable_mode15_Asimov1_SigXsecOverSM_L0.txt",1],
        ]
        upperLim=  5
        lowerLim=  0
        
    #mode -1 is single fit
    if mode=="-1":
        value=[
            ["Comb."         ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2L  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["1L  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["0L  "          ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
        ]

    #mode 1 is 2POI fit              
    if mode=="1":
        value=[
            ["Comb."         ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vv-mva/breakdown/muHatTable_mode11_Asimov1_SigXsecOverSM.txt",1],
            ["ZZ  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vv-mva/WH-ZH_2POI/breakdown/muHatTable_mode15_Asimov1_SigXsecOverSMZZ.txt",1],
            ["WZ  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vv-mva/WH-ZH_2POI/breakdown/muHatTable_mode15_Asimov1_SigXsecOverSMWZ.txt",1],
        ]
        upperLim=  5
        lowerLim=  0

    #mode 2 is 3POI fit and single comparison
    if mode=="2":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2L: single"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/3_POI/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L: single"  ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["1L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/3_POI/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L: single"  ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["0L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/3_POI/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L0.txt",1],
        ]

    #mode 3 is 3POI fit CBA vs MVA    
    if mode=="3":
        value=[
            ["Comb: MVA" ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["Comb: DMA" ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/VZ/CBA_1POI/012lep/muHatTable_mode10_Asimov0_SigXsecOverSM.txt",1],
            ["2L: MVA"   ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/3_POI/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L2.txt",1],
            ["2L: DMA"   ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/VH/CBA_3POI/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L: MVA"   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/3_POI/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L1.txt",1],
            ["1L: DMA"   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/VH/CBA_3POI/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L: MVA"   ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/3_POI/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L0.txt",1],
            ["0L: DMA"   ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/VH/CBA_3POI/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L0.txt",1],
        ]
    
    #mode 4 is 8POI fit
    if mode=="4":
        value=[
            ["Comb."     ,  0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2L: 2j high" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/VZ/8_POI/muHatTable_mode0_Asimov0_SigXsecOverSM_L2_J2_BMin150.txt",1],
            ["2L: 2j low"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/VZ/8_POI/muHatTable_mode0_Asimov0_SigXsecOverSM_L2_J2_BMin75.txt",1],
            ["2L: 3j high" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/VZ/8_POI/muHatTable_mode0_Asimov0_SigXsecOverSM_L2_J3_BMin150.txt",1],
            ["2L: 3j low"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/VZ/8_POI/muHatTable_mode0_Asimov0_SigXsecOverSM_L2_J3_BMin75.txt",1],
            ["1L: 2j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/VZ/8_POI/muHatTable_mode0_Asimov0_SigXsecOverSM_L1_J2_BMin150.txt",1],
            ["1L: 3j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/VZ/8_POI/muHatTable_mode0_Asimov0_SigXsecOverSM_L1_J3_BMin150.txt",1],
            ["0L: 2j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/VZ/8_POI/muHatTable_mode0_Asimov0_SigXsecOverSM_L0_J2_BMin150.txt",1],
            ["0L: 3j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/VZ/8_POI/muHatTable_mode0_Asimov0_SigXsecOverSM_L0_J3_BMin150.txt",1],
            
        ]   
    #mode 5 is mc16a only
    if mode=="5":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI_a/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2L"          ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/3_POI_a/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L  "        ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/3_POI_a/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L  "        ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/3_POI_a/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L0.txt",1],
        ]
    #mode 6 is mc16d only
    if mode=="6":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI_d/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2L"          ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/3_POI_d/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L  "        ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/3_POI_d/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L  "        ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/3_POI_d/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L0.txt",1],
        ]
    #mode 7 compares results in different periods
    if mode=="7":
        value=[
            ["Comb."         ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2017  "        ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI_d/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2015-2016  "   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Diboson/1_POI_a/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
        ] 
        upperLim=  7
        lowerLim= -0.5



    #mode 8 compares single and 3POI fit in CBA
    if mode=="8":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["2L: single"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["2L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["1L: single"  ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["1L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["0L: single"  ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["0L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
        ]

    #mode 9 is single fit in CBA
    if mode=="9":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["2L"          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["1L"          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["0L"          ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
        ]

    #mode 10 is 3POI fit in CBA
    if mode=="10":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["2L"          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["1L"          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["0L"          ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
        ]
########################################################################################################################################################################################################
########################################################################################################################################################################################################
########################################################################################################################################################################################################
########################################################################################################################################################################################################
else:
    #mode 0 is 3POI fit
    if mode=="0":
        value=[
            ["Comb."         ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vh-mva/breakdown/muHatTable_mode11_Asimov1_SigXsecOverSM.txt",1],
            ["2L  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vh-mva/L0-L1-L2_3POI/breakdown/muHatTable_mode15_Asimov1_SigXsecOverSM_L2.txt",1],
            ["1L  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vh-mva/L0-L1-L2_3POI/breakdown/muHatTable_mode15_Asimov1_SigXsecOverSM_L1.txt",1],
            ["0L  "          ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vh-mva/L0-L1-L2_3POI/breakdown/muHatTable_mode15_Asimov1_SigXsecOverSM_L0.txt",1],
        ]
        upperLim=  5
        lowerLim=  0

    #mode -1 is single fit
    if mode=="-1":
        value=[
            ["Comb."         ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2L  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["1L  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["0L  "          ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
        ]

    #upperLim=  8
    #lowerLim= -1

    #mode 1 is 2POI fit
    if mode=="1":
        value=[
            ["Comb."         ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vh-mva/breakdown/muHatTable_mode11_Asimov1_SigXsecOverSM.txt",1],
            ["ZH  "          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vh-mva/WH-ZH_2POI/breakdown/muHatTable_mode15_Asimov1_SigXsecOverSMZH.txt",1],
            ["WH  "          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/FullRunII2019/statArea/outputs/2020-02-05/comb/vh-mva/WH-ZH_2POI/breakdown/muHatTable_mode15_Asimov1_SigXsecOverSMWH.txt",1],
        ]
        upperLim=  5
        lowerLim=  0

    #mode 2 is 3POI fit and single comparison
    if mode=="2":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2L: single"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/3_POI/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L: single"  ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["1L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/3_POI/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L: single"  ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["0L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/3_POI/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L0.txt",1],
        ]
    #mode 3 is 3POI fit CBA vs MVA
    if mode=="3":
        value=[
            ["Comb: MVA" ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["Comb: DMA" ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/CBA_1POI/012lep/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2L: MVA"   ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/3_POI/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L2.txt",1],
            ["2L: DMA"   ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/CBA_3POI/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L: MVA"   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/3_POI/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L1.txt",1],
            ["1L: DMA"   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/CBA_3POI/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L: MVA"   ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/3_POI/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L0.txt",1],
            ["0L: DMA"   ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/CBA_3POI/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L0.txt",1],
        ]
        upperLim=  5
        lowerLim=  0

    #mode 4 is 8POI fit
    if mode=="4":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs/1_POI/combined/muHatTable_mode10_Asimov0_SigXsecOverSM.txt",1],
            ["2L: 2j high" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs/8_POI/muHatTable_mode5_Asimov0_SigXsecOverSM_L2_J2_BMin150.txt",1],
            ["2L: 2j low"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs/8_POI/muHatTable_mode5_Asimov0_SigXsecOverSM_L2_J2_BMin75.txt",1],
            ["2L: 3j high" ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs/8_POI/muHatTable_mode5_Asimov0_SigXsecOverSM_L2_J3_BMin150.txt",1],
            ["2L: 3j low"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs/8_POI/muHatTable_mode5_Asimov0_SigXsecOverSM_L2_J3_BMin75.txt",1],
            ["1L: 2j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs/8_POI/muHatTable_mode5_Asimov0_SigXsecOverSM_L1_J2_BMin150.txt",1],
            ["1L: 3j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs/8_POI/muHatTable_mode5_Asimov0_SigXsecOverSM_L1_J3_BMin150.txt",1],
            ["0L: 2j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs/8_POI/muHatTable_mode5_Asimov0_SigXsecOverSM_L0_J2_BMin150.txt",1],
            ["0L: 3j"      ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs/8_POI/muHatTable_mode5_Asimov0_SigXsecOverSM_L0_J3_BMin150.txt",1],
        ]
        upperLim=  10
        lowerLim= -2

    #mode 5 is 3POI mc16a only
    if mode=="5":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI_a/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2L"          ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/3_POI_a/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L  "        ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/3_POI_a/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L  "        ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/3_POI_a/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L0.txt",1],
        ]
    #mode 6 is 3POI mc16d only
    if mode=="6":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI_d/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2L"          ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/3_POI_d/2lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L2.txt",1],
            ["1L  "        ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/3_POI_d/1lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L1.txt",1],
            ["0L  "        ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/3_POI_d/0lep/muHatTable_mode5_Asimov0_SigXsecOverSM_L0.txt",1],
        ]
    #mode 7 compares results in 3POI different periods
    if mode=="7":
        value=[
            ["Comb."         ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2017  "        ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI_d/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
            ["2015-2016  "   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "Input_file/ggV_Higgs_2Jul/1_POI_a/combined/muHatTable_mode5_Asimov0_SigXsecOverSM.txt",1],
        ]
        #upperLim=  8  ### newValerio
        #lowerLim= -1
        upperLim=  7
        lowerLim= -0.5
    #mode 8 compares single and 3POI fit in CBA
    if mode=="8":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["2L: single"  ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["2L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["1L: single"  ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["1L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["0L: single"  ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["0L: 3#mu"    ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
        ]

    #mode 9 is single fit in CBA
    if mode=="9":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["2L"          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["1L"          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["0L"          ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
        ]

    #mode 10 is 3POI fit in CBA
    if mode=="10":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["2L"          ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["1L"          ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["0L"          ,3, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
        ]

    #mode 11 is 1POI mc16a only
    if mode=="11":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["2L"          ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["1L  "        ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["0L  "        ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
        ]
    #mode 12 is 1POI mc16d only
    if mode=="12":
        value=[
            ["Comb."       ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["2L"          ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["1L  "        ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["0L  "        ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
        ]
    #mode 13 compares results in 1POI different periods
    if mode=="13":
        value=[
            ["Comb."         ,0, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["2017  "        ,1, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
            ["2015-2016  "   ,2, 1.0, 0.0, 0.0, 0.0, 0.0, "",1],
        ]
        #upperLim=  8  ### newValerio
        #lowerLim= -1
        upperLim=  7
        lowerLim= -0.5
        
Nobj=len(value)

####################################################################################################
           
for obj in value:
    if "txt" not in obj[7]: continue
    theF=file(obj[7])
    lines=theF.readlines()
    index=obj[8]
    incremental=0
    mu=1
    errT_u=0
    errT_d=0
    errS_u=0
    errS_d=0
    readThis=False
    for line in lines:
        if "muhat" in line:
            incremental+=1
            if incremental==index: readThis=True
            else                 : readThis=False
            ##print line
            if readThis: mu=float(line.split(":")[1])
        elif "Total" in line and readThis:
            ##print line.split()
            errT_u=float(line.split()[2])
            errT_d=float(line.split()[4])
        elif "DataStat" in line and errS_u==0 and readThis:
            errS_u=float(line.split()[2])
            errS_d=float(line.split()[4])
    theF.close()
    obj[2]=mu
    obj[3]=errT_u
    obj[4]=errT_d
    obj[5]=errS_u
    obj[6]=errS_d

####################################################################################################################
    
for obj in value:
    print obj
    
print "info on final fit: "
print " Mu val: "+str( value[0][2] )
print " Stat Err:  UP= "+str( value[0][5] )+"   DO= "+str( value[0][6] )
print " Sys  Err:  UP= "+str( math.sqrt( value[0][3]*value[0][3]-value[0][5]*value[0][5]) )+"   DO= "+str( math.sqrt(value[0][4]*value[0][4] - value[0][6]*value[0][6]) )
print " TOT  Err:  UP= "+str( value[0][3] )+"   DO= "+str( value[0][4] )

    
#####################################################################################################################
value.append( ["Fake", 0, 0, 0, 0, 0, 0, 0, 0 ] )

if Nobj>4: value.append( ["Fake", 0, 0, 0, 0, 0, 0, 0, 0 ] )

theHisto2=TH2D("plot2","plot2",20,lowerLim,upperLim,len(value),-0.5,len(value) )  ##0.5)
theHisto2.GetYaxis().SetTickSize(0)
theHisto2.GetYaxis().SetTickLength(0)
#if doVH: theHisto2.GetXaxis().SetTitle("#mu_{VH}^{b#bar{b}}")   ##"best fit #mu_{VH}^{b#bar{b}}"
#else   : theHisto2.GetXaxis().SetTitle("#mu_{VZ}^{b#bar{b}}")
if doVH: theHisto2.GetXaxis().SetTitle("#mu_{VH}^{bb}")
else   : theHisto2.GetXaxis().SetTitle("#mu_{VZ}^{bb}")

##best fit #mu=#sigma^{t#bar{t}H}/#sigma^{t#bar{t}H}_{SM} for m_{H}=125 GeV")
theHisto2.GetYaxis().SetLabelSize(0.06)
theHisto2.GetXaxis().SetTitleOffset(1.0)
theHisto2.GetXaxis().SetTitleSize(0.06)
theHisto2.GetYaxis().SetLabelSize(0.07)
count=0
for obj in value:
    if "Fake" in obj[0]: continue
    count+=1
    ##print "setting: "+str(obj[1])
    theHisto2.GetYaxis().SetBinLabel(count,obj[0])

c2 = TCanvas("test2","test2",800,600)
##c2 = TCanvas("test2","test2",600,600)
gPad.SetLeftMargin(0.20)
gPad.SetTopMargin(0.05)
theHisto2.Draw()

tmp5=TH1D("tmp5", "tmp5", 1,1,2)
tmp6=TH1D("tmp6", "tmp6", 1,1,2)
tmp5.SetLineColor(1)
tmp6.SetLineWidth(4)
tmp5.SetLineWidth(3)
colorS=8
if not doVH: colorS=kAzure+7##4
tmp6.SetLineColor(colorS)


g = TGraphAsymmErrors()
g.SetLineWidth(4)
g.SetMarkerColor(1)
g.SetMarkerStyle(20)
g.SetMarkerSize(1.6)

g2 = TGraphAsymmErrors()
g2.SetLineWidth(3)
g2.SetMarkerColor(1)
g2.SetMarkerStyle(20)
g2.SetMarkerSize(1.6)
g2.SetLineColor(colorS)

count=-1.0
for obj in value:
    count+=1.
    if "Fake" in obj[0]: continue
    scale=1.15 ## was 1.1
    if Nobj==4: scale=1.09  #was 1.05
    if Nobj>=5: scale=1.05
    g.SetPoint( int(count)     , obj[2]    , float(count)*scale )
    g.SetPointEXhigh( int(count), obj[3])
    g.SetPointEXlow(  int(count), obj[4])
    g.SetPointEYhigh( int(count), float(0) )
    g.SetPointEYlow(  int(count), float(0) )
    g2.SetPoint( int(count)     , obj[2]    , float(count)*scale )
    g2.SetPointEYhigh( int(count), float(0) )
    g2.SetPointEYlow(  int(count), float(0) )
    g2.SetPointEXhigh( int(count), obj[5] )
    g2.SetPointEXlow(  int(count), obj[6] )
    print " --> Setting point: "+str(obj[2])+"  +/- "+str(obj[3])
    sysUp=0
    if obj[3]*obj[3]-obj[5]*obj[5]>0:
        sysUp= math.sqrt(obj[3]*obj[3]-obj[5]*obj[5])
    sysDo=0
    if obj[4]*obj[4]-obj[6]*obj[6]>0:
        sysDo= math.sqrt(obj[4]*obj[4]-obj[6]*obj[6])

    mystring1 = "%.2f  {}^{+%.2f}_{#minus%.2f}" % (obj[2], obj[3], obj[4])
    #mystring2 ="{}^{+%.1f}_{#minus%.1f}  , {}^{+%.1f}_{#minus%.1f}            " % (obj[5],obj[6], sysUp, sysDo) ## add a
    mystring2 = "                    {}^{+%.2f}_{#minus%.2f} , " % (obj[5],obj[6]) ## add a
    mystring3 = "                             {}^{+%.2f}_{#minus%.2f}           " % (sysUp, sysDo) ## add a
    mystring4 = "                   (                 )         "

    textsize=0.051
    if mode=="3" or mode=="2" or mode=="4": textsize=0.044 
    offset=0
    startX=0.57
    slope=0.157 ## was0.16
    if Nobj==3: slope=0.21 ##was 0.2
    startY=0.22+slope*count+offset
   # if Nobj>4: startY=0.18+0.095*count+offset
   
   # if Nobj==8: startY=0.18+0.082*count+offset
   # if Nobj==9: startY=0.18+0.075*count+offset
   # if mode=="3": startY=0.19+0.079*count+offset
    if Nobj>4: startY=0.18+0.089*count+offset
    #if Nobj==8: startY=0.18+0.082*count+offset
    if Nobj==8: startY=0.18+0.080*count+offset
    if Nobj==9: startY=0.18+0.075*count+offset
    if mode=="3": startY=0.19+0.079*count+offset
    if mode=="4": startY=0.18+0.072*count+offset
    #if mode=="2": startY=0.19+0.075*count+offset
    myTextBold(startX     ,startY,1, mystring1, textsize)  # 0.144
    myText(    startX+0.01,startY,1, mystring2, textsize)  
    myText(    startX+0.01,startY,1, mystring3, textsize)
    myText(    startX+0.01,startY,1, mystring4, textsize)

slope=0.12
if Nobj==3: slope=0.14
if Nobj>4 : slope=0.10
if mode=="3": slope=0.055
if mode=="4": slope=0.050
if mode=="2": slope=0.060

yValLabel=0.32+(len(value)-1)*slope#-0.02

#if mode!="3":
#    textsize=0.045
#    myTextBold(0.65, yValLabel , 1, "( Tot. )"        , textsize) ##was 0.66 for %.1
#    myText(    0.76, yValLabel , 1, "( Stat., Syst. )", textsize-0.001) ##84
#else:
#    textsize=0.040
#    myTextBold(0.64, yValLabel , 1, "( Tot. )"        , textsize) ##was 0.66 for %.1
#    myText(    0.74, yValLabel , 1, "( Stat., Syst. )", textsize-0.001) ##84
    
if mode=="3" or mode=="4" or mode=="2" or mode=="8":
    textsize=0.040
    myTextBold(0.66, yValLabel , 1, "Tot."        , textsize) ##was 0.66 for %.1
    myText(    0.74, yValLabel , 1, "( Stat., Syst. )", textsize-0.001) ##84
else:
    textsize=0.045
    myTextBold(0.67, yValLabel , 1, "Tot."        , textsize) ##was 0.66 for %.1
    myText(    0.76, yValLabel , 1, "( Stat., Syst. )", textsize-0.001) ##84

if mode=="-1":
    myText(    0.45, yValLabel+0.035 , 1, "#bf{#it{Single Fit}}", textsize-0.001)
if mode=="5":
    myText(    0.47, yValLabel+0.035 , 1, "#bf{mc16a}", textsize-0.001)
if mode=="6":
    myText(    0.47, yValLabel+0.035 , 1, "#bf{mc16d}", textsize-0.001)
if mode=="8":
    myText(    0.47, yValLabel+0.035 , 1, "#bf{CBA}", textsize-0.001)
if mode=="9":
    myText(    0.42, yValLabel+0.035 , 1, "#bf{#it{Single Fit} CBA}", textsize-0.001)
#if mode=="0":
#    myText(    0.47, yValLabel+0.035 , 1, "#bf{#it{3#mu Fit}}", textsize-0.001)
if mode=="-1" or mode=="5" or mode=="6" or mode=="8" or mode=="9":
    legend4=TLegend(0.2,0.77,0.45,0.87)
    legend4.SetTextFont(42)
    legend4.SetTextSize(0.047)
    legend4.SetFillColor(0)
    legend4.SetLineColor(0)
    legend4.SetFillStyle(0)
    legend4.SetBorderSize(0)
    legend4.SetNColumns(2)
    legend4.AddEntry(tmp5,"Total" ,"l")
    legend4.AddEntry(tmp6,"Stat.","l")
    legend4.Draw("SAME")
else:
    legend4=TLegend(0.25,0.77,0.57,0.87)
    legend4.SetTextFont(42)
    legend4.SetTextSize(0.047)
    legend4.SetFillColor(0)
    legend4.SetLineColor(0)
    legend4.SetFillStyle(0)
    legend4.SetBorderSize(0)
    legend4.SetNColumns(2)
    legend4.AddEntry(tmp5,"Total" ,"l")
    legend4.AddEntry(tmp6,"Stat.","l")
    legend4.Draw("SAME")

 
subtract=+1.25
if Nobj>4: subtract+=1
line0=TLine(0., -0.5, 0., len(value)-subtract)
line0.SetLineWidth(3)
line0.SetLineStyle(3)
line0.SetLineColor(kGray+1)

if lowerLim > 0:
    line0.Draw("SAMEL") 

line1=TLine(1., -0.5, 1., len(value)-subtract)
line1.SetLineWidth(2)
line1.SetLineColor(kBlack)
line1.Draw("SAMEL")

yVal=0.5
if mode=="3": yVal=1.5
lineC=None
if mode=="3":
    lineC=TLine(theHisto2.GetXaxis().GetXmin(), yVal+0.05,theHisto2.GetXaxis().GetXmax(), yVal+0.05)
else:
    lineC=TLine(theHisto2.GetXaxis().GetXmin(), yVal,theHisto2.GetXaxis().GetXmax(), yVal)
lineC.SetLineWidth(2)
lineC.SetLineStyle(2)
lineC.SetLineColor(kBlack)
lineC.Draw("SAMEL")

#lall.Draw("SAME")
g.Draw("SAMEP")
g2.Draw("SAMEP")

#myText(0.15,0.65,1,"(13.3 fb^{-1} )",0.03)
#myText(0.15,0.50,1,"(13.2 fb^{-1} )",0.03)
#myText(0.15,0.34,1,"(13.2 fb^{-1} )",0.03)

##############ATLAS_LABEL(0.27,0.88,1)
##############myText(     0.40,0.88,1,"Internal",0.05)
##myText(     0.40,0.88,1,"Preliminary",0.05)

###### paper :
#ATLAS_LABEL(0.22,0.88,1)
# if doVH: myText(  0.42,0.88,1,"VH, H#rightarrow b#bar{b}",0.048)
# else   : myText(  0.42,0.88,1,"VZ, Z#rightarrow b#bar{b}",0.048)
###### internal :
ATLASLabel(0.22,0.88,0.115,"Internal",1,0.048)
if doVH: myText(  0.47,0.88,1,"VH, H#rightarrow b#bar{b}",0.048)
else   : myText(  0.47,0.88,1,"VZ, Z#rightarrow b#bar{b}",0.048)

#CECILIA_LABEL(0.18,0.88,1)
#if doVH: myText(  0.40,0.88,1,"VH Analysis",0.048)
#else   : myText(  0.40,0.88,1,"VZ Analysis",0.048)

##myText(     0.60,0.93,1,"#sqrt{s}=13 TeV, #scale[0.6]{#int}L dt=3.2 fb^{-1}")
if mode=="5" or mode=="11":
    myText(     0.66,0.88,1,"#sqrt{s}=13 TeV, 36.1 fb^{-1}",0.045)
elif mode=="6" or mode=="12":
    myText(     0.66,0.88,1,"#sqrt{s}=13 TeV, 43.6 fb^{-1}",0.045)
elif mode=="-99": ## who knows ? we may need to remember the 79.8 :o
    myText(     0.66,0.88,1,"#sqrt{s}=13 TeV, 79.8 fb^{-1}",0.045)
else:
    myText(     0.66,0.88,1,"#sqrt{s}=13 TeV, 139 fb^{-1}",0.045)
#new lumi is 79.8
##8.3 fb^{-1}")

c2.Update()
name="Plot_mu_"+str(mode)
if doVH: name+="_VH"
else   : name+="_VZ"
c2.Print( outDir+name+".png" )
c2.Print( outDir+name+".pdf" )

#############################################################################
#############################################################################

