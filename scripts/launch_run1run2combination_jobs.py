#!/usr/bin/env python

import sys
import os
import AnalysisMgr_VHbbRun2 as Mgr
import BatchMgr as Batch
import time


InputVersion = "Run1" #"SMVH_mva_v1"
syst_type="Systs"
#syst_type="StatOnly"
#syst_type="FloatOnly"
#syst_type="MCStat"
#syst_type="NormSys"
createSimpleWorkspace = False
runPulls = True
runReduced = False
runBreakdown = False
runLimits = False
runP0 = False
signalInjection = 0 # DEFAULT 0: no injection
doExp = "1" # "0" to run observed, "1" to run expected only
WSLink = "/afs/cern.ch/user/z/zaidan/work/hbb/InputsProcessingTools/WSMaker/workspaces/Run1/combined/125.root"
obsDataName = "combData"

def add_config(mass, syst='Systs', channel='0', var2tag='mBB', var1tag='mBB', use1tag=False, use4pjet=False, doptvbin=True):
    conf = Mgr.WorkspaceConfig_VHbbRun2(syst, InputVersion=InputVersion, MassPoint=mass, DoInjection=signalInjection, SkipUnkownSysts=True)

    if "0" in channel:
        conf.append_regions(var2tag, var1tag, True, False, False,
                            use1tag, use4pjet, doptvbin)
    if "1" in channel:
        conf.append_regions(var2tag, var1tag, False, True, False,
                            use1tag, use4pjet, doptvbin)
    if "2" in channel:
        conf.append_regions(var2tag, var1tag, False, False, True,
                            use1tag, use4pjet, doptvbin)

    if (var2tag is not var1tag) and (use1tag is True) :
        fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + "use1tag" + str(use1tag) + "_use4pjet" + str(use4pjet) + "_" + var2tag + "2tag_" + var1tag + "1tag"
    else:
        fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + "use1tag" + str(use1tag) + "_use4pjet" + str(use4pjet) + "_" + var2tag
    return conf.write_configs(fullversion)


if __name__ == "__main__":

    if len(sys.argv) is not 2:
        print "Usage: launch_default_jobs.py <outversion>"
        exit(0)

    outversion = sys.argv[1]

    #print "WARNING: if you haven't touched the base configuration, prepare to"
    #print "see your lxbatch quota die quickly"

    #print "I'll give you 5 seconds to abort..."
    #time.sleep(5)


    print "Adding config files..."
    channels = ["012"] # ["0"] #, "0", "1", "2"

    print "Let's start by a few jobs where we run simple fits to get pulls"
    print "and other things when they are ready"
    configfiles = []
    masses = [125]
    vs2tag = ['BDT']
    vs1tag = ['dRBB']
    use1tag = False
    use4pjet = [True]
    doptvbin = False
    if signalInjection is not 0:
       doExp="1"

    for c in channels:
        for m in masses:
            for var2tag in vs2tag:
                for var1tag in vs1tag:
                    for fpj in use4pjet:
                        configfiles += add_config(str(m), syst=syst_type, channel = c, var2tag=var2tag, var1tag=var1tag, use1tag=use1tag, use4pjet=fpj, doptvbin=doptvbin)
    for fi in configfiles:
        print fi

    wsAlg = ["-w"]
    if WSLink != "":
        wsAlg += ["-k", WSLink]
        
    queue = "1nd"

    if createSimpleWorkspace:
        print 'creating simple ws named %s' % outversion
        Batch.run_lxplus_batch(configfiles, outversion+"_fullRes", [wsAlg], queue)

    # hopefully queue 8nh is enough for now.
    if runPulls:
        Batch.run_lxplus_batch(configfiles, outversion+"_fullRes",
        #Batch.run_local_batch(configfiles, outversion+"_fullRes",
                              # ["--fcc", "4,5,9,10@{MassPoint}@"+obsDataName, "-m", "4,5,9,10", "-p", "1@{MassPoint}", "-t", "1@{MassPoint}"]
                              wsAlg+["--fcc", "5,7,9@{MassPoint}@"+obsDataName, "-m", "5,7,9", "-p", "0,3@{MassPoint}", "-t", "0,1@{MassPoint}"], queue)

    if runReduced:
        Batch.run_local_batch(configfiles, outversion+"_fullRes",
                              wsAlg+["-m", "5,7,9"])

    if runLimits:
        Batch.run_lxplus_batch(configfiles, outversion+"_limits",
                               wsAlg+["-l", doExp+",{MassPoint}"], queue)

    if runP0:
        Batch.run_lxplus_batch(configfiles, outversion+"_limits",
                               wsAlg+["-s", doExp+",{MassPoint}"], queue)

    if runBreakdown:
        Batch.run_lxplus_batch(configfiles, outversion+"_breakdown",
                               wsAlg+["-l", doExp+",{MassPoint}", "-u", doExp+",{MassPoint}"], queue)
