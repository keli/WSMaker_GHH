#!/usr/bin/env python
""" A small script to check the difference between two sets of histograms

Author: Changqiao LI
Date:   2018-04-13
Email:  changqiao.li@cern.ch

Description:
    Once the new inputs is coming, you can run this script to check the yields difference between new and old.
    And it can also be used to check if the expected updates are in place or not by plotting the shape comparison,
    and this will call CompareShape.py

"""

import argparse
import logging
import time
from ROOT import *
import InputsCheck as IC

from fnmatch import fnmatch

# this script is similar like InputsCheck.py
# but it is used to validate the hadd process
# when I create the STXS inputs (merging ICHEP inputs with STXS inputs)

# settings
#eos_dir = '/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/ICHEP2016/'
#IC.input_ref  = eos_dir + 'LAL/LimitHistograms.VH.vvbb.13TeV.LAL.v30.root' #0lep
#IC.input_test = eos_dir + 'LAL/BUG_LimitHistograms.VH.vvbb.13TeV.LAL.v30.root' #0lep
eos_dir='/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/'
#IC.input_ref  = eos_dir+'v31-10/OneLep/LimitHistograms.VH.lvbb.13TeV.mc16ad.LAL.v06.allvariables.root'
# 0L
#IC.input_ref  = eos_dir+'v31-10/ZeroLep/LimitHistograms.VH.vvbb.13TeV.mc16ad.AcademiaSinica.v06fixed2.MVA.root'
#IC.input_test = eos_dir+'STXS/LimitHistograms.VH.vvbb.13TeV.mc16ad.AcademiaSinica.v06fixed2.MVA.STXS.v04.root'

# 2L
IC.input_ref  = eos_dir+'v31-10/TwoLep/LimitHistograms.VH.llbb.13TeV.mc16ad.LiverpoolBmham.v11.root'
IC.input_test = eos_dir+'STXS/LimitHistograms.VH.llbb.13TeV.mc16ad.LiverpoolBmham.v11.STXS.v04.root'

#IC.input_test  = eos_dir+'v31-10/OneLep/LimitHistograms.VH.lvbb.13TeV.mc16ad.LAL.v06.allvariables.root'
IC.lep = 'L2'

IC.exp_diff = [
#  '*_2tag2jet_150ptv_WhfSR_mva',
]


# basic structure for inputs
IC.Processes = [
'data',
# W+jets
'Wbb','Wbc','Wbl','Wcc','Wcl','Wl',
# Z+jets
'Zbb','Zbc','Zbl','Zcc','Zcl','Zl',
# signal
# 1 lepton
'qqWlvH125',
#0 lepton:
'qqZvvH125', 'ggZvvH125',
#2 lepton:
'qqZllH125','ggZllH125',
# top
'ttbar','stopWt','stops','stopt',
# diboson
'WW','WZ','ZZ',
# Multijet
'multijetEl','multijetMu'
]

IC.Vars = {
'L0':['mva'],
'L1':['mvaR21'],
'L2':['mva'],
#'mva28', # 0 lep
#'mBB',
}

IC.Regions = {
'L0':[
  '2tag2jet_150ptv_SR',
  '2tag3jet_150ptv_SR',
],

'L1':[
  '2tag2jet_150ptv_WhfCR',
  '2tag3jet_150ptv_WhfCR',
  '2tag2jet_150ptv_WhfSR',
  '2tag3jet_150ptv_WhfSR',
],

'L2':[
  '2tag2jet_0_75ptv_SR',
  '2tag2jet_75_150ptv_SR',
  '2tag2jet_150ptv_SR',
  '2tag3jet_0_75ptv_SR',
  '2tag3jet_75_150ptv_SR',
  '2tag3jet_150ptv_SR',
## 2lep CR, please check mBBMVA instead of mva
#'2tag2jet_0_75ptv_topemucr',
#'2tag2jet_75_150ptv_topemucr',
#'2tag2jet_150ptv_topemucr',
#'2tag3jet_0_75ptv_topemucr',
#'2tag3jet_75_150ptv_topemucr',
#'2tag3jet_150ptv_topemucr',
],
}

IC.Systs = []
Systs = [
#{'Name':'blablabla','OneSide':False,'Region':[],'Process':[]},
# two side general sys
{'Name':'SysEG_RESOLUTION_ALL','OneSide':False},
{'Name':'SysEG_SCALE_ALL','OneSide':False},
{'Name':'SysEL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR','OneSide':False},
{'Name':'SysEL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR','OneSide':False},
{'Name':'SysEL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR','OneSide':False},
{'Name':'SysEL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_0_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_1_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_B_2_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_C_0_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_C_1_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_C_2_AntiKt4EMTopoJets','OneSide':False},
#{'Name':'SysFT_EFF_Eigen_C_3_AntiKt4EMTopoJets','OneSide':False}, #not in the CDI
{'Name':'SysFT_EFF_Eigen_Light_0_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_1_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_2_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_3_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_Eigen_Light_4_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_extrapolation_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysFT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets','OneSide':False},
{'Name':'SysJET_23NP_JET_BJES_Response','OneSide':False},
{'Name':'SysJET_23NP_JET_EffectiveNP_1','OneSide':False},
{'Name':'SysJET_23NP_JET_EffectiveNP_2','OneSide':False},
{'Name':'SysJET_23NP_JET_EffectiveNP_3','OneSide':False},
{'Name':'SysJET_23NP_JET_EffectiveNP_4','OneSide':False},
{'Name':'SysJET_23NP_JET_EffectiveNP_5','OneSide':False},
{'Name':'SysJET_23NP_JET_EffectiveNP_6','OneSide':False},
{'Name':'SysJET_23NP_JET_EffectiveNP_7','OneSide':False},
{'Name':'SysJET_23NP_JET_EffectiveNP_8restTerm','OneSide':False},
{'Name':'SysJET_23NP_JET_EtaIntercalibration_Modelling','OneSide':False},
{'Name':'SysJET_23NP_JET_EtaIntercalibration_NonClosure_highE','OneSide':False},
{'Name':'SysJET_23NP_JET_EtaIntercalibration_NonClosure_negEta','OneSide':False},
{'Name':'SysJET_23NP_JET_EtaIntercalibration_NonClosure_posEta','OneSide':False},
{'Name':'SysJET_23NP_JET_EtaIntercalibration_TotalStat','OneSide':False},
{'Name':'SysJET_23NP_JET_Flavor_Composition','OneSide':False},
{'Name':'SysJET_23NP_JET_Flavor_Response','OneSide':False},
{'Name':'SysJET_23NP_JET_Pileup_OffsetMu','OneSide':False},
{'Name':'SysJET_23NP_JET_Pileup_OffsetNPV','OneSide':False},
{'Name':'SysJET_23NP_JET_Pileup_PtTerm','OneSide':False},
{'Name':'SysJET_23NP_JET_Pileup_RhoTopology','OneSide':False},
{'Name':'SysJET_23NP_JET_PunchThrough_MC16','OneSide':False},
{'Name':'SysJET_23NP_JET_SingleParticle_HighPt','OneSide':False},
{'Name':'SysJET_JvtEfficiency','OneSide':False},
##{'Name':'SysJET_SR1_JET_GroupedNP_1','OneSide':False}, # the reduced set of NPs for JES which we don't use
##{'Name':'SysJET_SR1_JET_GroupedNP_2','OneSide':False},
##{'Name':'SysJET_SR1_JET_GroupedNP_3','OneSide':False},
{'Name':'SysMET_JetTrk_Scale','OneSide':False},
{'Name':'SysMET_SoftTrk_Scale','OneSide':False},
{'Name':'SysMETTrigStat','OneSide':False},
{'Name':'SysMETTrigTop','OneSide':False},
{'Name':'SysMUON_EFF_RECO_STAT','OneSide':False},
{'Name':'SysMUON_EFF_RECO_STAT_LOWPT','OneSide':False},
{'Name':'SysMUON_EFF_RECO_SYS','OneSide':False},
{'Name':'SysMUON_EFF_RECO_SYS_LOWPT','OneSide':False},
{'Name':'SysMUON_EFF_TrigStatUncertainty','OneSide':False},
{'Name':'SysMUON_EFF_TrigSystUncertainty','OneSide':False},
{'Name':'SysMUON_ID','OneSide':False},
{'Name':'SysMUON_EFF_ISO_STAT','OneSide':False},
{'Name':'SysMUON_EFF_ISO_SYS','OneSide':False},
{'Name':'SysMUON_MS','OneSide':False},
{'Name':'SysMUON_SAGITTA_RESBIAS','OneSide':False},
{'Name':'SysMUON_SAGITTA_RHO','OneSide':False},
{'Name':'SysMUON_SCALE','OneSide':False},
{'Name':'SysMUON_EFF_TTVA_STAT','OneSide':False},
{'Name':'SysMUON_EFF_TTVA_SYS','OneSide':False},
{'Name':'SysPRW_DATASF','OneSide':False},
{'Name':'SysTAUS_TRUEHADTAU_SME_TES_MODEL','OneSide':False},
{'Name':'SysTAUS_TRUEHADTAU_SME_TES_INSITU','OneSide':False},
{'Name':'SysTAUS_TRUEHADTAU_SME_TES_DETECTOR','OneSide':False},

## one side booked here
{'Name':'SysJET_JER_SINGLE_NP','OneSide':True},
{'Name':'SysMET_SoftTrk_ResoPara','OneSide':True},
{'Name':'SysMET_SoftTrk_ResoPerp','OneSide':True},

## some special syst. Only check for the process in 'Process'
{'Name':'SysTTbarMBB','OneSide':False,'Process':['ttbar']},
{'Name':'SysTTbarPTV','OneSide':False,'Process':['ttbar']},
{'Name':'SysTTbarPTVMBB','OneSide':False,'Process':['ttbar']},

{'Name':'SysStopWtbbACC','OneSide':False,'Process':['stopWt']},
{'Name':'SysStopWtothACC','OneSide':False,'Process':['stopWt']},
{'Name':'SysStopWtMTOP','OneSide':False,'Process':['stopWt']},
{'Name':'SysStopWtMBB','OneSide':False,'Process':['stopWt']},
{'Name':'SysStopWtPTV','OneSide':False,'Process':['stopWt']},

{'Name':'SysStoptMBB','OneSide':False,'Process':['stopt']},
{'Name':'SysStoptPTV','OneSide':False,'Process':['stopt']},

{'Name':'SysWMbb','OneSide':False,'Process':['Wbb','Wbc','Wbl','Wcc','Wcl']},
{'Name':'SysWPtV','OneSide':False,'Process':['Wbb','Wbc','Wbl','Wcc','Wcl']},
{'Name':'SysWPtVMbb','OneSide':False,'Process':['Wbb','Wbc','Wbl','Wcc','Wcl']},

{'Name':'SysZMbb','OneSide':False,'Process':['Zbb','Zbc','Zbl','Zcc','Zcl']},
{'Name':'SysZPtV','OneSide':False,'Process':['Zbb','Zbc','Zbl','Zcc','Zcl']},
{'Name':'SysZPtVMbb','OneSide':False,'Process':['Zbb','Zbc','Zbl','Zcc','Zcl']},

{'Name':'SysVVMbbME','OneSide':False,'Process':['WZ','ZZ']},
{'Name':'SysVVMbbPSUE','OneSide':False,'Process':['WZ','ZZ']},
{'Name':'SysVVPTVME','OneSide':False,'Process':['WZ','ZZ']},
{'Name':'SysVVPTVPSUE','OneSide':False,'Process':['WZ','ZZ']},
{'Name':'SysVVMbbVPSUE','OneSide':False,'Process':['WZ','ZZ']},
{'Name':'SysVHQCDscalePTV','OneSide':False,'Process':['qqWlvH125','qqZvvH125','ggZvvH125','qqZllH125','ggZllH125']},
{'Name':'SysVHQCDscaleMbb','OneSide':False,'Process':['qqWlvH125','qqZvvH125','ggZvvH125','qqZllH125','ggZllH125']},

{'Name':'SysVHPDFPTV','OneSide':False,'Process':['qqWlvH125','qqZvvH125','ggZvvH125','qqZllH125','ggZllH125']},
{'Name':'SysVHQCDscalePTV_ggZH','OneSide':False,'Process':['ggZvvH125','ggZllH125']},
{'Name':'SysVHQCDscaleMbb_ggZH','OneSide':False,'Process':['ggZvvH125','ggZllH125']},
{'Name':'SysVHPDFPTV_ggZH','OneSide':False,'Process':['ggZvvH125','ggZllH125']},
{'Name':'SysVHPDFPTV','OneSide':False,'Process':['qqWlvH125','qqZvvH125','ggZvvH125','qqZllH125','ggZllH125']},
{'Name':'SysVHUEPSMbb','OneSide':False,'Process':['qqWlvH125','qqZvvH125','ggZvvH125','qqZllH125','ggZllH125']},
{'Name':'SysVHNLOEWK','OneSide':False,'Process':['qqWlvH125','qqZvvH125','ggZvvH125','qqZllH125','ggZllH125']},




]

start_time = time.time()
IC.main()
print("--- %s seconds for this job---" % round(time.time() - start_time, 2))
