#!/usr/bin/env python

import sys
import os,socket
import AnalysisMgr_VHbbRun2 as Mgr
import BatchMgr as Batch
import time,copy

print_only=False
do_local=False
is_lxplus = 'lxplus' in socket.gethostname()
InputVersion = "SMVH_mva_v14puw-db"
#InputVersion = "SMVH_mva_v14bemu"
#InputVersion = "SMVH_mva_v14b"
InputVersion = "SMVH_mva_v14_5p8"
is_db = 'db' in InputVersion
manual_workspace='workspaces/20160721_camilla/{}lep.root'
manual_workspace='workspaces/camilla_v14_7p4_VZ/{}lep.root'
do_man_ws=False
createSimpleWorkspace = False # THIS WILL SET DEFAULT FIT CONFIG TO TRUE--CHANGE BELOW IF YOU NEED SIMPLE WORKSPACES FOR ANYTHING OTHER THAN POSTFIT PLOTS
big_switch=False
runPulls = False
runBreakdown = True
runRanks = False
runLimits = False
runP0 = False
signalInjection = 0 # DEFAULT 0: no injection
doExp = "0" # "0" to run observed, "1" to run expected only
#the argument for ConfigFlags; delimited and camel-cased for readability (cf. scripts/AnalysisMgr.py, parse_config_flags for full options):
baseline_configs = {'UseJet19NP':True,'DoDiboson':is_db}#,'DoSystsPlots':True,"DoShapePlots":True}
doftonly=False
doplots=False
channels = ["0", "1", "2", "012"]
channels = ["0", "1", "2"]
#    channels = ['1El','1Mu','1ElMu']
#    channels = ['2ElMu']
#    channels = ['1','2','012']
channels=['0','012']
if createSimpleWorkspace:
    baseline_configs.update({"DefaultFitConfig":True})
    if doplots:
        baseline_configs.update({"DoAllFTJETShapePlots":True} if doftonly else {'DoSystsPlots':True,"DoShapePlots":True})

def add_config(mass, syst='Systs', channel='0', var2tag='mBB', var1tag='mBB', use1tag=False, use4pjet=False, doptvbin=True):
    is_2poi = '2poi' in outversion
    is_3poi = '3poi' in outversion
    baseline_configs.update({'InputVersion':InputVersion,'MassPoint':mass,'DoInjection':signalInjection,'SkipUnkownSysts':True,'FitWHZH':is_2poi,'DecorrPOI': 'L' if is_3poi else ''})
    conf = Mgr.WorkspaceConfig_VHbbRun2(syst, **baseline_configs)

    elmu=-1
    if 'ElMu' in  channel:
        elmu=2
    elif 'Mu' in  channel:
        elmu=1
    elif 'El' in  channel:
        elmu=0

    if "0" in channel:
        conf.append_regions(var2tag, var1tag, True, False, False,
                            use1tag, use4pjet, doptvbin)
    if "1" in channel:
        conf.append_regions(var2tag, var1tag, False, True, False,
                            use1tag, use4pjet, doptvbin,elmu=elmu)
    if "2" in channel:
        conf.append_regions(var2tag, var1tag, False, False, True,
                            use1tag, use4pjet, doptvbin,elmu=elmu)

    if (var2tag is not var1tag) and (use1tag is True) :
        fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + syst + "_" + "use1tag" + str(use1tag) + "_use4pjet" + str(use4pjet) + "_" + var2tag + "2tag_" + var1tag + "1tag"
    else:
        fullversion = "VHbbRun2_13TeV_" + outversion + "_" + channel + "_" + mass + "_" + syst + "_" + "use1tag" + str(use1tag) + "_use4pjet" + str(use4pjet) + "_" + var2tag

    return conf.write_configs(fullversion)

def run_rank_cmd(configs,out,local=False,key=''):
    #if the workspace doesn't exist, let's make it--otherwise, no need for overhead
    cmd_opts = ['-w']
    if do_man_ws:
        if key!='':cmd_opts += ['-k','{}'.format(manual_workspace.format(key))] 
        else:cmd_opts += ['-k','{}'.format(manual_workspace)] 
    cmd_opts += ['-r','{{MassPoint}},{0}'.format('asimovData' if doExp=='1' else 'obsData')]#,'-n','{MassPoint}']

    
    if print_only:
        print 'rank ',cmd_opts
    elif do_local:
        Batch.run_local_batch(configs, out+"_rank",cmd_opts)
    elif is_lxplus:
        Batch.run_lxplus_batch(configs, out+"_fullRes",cmd_opts,jobs=10)
    else: 
        Batch.run_slurm_batch(configs, out+"_rank",cmd_opts,jobtype='_rank',mem='16384',jobs=10)
    return
    
if __name__ == "__main__":

    if len(sys.argv) is not 2:
        print "Usage: launch_default_jobs.py <outversion>"
        exit(0)

    outversion = sys.argv[1]
    """
    print "WARNING: if you haven't touched the base configuration, prepare to"
    print "see your lxbatch quota die quickly"

    print "I'll give you 5 seconds to abort..."
    time.sleep(5)
    """

    print "Adding config files..."
    if do_man_ws:
        channels=['012']

    print "Let's start by a few jobs where we run simple fits to get pulls"
    print "and other things when they are ready"
    configfiles = []
    masses = [125]
    v2tag_base = ['mvadiboson' if is_db else 'mva']
#    v2tag_base = ['']
    vs1tag = ['dRBB']
    use1tag = False
    use4pjet = [False] #[False, True]
    doptvbin = False
    syst_type = ["Systs"]#, "StatOnly", "MCStat"]
    if signalInjection is not 0:
        doExp="1"

    for sys in syst_type:
        for c in channels:
            twotag = copy.copy(v2tag_base)
            if createSimpleWorkspace:
                if c=='0':
                    twotag += ['mBB','MET','pTB1','pTB2','dRBB','dEtaBB','dPhiVBB','pTJ3','mBBJ','MEff','MEff3']
                elif c=='1':
                    twotag += ['mBB','mTW','pTV','j2VecCorrPt','dYWH','Mtop','j1VecCorrPt','dPhiLBmin','dPhiVBB','dRBB','MET']
                elif c=='2':
                    twotag += ['mBB','pTV','mLL','pTB1','pTB2','dRBB','dEtaBB','dPhiVBB']
                elif c=='012':
                    twotag += ['mBB']
            for m in masses:
                for var2tag in twotag:
                    for var1tag in vs1tag:
                        for fpj in use4pjet:
                            configfiles += add_config(str(m), syst=sys, channel = c, var2tag=var2tag, var1tag=var1tag, use1tag=use1tag, use4pjet=fpj, doptvbin=doptvbin)
    for fi in configfiles:
        print fi

    if createSimpleWorkspace:
        print 'creating simple ws named %s' % outversion
        Batch.run_local_batch(configfiles, outversion+"_fullRes", ["-w"])

    # hopefully queue 8nh is enough for now.
    keys= ['0','1','2','012']
#    keys=['012']
    if not do_man_ws:
        keys=['']

    algos=[]
    for key in keys:
        cmd_init = ['-w']
        if do_man_ws:
            if key!='':cmd_init += ['-k','{}'.format(manual_workspace.format(key))] 
            else:cmd_init += ['-k','{}'.format(manual_workspace)] 
        if runPulls or big_switch:
            cmd=cmd_init+["--fcc", "2,5,7,9@{MassPoint}", "-m", "2,5,7,9", "-p", "0,2,3@{MassPoint}", "-t", "0,1@{MassPoint}"]
            algos.append([cmd,"_fullRes"])

        if runRanks or big_switch:
            run_rank_cmd(configfiles,outversion+key,key=key)

        if runBreakdown or big_switch:
            cmd=cmd_init+["-u", doExp+",{MassPoint}"]
            algos.append([cmd,"_breakdown"])

        if runLimits or big_switch:
            cmd=cmd_init+["-l", doExp+",{MassPoint}"]
            algos.append([cmd,"_limits"])

        if runP0 or big_switch:
            cmd=cmd_init+["-s", doExp+",{MassPoint}"]
            algos.append([cmd,"_sigs"])

    for alg in algos:
        if print_only:
            print alg
        elif do_local:
            Batch.run_local_batch(configfiles, outversion+alg[1],alg[0])
        elif is_lxplus:
            Batch.run_lxplus_batch(configfiles,outversion+alg[1],alg[0],'8nh')
        else:
            Batch.run_slurm_batch(configfiles, outversion+alg[1],alg[0],mem='32000',jobs=1)
