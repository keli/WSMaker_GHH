#!/usr/bin/env/python

import sys
import os
import os.path
import argparse
import doPlotFromWS as plots
from ROOT import *
from ROOT import RooFit as RF
import analysisPlottingConfig
from AtlasRounding import atlasRound

import tabulate

def main(cfg, version, mass):
    RooMsgService.instance().setGlobalKillBelow(RF.ERROR)
    gROOT.SetBatch(True)
    gSystem.Load("libWSMaker.so")

    cfg.tables = True

    ws,g = plots.getWorkspace(version, mass)
    if not cfg._main_is_prefit: # if directory is present, then assumes we want postfit
        rfr, suffix = plots.getFitResult(cfg)
        plots.transferResults(cfg, ws, rfr)
        plotdir = "output/{0}/plots/postfit".format(version)
        tsuffix = "Postfit"
    else : # else, compute prefit yields
        plotdir = "output/{0}/plots/prefit".format(version)
        rfr = plots.getInitialFitRes(cfg, ws)
        ws.loadSnapshot("vars_initial")
        suffix = "Prefit"
        tsuffix = "Prefit"

    # cfg.make_slides = False

    os.system("mkdir -vp "+plotdir)

    if cfg.window:
        cfg._yieldsfile = os.path.join(plotdir, "Yields_{0}_{1}_{2}.yik".format(suffix, cfg.window[0],
                                                                                    cfg.window[1]))
    else:
        cfg._yieldsfile = os.path.join(plotdir, "Yields_{0}.yik".format(suffix))
    plots.getYields(cfg, ws, rfr, window=cfg.window)
    raw_yields = cfg._yields

    pretty_yields = make_pretty_yields_map(cfg, raw_yields)

    if cfg.verbose:
        print pretty_yields
    tables = make_tables(cfg, pretty_yields)
    final_text =r"% You need to include this file from a bigger tex-document"+'\n'
       
    for t in sorted(tables.keys()):
        #[year, chan, reg] = t.split('_')
        final_text += r"\begin{table}"+"\n"
        final_text += r"\centering"+"\n"
        final_text += "\\small"+"\n"
        final_text += tables[t]
        #final_text += r"\caption{The "+tsuffix+" yield on the "+chan+", "+reg+" category from "+year+"\label{"+tsuffix+chan+reg+year+"}}"+"\n"
        final_text += r"\end{table}"+"\n\n\n"
    final_text += '\n'
    if cfg.verbose:
        print final_text

    latexfile = "table.tex"
    os.system("rm -f "+latexfile)
    f = file(latexfile, 'w')
    f.write(final_text)
    f.close()
    g.Close()
    cfg._reset()


def make_pretty_yields_map(cfg, yields):
    """ Ugly logic to get things right """
    
    regions = sorted(yields.keys())
    samples = []
    for r in regions:
        samples.extend([plots.getCompName(y) for y in yields[r].keys()])
    samples = list(set(samples))
    print samples
    samples.sort(key = lambda(x): cfg.priorities[x])
    pretty_yields = [[s] for s in samples]
    for r in regions:
        compname_dict = {}
        for s in yields[r].keys():
            compname_dict[plots.getCompName(s)] = s
        for i,s in enumerate(samples):
            if s in compname_dict:
                pretty_yields[i].append( yields[r][compname_dict[s]] )
            else:
                pretty_yields[i].append( [0.0, 0.0] )
    regions.insert(0, "")
    pretty_yields.insert(0, regions)
    return pretty_yields


def make_tables(cfg, pretty_yields, print_errors=True):
    regions = {}

    def find(L, J, BMin, SR):
        return find_region(cfg, pretty_yields[0], L, J, BMin, SR)

    regions["Signal regions"] = [
            find(0, 2, 150, "SR"),
            find(0, 3, 150, "SR"),
            find(0, 2, 250, "SR"),
            find(0, 3, 250, "SR"),
            find(1, 2, 150, "SR"),
            find(1, 3, 150, "SR"),
            find(1, 2, 250, "SR"),
            find(1, 3, 250, "SR")
            ]

    regions["Signal regions 2-lep"] = [
            find(2, 2, 75, "SR"),
            find(2, 3, 75, "SR"),
            find(2, 2, 150, "SR"),
            find(2, 3, 150, "SR"),
            find(2, 2, 250, "SR"),
            find(2, 3, 250, "SR")
            ]

    regions["0-lepton control regions"] = [
            find(0, 2, 150, "CRLow"),
            find(0, 3, 150, "CRLow"),
            find(0, 2, 250, "CRLow"),
            find(0, 3, 250, "CRLow"),
            find(0, 2, 150, "CRHigh"),
            find(0, 3, 150, "CRHigh"),
            find(0, 2, 250, "CRHigh"),
            find(0, 3, 250, "CRHigh")
            ]

    regions["1-lepton control regions"] = [
            find(1, 2, 150, "CRLow"),
            find(1, 3, 150, "CRLow"),
            find(1, 2, 250, "CRLow"),
            find(1, 3, 250, "CRLow"),
            find(1, 2, 150, "CRHigh"),
            find(1, 3, 150, "CRHigh"),
            find(1, 2, 250, "CRHigh"),
            find(1, 3, 250, "CRHigh")
            ]

    regions["2-lepton control-regions"] = [
            find(2, 2, 150, "CRLow"),
            find(2, 3, 150, "CRLow"),
            find(2, 2, 250, "CRLow"),
            find(2, 3, 250, "CRLow"),
            find(2, 2, 150, "CRHigh"),
            find(2, 3, 150, "CRHigh"),
            find(2, 2, 250, "CRHigh"),
            find(2, 3, 250, "CRHigh")
            ]


    regions["2-lep $75~\GeV<\ptv<150~\GeV$ CRs"] = [
            find(2, 2, 75, "CRLow"),
            find(2, 3, 75, "CRlow"),
            find(2, 2, 75, "CRHigh"),
            find(2, 3, 75, "CRHigh")
            ]
    tables = {}
    for reg in sorted(regions.keys()):
        print "Making table for region", reg, "with columns", regions[reg]
        tables[reg] = make_subtable(cfg, pretty_yields, regions[reg], print_errors, reg)
    return tables

def make_subtable(cfg, yields, cols, print_errors=True, reg="SR"):

    #table = r"\begin{tabular}{l"+"|d"*len(cols)+"|}\n"
    table = r"\begin{tabular}{l"+'\n'
    table+= " S[table-alignment=right,table-format=5.0]@{\,$\pm$\,}S[table-alignment=left,table-format=4.0]\n"*len(cols)
    table+= "}\n"
    table += r"\toprule"+"\n"
    table += yields[0][0]

    title = reg
    if reg == "Signal regions 2-lep":
        title = "Signal regions"
    
    # if "75GeV" in reg:
    #     title = "2-lep $75~\GeV<\ptv<150~\GeV$ CRs"
    # elif "SR" in reg:
    #     title = "Signal regions"
    # elif reg == "CRHigh":
    #     title = "High $\Delta R$ control-regions"
    # elif reg == "CRLow":
    #     title = "Low $\Delta R$ control-regions"
    # elif reg == "75GeV":
    #     title = "$75~\GeV~<~\ptv~<~150~\GeV$"
    # else:
    #     title = "Control regions"

    table += r"\multirow{2}{*}{"+title+"}"
    for pos,i in enumerate(cols):
        bin = yields[0][i]
        if pos%2 == 0:
            if "Signal regions" in reg :
                table += r" & \multicolumn{4}{c}{"+cfg.getTableTitle(bin,1)+"}"
            else :
                bin_tmp = cfg.getTableTitle(bin,2).replace("2-jet, 2-$b$-tag", "")
                bin_tmp2 = bin_tmp.replace("$75\,\GeV<\ptv<150\,\GeV$, ", "").replace("$150\,\GeV<\ptv<250\,\GeV$, ", "").replace("$\ptv>250\,\GeV$, ", "")
                binName = bin_tmp2.replace("SR, ","Signal regions").replace("CRHigh, ","High $\Delta R$ control-regions").replace("CRLow, ","Low $\Delta R$ control-regions")
                table += r" & \multicolumn{4}{c}{"+binName+"}"
    table += r"\\"
    table += '\n'
    table += r"\cline{2-"+str(1+2*len(cols))+"}"+"\n"
    for pos,i in enumerate(cols):
        bin = yields[0][i]
        if pos%2 == 0:
            pTVbin = cfg.getTableTitle(bin,2).replace(", 2-jet, 2-$b$-tag", "").replace("SR, ","").replace("CRHigh, ","").replace("CRLow, ","") 
                
            # if reg == "75GeV" :
            #     pTVbin_tmp = cfg.getTableTitle(bin,2).replace("$75\,\GeV<\ptv<150\,\GeV$, 2-jet, 2-$b$-tag", "")
            #     pTVbin = pTVbin_tmp.replace("SR, ","Signal regions").replace("CRHigh, ","High $\Delta R$ control-regions").replace("CRLow, ","Low $\Delta R$ control-regions")
            # else :
            #     pTVbin = cfg.getTableTitle(bin,2).replace(", 2-jet, 2-$b$-tag", "").replace("SR, ","").replace("CRHigh, ","").replace("CRLow, ","") 
            table += r" & \multicolumn{4}{c}{"+pTVbin+"}"
    table += r"\\ \midrule"+"\n"
    table += " Sample "
    for pos,i in enumerate(cols):
        bin = yields[0][i]
        table += r" & \multicolumn{2}{c}{"+cfg.getTableTitle(bin,3)+"}"
    table += r"\\ \midrule"+"\n"
    for line in yields:

        if cfg.blind and ("AZh" in line[0] or line[0]=="Signal" or line[0] == "S/B" or line[0] == "S/sqrt(S+B)"):
            continue
        if line[0] == "MC" or line[0] == '':
            continue

        if line[0] == "data":
            table += r"\midrule"+"\n"
            table += pretty_sample_name(line[0]) + '\n'
            for pos, i in enumerate(cols):
                if pos == 0:
                    spacer = " & "
                else:
                    spacer = " && "
                table += spacer + r"\multicolumn{1}{c@{\,\hphantom{$\pm$}\,}}{\tablenum[table-alignment=right,table-format=5.0]{"+str(int(line[i]))+r"}}"+'\n'
            table += r"\\ \bottomrule"+"\n"
            continue
        if line[0] == "S/B":
            continue
        
        if line[0] == "S/sqrt(S+B)":
            continue
        if line[0] == "Bkg" or line[0] == "Signal":
            table += r"\midrule"+"\n"
        table = add_line(cfg, line, table, cols, print_errors)
        if line[0] == "Bkg":
            table += r"\midrule"+"\n"
    table += r"\end{tabular}"+"\n"
    return table


def add_line(cfg, line, mytable, cols, print_errors=True):
    if line[0] == "VH125":
        return mytable

    table = pretty_sample_name(line[0])
    #avoid printing empty lines
    allzero = True
    for pos,i in enumerate(cols):
        if line[i][0] <1 and line[i][0]>0:
            table += r" & \multicolumn{2}{c}{$<1$}"
        elif line[i][0] != 0:
            ## table += " & {0:.2f}\\hspace{{3pt}}+{1:.2f}".format(line[i][0], line[i][1])
            allzero = False            

            if print_errors:
                table += " & {} & {}".format(*atlasRound(float(line[i][0]), float(line[i][1]), nsig = 2))
            else:
                table += " & \\multicolumn{2}{c}{0}".format(atlasRound(float(line[i][0]), float(line[i][1]), nsig = 2)[0])
        else:
            table += r" & \multicolumn{2}{c}{--}"

    table += r"\\"+"\n"

    if (not allzero):
        mytable += table
    return mytable


def find_region(cfg, regs, L, J, BMin, SR):
    import plotMaker
    for i, r in enumerate(regs):
        if r != '':
            props = plotMaker.getPropertiesFromTag(cfg, r)
            if int(props['L']) == L and int(props['J']) == J and int(props['BMin']) == BMin and SR in props['D']:
                return i
    return -1

def pretty_sample_name(sample):
    names = {
            "data": "Data", "Zl":"$Z+ll$", "Zcl":"$Z+cl$", "Zhf":"$Z$~+~HF", "Zjets":"$Z$~+~jets", "Wl":"$W+ll$", "Wcl":"$W+cl$", "Whf":"$W$~+~HF", "Wjets":"$W$~+~jets",
            "stop":"Single top", "ttbar":"$\\ttbar$", "emuCRData":"$e\\mu$-CR data", "Tops":"Top bkgs", "multijetMu":"Multi-jet $\\mu$ sub-ch.", "multijet":"Multi-jet",
            "multijetEl":"Multi-jet $e$ sub-ch.", "diboson":"Diboson", "VH125":"Signal (fit)", "VHSTXS":"Signal (post-fit)",
            "Bkg":"Total bkg.", "SignalExpected":"Signal", "Signal": "Signal (post-fit)"
            }
    return names[sample]


if __name__ == "__main__":

    gSystem.Load("libWSMaker.so")
    wdir = os.environ["WORKDIR"]
    gROOT.ProcessLine("#include \""+wdir+"/WSMaker/roofitUtils.hpp\"")

    class MyParser(argparse.ArgumentParser):
        def error(self, message):
            sys.stderr.write('error: %s\n' % message)
            self.print_help()
            sys.exit(2)

    parser = MyParser(description='Create yield tables from a given workspace.', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('workspace', help = 'workspace/{name}/{something}/{mass}.root -> pass {name}')
    parser.add_argument('-m', '--mass', type = int, default = 125,
                        help = 'workspace/{name}/{something}/{mass}.root -> pass {mass}', dest = 'mass')
    parser.add_argument('-t', '--table_modes', default = '0',
                        help = """Comma-separated list of tables to create:
    0: prefit table
    1: bkg-only postfit table
    2: s+b postfit table""", dest = 'mode')
    parser.add_argument('-f', '--fitres', help = "", default = None, dest = 'fitres')
    parser.add_argument('pass_to_user', nargs = argparse.REMAINDER, default = [])
    args = parser.parse_args()

    cfg = analysisPlottingConfig.Config(args.pass_to_user)

    wsname = args.workspace
    modes = [int(s) for s in args.mode.split(',')]

    mass = args.mass

    fitres = args.fitres
    if fitres is None:
        fitres = wsname

    if os.path.sep in fitres:
        fcc = fitres
    else:
        fcc = "output/" + fitres + "/fccs"

    cfg._fcc_directory = fcc

    for mode in modes:
        if mode == 0:
            print "Doing prefit tables"
            cfg._main_is_prefit = True
            main(cfg, wsname, mass)
        elif mode == 1:
            print "Doing bkg-only postfit tables"
            cfg._is_conditional = True
            cfg._is_asimov = False
            cfg._mu = 0
            cfg._main_is_prefit = False
            main(cfg, wsname, mass)
        elif mode == 2:
            print "Doing s+b postfit tables"
            cfg._is_asimov = False
            cfg._mu = 1
            cfg._main_is_prefit = False
            main(cfg, wsname, mass)
        elif mode == 3:
            print "Doing mu=1 postfit tables"
            cfg._main_is_prefit = False
            cfg._is_conditional = True
            cfg._is_asimov = False
            cfg._mu = 1
            main(cfg, wsname, mass)
        else:
            print "Mode", mode, "is not recognized !"
