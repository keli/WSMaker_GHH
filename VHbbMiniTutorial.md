# Mini-tutorial to get started with the basics of VHbb fit

## Where to find documentation ?
There is quite some documentation for the package, but it's scattered a lot. There is a
[gitlab issue about reorganizing it](https://gitlab.cern.ch/atlas-physics/higgs/hbb/WSMaker/issues/3)

### Help on usage
* [Twiki WorkspaceMaker](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/WorkspaceMaker)
Outdated in many aspects, but still useful to read, especially section
_What to do after making workspaces ?_
* README of the package, also [available online](https://nmorange.web.cern.ch/nmorange/WSMaker/html/index.html)
* The big HowTo of the package, also [available online](https://nmorange.web.cern.ch/nmorange/WSMaker/html/HowTo.html)

*Don't hesitate to improve the HowTo file while you learn the package, and open merge requests for it.*

### More specialized documentation
* C++ code documentation (basically all the workspace-making process) is
[gathered through doxygen](https://nmorange.web.cern.ch/nmorange/WSMaker/html/index.html)
* Config switches recognized in the workspace-making process are (when documented properly)
[gathered by doxygen](https://nmorange.web.cern.ch/nmorange/WSMaker/html/conf.html)
* [List of open issues (improvements and bugs)](https://gitlab.cern.ch/atlas-physics/higgs/hbb/WSMaker/issues)

### Mailing lists
Please use:

* atlas-phys-stat-wsmaker@cern.ch for everything on the package in general
* atlas-phys-higgs-hsg5vh-fits@cern.ch for all questions on the VHbb analysis itself

## Mini-tutorial

### Setup
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-physics/higgs/hbb/WSMaker_VHbb.git
cd WSMaker_VHbb
source setup.sh
mkdir inputs
cd build
cmake ..
make -j8
```

### Getting the inputs
The configurations for the inputs (i.e what input files should be used for each lepton
channel) are stored in inputConfigs. Example: getting the test inputs for MC16a

* `SplitInputs -v SMVHVZ_Summer18_MVA_mc16a_v03 -inDir /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/`
* The files are split so we have 1 file per analysis region per distribution
* The split files are stored in `inputs/SMVHVZ_Summer18_MVA_mc16a_v03`

### Running stuff
The main high-level file used to configure what we create and what we do is
`scripts/launch_default_jobs.py`

It is responsible for two things (which are done simultaneously):

* writing config files used for the creation of workspaces, based on some switches:
`InputVersion`, `doCutBase`, `doDiboson`, `channels`, `syst_type`, `doplots`...
* use these files to create workspaces, then fit them in various ways to extract results

Most of the configuration switches at the beginning of `launch_default_jobs` should be quite
self-explanatory. Set `InputVersion` to `SMVHVZ_Summer18_MVA_mc16a_v03`, make sure `GlobalRun` is off.
If you want a pretty complete set of results to look at, turn on
`createSimpleWorkspace`, `runPulls`, `runBreakdown`, `runRanks`, `runLimits`, `runP0`.
`runToyStudy` is long and very rarely used. Note that `runRanks` will only compute the values
needed for the ranking plot. You still need to call
`python WSMakerCore/scripts/makeNPrankPlots.py theFullWSName` by hand afterwards. This is a mini-bug in
`launch_default_jobs` and could be fixed in 1 line.

Once a configuration is chosen, one just has to type `python launch_default_jobs.py myOutputTag`.
That will send jobs in parallel on the local machine. 
The config files will appear in `configs/`, the workspaces will appear in `workspaces/`, and the
outputs will appear in... many places depending on what is run: `plots/`, `fccs/`, `logs/`,
possibly `root-files/`... There is an open issue to improve on that:
https://gitlab.cern.ch/atlas-physics/higgs/hbb/WSMaker/issues/18

### Changing more in-depth configuration
That goes outside the scope of this mini-tutorial. Please refer to the documentation,
and if you can't find what you're looking for, ask questions on the mailing lists.

## Reproducing ICHEP observation results
### Setup
Start in a completely new directory

```
git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-physics/higgs/hbb/WSMaker.git WSMaker_ICHEP
cd WSMaker_ICHEP
git checkout WSMaker-00-02-03
git submodule update
```

### Instructions:
Follow: [scripts/ObservationResults_HowTo.md](https://gitlab.cern.ch/atlas-physics/higgs/hbb/WSMaker_VHbb/blob/master/scripts/ObservationResults_HowTo.md)


## Reproducing EPS results
### Setup
Start in a completely new directory

```
git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-physics/higgs/hbb/WSMaker.git WSMaker_EPS
cd WSMaker_EPS
git checkout WSMaker-10-00-01
git submodule update
# edit setup.sh and add your login so you get $ANALYSISTYPE=VHbbRun2
source setup.sh
make -j8
mkdir inputs
```

### Getting the inputs
The following should be enough:

```
./SplitInputs -v SMVHVZ_LHCP17_MVA_v06Paper
./SplitInputs -v SMVH_LHCP17_CUT_v08Paper
```

### Reproduce the results
You should be able to use `scripts/VHbbRun2/launch_default_jobs.py` or `scripts/VHbbRun2/launch_all_jobs.py` to get
everything as in the paper.
Just edit those scripts to launch whatever you want. To get all results in one go, you can make use of the `GlobalRun`
keyword in `launch_default_jobs.py`. The usage is explained in `scripts/VHbbRun2/AdvGatherResults.HowTo`.

