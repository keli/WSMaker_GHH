#include "systematiclistsbuilder_ghhrun2.hpp"

#include <algorithm>
#include <map>
#include <unordered_map>
#include <vector>
#include <iostream>

#include <TString.h>
#include <TFile.h>
#include <TH2F.h>

#include "WSMaker/configuration.hpp"
#include "WSMaker/containerhelpers.hpp"
#include "WSMaker/properties.hpp"
#include "WSMaker/sample.hpp"
#include "WSMaker/systematic.hpp"
#include "WSMaker/finder.hpp"

void SystematicListsBuilder_GHHRun2::fillHistoSystsRenaming() {
  // rewrite rules

  // todo Ask 2 lepton guys to follow same convention as 0+1 lepton so we can get rid of this
  // Now everyone uses the same convention, but with the AntiKt.. name, making it long, still need remove it ?
  std::vector<std::string> btagSysts { "extrapolation_from_charm", "extrapolation",
      //L
      "Eigen_Light_0", "Eigen_Light_1", "Eigen_Light_2", "Eigen_Light_3", "Eigen_Light_4", "Eigen_Light_4",
      "Eigen_Light_5", "Eigen_Light_6", "Eigen_Light_7", "Eigen_Light_8", "Eigen_Light_9", "Eigen_Light_10",
      "Eigen_Light_11", "Eigen_Light_12", "Eigen_Light_13", "Eigen_Light_14", "Eigen_Light_15", "Eigen_Light_16",
      "Eigen_Light_17", "Eigen_Light_18", "Eigen_Light_19",
      //C
      "Eigen_C_0", "Eigen_C_1", "Eigen_C_2", "Eigen_C_3", "Eigen_C_4", "Eigen_C_5", "Eigen_C_6", "Eigen_C_7",
      "Eigen_C_8", "Eigen_C_9", "Eigen_C_10", "Eigen_C_11", "Eigen_C_12", "Eigen_C_13", "Eigen_C_14", "Eigen_C_15",
      "Eigen_C_16", "Eigen_C_17", "Eigen_C_18", "Eigen_C_19",
      //B
      "Eigen_B_0", "Eigen_B_1", "Eigen_B_2", "Eigen_B_3", "Eigen_B_4","Eigen_B_5", "Eigen_B_6", "Eigen_B_7",
      "Eigen_B_8", "Eigen_B_9", "Eigen_B_10", "Eigen_B_11", "Eigen_B_12", "Eigen_B_13", "Eigen_B_14", "Eigen_B_15",
      "Eigen_B_16", "Eigen_B_17", "Eigen_B_18", "Eigen_B_19", "Eigen_B_20", "Eigen_B_21", "Eigen_B_22", "Eigen_B_23",
      "Eigen_B_24", "Eigen_B_25", "Eigen_B_26", "Eigen_B_27", "Eigen_B_28", "Eigen_B_29", "Eigen_B_30", "Eigen_B_31",
      "Eigen_B_32", "Eigen_B_33", "Eigen_B_34", "Eigen_B_35", "Eigen_B_36", "Eigen_B_37", "Eigen_B_38", "Eigen_B_39",
      "Eigen_B_40", "Eigen_B_41", "Eigen_B_42", "Eigen_B_43", "Eigen_B_44"
  };

  for (auto sysname : btagSysts) {
    m_renameHistoSysts.emplace( "SysFT_EFF_"+sysname+"_AntiKt4EMTopoJets" , "SysFT_EFF_"+sysname);
  }
}

void SystematicListsBuilder_GHHRun2::listAllUserSystematics(bool useFltNorms) {
  using P = Property;

  bool doNewRegions = m_config.getValue("NewRegions", false); 
  bool doDiboson = m_config.getValue("DoDiboson", false);
  bool doWHZH = m_config.getValue("FitWHZH", false);
  bool doWZZZ = m_config.getValue("FitWZZZ", false);
  bool doVHVZ = m_config.getValue("FitVHVZ", false);
  bool doSTXS = m_config.getValue("DoSTXS", false);
  bool doCutBase = m_config.getValue("CutBase", false);

  std::vector<TString> sig_decorr = m_config.getStringList("DecorrPOI");

  TString massPoint = m_config.getValue("MassPoint", "125");

  bool defltFitConfig = m_config.getValue("DefaultFitConfig", false);
  bool hasZeroLep = defltFitConfig ? true : CategoryIndex::hasMatching(P::nLep==0);
  bool hasOneLep = defltFitConfig ? true : CategoryIndex::hasMatching(P::nLep==1);
  bool hasTwoLep = defltFitConfig ? true : CategoryIndex::hasMatching(P::nLep==2);

  // DO NOT APPLY ! RESCALE ALL MC TO A DIFFERENT INTEGRATED LUMINOSITY
  //  normFact("lumirescale", {"MC"}, 5.81/13.18, -10, 10, true); // this one is constant
  //  normFact("lumirescale", {"MC"}, 7.37/13.18, -10, 10, true); // this one is constant

  // list them all !

  if(doWHZH) {
    addPOI("SigXsecOverSMWH", {"WH"}, 1, -40, 40);
    addPOI("SigXsecOverSMZH", {"ZH"}, 1, -40, 40);
  }
  else if(doWZZZ) {
    addPOI("SigXsecOverSMWZ", {"WZ"}, 1, -40, 40);
    addPOI("SigXsecOverSMZZ", {{"ZZ","ggZZ"}}, 1, -40, 40);
  }
  else if(doSTXS) {
    addPOI_STXS();
  }
  else if(sig_decorr.size() != 0) { // TODO or just extend decorrSysForCategories for the POIs ?
    std::vector<Property> decorrelations;
    for(auto& d : sig_decorr) {
      decorrelations.push_back(Properties::props_from_names.at(d));
    }
    addPOI("SigXsecOverSM", {"Sig", {}, std::move(decorrelations)}, 1, -40, 40);
  } else {
    //addPOI("SigXsecOverSM", {"Sig", {{}}, {P::nLep}}, 1, -40, 40);
    addPOI("SigXsecOverSM", {"Sig"}, 1, -40, 40);
  }

  // ttbar
  // --------------------------- observation
  // // NM 17-05-16
  // // floating parameters decorrelated between 2 and (0+1) lepton channels.
  // // In 2 lepton, float 2jet and 3jet independently
  // normFact("ttbar", SysConfig{"ttbar"}.decorrIn({ (P::nLep==2)&&(P::nJet==2),(P::nLep==2)&&(P::nJet==3),(P::nLep==1)&&(P::binMin==75) }));
  // // 3/2J uncertainty
  // // NM 17-05-19
  // // correlated (0+1) lepton channels
  // // 9% prior for 1-lepton, 9% prior for 0 lepton
  // // Applied on 2jet regions
  // normSys("SysttbarNorm", 0.09, SysConfig{"ttbar"}.applyIn((P::nJet==2)&&(P::nLep==0)).decorr(P::nJet));
  // normSys("SysttbarNorm", 0.09, SysConfig{"ttbar"}.applyIn((P::nLep==1)&&(P::nJet==2)).decorr(P::nJet).decorrIn({(P::binMin==75)}));
  // // 0L/1L uncertainty: 8%
  // // NM 17-05-19
  // if(hasOneLep) {
  //   normSys("SysttbarNorm", 0.08, SysConfig{"ttbar"}.applyIn(P::nLep==0).decorr(P::nLep));
  // }
  // // 1L WhfCR vs SR: 25%
  // // NM 17-05-19
  // normSys("SysttbarNorm", 0.25, SysConfig{"ttbar"}.applyIn(P::descr=="WhfCR").decorr({P::descr, P::nLep}));
  // --------------------------- full Run 2
  // Th.C 10-09-19
  // Floating parameters decorrelated between 2/3 jets and cross-channels correlated
  // In 0-lep fit alone keep only one floating normalization
  normFact("ttbar", SysConfig{"ttbar"}.decorr(P::nJet).decorrIn({ (P::nLep==1)&&(P::binMin==75) }));
  // Additional prior for 0-lep extrapolations
  // 8% prior 1/2 -> 0 lep in combined fit
  // 9% prior 2->3 jet in 0-lep standalone
  if(hasOneLep) {
    normSys("SysttbarNorm", 0.08, SysConfig{"ttbar"}.applyIn(P::nLep==0).decorr(P::nLep));
  } 

  // single top
  // cross-sections from Top MC Twiki
  sampleNormSys("stops", 0.046);
  sampleNormSys("stopt", 0.044);//PF
  sampleNormSys("stopWt", 0.062);//PF
  normSys("SysstoptAcc", 0.17, SysConfig{"stopt"}.applyIn(P::nJet==2).decorrIn({ (P::nLep==1)&&(P::binMin==75) }));//PF   
  normSys("SysstoptAcc", 0.20, SysConfig{"stopt"}.applyIn(P::nJet==3).decorrIn({ (P::nLep==1)&&(P::binMin==75) }));//PF   
  // normSys("SysstopWtAcc", 0.35, SysConfig{"stopWt"}.applyIn(P::nJet==2));//PF
  // normSys("SysstopWtAcc", 0.41, SysConfig{"stopWt"}.applyIn(P::nJet==3));//PF

  // data driven ttbar
  normSys("SysTop_Extrapolation_From_Topemu_2J", 0.008, SysConfig{"emuCRData"}.applyIn((P::nLep==2)&&(P::nJet==2)));
  normSys("SysTop_Extrapolation_From_Topemu_3pJ", 0.005, SysConfig{"emuCRData"}.applyIn((P::nLep==2)&&(P::nJet==3)));



  // V+jets
  //
  // UPDATE of all V+jets normalizations uncertainties:
  // NM 17-05-19

  // Wl and Zl
  sampleNormSys("Wl", 0.32);
  sampleNormSys("Zl", 0.18);

  // Wcl and Zcl
  sampleNormSys("Wcl", 0.37);
  sampleNormSys("Zcl", 0.23);

  // floating Wbb+Wbc+Wbl+Wcc - put extra systs on Wbl and Wcc to allow ratios to change
  if(hasOneLep) {
    normFact("Wbb", SysConfig{"Whf"}.decorr(P::nJet).decorrIn({ (P::nLep==1)&&(P::binMin==75) }));
    // 0/1L
    normSys("SysWbbNorm", 0.05, SysConfig{"Whf"}.applyIn(P::nLep==0).decorr(P::nLep));
  }
  else {
    normSys("SysWbbNorm", 0.33, SysConfig{"Whf"}.decorr(P::nJet));
  }

  // CR to SR extrapolation: 7% on SR
  // if (doNewRegions) normSys("SysWbbNorm", 0.10, SysConfig{"Whf"}.applyIn({(P::descr=="SR")&&(P::nLep==1)}).decorr(P::descr));
  if (!doCutBase && !doNewRegions) {
    normSys("SysWbbNorm", 0.10, SysConfig{"Whf"}.applyIn(P::descr=="WhfSR").decorr(P::descr)); //Only use WhfCR when running MVA fit
  } else if(doNewRegions){ // additional SR-CR unc when BDT_ShToMG is shape only
    normSys("SysWbbCRSRextrap", -0.115, SysConfig{"Wjets"}.applyIn({P::descr=="CRLow" && P::nJet==2 && P::nLep==0}));
    normSys("SysWbbCRSRextrap", 0.148, SysConfig{"Wjets"}.applyIn({P::descr=="CRHigh" && P::nJet==2 && P::nLep==0}));
    normSys("SysWbbCRSRextrap", -0.036, SysConfig{"Wjets"}.applyIn({P::descr=="CRLow" && P::nJet==3 && P::nLep==0}));
    normSys("SysWbbCRSRextrap", 0.051, SysConfig{"Wjets"}.applyIn({P::descr=="CRHigh" && P::nJet==3 && P::nLep==0}));
    
    normSys("SysWbbCRSRextrap", -0.077, SysConfig{"Wjets"}.applyIn({P::descr=="CRLow" && P::nJet==2 && P::nLep==1}));
    normSys("SysWbbCRSRextrap", 0.149, SysConfig{"Wjets"}.applyIn({P::descr=="CRHigh" && P::nJet==2 && P::nLep==1}));
    normSys("SysWbbCRSRextrap", -0.056, SysConfig{"Wjets"}.applyIn({P::descr=="CRLow" && P::nJet==3 && P::nLep==1}));
    normSys("SysWbbCRSRextrap", 0.070, SysConfig{"Wjets"}.applyIn({P::descr=="CRHigh" && P::nJet==3 && P::nLep==1}));
  }
  
  // FLAVOUR COMPOSITION
  normSys("SysWblWbbRatio", 0.10, SysConfig{"Wbl"}.applyIn(P::nLep==0));
  normSys("SysWblWbbRatio", 0.30, SysConfig{"Wbl"}.applyIn(P::nLep==1));
  //
  normSys("SysWccWbbRatio", 0.26, SysConfig{"Wcc"}.applyIn(P::nLep==0));
  normSys("SysWccWbbRatio", 0.23, SysConfig{"Wcc"}.applyIn(P::nLep==1));
  //
  normSys("SysWbcWbbRatio", 0.15, SysConfig{"Wbc"}.applyIn(P::nLep==0));
  normSys("SysWbcWbbRatio", 0.30, SysConfig{"Wbc"}.applyIn(P::nLep==1));

  // floating Zbb+Zbc+Zbl+Zcc - put extra systs on Zbl and Zcc to allow ratios to change
  if(hasTwoLep || hasZeroLep) {
    normFact("Zbb", SysConfig{"Zhf"}.decorr(P::nJet).decorrIn({ (P::nLep==2)&&(P::binMin==75) }) );
  }
  else {
    normSys("SysZbbNorm", 0.48, {"Zhf"});
  }
  // 0/2L channel
  if(hasTwoLep) {
    normSys("SysZbbNorm", 0.07, SysConfig{"Zhf"}.applyIn(P::nLep==0).decorr(P::nLep));
  }

  if(doNewRegions){ // additional SR-CR unc when Z_mBB is shape only
    normSys("SysZbbCRSRextrap_CRLow", -0.06, SysConfig{"Zjets"}.applyIn({P::descr=="CRLow" && P::nJet==2 && P::nLep==0}));
    normSys("SysZbbCRSRextrap_CRHigh", 0.038, SysConfig{"Zjets"}.applyIn({P::descr=="CRHigh" && P::nJet==2 && P::nLep==0}));
    normSys("SysZbbCRSRextrap_CRLow", -0.066, SysConfig{"Zjets"}.applyIn({P::descr=="CRLow" && P::nJet==3 && P::nLep==0}));
    normSys("SysZbbCRSRextrap_CRHigh", 0.039, SysConfig{"Zjets"}.applyIn({P::descr=="CRHigh" && P::nJet==3 && P::nLep==0}));
    
    normSys("SysZbbCRSRextrap_CRLow", -0.099, SysConfig{"Zjets"}.applyIn({(P::descr=="CRLow" && P::nJet==2 && P::nLep==2 && P::binMin==150) , (P::descr=="CRLow" && P::nJet==2 && P::nLep==2 && P::binMin==250)}));
    normSys("SysZbbCRSRextrap_CRHigh", 0.027, SysConfig{"Zjets"}.applyIn({(P::descr=="CRHigh" && P::nJet==2 && P::nLep==2 && P::binMin==150) , (P::descr=="CRHigh" && P::nJet==2 && P::nLep==2 && P::binMin==250)}));
    normSys("SysZbbCRSRextrap_CRLow", -0.038, SysConfig{"Zjets"}.applyIn({(P::descr=="CRLow" && P::nJet==3 && P::nLep==2 && P::binMin==150) , (P::descr=="CRLow" && P::nJet==3 && P::nLep==2 && P::binMin==250)}));
    normSys("SysZbbCRSRextrap_CRHigh", 0.041, SysConfig{"Zjets"}.applyIn({(P::descr=="CRHigh" && P::nJet==3 && P::nLep==2 && P::binMin==150) , (P::descr=="CRHigh" && P::nJet==3 && P::nLep==2 && P::binMin==250)}));
    
    normSys("SysZbbCRSRextrap_75-150_L2_CRLow", -0.099, SysConfig{"Zjets"}.applyIn({P::descr=="CRLow" && P::nJet==2 && P::nLep==2 && P::binMin==75}));
    normSys("SysZbbCRSRextrap_75-150_L2_CRHigh", 0.027, SysConfig{"Zjets"}.applyIn({P::descr=="CRHigh" && P::nJet==2 && P::nLep==2 && P::binMin==75}));
    normSys("SysZbbCRSRextrap_75-150_L2_CRLow", -0.038, SysConfig{"Zjets"}.applyIn({P::descr=="CRLow" && P::nJet==3 && P::nLep==2 && P::binMin==75}));
    normSys("SysZbbCRSRextrap_75-150_L2_CRHigh", 0.041, SysConfig{"Zjets"}.applyIn({P::descr=="CRHigh" && P::nJet==3 && P::nLep==2 && P::binMin==75}));
  }
  
  // Let's introduce helpers to define the uncertainties
  PropertiesSet ps_2Lep2Jet = (P::nLep==2)&&(P::nJet==2);
  PropertiesSet ps_2Lep3Jet = (P::nLep==2)&&(P::nJet==3);
  PropertiesSet ps_0Lep2Jet = (P::nLep==0)&&(P::nJet==2);
  PropertiesSet ps_0Lep3Jet = (P::nLep==0)&&(P::nJet==3);

  // Flavour composition
  normSys("SysZblZbbRatio", 0.25, SysConfig{"Zbl"}.applyIn(P::nLep==0));
  normSys("SysZblZbbRatio", 0.28, SysConfig{"Zbl"}.applyIn(ps_2Lep2Jet));
  normSys("SysZblZbbRatio", 0.20, SysConfig{"Zbl"}.applyIn(ps_2Lep3Jet));
  //
  normSys("SysZccZbbRatio", 0.15, SysConfig{"Zcc"}.applyIn(P::nLep==0));
  normSys("SysZccZbbRatio", 0.16, SysConfig{"Zcc"}.applyIn(ps_2Lep2Jet));
  normSys("SysZccZbbRatio", 0.13, SysConfig{"Zcc"}.applyIn(ps_2Lep3Jet));
  //
  normSys("SysZbcZbbRatio", 0.40, SysConfig{"Zbc"}.applyIn(P::nLep==0));
  normSys("SysZbcZbbRatio", 0.40, SysConfig{"Zbc"}.applyIn(ps_2Lep2Jet));
  normSys("SysZbcZbbRatio", 0.30, SysConfig{"Zbc"}.applyIn(ps_2Lep3Jet));

  // Diboson
  // Overall norm
  normSys("SysWWNorm", 0.25, {"WW"});
  if ( doVHVZ ) {
    // do VH fit using floating VZ
    normFact("VZ", {{"WZ", "ZZ", "ggZZ"}});
  } else if ( !doDiboson ) {
    normSys("SysZZNorm", 0.20, {"ZZ"});
    normSys("SysWZNorm", 0.26, {"WZ"});
  }
  

  normSys("SysVZUEPSAcc", 0.056, SysConfig{{"ZZ", "ggZZ"}}.applyIn(P::nLep==0));
  normSys("SysVZUEPSAcc", 0.039, {"WZ"});
  normSys("SysVZUEPSAcc", 0.058, SysConfig{{"ZZ", "ggZZ"}}.applyIn(P::nLep==2));

  normSys("SysVZUEPS", 0.073, SysConfig{{"ZZ", "ggZZ"}}.applyIn(ps_0Lep3Jet).decorr(P::nJet));
  normSys("SysVZUEPS", 0.108, SysConfig{"WZ"}.applyIn(P::nJet==3).decorr(P::nJet));
  normSys("SysVZUEPS", 0.031, SysConfig{{"ZZ", "ggZZ"}}.applyIn(ps_2Lep3Jet).decorr(P::nJet));

  normSys("SysZZUEPSResid", 0.06, SysConfig{{"ZZ", "ggZZ"}}.applyIn(P::nLep==0).decorr(P::nLep));
  normSys("SysWZUEPSResid", 0.11, SysConfig{"WZ"}.applyIn(P::nLep==0).decorr(P::nLep));

  normSys("SysVZQCDscale_J2", 0.103, SysConfig{"ZZ"}.applyIn(ps_0Lep2Jet));
  normSys("SysVZQCDscale_J2", 0.127, SysConfig{"WZ"}.applyIn(P::nJet==2));
  normSys("SysVZQCDscale_J2", 0.119, SysConfig{"ZZ"}.applyIn(ps_2Lep2Jet));

  normSys("SysVZQCDscale_J3", -0.152, SysConfig{"ZZ"}.applyIn(ps_0Lep2Jet));
  normSys("SysVZQCDscale_J3", 0.174, SysConfig{"ZZ"}.applyIn(ps_0Lep3Jet));
  normSys("SysVZQCDscale_J3", -0.177, SysConfig{"WZ"}.applyIn(P::nJet==2));
  normSys("SysVZQCDscale_J3", 0.212, SysConfig{"WZ"}.applyIn(P::nJet==3));
  normSys("SysVZQCDscale_J3", -0.164, SysConfig{"ZZ"}.applyIn(ps_2Lep2Jet));
  normSys("SysVZQCDscale_J3", 0.101, SysConfig{"ZZ"}.applyIn(ps_2Lep3Jet));

  normSys("SysVZQCDscale_JVeto", 0.182, SysConfig{"ZZ"}.applyIn(ps_0Lep3Jet));
  normSys("SysVZQCDscale_JVeto", 0.190, SysConfig{"WZ"}.applyIn(P::nJet==3));

  normSys("SysggZZQCDscale", 0.59, {"ggZZ"}); // = 1.0 / 1.7 because k-factor is 1.7\pm1.0

  // extrapolation uncertainties

  // Higgs
  //add Higgs normalisation systematic of 50% when running diboson fit
  if(doDiboson) {
    normSys("SysHiggsNorm", 0.50, {"Higgs"});
  }

  // Branching ratio
  // numbers from Paolo, 2016/07/19 PF
  normSys("SysTheoryBRbb", 0.017, {"Higgs"});

  // NM 18-06-29 new uncertainties, in principle bug-free this time :)
  if(!doSTXS){ 
    ////// Non-STXS configuration for single mu's
    ////// UEPS acceptances still "buggy" i.e. extra large
    // QCD scales for total x-sec uncertainty
    normSys("SysTheoryQCDscale", 0.007, SysConfig{"qqVH"}.decorrTo({"qqVH", "ggZH"}));//PF
    normSys("SysTheoryQCDscale", 0.27, SysConfig{"ggZH"}.decorrTo({"qqVH", "ggZH"}));//PF
    // PDF and scales for total x-sec uncertainty
    normSys("SysTheoryPDF_qqVH", 0.019, {"WlvH"});//PF
    normSys("SysTheoryPDF_qqVH", 0.016, {"qqZH"});//PF
    normSys("SysTheoryPDF_ggZH", 0.05, {"ggZH"});//PF
    // Acceptances uncertainties from scale and PDF variations
    normSys("SysTheoryAcc_J2" , 0.069, SysConfig{"ZvvH"}.applyIn(P::nJet==2).decorrTo({"qqVH", "ggZH"}));//PF
    normSys("SysTheoryAcc_J2" , 0.088, SysConfig{"WlvH"}.applyIn(P::nJet==2).decorrTo({"qqVH", "ggZH"}));//PF
    normSys("SysTheoryAcc_J2" , 0.033, SysConfig{"ZllH"}.applyIn(P::nJet==2).decorrTo({"qqVH", "ggZH"}));//PF

    normSys("SysTheoryAcc_J3" , -0.07,  SysConfig{"ZvvH"}.applyIn(P::nJet==2).decorrTo({"qqVH", "ggZH"}));//PF
    normSys("SysTheoryAcc_J3" , 0.05,   SysConfig{"ZvvH"}.applyIn(P::nJet==3).decorrTo({"qqVH", "ggZH"}));//PF
    normSys("SysTheoryAcc_J3" , -0.086, SysConfig{"WlvH"}.applyIn(P::nJet==2).decorrTo({"qqVH", "ggZH"}));//PF
    normSys("SysTheoryAcc_J3" , 0.068,  SysConfig{"WlvH"}.applyIn(P::nJet==3).decorrTo({"qqVH", "ggZH"}));//PF
    normSys("SysTheoryAcc_J3" , -0.032, SysConfig{"ZllH"}.applyIn(P::nJet==2).decorrTo({"qqVH", "ggZH"}));//PF
    normSys("SysTheoryAcc_J3" , 0.039,  SysConfig{"ZllH"}.applyIn(P::nJet==3).decorrTo({"qqVH", "ggZH"}));//PF
      
    normSys("SysTheoryAcc_JVeto", -0.025, SysConfig{"ZvvH"}.applyIn(P::nJet==3).decorrTo({"qqVH", "ggZH"}));//PF
    normSys("SysTheoryAcc_JVeto", 0.038,  SysConfig{"WlvH"}.applyIn(P::nJet==3).decorrTo({"qqVH", "ggZH"}));//PF

    normSys("SysTheoryPDFAcc", 0.011, SysConfig{"ZvvH"}.decorrTo({"qqVH", "ggZH"}));//PF
    normSys("SysTheoryPDFAcc", 0.013, SysConfig{"WlvH"}.decorrTo({"qqVH", "ggZH"}));//PF
    normSys("SysTheoryPDFAcc", 0.005, SysConfig{"ZllH"}.decorrTo({"qqVH", "ggZH"}));//PF
    // Parton-shower for acceptances
    normSys("SysTheoryUEPSAcc", 0.041, {"ZvvH"});
    normSys("SysTheoryUEPSAcc", 0.062, {"WlvH"});
    normSys("SysTheoryUEPSAcc", 0.029, {"ZllH"});
    normSys("SysTheoryUEPSAcc", 0.022, SysConfig{"ZvvH"}.applyIn(P::nJet==3).decorr(P::nJet));
    normSys("SysTheoryUEPSAcc", 0.018, SysConfig{"WlvH"}.applyIn(P::nJet==3).decorr(P::nJet));
    normSys("SysTheoryUEPSAcc", 0.112, SysConfig{"ZllH"}.applyIn(P::nJet==3).decorr(P::nJet));
  }  else {
    ////// STXS based uncertainties - currently based on stage 1.1 (=1.2)
    // QCD scales for total x-sec uncertainty
    listAllUserSystematics_STXSQCD();
    // PDF and scales for total x-sec uncertainty
    listAllUserSystematics_STXSPDF();
    // Parton-shower for acceptances
    TString inputFile = "root://eosatlas.cern.ch:1094///eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/out_PS_muOnly_short_fullRunII_23012020.root";
    getPSSysFromFile(inputFile, "PSUE_H7");
    getPSSysFromFile(inputFile, "PSUE_AZNLO_Var1");
    getPSSysFromFile(inputFile, "PSUE_AZNLO_Var2");
    getPSSysFromFile(inputFile, "PSUE_AZNLO_MPI");
    getPSSysFromFile(inputFile, "PSUE_AZNLO_Ren");
  }

  // multijet ICHEP

  /* First set of MJ normalization uncertainty from Rel21
   * https://indico.cern.ch/event/734133/contributions/3027738/subcontributions/256594/attachments/1662871/2664757/GHH_1lepton_20180606.pdf
   *
   * CBA numbers also available now
   * https://espace.cern.ch/HbbSP/GHH/PublishingImages/Lists/Discussion%20Board2/Flat/CBA-MJ_Short.pdf
   */

  if (!doCutBase || doNewRegions){ 
  	// Normalisation uncertainty for full Run 2: https://indico.cern.ch/event/831956/contributions/3487614/attachments/1874704/3086463/MJ_1L_GHHResolved.pdf
    //HighpTV Region
  	normSys("SysMJNorm_El", 0.05, 3.03, SysConfig{"multijetEl"}.applyIn({(P::nJet==2)&&(P::binMin==150), (P::nJet==2)&&(P::binMin==250)} ).decorr(P::nJet));
    normSys("SysMJNorm_El", 0.05, 2.00, SysConfig{"multijetEl"}.applyIn({(P::nJet==3)&&(P::binMin==150), (P::nJet==3)&&(P::binMin==250)} ).decorr(P::nJet));
    normSys("SysMJNorm_Mu", 0.68, 1.47, SysConfig{"multijetMu"}.applyIn({(P::nJet==2)&&(P::binMin==150), (P::nJet==2)&&(P::binMin==250)} ).decorr(P::nJet)); 
    normSys("SysMJNorm_Mu", 0.05, 2.00, SysConfig{"multijetMu"}.applyIn({(P::nJet==3)&&(P::binMin==150), (P::nJet==3)&&(P::binMin==250)} ).decorr(P::nJet)); 
    //MediumpTV Region
  	normSys("SysMJNorm_El", 0.76, 1.08, SysConfig{"multijetEl"}.applyIn((P::nJet==2)&&(P::binMin==75)).decorr({P::nJet, P::binMin}));
  	normSys("SysMJNorm_El", 0.62, 1.15, SysConfig{"multijetEl"}.applyIn((P::nJet==3)&&(P::binMin==75)).decorr({P::nJet, P::binMin}));
    normSys("SysMJNorm_Mu", 0.80, 1.84, SysConfig{"multijetMu"}.applyIn((P::nJet==2)&&(P::binMin==75)).decorr({P::nJet, P::binMin}));
  	normSys("SysMJNorm_Mu", 0.83, 1.12, SysConfig{"multijetMu"}.applyIn((P::nJet==3)&&(P::binMin==75)).decorr({P::nJet, P::binMin}));


  	//Below are the results from ICHEP
    //HighpTV Region
    // normSys("SysMJNorm_El", 0.0, 2.05, SysConfig{"multijetEl"}.applyIn((P::nJet==2)&&(P::binMin==150)).decorr({P::nJet, P::binMin}));
    // normSys("SysMJNorm_El", 0.2, 1.90, SysConfig{"multijetEl"}.applyIn((P::nJet==3)&&(P::binMin==150)).decorr({P::nJet, P::binMin}));
    // normSys("SysMJNorm_Mu", 0.0, 1.60, SysConfig{"multijetMu"}.applyIn((P::nJet==2)&&(P::binMin==150)).decorr({P::nJet, P::binMin}));
    // normSys("SysMJNorm_Mu", 0.0, 2.40, SysConfig{"multijetMu"}.applyIn((P::nJet==3)&&(P::binMin==150)).decorr({P::nJet, P::binMin}));
    // //MediumpTV Region
    // normSys("SysMJNorm_El", 0.88, 1.22, SysConfig{"multijetEl"}.applyIn((P::nJet==2)&&(P::binMin==75)).decorr({P::nJet, P::binMin}));
    // normSys("SysMJNorm_El", 0.80, 1.40, SysConfig{"multijetEl"}.applyIn((P::nJet==3)&&(P::binMin==75)).decorr({P::nJet, P::binMin}));
    // normSys("SysMJNorm_Mu", 0.53, 1.50, SysConfig{"multijetMu"}.applyIn((P::nJet==2)&&(P::binMin==75)).decorr({P::nJet, P::binMin}));
    // normSys("SysMJNorm_Mu", 0.54, 1.12, SysConfig{"multijetMu"}.applyIn((P::nJet==3)&&(P::binMin==75)).decorr({P::nJet, P::binMin})); 	
  }else{
    //HighpTV Region
    normSys("SysMJNorm_El", 0.0, 2.00, SysConfig{"multijetEl"}.applyIn({(P::nJet==2)&&(P::binMin==150), (P::nJet==2)&&(P::binMin==200)} ).decorr(P::nJet));
    normSys("SysMJNorm_El", 0.0, 2.00, SysConfig{"multijetEl"}.applyIn({(P::nJet==3)&&(P::binMin==150), (P::nJet==3)&&(P::binMin==200)} ).decorr(P::nJet));
    normSys("SysMJNorm_Mu", 0.0, 2.60, SysConfig{"multijetMu"}.applyIn({(P::nJet==2)&&(P::binMin==150), (P::nJet==2)&&(P::binMin==200)} ).decorr(P::nJet));
    normSys("SysMJNorm_Mu", 0.0, 2.00, SysConfig{"multijetMu"}.applyIn({(P::nJet==3)&&(P::binMin==150), (P::nJet==3)&&(P::binMin==200)} ).decorr(P::nJet));
    //MediumpTV Region
    normSys("SysMJNorm_El", 0.85, 1.20, SysConfig{"multijetEl"}.applyIn((P::nJet==2)&&(P::binMin==75)).decorr({P::nJet, P::binMin}));
    normSys("SysMJNorm_El", 0.0, 2.00, SysConfig{"multijetEl"}.applyIn((P::nJet==3)&&(P::binMin==75)).decorr({P::nJet, P::binMin}));
    normSys("SysMJNorm_Mu", 0.80, 1.55, SysConfig{"multijetMu"}.applyIn((P::nJet==2)&&(P::binMin==75)).decorr({P::nJet, P::binMin}));
    normSys("SysMJNorm_Mu", 0.95, 1.65, SysConfig{"multijetMu"}.applyIn((P::nJet==3)&&(P::binMin==75)).decorr({P::nJet, P::binMin})); 
  }


  // Note: lumi removed from floating bkg in engine.cpp
  // can re-test switch on with LumiForAll option
  normSys("ATLAS_LUMI_2015_2018", 0.017, SysConfig{"MC"}); //Update lumi uncertainty for 2015+2016+2017+2018 GRLs

  MCStatSys().applyTo("emuCRData");

}


void SystematicListsBuilder_GHHRun2::listAllHistoSystematics() {

  bool doICHEPStyle = m_config.getValue("ICHEPStyle", false);
  bool doNewRegions = m_config.getValue("NewRegions", false); 
  bool PCBTInputs = m_config.getValue("PCBTInputs", false); 
  using T   = SysConfig::Treat;
  using S   = SysConfig::Smooth;
  using Sym = SysConfig::Symmetrise;
  using P   = Property;
  bool doCutBase = m_config.getValue("CutBase", false);
  bool doSTXS = m_config.getValue("DoSTXS", false);
  //bool doSTXSShapeOnly = m_config.getValue("DoSTXSShapeOnly", false);
  bool defltFitConfig = m_config.getValue("DefaultFitConfig", false);
  bool hasOneLep = defltFitConfig ? true : CategoryIndex::hasMatching(P::nLep==1);
  SysConfig::SmoothingFunction GHHSmoothing = [](const PropertiesSet& pset, const Sample&) {
    TString dist = pset.getStringProp(Property::dist);
    if(dist == "mjj" || dist == "mBB" || dist == "mBBMVA") {
      return S::smoothRebinParabolic;
    }
    return S::smoothRebinMonotonic;
  };

  SysConfig noSmoothConfig          { T::shape, S::noSmooth, Sym::noSym };
  SysConfig noSmoothShapeOnlyConfig { T::shapeonly, S::noSmooth, Sym::noSym };
  SysConfig smoothConfig            { T::shape, GHHSmoothing, Sym::noSym };
  SysConfig smoothAndSymConfig      { T::shape, GHHSmoothing, Sym::symmetriseOneSided }; 


  // AT THE MOMENT ONLY RECO SYSTEMATIC CONSIDERED
  m_histoSysts.insert({ "SysEG_RESOLUTION_ALL" , smoothConfig});
  m_histoSysts.insert({ "SysEG_SCALE_ALL" , smoothConfig});

  m_histoSysts.insert({ "SysEL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR" , noSmoothConfig});
  m_histoSysts.insert({ "SysEL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR" , noSmoothConfig});
  m_histoSysts.insert({ "SysEL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR" , noSmoothConfig});
  m_histoSysts.insert({ "SysEL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR" , noSmoothConfig});

  //m_histoSysts.insert({ "SysEL_EFF_ID_TotalCorrUncertainty" , noSmoothConfig});
  //m_histoSysts.insert({ "SysEL_EFF_Iso_TotalCorrUncertainty" , noSmoothConfig});
  //m_histoSysts.insert({ "SysEL_EFF_Reco_TotalCorrUncertainty" , noSmoothConfig});

  m_histoSysts.insert({ "SysMETTrigStat" , smoothConfig});
  m_histoSysts.insert({ "SysMETTrigTop" , smoothConfig});
  m_histoSysts.insert({ "SysMETTrigZ" , smoothConfig});
  m_histoSysts.insert({ "SysMETTrigSumpt" , smoothConfig});
  m_histoSysts.insert({ "SysMET_JetTrk_Scale" , smoothConfig});
  m_histoSysts.insert({ "SysMET_SoftTrk_ResoPerp" , smoothAndSymConfig});
  m_histoSysts.insert({ "SysMET_SoftTrk_ResoPara" , smoothAndSymConfig});
  m_histoSysts.insert({ "SysMET_SoftTrk_Scale" , smoothConfig});

  // special check on the ResoPerp
  /*
  SysConfig MET_SoftTrk_ResoPerp_Config = smoothAndSymConfig;
  MET_SoftTrk_ResoPerp_Config.decorrFun([](const PropertiesSet& pset, const Sample& s) {
                                 if( s.name() == "ttbar" && pset[P::nLep] == 2 ) return "_ttbar_L2";
                                 else if( s.hasKW("Zjets") && pset[P::nLep] == 2 ) return "_Zhf_L2";
                                 else if( s.hasKW("Diboson") && pset[P::nLep] == 2 ) return "_diboson_L2";
                                 else if( s.hasKW("Higgs") && pset[P::nLep] == 2 ) return "_Sig_L2";
                                 else if( s.hasKW("Higgs") && pset[P::nLep] == 1 ) return "_Sig_L1";
                                 else if( s.hasKW("Higgs") && pset[P::nLep] == 0 ) return "_Sig_L0";
                                 else if( pset[P::nLep] == 0 ) return "_Bkg_L0";
                                 else if( pset[P::nLep] == 1 ) return "_Bkg_L1";
                                 else if( pset[P::nLep] == 2 ) return "_Bkg_L2";
                                 else return "";
                                 });
  m_histoSysts.insert( { "SysMET_SoftTrk_ResoPerp", MET_SoftTrk_ResoPerp_Config} );
  SysConfig MET_SoftTrk_ResoPerp_Config = smoothAndSymConfig;
  MET_SoftTrk_ResoPerp_Config.decorrFun([](const PropertiesSet& pset, const Sample& s) {
                                 if( (pset[P::nJet]==3) && (pset[P::binMin]==75) && s.hasKW("Zjets") ) return "_Zhf_3J_lowptv";
                                 else if( (pset[P::nJet]==2) && (pset[P::binMin]==75) && s.hasKW("Zjets") ) return "_Zhf_2J_lowptv";
                                 //if( (pset[P::nLep] == 2) && (pset[P::nJet]==3) && (pset[P::binMin]==75) ) return "_2L_3J_lowptv";
                                 else return "";
                                 });
  m_histoSysts.insert({ "SysMET_SoftTrk_ResoPerp", MET_SoftTrk_ResoPerp_Config});
  */

  m_histoSysts.insert({ "SysMUON_EFF_RECO_SYS" , noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_RECO_STAT" , noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_ISO_SYS" , noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_ISO_STAT" , noSmoothConfig});

  // FIXME NM 17-05-24: bugged syst for events with pT<10GeV. Leave out for now
  m_histoSysts.insert({ "SysMUON_EFF_TTVA_SYS" , noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_TTVA_STAT" , noSmoothConfig});

  m_histoSysts.insert({ "SysMUON_SCALE" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_MS" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_ID" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_SAGITTA_RHO", smoothConfig});
  m_histoSysts.insert({ "SysMUON_SAGITTA_RESBIAS" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_RECO_STAT_LOWPT" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_RECO_SYST_LOWPT" , smoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_RECO_SYS_LOWPT" , smoothConfig}); // sys_V28
  m_histoSysts.insert({ "SysMUON_EFF_TrigSystUncertainty" , smoothConfig}); // sys_V28
  m_histoSysts.insert({ "SysMUON_EFF_TrigStatUncertainty" , smoothConfig}); // sys_V28


  // new v28, tau systematic uncertainty in 0 lepton
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_DETECTOR" , smoothConfig}); // sys_V28
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_INSITU" , smoothConfig}); // sys_V28
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_MODEL" , smoothConfig}); // sys_V28

  m_histoSysts.insert({ "SysPRW_DATASF" , smoothConfig});

  //--------------------------------------------
  // // JETs
  m_histoSysts.insert({ "SysJET_JvtEfficiency" , smoothConfig});
  // JES: category reduction scheme
  std::vector<std::string> jetJESsysts {"BJES_Response", "Flavor_Response", //"Flavor_Composition",
      "Pileup_OffsetNPV", "Pileup_OffsetMu", "Pileup_PtTerm", "Pileup_RhoTopology", "SingleParticle_HighPt", "PunchThrough_MC16",
      "EffectiveNP_Detector1", "EffectiveNP_Detector2",
      "EffectiveNP_Modelling1", "EffectiveNP_Modelling2", "EffectiveNP_Modelling3", "EffectiveNP_Modelling4",
      "EffectiveNP_Statistical1", "EffectiveNP_Statistical2", "EffectiveNP_Statistical3", "EffectiveNP_Statistical4", "EffectiveNP_Statistical5", "EffectiveNP_Statistical6",
      "EffectiveNP_Mixed1", "EffectiveNP_Mixed2", "EffectiveNP_Mixed3",
      "EtaIntercalibration_Modelling", "EtaIntercalibration_NonClosure_highE", "EtaIntercalibration_NonClosure_posEta", "EtaIntercalibration_NonClosure_negEta", "EtaIntercalibration_TotalStat"
      };
  for(auto sysname: jetJESsysts) {
    m_histoSysts.insert({ "SysJET_CR_JET_"+sysname , smoothConfig});
  }
  // JER: 8 NP scheme ?
  SysConfig JER_Config = { T::shape, S::smoothRebinParabolic, Sym::symmetriseOneSided };
  std::vector<std::string> jetJERsysts {"JER_DataVsMC",
      "JER_EffectiveNP_1", "JER_EffectiveNP_2", "JER_EffectiveNP_3", "JER_EffectiveNP_4", "JER_EffectiveNP_5", "JER_EffectiveNP_6",
      "JER_EffectiveNP_7restTerm"
      };
  for(auto sysname: jetJERsysts) {
    m_histoSysts.insert({ "SysJET_CR_JET_"+sysname , JER_Config});
  }


  
  // Decorrelated:
  // - Correlate W+jets and Z+jets in all channels (call it
  // Flavor_Composition_Vjets)
  // - Correlate top in the 0- and 1-lepton channels (call it
  // Flavor_Composition_top)
  // - Have an uncorrelated uncertainty for top in the 2-lepton channel (call
  // it Flavor_Composition_top_L2)
  // - Single-top (all channels) go with ttbar in 0/1lep
  // - Higgs and Diboson stay together on their own.
  SysConfig JET_Flav_Comp_Config = smoothConfig;
  JET_Flav_Comp_Config.decorrFun([](const PropertiesSet& pset, const Sample& s) {
                                 if( s.name() == "ttbar" && pset[P::nLep] == 2) return "_ttbar_L2";
                                 else if( s.hasKW("Top") ) return "_Top";
                                 else if( s.hasKW("Higgs") || s.hasKW("Diboson") ) return "_VV";
                                 else if( s.hasKW("Wjets") || s.hasKW("Zjets")) return "_Vjets";
                                 else return "";
                                 });
  m_histoSysts.insert( { "SysJET_CR_JET_Flavor_Composition", JET_Flav_Comp_Config} );

  // B-tag
  std::vector<std::string> btagSysts { "extrapolation_from_charm", "extrapolation",
      //L
      "Eigen_Light_0", "Eigen_Light_1", "Eigen_Light_2", "Eigen_Light_3", "Eigen_Light_4", "Eigen_Light_4",
      "Eigen_Light_5", "Eigen_Light_6", "Eigen_Light_7", "Eigen_Light_8", "Eigen_Light_9", "Eigen_Light_10",
      "Eigen_Light_11", "Eigen_Light_12", "Eigen_Light_13", "Eigen_Light_14", "Eigen_Light_15", "Eigen_Light_16",
      "Eigen_Light_17", "Eigen_Light_18", "Eigen_Light_19",
      //C
      "Eigen_C_0", "Eigen_C_1", "Eigen_C_2", "Eigen_C_3", "Eigen_C_4", "Eigen_C_5", "Eigen_C_6", "Eigen_C_7",
      "Eigen_C_8", "Eigen_C_9", "Eigen_C_10", "Eigen_C_11", "Eigen_C_12", "Eigen_C_13", "Eigen_C_14", "Eigen_C_15",
      "Eigen_C_16", "Eigen_C_17", "Eigen_C_18", "Eigen_C_19",
      //B
      "Eigen_B_0", "Eigen_B_1", "Eigen_B_2", "Eigen_B_3", "Eigen_B_4","Eigen_B_5", "Eigen_B_6", "Eigen_B_7",
      "Eigen_B_8", "Eigen_B_9", "Eigen_B_10", "Eigen_B_11", "Eigen_B_12", "Eigen_B_13", "Eigen_B_14", "Eigen_B_15",
      "Eigen_B_16", "Eigen_B_17", "Eigen_B_18", "Eigen_B_19", "Eigen_B_20", "Eigen_B_21", "Eigen_B_22", "Eigen_B_23",
      "Eigen_B_24", "Eigen_B_25", "Eigen_B_26", "Eigen_B_27", "Eigen_B_28", "Eigen_B_29", "Eigen_B_30", "Eigen_B_31",
      "Eigen_B_32", "Eigen_B_33", "Eigen_B_34", "Eigen_B_35", "Eigen_B_36", "Eigen_B_37", "Eigen_B_38", "Eigen_B_39",
      "Eigen_B_40", "Eigen_B_41", "Eigen_B_42", "Eigen_B_43", "Eigen_B_44"
  };

  for(auto sysname: btagSysts) {
    if(!PCBTInputs) m_histoSysts.insert({ "SysFT_EFF_"+sysname , noSmoothConfig});
    else m_histoSysts.insert({ "SysFT_EFF_"+sysname , SysConfig{T::shape, S::noSmooth, Sym::symmetriseOneSided}});
  }

  // Background modelling
  
  if (doICHEPStyle) m_histoSysts.insert( {"SysTTbarPTV", SysConfig{T::shape, S::noSmooth, Sym::noSym}.applyTo("ttbar").decorrIn({ (P::nLep==2),(P::nLep==1)&&(P::binMin==75)}) });
  else m_histoSysts.insert( {"SysTTbarPtV_BDTr", SysConfig{T::shape, S::noSmooth, Sym::noSym}.applyTo("ttbar").decorr({ (P::nJet) }).decorrIn({ (P::nLep==2),(P::nLep==1)&&(P::binMin==75)}) });

  if (doICHEPStyle) {
    if (doNewRegions) m_histoSysts.insert( {"SysTTbarMBB", SysConfig{T::shape, S::noSmooth, Sym::noSym}.applyTo("ttbar").decorrIn({ (P::nLep==2),(P::nLep==1)&&(P::binMin==75)}) }); //ttbar mBB is shape
    else m_histoSysts.insert( {"SysTTbarMBB", SysConfig{T::shapeonly, S::noSmooth, Sym::noSym}.applyTo("ttbar").decorrIn({ (P::nLep==2),(P::nLep==1)&&(P::binMin==75)}) });
  }else{
    m_histoSysts.insert({ "SysBDTr_ttbar_PS", SysConfig{T::shape, S::noSmooth, Sym::symmetriseOneSided}.applyTo("ttbar").decorrIn({ (P::nLep==2),(P::nLep==0),(P::nLep==1)&&(P::binMin==75),(P::nLep==1)&&(P::binMin==150)&&(P::nJet==3)}) });
    m_histoSysts.insert({ "SysBDTr_ttbar_ME", SysConfig{T::shape, S::noSmooth, Sym::symmetriseOneSided}.applyTo("ttbar").decorr({ (P::nJet) }).decorrIn({ (P::nLep==2),(P::nLep==1)&&(P::binMin==75)}) });

    // new ttbar bc systematics
      // PS and ME correlated everywhere
      m_histoSysts.insert({ "SysTTbarbcMEACC", SysConfig{T::shape, S::noSmooth, Sym::symmetriseOneSided}.applyTo("ttbar")});
      m_histoSysts.insert({ "SysTTbarOthMEACC", SysConfig{T::shape, S::noSmooth, Sym::symmetriseOneSided}.applyTo("ttbar")});
      m_histoSysts.insert({ "SysTTbarbcPSACC", SysConfig{T::shape, S::noSmooth, Sym::symmetriseOneSided}.applyTo("ttbar")});
      m_histoSysts.insert({ "SysTTbarOthPSACC", SysConfig{T::shape, S::noSmooth, Sym::symmetriseOneSided}.applyTo("ttbar")});

  }
  
		// 2D
  //m_histoSysts.insert( {"SysTTbarPTVMBB", SysConfig{T::shape, S::noSmooth, Sym::noSym, "ttbar", {}, {}, {{P::nLep, 2}} }});
  //m_histoSysts.insert( {"SysTTbarPTVMBB", SysConfig{T::shape, S::noSmooth, Sym::noSym, "ttbar", {}, {}, {{P::nLep, 2}} }});//sys_V28, 1lep WhfCR, cross checked


  //===== If run on ICHEP inputs, SysWPtV corresponds to the single shape derived in 2j and applied to both 2j and 3j regions =====// ->correlated
  //===== If run on the new version of the inputs produced by CorrsAndSysts, SysWPtV shape corresponds to the one derived and applied to 2j(3j) region  =====// ->correlated
  //m_histoSysts.insert({ "SysWPtV" , SysConfig{T::shape, S::noSmooth, Sym::noSym}.applyTo("Wjets") }); 
  
  //===== If run on the new version of the inputs produced by CorrsAndSysts, SysWPtV shape corresponds to the one derived and applied to 2j(3j) region ->uncorrelated

  if (doICHEPStyle) {
    if(hasOneLep) {
      m_histoSysts.insert({ "SysWPtV" , SysConfig{T::shape, S::noSmooth, Sym::noSym}.applyTo("Wjets").decorr(P::nJet).decorrIn({(P::nLep==1)&&(P::binMin==75)}) });
    } else {
      m_histoSysts.insert({ "SysWPtV" , SysConfig{T::shape, S::noSmooth, Sym::noSym}.applyTo("Wjets") });
    }
  } else {
    if(hasOneLep) {
      m_histoSysts.insert({ "SysWPtV_BDTr" , SysConfig{T::shape, S::noSmooth, Sym::noSym}.applyTo("Wjets").decorr(P::nJet).decorrIn({(P::nLep==1)&&(P::binMin==75)}) });
    } else {
      m_histoSysts.insert({ "SysWPtV_BDTr" , SysConfig{T::shape, S::noSmooth, Sym::noSym}.applyTo("Wjets").decorr(P::nJet) });
    }
  }


  if (doICHEPStyle) {
    if (doNewRegions) m_histoSysts.insert({ "SysWMbb" , SysConfig{ T::shape, S::noSmooth, Sym::noSym }.applyTo("Wjets").decorrIn({ (P::nLep==1)&&(P::binMin==75) })});
    else m_histoSysts.insert({ "SysWMbb" , noSmoothShapeOnlyConfig});

  } else {
    m_histoSysts.insert({ "SysBDTr_W_SHtoMG5" , SysConfig{T::shapeonly, S::noSmooth, Sym::symmetriseOneSided}.applyTo("Wjets").decorrIn({ (P::nLep==1)&&(P::binMin==75) }) });
  }


  m_histoSysts.insert({ "SysZPtV" , SysConfig{ T::shape, S::noSmooth, Sym::noSym }.decorrIn({ (P::nLep==2)&&(P::binMin==75) }) });
  if (doNewRegions) m_histoSysts.insert({ "SysZMbb" , SysConfig{ T::shapeonly, S::noSmooth, Sym::noSym }.decorrIn({ (P::nLep==2)&&(P::binMin==75) }) });     
  else m_histoSysts.insert({ "SysZMbb" , SysConfig{ T::shapeonly, S::noSmooth, Sym::noSym }.decorrIn({ (P::nLep==2)&&(P::binMin==75) }) });


  //
  // 2D
  //m_histoSysts.insert( {"SysWPtVMbb",  noSmoothConfig});
  //m_histoSysts.insert( {"SysZPtVMbb",  noSmoothConfig});

  //m_histoSysts.insert({ "SysVVJetScalePtST1" , noSmoothConfig});
  m_histoSysts.insert({ "SysVVMbbME" , noSmoothConfig});
  m_histoSysts.insert({ "SysVVPTVME" , noSmoothConfig});
  m_histoSysts.insert({ "SysVVPTVPSUE" , noSmoothConfig}); //sys_V28
  m_histoSysts.insert({ "SysVVMbbPSUE" , noSmoothConfig}); //sys_V28

// PTV shape (QCD/PDF/UEPS) from HistoSysts
  if(doSTXS){
    m_histoSysts.insert({ "SysVHQCDscalePTV" , noSmoothShapeOnlyConfig});
    m_histoSysts.insert({ "SysVHQCDscalePTV_ggZH" , noSmoothShapeOnlyConfig});
  }else{
    m_histoSysts.insert({ "SysVHQCDscalePTV" , noSmoothConfig});
    m_histoSysts.insert({ "SysVHQCDscalePTV_ggZH" , noSmoothConfig});
  }

  if(doSTXS){
    m_histoSysts.insert({ "SysVHPDFPTV" , noSmoothShapeOnlyConfig});
    m_histoSysts.insert({ "SysVHPDFPTV_ggZH" , noSmoothShapeOnlyConfig});
    m_histoSysts.insert({ "SysVHUEPSPTV" , noSmoothShapeOnlyConfig});
  }else{
    m_histoSysts.insert({ "SysVHPDFPTV" , noSmoothConfig});
    m_histoSysts.insert({ "SysVHPDFPTV_ggZH" , noSmoothConfig});
    m_histoSysts.insert({ "SysVHUEPSPTV" , noSmoothConfig});
  }

// Mbb (QCD/PDF/UEPS) always shape only to avoid double counting of Norm. effect with scale pt
  m_histoSysts.insert({ "SysVHQCDscaleMbb" , noSmoothConfig});           
  m_histoSysts.insert({ "SysVHQCDscaleMbb_ggZH" , noSmoothConfig});
  m_histoSysts.insert({ "SysVHUEPSMbb" , noSmoothConfig});

// NLOEWK systematics
  m_histoSysts.insert({ "SysVHNLOEWK" , noSmoothConfig});
  m_histoSysts.insert({ "SysStoptPTV" , SysConfig{T::shape, S::noSmooth, Sym::noSym}.decorrIn({ (P::nLep==1)&&(P::binMin==75) }) }); 
  m_histoSysts.insert({ "SysStoptMBB" , SysConfig{T::shape, S::noSmooth, Sym::noSym}.decorrIn({ (P::nLep==1)&&(P::binMin==75) }) }); 
  m_histoSysts.insert({ "SysStopWtPTV" , SysConfig{T::shape, S::noSmooth, Sym::noSym}.decorrIn({ (P::nLep==1)&&(P::binMin==75) }) }); 
  m_histoSysts.insert({ "SysStopWtMBB" , SysConfig{T::shape, S::noSmooth, Sym::noSym}.decorrIn({ (P::nLep==1)&&(P::binMin==75) }) });

  // m_histoSysts.insert({ "SysStopWtMTOP" , noSmoothShapeOnlyConfig});

  // m_histoSysts.insert({ "SysStopWtbbACC" , noSmoothConfig});
  // m_histoSysts.insert({ "SysStopWtothACC" , noSmoothConfig});
  m_histoSysts.insert({ "SysStopWtbbACC" , SysConfig{T::shape, S::noSmooth, Sym::noSym}.applyTo("stopWt").decorrIn({ (P::nLep==1)&&(P::binMin==75) }) });
  m_histoSysts.insert({ "SysStopWtothACC", SysConfig{T::shape, S::noSmooth, Sym::noSym}.applyTo("stopWt").decorrIn({ (P::nLep==1)&&(P::binMin==75) }) });

  m_histoSysts.insert({ "SysMJ_El_METstr" , smoothAndSymConfig});//PF
  m_histoSysts.insert({ "SysMJ_Mu_METstr" , smoothAndSymConfig});//PF
  m_histoSysts.insert({ "SysMJ_El_EWK"    , smoothAndSymConfig});
  //m_histoSysts.insert({ "SysMJ_El_EWK" , smoothAndSymConfig});
  m_histoSysts.insert({ "SysMJ_El_flavor" , smoothAndSymConfig});//PF

  // multijet
  // normalization systematic uncertainty added separately above, here shapeonly
  // CBA SysMJReduced from parametrized syst, so use noSmooth, the others use GHHSmoothing
  SysConfig MJConfig1 { T::shapeonly,  S::smoothRebinParabolic, Sym::symmetriseOneSided };
  SysConfig MJConfig2 { T::shapeonly, S::noSmooth, Sym::symmetriseOneSided };
  SysConfig MJConfig3 { T::shape,  S::smoothRebinParabolic, Sym::symmetriseOneSided };
  MJConfig1.applyTo({"multijetEl","multijetMu"}).decorrTo({"multijetEl", "multijetMu"}).decorrIn({ (P::nJet==2),(P::nJet==3),(P::binMin==75)});
  MJConfig2.applyTo({"multijetEl","multijetMu"}).decorrTo({"multijetEl", "multijetMu"}).decorrIn({ (P::nJet==2),(P::nJet==3),(P::binMin==75)});
  MJConfig3.applyTo({"multijetEl","multijetMu"}).decorrTo({"multijetEl", "multijetMu"}).decorrIn({ (P::nJet==2),(P::nJet==3),(P::binMin==75)});
  m_histoSysts.insert({"SysMJTrigger", MJConfig1 }); 
  if (doCutBase && !doNewRegions) m_histoSysts.insert({"SysMJReduced", MJConfig2 });
  else m_histoSysts.insert({"SysMJReduced", MJConfig3 });
  m_histoSysts.insert({"SysMJ2Tag", MJConfig1 }); 
  m_histoSysts.insert({"SysMJSFsCR", MJConfig1 });

  /////////////////////////////////////////////////////////////////////////////////////////
  //
  //                          Tweaks, if needed
  //
  /////////////////////////////////////////////////////////////////////////////////////////

  // later can add some switches, e.g looping through m_histoSysts and
  // putting to T::skip all the JES NP

}

/////////////////////////////////////////////////////////////////////////////////////////
//
//      The following are STXS related function, they will be only called
//      when DoSTXS == True
//
/////////////////////////////////////////////////////////////////////////////////////////

void SystematicListsBuilder_GHHRun2::addPOI_STXS() {
  // This function will be only called when doSTXS == True
  std::vector<TString> sig_decorr = m_config.getStringList("DecorrPOI");
  int  FitSTXSScheme = m_config.getValue("FitSTXSScheme", 1);
  bool doXSWS = m_config.getValue("DoXSWS", false);
  if(doXSWS && FitSTXSScheme < 3){
    std::cerr << "Sorry the FitSTXSScheme " << FitSTXSScheme << " is not supported for doXSWS == true." << std::endl;
    throw;
  }
  switch(FitSTXSScheme){
    case 1:
      // merging all STXS signals into an inclusive signal, should be same as GHH
      if(sig_decorr.size() != 0) {
	  std::vector<Property> decorrelations;
	  for(auto& d : sig_decorr) {
	      decorrelations.push_back(Properties::props_from_names.at(d));
	  }
	  addPOI("SigXsecOverSM", {"Sig", {}, std::move(decorrelations)}, 1, -40, 40);
      } else {
	  addPOI("SigXsecOverSM", {"Sig"}, 1, -40, 40);
      }
      break;
    case 2:
      // now it equals 1, because we split POI assignment and systematics assignment
      // We keep it because we don't want to mess up the following POI definition
      addPOI("SigXsecOverSMWH", {"WH"}, 1, -40, 40);
      addPOI("SigXsecOverSMZH", {"ZH"}, 1, -40, 40);
      break;
    case 3:
      {
        std::vector<TString> STXS_signals = {
          "ZHx75x150PTV", "ZHx150x250PTV", "ZHxGT250PTV", "WHx150x250PTV", "WHxGT250PTV",
        };
        for (const auto& sig : STXS_signals){
          addPOI("SigXsecOverSM"+sig,   {sig.Data()}, 1, -40, 40);
        }
        // normSys("SysWHx0x150PTVNorm", 0.50, {"WHx0x150PTV"});
      }
      break;
    case 4:
      {
        std::vector<TString> STXS_signals = {
          "GGZHx0x150PTV", "GGZHx150x250PTV", "GGZHxGT250PTV", 
          "QQZHx0x150PTV", "QQZHx150x250PTV", "QQZHxGT250PTV", 
          "WHx0x150PTV",   "WHx150x250PTV",   "WHxGT250PTV",
        };
        for (const auto& sig : STXS_signals){
          addPOI("SigXsecOverSM"+sig,   {sig.Data()}, 1, -40, 40);
        }
        // also keep this NP, to make the following ws edit more easier
        // normSys("SysWHx0x150PTVNorm", 0.50, {"WHx0x150PTV"});
      }
      break;
    case 5: // auxiliary fit
      {
        std::vector<TString> STXS_signals = {
          "ZHx75x150PTV", "ZHxGT150PTV", "WHxGT150PTV",
        };
        for (const auto& sig : STXS_signals){
          addPOI("SigXsecOverSM"+sig,   {sig.Data()}, 1, -40, 40);
        }
        // normSys("SysWHx0x150PTVNorm", 0.50, {"WHx0x150PTV"});
      }
      break;
    case 6: //Same for case 3, for EFT
      {
        std::vector<TString> STXS_signals = {
        "GGZHx0x75PTV", "GGZHx75x150PTV", "GGZHx150x250PTV", "GGZHxGT250PTV",
        "QQZHx0x75PTV", "QQZHx75x150PTV", "QQZHx150x250PTV", "QQZHxGT250PTV",
                        "WHx0x150PTV",    "WHx150x250PTV",   "WHxGT250PTV",
        };
        for (const auto& sig : STXS_signals){
          addPOI("SigXsecOverSM"+sig,   {sig.Data()}, 1, -40, 40);
        }
      }
      break;
    case 7: //For 3POI EFT (with QQ and GG split)
      {
        std::vector<TString> STXS_signals = {
          "GGZHx75x150PTV", "GGZHxGT150PTV", "QQZHx75x150PTV", "QQZHxGT150PTV", "WHxGT150PTV",
        };
        for (const auto& sig : STXS_signals){
          addPOI("SigXsecOverSM"+sig,   {sig.Data()}, 1, -40, 40);
        }
      }
      break;
    case 8: //For WH+ZH Vpt fit
      {
        std::vector<TString> STXS_signals = {
          "VHx75x150","VHx150x250", "VHxGT250"
        };
        for (const auto& sig : STXS_signals){
          addPOI("SigXsecOverSM"+sig,   {sig.Data()}, 1, -40, 40);
        }
      }
      break;
  case 9: // 6 POI STXS fit
      std::vector<TString> STXS_signals = {
          "ZHx75x150PTV", "ZHx150x250PTV", "ZHxGT250PTV", "WHx75x150PTV", "WHx150x250PTV", "WHxGT250PTV",
      };
      for (const auto& sig : STXS_signals){
          addPOI("SigXsecOverSM"+sig,   {sig.Data()}, 1, -40, 40);
      }      
      break;
  }
}


void SystematicListsBuilder_GHHRun2::listAllUserSystematics_STXSQCD() {
  // This function will be only called when (doSTXS)
  /////////////////////////////
  // 
  // QCD scale variations
  //
  /////////////////////////////

  bool doXSWS = m_config.getValue("DoXSWS", false);

  // stage 1 ++ sys
  // QQVH, delta 1 and delta2

  normSys("SysTheoryDelta1_qqVH",-0.03,{"QQVHxPTVx0x75x0J"});
  normSys("SysTheoryDelta1_qqVH",0.068,{"QQVHxPTVx0x75x1J"});
  normSys("SysTheoryDelta1_qqVH",0.068,{"QQVHxPTVx0x75xGE2J"});
  normSys("SysTheoryDelta1_qqVH",-0.036,{"QQVHxPTVx75x150x0J"});
  normSys("SysTheoryDelta1_qqVH",0.06,{"QQVHxPTVx75x150x1J"});
  normSys("SysTheoryDelta1_qqVH",0.06,{"QQVHxPTVx75x150xGE2J"});
  normSys("SysTheoryDelta1_qqVH",-0.041,{"QQVHxPTVx150x250x0J"});
  normSys("SysTheoryDelta1_qqVH",0.051,{"QQVHxPTVx150x250x1J"});
  normSys("SysTheoryDelta1_qqVH",0.051,{"QQVHxPTVx150x250xGE2J"});
  normSys("SysTheoryDelta1_qqVH",-0.054,{"QQVHxPTVx250x400x0J"});
  normSys("SysTheoryDelta1_qqVH",0.053,{"QQVHxPTVx250x400x1J"});
  normSys("SysTheoryDelta1_qqVH",0.053,{"QQVHxPTVx250x400xGE2J"});
  normSys("SysTheoryDelta1_qqVH",-0.068,{"QQVHxPTVxGT400x0J"});
  normSys("SysTheoryDelta1_qqVH",0.055,{"QQVHxPTVxGT400x1J"});
  normSys("SysTheoryDelta1_qqVH",0.055,{"QQVHxPTVxGT400xGE2J"});

  normSys("SysTheoryDelta2_qqVH",-0.045,{"QQVHxPTVx0x75x1J"});
  normSys("SysTheoryDelta2_qqVH",-0.047,{"QQVHxPTVx75x150x1J"});
  normSys("SysTheoryDelta2_qqVH",-0.05,{"QQVHxPTVx150x250x1J"});
  normSys("SysTheoryDelta2_qqVH",-0.05,{"QQVHxPTVx250x400x1J"});
  normSys("SysTheoryDelta2_qqVH",-0.057,{"QQVHxPTVxGT400x1J"});

  normSys("SysTheoryDelta2_qqVH",0.1,{"QQVHxPTVx0x75xGE2J"});
  normSys("SysTheoryDelta2_qqVH",0.093,{"QQVHxPTVx75x150xGE2J"});
  normSys("SysTheoryDelta2_qqVH",0.08,{"QQVHxPTVx150x250xGE2J"});
  normSys("SysTheoryDelta2_qqVH",0.067,{"QQVHxPTVx250x400xGE2J"});
  normSys("SysTheoryDelta2_qqVH",0.067,{"QQVHxPTVxGT400xGE2J"});

  //ggZH, delta1 and delta 2

  normSys("SysTheoryDelta1_ggZH",-0.31,{"GGZHxPTVx0x75x0J"});
  normSys("SysTheoryDelta1_ggZH",0.25,{"GGZHxPTVx0x75x1J"});
  normSys("SysTheoryDelta1_ggZH",0.25,{"GGZHxPTVx0x75xGE2J"});
  normSys("SysTheoryDelta1_ggZH",-0.3,{"GGZHxPTVx75x150x0J"});
  normSys("SysTheoryDelta1_ggZH",0.25,{"GGZHxPTVx75x150x1J"});
  normSys("SysTheoryDelta1_ggZH",0.25,{"GGZHxPTVx75x150xGE2J"});
  normSys("SysTheoryDelta1_ggZH",-0.5,{"GGZHxPTVx150x250x0J"});
  normSys("SysTheoryDelta1_ggZH",0.26,{"GGZHxPTVx150x250x1J"});
  normSys("SysTheoryDelta1_ggZH",0.26,{"GGZHxPTVx150x250xGE2J"});
  normSys("SysTheoryDelta1_ggZH",-1.0,{"GGZHxPTVx250x400x0J"});
  normSys("SysTheoryDelta1_ggZH",0.28,{"GGZHxPTVx250x400x1J"});
  normSys("SysTheoryDelta1_ggZH",0.28,{"GGZHxPTVx250x400xGE2J"});
  normSys("SysTheoryDelta1_ggZH",-1.0,{"GGZHxPTVxGT400x0J"});
  normSys("SysTheoryDelta1_ggZH",0.3,{"GGZHxPTVxGT400x1J"});
  normSys("SysTheoryDelta1_ggZH",0.3,{"GGZHxPTVxGT400xGE2J"});

  normSys("SysTheoryDelta2_ggZH",-0.15,{"GGZHxPTVx0x75x1J"});
  normSys("SysTheoryDelta2_ggZH",-0.15,{"GGZHxPTVx75x150x1J"});
  normSys("SysTheoryDelta2_ggZH",-0.2,{"GGZHxPTVx150x250x1J"});
  normSys("SysTheoryDelta2_ggZH",-0.38,{"GGZHxPTVx250x400x1J"});
  normSys("SysTheoryDelta2_ggZH",-0.66,{"GGZHxPTVxGT400x1J"});

  normSys("SysTheoryDelta2_ggZH",0.25,{"GGZHxPTVx0x75xGE2J"});
  normSys("SysTheoryDelta2_ggZH",0.26,{"GGZHxPTVx75x150xGE2J"});
  normSys("SysTheoryDelta2_ggZH",0.26,{"GGZHxPTVx150x250xGE2J"});
  normSys("SysTheoryDelta2_ggZH",0.28,{"GGZHxPTVx250x400xGE2J"});
  normSys("SysTheoryDelta2_ggZH",0.3,{"GGZHxPTVxGT400xGE2J"});

  TString inputPath = "root://eosatlas.cern.ch:1094///eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/";
  if( !doXSWS) //mu WS includes theory sys
    {
      // overall scale uncertainty, applied to all bins and TheoryDelta
      getSysFromFile(inputPath + "out_Scale_Unc_MU.root", "Scale_UNC");
    }
  else //XS WS only includes residual theory sys
    {
      int  FitSTXSScheme = m_config.getValue("FitSTXSScheme", 1);

      if( FitSTXSScheme == 3 ) // 5 POI 
	{
          getSysFromFile(inputPath + "out_Scale_Unc_XS5poi_Frank.root", "Scale_UNC");
	  // getSysFromFile(inputPath + "out_Scale_Unc_XS5poi_updated.root", "Scale_UNC");
	}
      if( FitSTXSScheme == 4 ) // 9 POI, only the 0-75 + 75-150 and 250-400 + GT400 bins are merged
	{
	  //QQVH and ggZH
	  getSysFromFile(inputPath + "out_Scale_Unc_XS9poi.root", "Scale_UNC");
	}
      if( FitSTXSScheme == 5 ) //TODO: 3POI, input file to be checked. 
	{
	  //QQVH and ggZH
          getSysFromFile(inputPath + "out_Scale_Unc_XS3poi_Frank.root", "Scale_UNC");
	  //getSysFromFile(inputPath + "out_Scale_Unc_XS3poi_updated.root", "Scale_UNC");
	}
      if( FitSTXSScheme == 6 ) // 5 POI EFT
	{
	  getSysFromFile(inputPath + "out_Scale_Unc_XS5poi_EFT.root", "Scale_UNC");
	}
      if( FitSTXSScheme == 7 ) // 3 POI EFT (with QQ and GG split)
  {
    getSysFromFile(inputPath + "out_Scale_Unc_XS3poi_EFT.root", "Scale_UNC");
  }  
      if( FitSTXSScheme == 8 ) // 
  {
    getSysFromFile(inputPath + "out_Scale_Unc_XS3VPT.root", "Scale_UNC");
  }   
    }

}

void SystematicListsBuilder_GHHRun2::listAllUserSystematics_STXSPDF() {
  // This function will be only called when (doSTXS)

  /////////////////////////////
  // 
  // PDF and alphaS variations
  //
  /////////////////////////////
  
  bool doXSWS = m_config.getValue("DoXSWS", false);
  int  FitSTXSScheme = m_config.getValue("FitSTXSScheme", 1);
  TString inputFile;
  if(!doXSWS){
    inputFile = "root://eosatlas.cern.ch:1094///eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/out_PDF_raw.root";  
  }
  else{
    if(FitSTXSScheme == 3) inputFile = "root://eosatlas.cern.ch:1094///eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/out_PDF_5poiXS.root";
    if(FitSTXSScheme == 4) inputFile = "root://eosatlas.cern.ch:1094///eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/out_PDF_9poiXS.root";
    if(FitSTXSScheme == 5) inputFile = "root://eosatlas.cern.ch:1094///eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/out_PDF_5poiXS.root"; //TODO: 3POI, input file not ready yet. 
    if(FitSTXSScheme == 6) inputFile = "root://eosatlas.cern.ch:1094///eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/out_PDF_5poiXS_EFT.root"; 
    if(FitSTXSScheme == 7) inputFile = "root://eosatlas.cern.ch:1094///eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/out_PDF_5poiXS.root"; //TODO: 3POI, input file not ready yet. 
    if(FitSTXSScheme == 8) inputFile = "root://eosatlas.cern.ch:1094///eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/Summer2018/STXS/out_PDF_5poiXS.root"; //TODO: 3POI, input file not ready yet. 
  }
  getSysFromFile(inputFile, "PDF_for_WH");
  getSysFromFile(inputFile, "PDF_for_qqZH");
  getSysFromFile(inputFile, "PDF_for_ggZH");

}

void SystematicListsBuilder_GHHRun2::getSysFromFile(const TString& rootFile, const TString& inputHisto) {

  TFile* fileSysTheory = TFile::Open(rootFile);
  if ( !rootFile ) {
    std::cout << "Error: I can't open this file: " + rootFile << std::endl;
    throw;
  }
  TH2F * hSysTheory = (TH2F *) fileSysTheory->Get(inputHisto);
  if ( !hSysTheory ) {
    std::cout << "Error: I can't open histo " + inputHisto + " from " + rootFile << std::endl;
    throw;
  }
  for(int y=1; y<= hSysTheory->GetNbinsY(); y++){
    for(int x=1; x<= hSysTheory->GetNbinsX(); x++){
      if( fabs(hSysTheory->GetBinContent(x,y)) > 2e-3 ){
        //Do not set the systematics for empty bins        
        SampleIndex::findAll(hSysTheory->GetXaxis()->GetBinLabel(x));
        //The function above will throw an exception and crash if does not find the keyword
        normSys(hSysTheory->GetYaxis()->GetBinLabel(y), hSysTheory->GetBinContent(x,y), {hSysTheory->GetXaxis()->GetBinLabel(x)});
      }
    }
  }
  fileSysTheory->Close();

}

void SystematicListsBuilder_GHHRun2::getPSSysFromFile(const TString& rootFile, const TString& inputHisto) {

  TFile* fileSysTheoryPSs = TFile::Open(rootFile);
  if ( !rootFile ) {
    std::cout << "Error: I can't open this file: " + rootFile << std::endl;
    throw;
  }
  TH2F * hSysTheoryPSs = (TH2F *) fileSysTheoryPSs->Get(inputHisto);
  if ( !hSysTheoryPSs ) {
    std::cout << "Error: I can't open histo " + inputHisto + " from " + rootFile << std::endl;
    throw;
  }
  TString sysName = "SysTheory" + inputHisto;
  for(int y=1; y<= hSysTheoryPSs->GetNbinsY(); y++){
    TString yLabel = hSysTheoryPSs->GetYaxis()->GetBinLabel(y);
    PropertiesSet pset(yLabel);
    if(CategoryIndex::findAll(yLabel).size() == 0){
      std::cout << "Warning: found in the PS sys file " + yLabel + ". No matching category for it." << std::endl;
    }
    for(int x=1; x<= hSysTheoryPSs->GetNbinsX(); x++){
      if( fabs(hSysTheoryPSs->GetBinContent(x,y)) > 2e-3 ){
        TString xLabel = hSysTheoryPSs->GetXaxis()->GetBinLabel(x);
        if(xLabel.EqualTo("") || xLabel.Contains("nknown")) continue;
        SampleIndex::findAll(xLabel);
        normSys(sysName, hSysTheoryPSs->GetBinContent(x,y), SysConfig{xLabel}.applyIn(pset)); 
      }
    }
  }
  fileSysTheoryPSs->Close();

}
