#include <iostream>
#include <vector>
#include <regex>
#include "TXMLEngine.h"

#include "postprocessing_eft_ghhrun2.hpp"
#include "xmlutils.hpp"
#include "WSMaker/configuration.hpp"
#include "WSMaker/utility.hpp"
#include "WSMaker/outputhandler.hpp"

#include <RooWorkspace.h>
#include <RooAbsData.h>
#include <RooArgSet.h>

#include "RooStats/ModelConfig.h"

PostProcessingTool_EFT_GHHRun2::PostProcessingTool_EFT_GHHRun2(const Configuration& conf) : 
    PostProcessingTool(conf)
{
    // rest of constructor
}

void PostProcessingTool_EFT_GHHRun2::createInstance(const Configuration& conf) {
    if(!the_instance) {
	std::cout << "INFO:    PostProcessingTool_GHHRun2::createInstance() PostProcessingTool pointer is NULL." << std::endl;
	std::cout << "         Will instanciate the PostProcessingTool service first." << std::endl;
	the_instance.reset(new PostProcessingTool_EFT_GHHRun2(conf));
    }
    else {
	std::cout << "WARNING: PostProcessingTool_EFT_GHHRun2::createInstance() PostProcessingTool pointer already exists." << std::endl;
	std::cout << "         Don't do anything !" << std::endl;
    }
}

PostProcessingTool_EFT_GHHRun2::operator_list PostProcessingTool_EFT_GHHRun2::loadOperatorList(const TString& path) {

    std::vector<TString> retlist;

    TXMLEngine xml;
    XMLDocPointer_t xmldoc = xml.ParseFile(path);
    if(!xmldoc) {
	std::cout << "ERROR: parsing of XML input failed." << std::endl;
	throw;
    }

    XMLNodePointer_t mainnode = xml.DocGetRootElement(xmldoc);

    // extract the operators available in this parametrisation
    XMLNodePointer_t operators_node = XMLUtils::GetChildNodeByName(xml, mainnode, "operators");
    retlist = XMLUtils::GetStringListAttribute(xml, operators_node, "operators");

    xml.FreeDoc(xmldoc);

    return retlist;
}

void PostProcessingTool_EFT_GHHRun2::placeCommand(ParametrisationScheme& scheme, const TString& cmd) {
    std::cout << "WS EDIT <-> " << cmd << std::endl;
    scheme.AddExpression(cmd.Data());
}

PostProcessingTool_EFT_GHHRun2::bin_list PostProcessingTool_EFT_GHHRun2::loadBinListFromParametrisation(const TString& path) {
    std::vector<TString> retlist;

    TXMLEngine xml;
    XMLDocPointer_t xmldoc = xml.ParseFile(path);
    if(!xmldoc) {
	std::cout << "ERROR: parsing of XML input failed." << std::endl;
	throw;
    }

    XMLNodePointer_t mainnode = xml.DocGetRootElement(xmldoc);

    // extract the bins available in this parametrisation
    XMLNodePointer_t param_node = XMLUtils::GetChildNodeByName(xml, mainnode, "parametrisation");
    XMLNodePointer_t bin_node = xml.GetChild(param_node);
    do
    {
	TString bin_name = XMLUtils::GetAttribute(xml, bin_node, "name");
	retlist.push_back(bin_name);
    }
    while((bin_node = xml.GetNext(bin_node)));

    xml.FreeDoc(xmldoc);
    return retlist;
}

PostProcessingTool_EFT_GHHRun2::bin_list PostProcessingTool_EFT_GHHRun2::loadBinListFromWorkspace(const TString& path) {
    bin_list retlist;

    // open the STXS workspace and get the list of POIs -> these correspond to the STXS bins
    // that can be reparametrised in terms of Wilson coefficients
    TFile* wsFile = TFile::Open(path, "READ");
    RooWorkspace* w = (RooWorkspace*)(wsFile -> Get("combined"));
    RooStats::ModelConfig* mc = (RooStats::ModelConfig*)(w -> obj("ModelConfig"));
    
    // get the POIs and store their names
    const RooArgSet* bins = mc -> GetParametersOfInterest();
    
    auto it = bins -> createIterator();
    RooRealVar* var;
    while((var = (RooRealVar*)(it -> Next()))) {
	retlist.push_back(var -> GetName());
    }
    
    wsFile -> Close();
    return retlist;
}

PostProcessingTool_EFT_GHHRun2::multi_bin_parametrisation PostProcessingTool_EFT_GHHRun2::loadParametrisation(const TString& path) {

    multi_bin_parametrisation param;

    TXMLEngine xml;
    XMLDocPointer_t xmldoc = xml.ParseFile(path);
    if(!xmldoc) {
	std::cout << "ERROR: parsing of XML input failed." << std::endl;
	throw;
    }
    
    XMLNodePointer_t mainnode = xml.DocGetRootElement(xmldoc);

    // extract the parametrisation itself
    XMLNodePointer_t param_node = XMLUtils::GetChildNodeByName(xml, mainnode, "parametrisation");

    // loop through all bins defined in the parametrisation and extract the individual contributions
    XMLNodePointer_t bin_node = xml.GetChild(param_node);
    do
    {
	TString bin_name = XMLUtils::GetAttribute(xml, bin_node, "name");
	single_bin_parametrisation cur_bin_param;
	XMLNodePointer_t contrib_node = xml.GetChild(bin_node);
	do
	{
	    std::vector<TString> term = XMLUtils::GetStringListAttribute(xml, contrib_node, "term");
	    float coeff = XMLUtils::GetFloatAttribute(xml, contrib_node, "coeff");
	    cur_bin_param.push_back(std::make_pair(term, coeff));
	}
	while((contrib_node = xml.GetNext(contrib_node)));

	param.insert(std::make_pair(bin_name, cur_bin_param));
    }
    while((bin_node = xml.GetNext(bin_node)));

    xml.FreeDoc(xmldoc);    

    return param;
}

PostProcessingTool_EFT_GHHRun2::polynomial PostProcessingTool_EFT_GHHRun2::loadPolynomial(const TString& param_file_path) {

    operator_list oplist = this -> loadOperatorList(param_file_path);
    bin_list binlist = this -> loadBinListFromParametrisation(param_file_path);
    multi_bin_parametrisation param = this -> loadParametrisation(param_file_path);

    return std::make_tuple(oplist, binlist, param);
}

bool PostProcessingTool_EFT_GHHRun2::checkPolynomialIntegrity(const polynomial& to_check, const std::vector<TString>& bins, const std::vector<TString>& operators) {

    operator_list available_operators;
    bin_list available_bins;
    multi_bin_parametrisation param;

    std::tie(available_operators, available_bins, param) = to_check;

    // check that all operators that have been requested are actually present in the parametrisation
    for(const auto& cur_operator: operators) {
	if(std::find(available_operators.begin(), available_operators.end(), cur_operator) != available_operators.end()) {
	    std::cout << "Confirmed presence of variable '" << cur_operator << "' in polynomial!" << std::endl;	    
	}
	else {
	    std::cout << "Required presence of nonexistant variable '" << cur_operator << "' in polynomial - Aborting!" << std::endl;	    
	    throw;
	}
    }

    // ensure that have a parametrisation available for each bin that has been requested
    for(const auto& cur_bin: bins) {
	if(std::find(available_bins.begin(), available_bins.end(), cur_bin) != available_bins.end()) {
	    std::cout << "Confirmed presence of bin '" << cur_bin << "' in polynomial!" << std::endl;	    
	}
	else {
	    std::cout << "Required presence of nonexistant bin '" << cur_bin << "', in polynomial - Aborting!" << std::endl;	    
	    throw;
	}
    }

    // check if there are any bins in the parametrisation that are not available in the WS. This may not be a problem, if these bins are
    // not touched during the reparametrisation. To be safe, these bins are flagged and a warning message gets printed.
    // (this is supposed to catch any problems with using inappropriate parametrisation)
    for(const auto& cur_bin: available_bins) {
	if(!(std::find(bins.begin(), bins.end(), cur_bin) != bins.end())) {	
	    std::cout << "WARNING: Polynomial contains additional bin '" << cur_bin << "', which has not been asked for! This will not trigger a crash, just make sure you are using the correct parametrisation!" << std::endl;
	}
    }

    return true;
}

void PostProcessingTool_EFT_GHHRun2::printPolynomial(const polynomial& to_print) {
    
    multi_bin_parametrisation param;
    std::tie(std::ignore, std::ignore, param) = to_print;
    
    for(const auto& cur: param) {
    	TString cur_bin_name = cur.first;
    	single_bin_parametrisation cur_param = cur.second;

    	std::cout << "------------------------------" << std::endl;
    	std::cout << " bin : " << cur_bin_name << std::endl;
    	std::cout << "- - - - - - - - - - - - - - - " << std::endl;
    	for(const auto& cur_term: cur_param) {
    	    auto cur_operators = cur_term.first;
    	    for(const auto& cur_operator: cur_operators)
    		std::cout << cur_operator << " ";
    	    std::cout << ": " << cur_term.second << std::endl;
    	}
    	std::cout << "------------------------------" << std::endl;
    }
}

void PostProcessingTool_EFT_GHHRun2::assemblePolynomial(ParametrisationScheme& param_scheme, const polynomial& poly, const TString& expr_name) {

    multi_bin_parametrisation param;
    std::tie(std::ignore, std::ignore, param) = poly;

    // now assemble the actual parametrisation expressions for each bin
    for(const auto& cur: param) {
    	TString cur_bin_name = cur.first;
    	single_bin_parametrisation cur_param = cur.second;
    	TString expr_preamble = "expr::" + expr_name + "_" + cur_bin_name;
	
    	std::vector<TString> expr_strings;
    	std::vector<TString> arglist;

    	for(const auto& cur_term: cur_param) {
    	    auto cur_operators = cur_term.first;
    	    float term_coeff = cur_term.second;

    	    TString term_expr = Utils::assemble(cur_operators, '*');
    	    arglist.insert(std::end(arglist), std::begin(cur_operators), std::end(cur_operators));
	    
    	    expr_strings.push_back("(" + std::to_string(term_coeff) + ")*" + term_expr);
    	}

    	TString expr_body = Utils::assemble(expr_strings, '+');
    	TString arglist_expr = Utils::assemble(arglist, ',');

    	TString exprstring = expr_preamble + "('" + expr_body + "', " + arglist_expr + ")";
    	this -> placeCommand(param_scheme, exprstring);
    }    
}

void PostProcessingTool_EFT_GHHRun2::reparametrise(const TString& wsfilename, const TString& param_expr, const std::vector<TString>& polynomial_names, const std::vector<TString>& polynomial_defs, const std::vector<TString>& POI_names, const std::vector<double>& POI_ranges, const std::vector<TString>& active_bins) {

    std::cout << "INFO:    starting to re-parametrise the workspace!" << std::endl;

    TString root_dir = std::getenv("ANALYSISDIR");

    std::cout << "using the following active POIs" << std::endl;
    for(const auto& cur: POI_names) {
	std::cout << cur << std::endl;
    }

    std::cout << "using the following active bins" << std::endl;
    for(const auto& cur: active_bins) {
	std::cout << cur << std::endl;
    }

    // -------------------------------------
    // take a look into the input workspace and check which STXS bins are available
    // -------------------------------------
    bin_list available_bins_WS = this -> loadBinListFromWorkspace(wsfilename);
    
    std::cout << "have the following bins available in the workspace:" << std::endl;
    for(const auto& cur_bin: available_bins_WS) {
	std::cout << cur_bin << std::endl;
    }

    // ensure that the bins that have been requested to be reparametrised are actually available in the WS
    for(const auto& cur_active_bin: active_bins) {
	if(std::find(available_bins_WS.begin(), available_bins_WS.end(), cur_active_bin) != available_bins_WS.end()) {
	    std::cout << "Requested to reparametrise bin '" << cur_active_bin << "', which is available in the WS" << std::endl;
	}
	else {
	    std::cout << "ERROR: Requested to reparametrise bin '" << cur_active_bin << "', which is not available in the WS - Aborting!" << std::endl;
	    throw;
	}
    }

    // ----------------------------------------------------------------
    // load the polynomials from disk and check that they make sense
    // ----------------------------------------------------------------
    std::vector<polynomial> polynomials;
    operator_list available_operators;
    for(auto cur_file_path: polynomial_defs) {

	TString polynomial_file_path = root_dir + "/inputs/" + cur_file_path;
	std::cout << "trying to load polynomial from " << polynomial_file_path << std::endl;

	polynomial cur_polynomial = loadPolynomial(polynomial_file_path);

        // only require that each polynomial covers the correct bins; check the presence of operators later on
	checkPolynomialIntegrity(cur_polynomial, active_bins, {});  
	polynomials.push_back(cur_polynomial);

	std::cout << "successfully loaded the following polynomial" << std::endl;
	printPolynomial(cur_polynomial);

	// also extract the operators this polynomial depends on, and keep track of it for later
	operator_list cur_available_operators;
	std::tie(cur_available_operators, std::ignore, std::ignore) = cur_polynomial;
	available_operators.insert(std::end(available_operators), std::begin(cur_available_operators), std::end(cur_available_operators));
    }

    // remove duplicates
    std::set<TString> tmpset(std::begin(available_operators), std::end(available_operators));
    available_operators.assign(std::begin(tmpset), std::end(tmpset));

    // check whether all required operators are available
    for(const auto& cur_operator: POI_names) {
	if(std::find(available_operators.begin(), available_operators.end(), cur_operator) != available_operators.end()) {
	    std::cout << "Confirmed presence of operator '" << cur_operator << "' in loaded polynomials!" << std::endl;	    
	}
	else {
	    std::cout << "Required presence of nonexistant operator '" << cur_operator << "' in loaded polynomials - Aborting, since the generated workspace will have no dependence on this operator!" << std::endl;
	    throw;
	}
    }    

    // Sanity checks done; print a summary of what is going to happen
    std::vector<TString> constant_operators = Utils::vector_difference(available_operators, POI_names);
    std::vector<TString> inactive_bins = Utils::vector_difference(available_bins_WS, active_bins);

    // print a few things
    std::cout << "have the following operators available:" << std::endl;
    for(const auto& cur_operator: available_operators) {
	std::cout << cur_operator << std::endl;
    }

    std::cout << "will fit the following operators:" << std::endl;
    for(const auto& cur_operator: POI_names) {
	std::cout << cur_operator << std::endl;
    }

    std::cout << "keep the following operators constant:" << std::endl;
    for(const auto& cur_operator: constant_operators) {
	std::cout << cur_operator << std::endl;
    }

    std::cout << "do not touch the following bins:" << std::endl;
    for(const auto& cur_bin: inactive_bins) {
	std::cout << cur_bin << std::endl;
    }
    
    // -------------------------------------
    // perform the reparametrisation
    // -------------------------------------
    CombinedMeasurement combined("combined_master");
    Measurement VHWS("VH");
    VHWS.SetSnapshotName("nominalNuis");
    VHWS.SetFileName(wsfilename.Data());
    VHWS.SetWorkspaceName("combined");
    VHWS.SetModelConfigName("ModelConfig");
    VHWS.SetDataName("obsData");
    combined.AddMeasurement(VHWS);

    // In preparation of the reparametrisation, rename the STXS POIs (i.e. the signal strengths for each
    // STXS bin). Note: this is a purely technical exercise, there is no actual "correlation" being 
    // changed.
    CorrelationScheme correlation("CorrelationScheme");
    for(const auto& cur_stxs_bin: available_bins_WS) {
    	correlation.CorrelateParameter("VH::" + cur_stxs_bin, cur_stxs_bin);
    }
    
    combined.SetCorrelationScheme(&correlation);
    combined.CollectMeasurements();
    combined.CombineMeasurements();

    ParametrisationSequence CustomParametrisationSequence("CustomParametrisation");
    std::string POI_string = (Utils::assemble(POI_names, ',')).Data();
    std::cout << "setting POI_string = " << POI_string << std::endl;
    CustomParametrisationSequence.SetParametersOfInterest(POI_string);

    ParametrisationScheme KappaTScheme("VH");

    // create a RooRealVar for each Wilson coefficient (regardless of whether they should be fitted or not)
    //for(auto cur_name: POI_names) {
    for(unsigned int POI_ind = 0; POI_ind < POI_names.size(); POI_ind++) {
    	TString cur_name = POI_names[POI_ind];
    	double cur_range = POI_ranges[POI_ind];
    	TString varstring = cur_name + "[0.0, " + std::to_string(-cur_range) + ", " + std::to_string(cur_range) + "]";
    	this -> placeCommand(KappaTScheme, varstring);
    }

    for(const auto& cur_name: constant_operators) {
    	TString varstring = cur_name + "[0.0]";
    	this -> placeCommand(KappaTScheme, varstring);
    }

    // create auxiliary variable
    this -> placeCommand(KappaTScheme, "one[1.0]");

    // create all loaded polynomials
    for(unsigned int cur_poly = 0; cur_poly < polynomials.size(); cur_poly++) {
	assemblePolynomial(KappaTScheme, polynomials[cur_poly], polynomial_names[cur_poly]);
    }

    // now apply the parametrisation only to the requested bins
    for(const auto& cur_active_bin: active_bins) {

	TString final_expr_name = cur_active_bin + "_param";

	// form the final expression that ties together the individual polynomials
	TString final_param_expr = param_expr;
	std::vector<TString> arglist;
	for(const auto& cur_polynomial_name: polynomial_names) {
	    // replace cur_polynomial_name -> cur_polynomial_name + "_" + cur_active_bin
	    TString renamed_term = cur_polynomial_name + "_" + cur_active_bin;
	    std::regex term_finder = std::regex("(\\s|\\+|\\-|\\*|\\(|/|^)(" + cur_polynomial_name + ")(?=(\\s|\\+|\\-|\\*|\\)|/|$))");
	    final_param_expr = std::regex_replace(final_param_expr.Data(), term_finder, ("$1" + renamed_term).Data());
	    arglist.push_back(renamed_term);
	}

	final_param_expr = "expr::" + final_expr_name + "('" + final_param_expr + "'," + Utils::assemble(arglist, ',') + ")";
	this -> placeCommand(KappaTScheme, final_param_expr);

    	TString exprstring = cur_active_bin + "=" + final_expr_name;
    	this -> placeCommand(KappaTScheme, exprstring);
    }

    for(const auto& cur_inactive_bin: inactive_bins) {
    	TString exprstring = cur_inactive_bin + "=one";
    	this -> placeCommand(KappaTScheme, exprstring);
    }

    CustomParametrisationSequence.AddScheme(KappaTScheme);
    combined.SetParametrisationSequence(&CustomParametrisationSequence);
    combined.ParametriseMeasurements();

    combined.writeToFile(wsfilename);

    //what happes is that the CombinationTool creates a new dataset "combData" instead of the original "obsData" (which also all the other tools expect to find)
    //to mitigate issues, open the WS again and rename the dataset
    TFile* wsFile = TFile::Open(wsfilename, "UPDATE");
    RooWorkspace* w = nullptr;
    wsFile -> GetObject("combined", w);
    RooAbsData* data = w -> data("combData");
    w -> import(*data, Rename("obsData"));
    w -> writeToFile(wsfilename);
    wsFile -> Close();
}


void PostProcessingTool_EFT_GHHRun2::process(const TString& wsfilename) {
    bool doEFT = m_config.getValue("doEFT", false);
    bool doEigenOperators = m_config.getValue("doEigenoperators", false);

    if(doEigenOperators && !doEFT)
    {
	std::cout << "ERROR:   you need to set doEFT = True to be able to fit eigenoperators!" << std::endl;
	throw;
    }

    if(!doEFT)
    {
	std::cout << "INFO:    no need to re-parametrise the workspace!" << std::endl;
	return;
    }

    // perform the WS reparametrisation
    TString param_expr = m_config.getValue("paramExpr", "param");
    std::vector<TString> polynomial_names = m_config.getStringList("polynomialNames");
    std::vector<TString> polynomial_defs = m_config.getStringList("polynomialDefs");

    if(polynomial_names.size() != polynomial_defs.size()) {
	std::cout << "ERROR:  Require a unique name for each polynomial!" << std::endl;
	throw;
    }

    std::vector<TString> POI_names = m_config.getStringList("activePOIs");
    std::vector<double> POI_ranges = m_config.getDoubleList("POIRanges");
    std::vector<TString> active_bins = m_config.getStringList("activeBins");
    reparametrise(wsfilename, param_expr, polynomial_names, polynomial_defs, POI_names, POI_ranges, active_bins);

    if(doEigenOperators) 
    {
	// perform the reparametrisation in terms of eigenoperators
	TString paramFileEigenoperators = m_config.getValue("EigenoperatorDefsFile", "eigen_default.xml");
	std::vector<TString> EigenPOI_names = m_config.getStringList("activeEigenoperators");
	std::vector<double> EigenPOI_ranges = m_config.getDoubleList("EigenPOIRanges");

	TString eigenparam_expr = "eigenparam";
	std::vector<TString> eigenpolynomial_names = {eigenparam_expr};
	std::vector<TString> eigenpolynomial_defs = {paramFileEigenoperators};
	
	reparametrise(wsfilename, eigenparam_expr, eigenpolynomial_names, eigenpolynomial_defs, EigenPOI_names, EigenPOI_ranges, POI_names);
    }
}
