#include "binning_ghhrun2.hpp"

#include <iostream>
#include <cmath>
#include <map>
#include <memory>

#include <TH1.h>
#include <TString.h>

#include "WSMaker/binning.hpp"
#include "WSMaker/configuration.hpp"
#include "WSMaker/category.hpp"
#include "WSMaker/properties.hpp"

#include "TransformTool/HistoTransform.h"

#include <iostream>

class Sample;

BinningTool_GHHRun2::BinningTool_GHHRun2(const Configuration& conf) :
  BinningTool(conf)
{
  // here, get parameters from m_config to tune HistoTransform parameters
  // m_method =  6: transformation D
  // m_method = 12: transformation F
  // see transform/HistoTransform.C for more m_method
  // default: transformation D, Zsig = 10, Zbkg = 5, sigma_b < 20%;
  m_htrafo->trafoSixY = m_config.getValue("RebinMVAZsig", 10.);
  m_htrafo->trafoSixZ = m_config.getValue("RebinMVAZbkg", 5.);
  m_htrafo->trafoFzSig = m_config.getValue("RebinMVAZsig", 2.);
  m_htrafo->trafoFzBkg = m_config.getValue("RebinMVAZbkg", 5.);
  m_htrafo->trafoSixMCstatUpBound = m_config.getValue("RebinMVAsigmaB", 0.2); // default: sigma_b  < 20% requirement
  m_htrafo->trafoFMCstatUpBound = m_config.getValue("RebinMVAsigmaB", 0.2);
}

void BinningTool_GHHRun2::createInstance(const Configuration& conf) {
  if( !the_instance ) {
    std::cout << "INFO:    BinningTool_GHHRun2::createInstance() BinningTool pointer is NULL." << std::endl;
    std::cout << "         Will instanciate the BinningTool service first." << std::endl;
    the_instance.reset(new BinningTool_GHHRun2(conf));
  }
  else {
    std::cout << "WARNING: BinningTool_GHHRun2::createInstance() BinningTool pointer already exists." << std::endl;
    std::cout << "         Don't do anything !" << std::endl;
  }
}

std::vector<int> BinningTool_GHHRun2::getMVABinning(const Category& cat) {
  TH1* sig = cat.getSigHist();
  TH1* bkg = cat.getBkgHist();
  std::vector<int> bins;
  if ( cat(Property::dist) == "mvadiboson" && cat[Property::binMin] == 250) {
    m_htrafo->trafoSixY = 3;
    m_htrafo->trafoSixZ = 2;
  } else if ( cat(Property::dist) == "mvadiboson" ) {
    m_htrafo->trafoSixY = 5;
    m_htrafo->trafoSixZ = 5;
  }

  if ( cat(Property::dist) == "mva" && cat[Property::binMin] == 250) {
    // std::cout <<"I'm inside the >250ptv for: " << cat.name() <<std::endl;
    m_htrafo->trafoSixY = m_config.getValue("RebinMVAZsig", 5.);
    m_htrafo->trafoSixZ = m_config.getValue("RebinMVAZbkg", 3.);
  } else if ( cat(Property::dist) == "mva" ){
    // std::cout <<"I'm outside the >250ptv for: " << cat.name() <<std::endl;
    m_htrafo->trafoSixY = m_config.getValue("RebinMVAZsig", 10.);
    m_htrafo->trafoSixZ = m_config.getValue("RebinMVAZbkg",  5.);
  }


  bins = ( cat(Property::descr).Contains("topemucr") || cat(Property::descr).Contains("WhfCR") ) ?
    oneBin(cat) : m_htrafo->getRebinBins(bkg, sig, m_method, 0);

//    m_htrafo->getRebinBins(bkg, sig, 13, 0.1) :  m_htrafo->getRebinBins(bkg, sig, 6);
//    m_htrafo->getRebinBins(bkg, sig, 13, 0.1) :  m_htrafo->getRebinBins(bkg, sig, 13, 0.1);

  std::cout << "Binning_GHHRun2::getMVABinning" << std::endl
    << "  Binning for category " << cat.name() << std::endl;
  std::cout << "{";
  for( auto b: bins ) {
    std::cout << b << ", ";
  }
  std::cout << "}" << std::endl;

  delete sig;
  delete bkg;
  return bins;
}

std::vector<int> BinningTool_GHHRun2::getCategoryBinning(const Category& c) {

  int forceRebin = m_config.getValue("ForceBinning", 0);
  bool doCutBase = m_config.getValue("CutBase", false);
  bool doPostFit = m_config.getValue("PostFit", false);
  bool doNewRegions = m_config.getValue("NewRegions", false);
  if(forceRebin > 0) {
    return {forceRebin};
  }

  std::vector<int> res{};
  //TH1* hist = nullptr;
  //TH1* sig = c.getSigHist();
  //if(sig) { hist = sig; }
  //else {
  //  TH1* bkg = c.getBkgHist();
  //  hist = bkg;
  //}


  if (doNewRegions && (c(Property::descr).Contains("CR") && (c(Property::dist) == "pTV" || c(Property::dist) == "MET")) && !doPostFit){
    return oneBin(c);
  }

  if (doPostFit) {
    res = getCategoryBinningPostFit(c);
  } else {
    if(c(Property::dist) == "mBB" || c(Property::dist) == "mBBMVA" || c(Property::dist)=="pTV"){    //======= Adding pTV to define binning for WPtV CR =======//      //sgargiul 06.03.2019
         //res = {1}; // 100 bins in [0, 500]?
      if(c[Property::nLep] == 0)
        res = {2}; // 20GeV for 0 lepton?
      else if(c[Property::nLep] == 1){
        if( c(Property::descr).Contains("WhfCR") && doCutBase ) return oneBin(c);// for 1lep CR, use OneBin
        else if (doNewRegions) res = {2}; 
        else{
          res = {4}; // ? for 1 lepton? 
	  //getMVABinning(c);//TEST: CR with 2 bins with edges like the STXS definition or ptv regions 
        }
      }
      else{
        if( c(Property::descr).Contains("topemucr") ){
          if ( (c[Property::binMin] == 150 && c[Property::nJet] == 2)
	       || c[Property::binMin] == 250 ) return oneBin(c);
          else{
            res = {50};// 50 GeV bins for 2lep low ptv and medium ptv
          }
        } else res = {10}; // 10 GeV
      }
    }

    if(c(Property::dist) == "mva" || c(Property::dist) == "mvadiboson"){
         //res = {50}; // 20 bins entre -1 et 1 pour 0/1/2 lep
         //delete hist;
      return getMVABinning(c);
    }
  }

  //delete hist;
  return res;

}

void BinningTool_GHHRun2::changeRange(TH1* h, const Category& c) {

  bool doPostFit = m_config.getValue("PostFit", false);
  bool doCutBase = m_config.getValue("CutBase", false);
  bool doMbbPlot = m_config.getValue("MbbPlot", false); // adapt same range for mbb plot
  bool doNewRegions = m_config.getValue("NewRegions", false);

  if (doNewRegions && !doPostFit){
    if (c(Property::descr).Contains("CR") && (c(Property::dist) == "pTV" || c(Property::dist) == "MET")){
       if (c[Property::binMin] == 75) changeRangeImpl(h,75,150);
       else if (c[Property::binMin] == 250) changeRangeImpl(h,250,500);
       else if (c[Property::binMin] == 150) changeRangeImpl(h,150,250);
       return;
    } 
  }

  if (doPostFit){
    changeRangePostFit(h,c);
  } else {
    if(c(Property::dist) == "mBB" || c(Property::dist) == "mBBMVA" || c(Property::dist) == "pTV"){  //======= Adding pTV to define WPtV CR range in 2j and 3j regions =======//    //sgargiul 06.03.2019
      //changeRangeImpl(h,0,500); // full range
      //     changeRangeImpl(h,15,315); // centered on 125 GeV
      //changeRangeImpl(h,0,250);
      if (doMbbPlot) {
        changeRangeImpl(h,30,210);
      }
      else {
        if ( c[Property::nLep] == 0 ) {
          if(doCutBase) changeRangeImpl(h,30,210);
          else changeRangeImpl(h,0,500);
        }
        if ( c[Property::nLep] == 1 ) {
      	  if( c(Property::descr).Contains("WhfCR") && c[Property::nJet] == 2) changeRangeImpl(h,150,430);    //======= Range definition for WPtV CR in 2j and 3j regions =======//  //sgargiul 06.03.2019
      	  else if( c(Property::descr).Contains("WhfCR") && c[Property::nJet] == 3) changeRangeImpl(h,150,710);

          //if( c(Property::descr).Contains("WhfCR") ) changeRangeImpl(h,0,100);
          if( c(Property::descr).Contains("WhfSR") ) {
            if(doCutBase) changeRangeImpl(h,30,200);
            else changeRangeImpl(h,0,500);
          }

          if (doNewRegions) changeRangeImpl(h,30,200);
        }
        if ( c[Property::nLep] == 2 ){
          if( c(Property::descr).Contains("SR") && doCutBase ){
            if( c[Property::binMin] == 75 )
            {
              if( c[Property::nJet] == 2 ) changeRangeImpl(h,20,300);
              if( c[Property::nJet] == 3 ) changeRangeImpl(h,20,300);
            }
            if( c[Property::binMin] == 150 )
            {
              if( c[Property::nJet] == 2 ) changeRangeImpl(h,40,200);
              if( c[Property::nJet] == 3 ) changeRangeImpl(h,20,200);
            }
            if( c[Property::binMin] == 250 )
            {
              if( c[Property::nJet] == 2 ) changeRangeImpl(h,40,200);
              if( c[Property::nJet] == 3 ) changeRangeImpl(h,20,200);
            }
          }
          else if (c(Property::descr).Contains("SR")) changeRangeImpl(h,0,500);
          else  changeRangeImpl(h,0,300);
        }
      }
    } 
  }
}


std::vector<int> BinningTool_GHHRun2::getCategoryBinningPostFit(const Category& c) {

  std::vector<int> res{};
  bool doCutBase = m_config.getValue("CutBase", false);
  bool doNewRegions = m_config.getValue("NewRegions", false);
  bool dopTVin3bins = m_config.getValue("pTVin3bins", true);

  // special case of MVA distributions
  if(c(Property::dist) == "mva" || c(Property::dist) == "mvadiboson"){

    // Force top e-mu CR to use same MVA binning as signal region. For now, hardcode it.
    if(c(Property::descr).Contains("topemucr")) {
      if( c[Property::nJet] == 2 ) {
        if( c[Property::binMin] == 75 ) {
          res = {1002, 782, 744, 713, 686, 658, 627, 594, 555, 506, 440, 350, 242, 162, 100, 0 };
        }
        else {
          res = {1002, 844, 808, 776, 744, 711, 675, 636, 592, 538, 456, 346, 222, 141, 73, 0 };
        }
      }
      else {
        if( c[Property::binMin] == 75 ) {
          res = {1002, 788, 746, 713, 683, 655, 625, 592, 555, 509, 452, 380, 300, 230, 164, 0 };
        }
        else {
          res = {1002, 837, 798, 763, 730, 696, 660, 620, 573, 516, 447, 364, 278, 210, 156, 0 };
        }
      }

    }
    else {
      res = getMVABinning(c);
    }

    return res;
  }


  if(dopTVin3bins){
    // pTV binning 75-150-250-inf GeV
    if (c(Property::dist) == "pTV" || (c(Property::dist) == "MET" && c[Property::nLep] == 0) ){
      TH1* sig = c.getSigHist();
      if ( c[Property::nLep] == 0 ||  c[Property::nLep] == 1) res = getBinsFromEdges(sig, {400,250,150,70});
      else res = getBinsFromEdges(sig, {400,250,150,70});
      delete sig;
      return res;
    }
  }

  // the bin width in postfit plots
  if ( c[Property::nLep] == 0 ){
     // 0lep:
     if (c(Property::dist) == "MET" ) res = {1};

     if (c(Property::dist) == "pTB1" ) res = {1};

     if (c(Property::dist) == "pTB2" ) res = {2};

     if (c(Property::dist) == "mBB" ){
       if (c(Property::descr).Contains("CRHigh")) res = {4}; // 20GeV cut-based
       else if (c(Property::descr).Contains("SRcuts") || doNewRegions) res = {2}; // 10GeV cut-based
       else res = {4}; // 20 GeV MVA
     }

     if (c(Property::dist) == "dRBB" ) res = {5};

     if (c(Property::dist) == "dEtaBB" ) res = {5};

     if (c(Property::dist) == "dPhiVBB" ) res = {2};

     if (c(Property::dist) == "dPhiBB" ) res = {5};

     if (c(Property::dist).Contains("MEff") || c(Property::dist).Contains("HT") ) res = {5};

     if (c(Property::dist) == "pTJ3" ) res = {4};

     if (c(Property::dist) == "mBBJ" ) res = {10};

     if (c(Property::dist) == "softMET" ) res = {1}; //To be tested

  } else if ( c[Property::nLep] == 1 ){ 
    // 1lep:
    if (c(Property::dist) == "pTV" ) res = {1};
    
    if (c(Property::dist) == "MET" ) res = {2};

     if (c(Property::dist) == "pTB1" ) res = {4};

     if (c(Property::dist) == "pTB2" ) res = {4};

     if (c(Property::dist) == "mBB" ){
       if (c(Property::descr).Contains("CRHigh")) res = {4}; // 20GeV cut-based
       else if (doCutBase || doNewRegions) res = {2};
       else res = {4};
     }

     if (c(Property::dist) == "dRBB" ) res = {4};

     if (c(Property::dist) == "dPhiVBB" ) res = {4};

     if (c(Property::dist) == "dPhiLBmin" ) res = {4};

     if (c(Property::dist) == "mTW" ) res = {4};

     if (c(Property::dist) == "dYWH" ) res = {4};

     if (c(Property::dist) == "Mtop" ) res = {25};

     if (c(Property::dist) == "pTJ3" ) res = {4};

     if (c(Property::dist) == "mBBJ" ) res = {5};

  } else {
     // 2lep:
    if (c(Property::dist) == "pTV" ) res = {3};

     if (c(Property::dist) == "MET" ) res = {4};

     if (c(Property::dist) == "pTB1" ) res = {4};

     if (c(Property::dist) == "pTB2" ) res = {4};

     if ( c(Property::dist) == "mBB" ){
       //if ( c(Property::descr).Contains("SR") ) res = {4};
       //if ( c(Property::descr).Contains("topemucr") ) res = {4};
       if (c(Property::descr).Contains("CRHigh")) res = {20}; // 20GeV cut-based
       else if (doCutBase || doNewRegions) res = {10}; // 10GeV cut-based
       else res = {4};
     }

     if ( c(Property::dist) == "mBBMVA" ){
       if ( c(Property::descr).Contains("SR") ) res = {20};
       if ( c(Property::descr).Contains("topemucr") ) res = {20};
     }

     if (c(Property::dist) == "dRBB" ) res = {4};

     if (c(Property::dist) == "dEtaBB" ) res = {5};

     if (c(Property::dist) == "dPhiVBB" ) res = {4};

     if (c(Property::dist) == "dEtaVBB" ) res = {10};

     if (c(Property::dist) == "mLL" ) res = {2};

     if (c(Property::dist) == "pTJ3" ) res = {10};

     if (c(Property::dist) == "mBBJ3" ) res = {10};

     if (c(Property::dist) == "mBBJ" ) res = {5};

     if (c(Property::dist) == "cosThetaLep" ) res = {2};
  }
  return res;

}

void BinningTool_GHHRun2::changeRangePostFit(TH1* h, const Category& c) {

   bool doCutBase = m_config.getValue("CutBase", false);
  bool doNewRegions = m_config.getValue("NewRegions", false);

   if ( c[Property::nLep] == 0 ){
      // 0lep:
      // pTV binning 75-150-250-inf GeV
      if (c(Property::dist) == "MET" || c(Property::dist) == "pTV" ) changeRangeImpl(h,70,400);
      // // keep old full bins - add a switch ?
      // if (c(Property::dist) == "MET" || c(Property::dist) == "pTV" ) changeRangeImpl(h,150,500);

      if (c(Property::dist) == "pTB1" ) changeRangeImpl(h,40.,500.);

      if (c(Property::dist) == "pTB2" ) changeRangeImpl(h,20.,250.);

      if (c(Property::dist) == "mBB" ){
        if ( c(Property::descr).Contains("SRcuts") 
	     || (doNewRegions && c(Property::descr).Contains("SR")) ) changeRangeImpl(h,30,210);
	else if (c(Property::descr).Contains("CRLow")) changeRangeImpl(h,30,210);
	else if (c(Property::descr).Contains("CRHigh")) changeRangeImpl(h,50,500);
        else changeRangeImpl(h,0,500);
      }

      if (c(Property::dist) == "dRBB" ) changeRangeImpl(h,0,5);

      if (c(Property::dist) == "dEtaBB" ) changeRangeImpl(h,0,4.5);

      if (c(Property::dist) == "dPhiVBB" ) changeRangeImpl(h,2.205,3.2);

      if ( c(Property::dist).Contains("MEff") || c(Property::dist).Contains("HT")) changeRangeImpl(h,240,1000);

      if (c(Property::dist) == "pTJ3" ) changeRangeImpl(h,20,240);

      if (c(Property::dist) == "mBBJ" ) changeRangeImpl(h,0,750);
      
      if (c(Property::dist) == "softMET" ) changeRangeImpl(h,0,60);

   } else if ( c[Property::nLep] == 1 ){
      // 1lep:
      // pTV binning 75-150-250-inf GeV
      if (c(Property::dist) == "pTV" ) changeRangeImpl(h,70,400);
      // // keep old full bins - add a switch ?
      // if (c(Property::dist) == "pTV" ) changeRangeImpl(h,150,500);

      if (c(Property::dist) == "MET" ) changeRangeImpl(h,0,300);

      if (c(Property::dist) == "pTB1" ) changeRangeImpl(h,40,500);

      if (c(Property::dist) == "pTB2" && c(Property::descr).Contains("WhfCR")) changeRangeImpl(h,20.,200);
      if (c(Property::dist) == "pTB2" && c(Property::descr).Contains("WhfSR")) changeRangeImpl(h,20.,500);

      if (c(Property::dist) == "mBB" && c(Property::descr).Contains("WhfCR")) changeRangeImpl(h,0,100);
      if (c(Property::dist) == "mBB" && c(Property::descr).Contains("WhfSR")){
        if(doCutBase) changeRangeImpl(h,30,200);
        else changeRangeImpl(h,0,500);
      }
      if (c(Property::dist) == "mBB" && doNewRegions
	  && c(Property::descr).Contains("SR")) changeRangeImpl(h,30,210);
      if ((c(Property::dist) == "mBB" || c(Property::dist) == "mBBMVA")
          && c(Property::descr).Contains("CRLow")) changeRangeImpl(h,30,210);
      if ((c(Property::dist) == "mBB" || c(Property::dist) == "mBBMVA")
          && c(Property::descr).Contains("CRHigh")) changeRangeImpl(h,50,500);

      if (c(Property::dist) == "dRBB" ){
         if (c(Property::descr).Contains("WhfCR") && (c[Property::nJet] == 2) ) changeRangeImpl(h,0,1);
         else if (c(Property::descr).Contains("WhfCR") && (c[Property::nJet] == 3) ) changeRangeImpl(h,0,2);
         else changeRangeImpl(h,0,5);
      }

      if (c(Property::dist) == "dPhiVBB" && (c[Property::nJet] == 2) ){
         if(c(Property::descr).Contains("WhfCR")) changeRangeImpl(h,2.898,3.15);
         else changeRangeImpl(h,2.4885,3.15);
      }
      if (c(Property::dist) == "dPhiVBB" && (c[Property::nJet] == 3) ) {
         if(c(Property::descr).Contains("WhfCR")) changeRangeImpl(h,1.1655,3.15);
         else changeRangeImpl(h,0.,3.15);
      }

      if (c(Property::dist) == "dPhiLBmin" ) changeRangeImpl(h,0,3.36);

      if (c(Property::dist) == "mTW" ) changeRangeImpl(h,0,260);

      if (c(Property::dist) == "dYWH" ) changeRangeImpl(h,0,4.56);

      if (c(Property::dist) == "Mtop" ) {
        if (c(Property::descr).Contains("WhfCR")) changeRangeImpl(h,200,500);
        else changeRangeImpl(h,75,500);
      }

      if (c(Property::dist) == "pTJ3" ) changeRangeImpl(h,20,500);

      if (c(Property::dist) == "mBBJ" ) changeRangeImpl(h,0,500);

   } else {
      // 2lep:
      if (c(Property::dist) == "pTV" ){
	// pTV binning 75-150-250-inf GeV
        changeRangeImpl(h,70,400);
	// // keep old full bins - add a switch ?
        // if (c(Property::descr).Contains("topemucr")) changeRangeImpl(h,75,345);
        // else changeRangeImpl(h,75,495);
      }

      if (c(Property::dist) == "METSig" ) changeRangeImpl(h,0,8);

      if (c(Property::dist) == "pTB1" ){
        if (c[Property::binMin] == 150) changeRangeImpl(h,40.,320);
        else changeRangeImpl(h,40.,280);
      }

      if (c(Property::dist) == "pTB2" ) changeRangeImpl(h,20.,200);

      if ((c(Property::dist) == "mBB" || c(Property::dist) == "mBBMVA")
          && c(Property::descr).Contains("topemucr")) changeRangeImpl(h,0,360);
      if ((c(Property::dist) == "mBB" || c(Property::dist) == "mBBMVA")
          && c(Property::descr).Contains("SR")){
        if( doCutBase ){
          if( c[Property::binMin] == 75 )
          {
            if( c[Property::nJet] == 2 ) changeRangeImpl(h,20,300);
            if( c[Property::nJet] == 3 ) changeRangeImpl(h,20,300);
          }
          if( c[Property::binMin] == 150 || c[Property::binMin] == 200)
          {
            if( c[Property::nJet] == 2 ) changeRangeImpl(h,40,200);
            if( c[Property::nJet] == 3 ) changeRangeImpl(h,20,200);
          }
        }
	else if ( doNewRegions ) changeRangeImpl(h,30,210);
	else changeRangeImpl(h,0,500);
      }
      if ((c(Property::dist) == "mBB" || c(Property::dist) == "mBBMVA")
          && c(Property::descr).Contains("CRLow")) changeRangeImpl(h,30,210);
      if ((c(Property::dist) == "mBB" || c(Property::dist) == "mBBMVA")
          && c(Property::descr).Contains("CRHigh")) changeRangeImpl(h,50,500);

      if (c(Property::dist) == "dRBB" ) changeRangeImpl(h,0,5);

      if (c(Property::dist) == "dEtaBB" ) changeRangeImpl(h,0,4.5);

      if (c(Property::dist) == "dPhiVBB" ){
        if ( c[Property::binMin] == 150 && (c[Property::nJet] == 2) ) changeRangeImpl(h, 2.011, 3.15000009537);
        else changeRangeImpl(h,0,3.15000009537);
      }

      if (c(Property::dist) == "dEtaVBB" ) changeRangeImpl(h,0,5);

      if (c(Property::dist) == "mLL" ) changeRangeImpl(h,80,102);

      if (c(Property::dist) == "pTJ3" ) changeRangeImpl(h,0,150);

      if (c(Property::dist) == "mBBJ3" ) changeRangeImpl(h,0,750);
   }
}
