#include "xmlutils.hpp"
#include "WSMaker/utility.hpp"
#include <iostream>
#include <string.h>

XMLNodePointer_t XMLUtils::GetChildNodeByName(TXMLEngine& xml, XMLNodePointer_t parent_node, const TString& requested_name)
{
    // iterate over all child nodes connected to here, then return the first one whose name matches
    XMLNodePointer_t child = xml.GetChild(parent_node);
    while(child != 0) {
	if(strcmp(xml.GetNodeName(child), requested_name.Data()) == 0) {
	    break;
	}
	
	child = xml.GetNext(child);
    }
    return child;
}

TString XMLUtils::GetAttribute(TXMLEngine& xml, XMLNodePointer_t node, const TString& attr_name)
{
    TString res(xml.GetAttr(node, attr_name));
    return res;
}

float XMLUtils::GetFloatAttribute(TXMLEngine& xml, XMLNodePointer_t node, const TString& attr_name)
{
    return XMLUtils::GetAttribute(xml, node, attr_name).Atof();
}

std::vector<TString> XMLUtils::GetStringListAttribute(TXMLEngine& xml, XMLNodePointer_t node, const TString& attr_name, char delim)
{
    XMLAttrPointer_t attr = xml.GetFirstAttr(node);
    while (attr!=0) {
	attr = xml.GetNextAttr(attr);
    }

    return Utils::splitString(XMLUtils::GetAttribute(xml, node, attr_name), delim);
}
