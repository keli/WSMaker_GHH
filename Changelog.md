# Changelog for WSMaker\_VHbb
## Unreleased


## [00-03-04-fix] - 2020-02-29
VH-unblinded results with plots

### Fixed
   - from WSMakerCore : pullComp still improved for comp 2 fits but as before for more

## [00-03-04] - 2020-02-29
VH-unblinded results

### Fixed
   - ZZ PtV hack removed
   - from WSMakerCore : fix from symmetrization when using smoothaftermerge

### Changed
   - separate CRLow/CRHigh extrapolation from mBB(BDTr) shapes for Z(W)+jets (uncorrelated in the case of Z)


## [00-03-03] - 2020-02-18
VV-unblinded results

### Fixed
  - plot range for 2-lep pTV plots
  - post-processing small fix

### Changed
  - ratio y-range in post-fit (flag introduced set on)
  - uncorrelated ttbar PtV/ME in jet bins
  - softMET binning added
  - mu plotting (and STXS) updates
  - from WSMakerCore : other POIs in float norm and data stat now
  - from WSMakerCore : various plotting and changes (including to money plot)

## [00-03-02bis] - 2020-02-05
VV-unblinding closure 0-lep and combined

### Fixed
  - VV PtV protection now universal

## [00-03-02] - 2020-02-05
VV-unblinding closure

### Fixed
  - Various ploting labels/features

### Added
  - STXS plotting tools and 6POI param (75-150 in 1-lep)
  - EFT parametrizations
  - BDTr syst model
  - Split-input post-processing
  - pre-fit to post-fit ratio on data/MC
  - ttbar bc/oth acc unc
  - prior for empty bins in DD-ttbar

### Changed
  - PCBT b-tagging unc scheme
  - low pTV (75-150) correlation scheme
  - VH acceptance uncertainties
  - no prunYao on light-0 and Vcl/Vl acc unc
  - ZZ PtV in 0-lep always pruned (input bug)

## [00-03-01] - 2019-09-17
Milestone 1: see [P&P talk link](https://indico.cern.ch/event/845094/contributions/3548756/attachments/1906579/3149092/calvet_VHbb-ppweek_190912.pdf)

### Fixed
  - Fixes for STXS inputs: diboson analysis now working, post-fit in 1-mu scheme

### Added
  - New analysis categories

### Changed
  - Gammas to Gaussian
  - Prune/Smooth after merge always
  - Update unc schemes: WPtV, ttbarNorms
  - Parabolic JER smoothing
  - Update DD-ttbar values
  - launch_default setup (DD-ttbar, 1-mu STXS)


## [00-03-00] - 2019-09-05
First full Run 2 setup

### Fixed
  - Lumi labels

### Changed
  - Update unc: JES/JER+ftag schemes, lumi
  - Blinding !
  - Plotting VpT summed over VpT categories

### Added
  - Full support for pTV region split
  - Support for adding mc16e

### Removed
  - ggZZ scale factor (core)


## [00-02-03] - 2019-05-23
STXS paper tag + first fullRun2 adds on

### Changed
  - Latest WSMakerCore
  - final STXS prescriptions: unc and bins
  - get PS for STXS from external histo

### Added
  - Runing on condor batch
  - XS version of STXS workspaces
  - possibility to use PtV in Whf CR
  - prepare data-driven ttbar to PtV split at 250 GeV


## [00-02-02] - 2018-09-13
ICHEP 2018 Hbb Observation paper tag !

### Fixed
  - scripts to make paper-ready tables
  - stat error calculation in `MakeDataDrivenTTbarHistos.py`

### Changed
  - Latest WSMakerCore
  - Reduced UE/PS uncertainties
  - Unblinding !
  - Lots of fixes on the plots
  - PruneAfterMerge and SmoothAfterMerge as default for stxs

### Added
  - `NicePlot_SimpleMu` script to make final mu plots
  - code for combinations of results
  - `ObservationResults_HowTo.md` documentation to reproduce ICHEP results and plots
  - ggZZ k-factor
  - Possibility for mc16a+d fits (separated histograms 2016/2017)
  - Samples and systematics for STXS studies


## [00-02-01] - 2018-06-29
First unblinded version, before agreed-upon changes

### Changed
  - Latest WSMakerCore version
  - Diboson always gray in postfit plots, even when signal


## [00-02-00] - 2018-06-29
### Fixed
  - lxbatch submission scripts

### Changed
  - Lots of logic and switches in `launch_all_default.py` (not listing everything here)
  - Update to ICHEP systematics
  - New blinding strategies
  - Improved `RenameVar.py`
  - `postfit_plot.py` for postfit plots of input variables
  - Latest WSMakerCore version

### Removed
  - unmaintained `launch_all_jobs.py`

### Added
  - Implementation of data-driven ttbar fit
  - ICHEP inputs config files
  - MJ in inputsChecks script
  - Script to make directly yields table in same format as the paper `makePaperTables.py`
  - script to make breakdown table in same format as in the paper `Breakdown_latex.sh`


## [00-01-02] - 2018-04-27
Results based on CxAOD31 for P&P Week
### Added
  - script for inputs validation: `InputsChecks_VHbb.py`
  - new inputs based on CxAOD v31 ; no exp/modelling systs

### Changed
  - use symmetriseOneSided for sys configs (follow new master of WSMaker)
  - `launch_default_jobs` needs mcType config to set the lumi on plots correctly
  - some changes to scripts to be compatible with python3

### Fixed
  - setup script now works for everyone
  - some tiny bugfixes
  - make GammaLx pull plots work again


## [00-01-01] - 2018-03-20
Based on tag 11-01-00 from WSMaker
### Removed
  - `RegionTracker_VHbbRun2`

### Changed
  - Updated HowToMigrate


## [00-01-00] - 2018-03-20
First release
### Added
  - Everything coming from WSMaker
  - Changelog.md
  - HowToMigrate.md

