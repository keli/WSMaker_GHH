#ifndef postprocessing_eft_ghhrun2
#define postprocessing_eft_ghhrun2

#include <TString.h>
#include <map>
#include "WSMaker/postprocessing.hpp"

#include "CombinedMeasurement.h"
#include "Measurement.h"
#include "CorrelationScheme.h"
#include "ParametrisationSequence.h"

class Configuration;

class PostProcessingTool_EFT_GHHRun2 : public PostProcessingTool
{
protected:
    // some meaningful aliases for important types
    using single_bin_parametrisation = std::vector<std::pair<std::vector<TString>, float>>;
    using multi_bin_parametrisation = std::map<TString, single_bin_parametrisation>;
    using operator_list = std::vector<TString>;
    using bin_list = std::vector<TString>;

    // a polynomial is defined by its variables, the bins it affects, and the coefficients
    using polynomial = std::tuple<operator_list, bin_list, multi_bin_parametrisation>;

    PostProcessingTool_EFT_GHHRun2() = delete;
    PostProcessingTool_EFT_GHHRun2(const Configuration& conf);

    // -------------------------------------------------
    // some helper functions that are used internally
    // -------------------------------------------------
    /**
     * @brief load the STXS parametrisation from its XML file
     *
     * @param path path to the XML file containing the parametrisation
     * @return the parametrisation object
     *
     * The XML configuration file contains two pieces of information:
     * 1) The names of the operators which it supports
     * 2) The actual parametrisation of a number of (STXS) bins in terms
     *    of the Wilson coefficients of these operators. For each bin,
     *    the quantity \sigma / \sigma_{SM} is expressed as a polynomial of
     *    order n:
     *
     *    \sigma / \sigma_{SM} = 1 + c_i \theta_i  + c_{ij} \theta_i\theta_j + ...,
     *
     *    where the \theta_i are the Wilson coefficients of the individual
     *    operators and the constants c_i are the corresponding coefficients.
     *
     *    The only information required to fully determine such a polynomial are the
     *    parametrisation coefficients in front of each occuring monomial. Each such
     *    term will always be a product of at most n Wilson coefficients, so it can 
     *    be represented as a list (of at most length n) that contains the names of
     *    the corresponding operators.
     *
     * The XML file must have the following structure (an example is provided in src/parametrisation_example.xml):
     * 
     * <root>
     *    <operators operators="op_1,op_2,op_3,...,op_n" />
     *
     *    <parametrisation name="parametrisation">
     *       <bin name="SigXsecOverSMQQZHx75x150PTV">
     *          <contrib coeff="1.23456789" term="op_1,op_5,..." />
     *          <contrib ... />
     *       </bin>
     *       ...
     * </root>
     * 
     * The <operators> node defines the names of supported operators as a comma-separated list.
     * The <parametrisation> node defines the actual parametrisation. It contains a separate
     * <bin> node for each bin that is part of the parametrisation and identifies it with its
     * bin name. The body of the <bin> section determines the shape of the polynomial that
     * parametrises this bin. Each term in the polynomial (except the leading "1", which is 
     * hard-coded) is specified by a <contrib> node, listing the numerical value of the coefficient
     * c_{ij...} as well as the (comma-separated) list of Wilson coefficients whose product
     * multiplies this coefficient.
     */
    multi_bin_parametrisation loadParametrisation(const TString& path);

    /**
     * @brief load the list of operators supported by a certain parametrisation
     *
     * @param path path to the XML file containing the parametrisation
     * @return the list of operator names whose parametrisations are available
     */    
    operator_list loadOperatorList(const TString& path);

    /** 
     * @brief load the list of bins supported by a certain parametrisation
     *
     * @param path path to the XML file containing the parametrisation
     * @return the list of bins for which parametrisations are available
     */
    bin_list loadBinListFromParametrisation(const TString& path);

    /**
     * @brief load the list of bins available in a certain workspace
     *
     * Fetches the list of POI names from the input workspace. These
     * correspond to the names of the STXS bins.
     *
     * @param path path to the ROOT file containing the input STXS workspace
     * @return the list of POIs found in this workspace, i.e. the list of names
     * of the STXS bins.
     */
    bin_list loadBinListFromWorkspace(const TString& path);

    /**
     * @brief thin wrapper around a call to the CombinationTool that actually
     * invokes the workspace EDIT commands
     *
     * @param scheme reference to the ParametrisationScheme for the workspace
     * @param cmd RooFit command to edit the workspace
     */
    void placeCommand(ParametrisationScheme& scheme, const TString& cmd);

    /**
     * @brief load a polynomial from its defining xml file
     *
     * @param param_file_path path to the xml file containing the definition of this polynomial
     */
    polynomial loadPolynomial(const TString& param_file_path);

    /**
     * @brief checks whether a polynomial contains certain bins and operators, raises an Exception if it doesn't
     *
     * @param to_check polynomial to be checked
     * @param bins list of bins that this polynomial is required to cover
     * @param operators list of operators this polynomial is required to contain
     */    
    bool checkPolynomialIntegrity(const polynomial& to_check, const std::vector<TString>& bins, const std::vector<TString>& operators);

    /**
     * @brief print a polynomial
     *
     * @param to_print the polynomial to print
     */    
    void printPolynomial(const polynomial& to_print);

    /**
     * @brief assemble a polynomial in the RooFit workspace
     * 
     * @param param_scheme the ParametrisationScheme to contain the polynomial
     * @param poly the polynomial that is to be constructed
     * @expr_name the name of the resulting expression in the workspace
     */    
    void assemblePolynomial(ParametrisationScheme& param_scheme, const polynomial& poly, const TString& expr_name);

    /**
     * @brief reparametrise a RooFit workspace
     *
     * @param wsfilename path to the ROOT file containing the input workspace. The reparametrised workspace will be written to the same location
     * @param paramFile path to the xml file containing the parametrisation
     * @param POI_names names of the new POIs after the reparametrisation
     * @param POI_ranges numerical ranges of these POIs, given as symmetric intervals around zero
     * @param active_bins names of the original POIs in the workspace that should be reparametrised (usually STXS signal strenghts)
     */
    void reparametrise(const TString& wsfilename, const TString& param_expr, const std::vector<TString>& polynominal_names, const std::vector<TString>& polynomial_defs, const std::vector<TString>& POI_names,
		       const std::vector<double>& POI_ranges, const std::vector<TString>& active_bins);

public:
    // implementation of the methods prescribed by the base class in WSMaker/postprocessing.hpp
    virtual ~PostProcessingTool_EFT_GHHRun2() = default;
    static void createInstance(const Configuration& conf);

    /**
     * @brief apply the EFT parametrisation of STXS bins.
     *
     * @param wsfilename The path to the file containing the HistFactory 
     * workspace created by the @c Engine.
     *
     * Configuration flags used:
     * @conf{doEFT,false} EFT master switch, enables EFT reparametrisation of the workspace.
     * @conf{doEigenoperators,false} switch to enable fitting of eigenoperators or other linear combinations of operators.
     * @conf{paramExpr} expression for the final parametrisation
     * @conf{polynomialNames} identifiers for the individual polynomials
     * @conf{polynomialDefs} paths to the xml files defining each polynomial
     * @conf{EigenoperatorDefsFile} path to the xml file containing the eigenoperator definitions
     * @conf{activePOIs} List of POIs (= Wilson coefficients) that will show up in the reparametrised workspace.
     * @conf{activeEigenoperators} List of Eigenoperators that will show up in the final reparametrised workspace.
     * @conf{POIRanges} List of numerical ranges for the individual POIs, this will determine the limits of the underlying RooRealVars.
     * @conf{EigenPOIRanges} List of numerical ranges for the individual eigen POIs, this will determine the limits of the underlying RooRealVars.
     * @conf{activeBins} List of STXS bins that should be used as input to the reparametrisation.
     */
    void process(const TString& wsfilename);
};

#endif
