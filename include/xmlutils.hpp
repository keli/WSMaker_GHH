#ifndef xmlutils_hpp
#define xmlutils_hpp

#include <TString.h>
#include "TXMLEngine.h"

/**
 * @brief Utility class to parse XML files.
 *
 * This class contains several convenience methods to retrieve attributes from an XML file.
 */

class XMLUtils {
public:
    /**
     * @brief Retrieve pointer to XML child node.
     *
     * @param xml the ROOT::TXMLEngine handling the file.
     * @param parent_node the XML node where we should start searching.
     * @param requested_name name of the requested child node.
     * @return reference to the child node.
     */
    static XMLNodePointer_t GetChildNodeByName(TXMLEngine& xml, XMLNodePointer_t parent_node, const TString& requested_name);

    /**
     * @brief Fetch a named attribute of an XML node.
     * 
     * @param xml the ROOT::TXMLEngine handling the file.
     * @param node the xml node whose attribute we want.
     * @param attr_name the name of the desired attribute.
     * @return the attribute content.
     */
    static TString GetAttribute(TXMLEngine& xml, XMLNodePointer_t node, const TString& attr_name);

    /**
     * @brief Fetch a named attribute of an XML node and convert it into a float.
     *
     * @param xml the ROOT::TXMLEngine handling the file.
     * @param node the xml node whose attribute we want.
     * @param attr_name the name of the desired attribute.
     * @return the attribute content.
     */
    static float GetFloatAttribute(TXMLEngine& xml, XMLNodePointer_t node, const TString& attr_name);

    /**
     * @brief Fetch a named attribute of an XML node representing a list and convert it into an std::vector.
     *
     * Sometimes it is convenient to store a list as a string with the individual list entries separated by a delimiter. This function reads this list representation and converts it back into a genuine std::vector.
     *
     * @param xml the ROOT::TXMLEngine handling the file.
     * @param node the xml node whose attribute we want.
     * @param attr_name the name of the desired attribute.
     * @delim delimiter used in the specification of the list.
     * @return the attribute content.
     */
    static std::vector<TString> GetStringListAttribute(TXMLEngine& xml, XMLNodePointer_t node, const TString& attr_name, char delim = ',');
};

#endif
