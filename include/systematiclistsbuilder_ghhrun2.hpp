#ifndef systematiclistsbuilder_ghhrun2_hpp
#define systematiclistsbuilder_ghhrun2_hpp

#include "WSMaker/systematiclistsbuilder.hpp"

class RegionTracker;

/**
 * @brief Implementation of @c SystematicListsBuilder for the Run2 HVT/AZh analyses
 *
 */
class SystematicListsBuilder_GHHRun2 : public SystematicListsBuilder
{
  public:

    /// Inherit the constructors
    using SystematicListsBuilder::SystematicListsBuilder;

    /// Empty destructor
    virtual ~SystematicListsBuilder_GHHRun2() {}

    /**
     * @brief Fill the map of renaming rules for systematics names
     *
     * This is typically used to correct for bugs in syst names in some inputs
     */
    virtual void fillHistoSystsRenaming();

    // list them all !
    /**
     * @brief Populate the list of user-defined systematics
     *
     * At the moment, store a stripped-down copy of the GHH Run1 function
     *
     * Configuration flags used:
     * @conf{DoDiboson,false} use @c WZ+ZZ as signal
     * @conf{DecorrPOI,""} decorrelate POI using the provided comma-separated list of Property names
     * @conf{MassPoint,125} Higgs mass considered
     *
     * @see @c SysConfig for the possibilities to configure systematics
     *
     * @param useFltNorms Whether some backgrounds should be let free, or be constrained to a prior.
     * That parameter is no longer used.
     */
    virtual void listAllUserSystematics(bool useFltNorms);

  /**                                                                                                                                                                                                    
   * @brief Populate the list of user-defined QCD scale systematics for STXS
   *
   * @see @c listAllUserSystematics adding the STXS section for the listAllUserSystematics [not added], should be only called when doSTXS is true
   *
   *
   */
  void listAllUserSystematics_STXSQCD();

  /**                                                                                                                                                                                                    
   * @brief Populate the list of user-defined QCD scale systematics for STXS
   *
   * @see @c listAllUserSystematics adding the STXS section for the listAllUserSystematics [not added], should be only called when DoSTXS is true
   *
   *
   */
  void listAllUserSystematics_STXSPDF();

  /**                                                                                                                                                                                                    
   *                                                                                                                                                                                                     
   * @brief This function allows to set the theory sys from file (PDFs and scales)
   *                                                                              
   * This function allows to set pdf sys using a TH2F stored inside a root file. 
   *         
   * @param rootFile Input file with its path
   * @param inputHisto Name of the TH2F with the sys
   *                                                                                                                                                                     
   */
  void getSysFromFile(const TString& rootFile, const TString& inputHisto);

  /**                                                                                                                                                                                                    
   *                                                                                                                                                                                                     
   * @brief This function allows to set the PS sys from file 
   *                                                                              
   * This function allows to set PS sys using a TH2F stored inside a root file. This function is called only for doSTXS = True
   *         
   * @param rootFile Input file with its path
   * @param inputHisto Name of the TH2F with the PS sys
   *                                                                                                                                                                     
   */
  void getPSSysFromFile(const TString& rootFile, const TString& inputHisto);
  
  /**                                                                                                                                                                                                    
   *                                                                                                                                                                                                     
   * inside listAllUserSystematics, to add POI for STXS study                                                                                                                                            
   * The POI sets depend on the fitting scheme.                                                                                                                                                          
   *                                                                                                                                                                                                     
   * Configuration flags used:                                                                                                                                                                           
   * @conf{FitSTXSScheme,1} use @c one POI as default                                                                                                                                                    
   */
  void addPOI_STXS();

    /**
     * @brief Populate the list of expected histo-based systematics
     *
     * Empty at the moment
     *
     * @see @c SysConfig for the possibilities to configure systematics
     *
     * Configuration flags used:
     * @conf{DoSTXS,False} by default, it should be as same as ICHEP
     * @conf{DoSTXSShapeOnly,False} (DoSTXS&&DoSTXSShapeOnly) makes mBB/PTV signal shape systematics shape only, otherwise as same as ICHEP
     *
     */
    virtual void listAllHistoSystematics();

};

#endif
