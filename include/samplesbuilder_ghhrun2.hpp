#ifndef samplesbuilder_ghhrun2
#define samplesbuilder_ghhrun2

#include "WSMaker/samplesbuilder.hpp"
#include "WSMaker/sample.hpp"

/**
 * @brief Implementation of @c SamplesBuilder for the Run2 GHH analysis
 *
 */
class SamplesBuilder_GHHRun2 : public SamplesBuilder {
  public:
    /// Inherit the constructors
    using SamplesBuilder::SamplesBuilder;

    /// Destructor
    virtual ~SamplesBuilder_GHHRun2() {}

    /*
     * @brief Builds the declared samples and add them to the list
     *
     * We only change the signal (based on @c Configuration::analysisType() )
     * and the backgrounds (SM Higgs, diboson) compared to the GHH analysis
     *
     * The switches read in the configuration file are:
     * @conf{MassPoint,125} Resonance mass to use
     * @conf{DoDiboson,false} to run with diboson as signal instead of Higgs
     *
     * @see SamplesBuilder
    */
    virtual void declareSamples();

    /**
     * @brief Fill @c m_keywords
     *
     * @see SamplesBuilder
     */
    virtual void declareKeywords();
    
    

    /**
     * @brief Main function to declare the merging of samples
     *
     * The switches read in the configuration file are:
     * @conf{MassPoint,125} Resonance mass to use
     * @conf{DoDiboson,false} to run with diboson as signal instead of Higgs
     * @conf{FitWHZH,false} prepare workspace with split ZH and WH (but ZH=qqZH+ggZH)
     * @conf{FitWZZZ,false} prepare workspace with split ZZ and WZ
     * @conf{FitVHVZ,false} prepare workspace with VH and VZ samples
     *
     * @see SamplesBuilder
     */
    virtual void declareSamplesToMerge();

  private:

    /*
     * @brief Builds the declared samples and add them to the list
     *
     * We only change the signal (based on @c Configuration::analysisType() )
     * and the backgrounds (SM STXS Higgs, diboson) compared to the GHH analysis
     *
     * The switches read in the configuration file are:
     * @conf{DoSTXS,False} to use
     *
     * @see SamplesBuilder_GHH
    */
    void declareSamples_STXS(SType type = SType::Sig);
    void declareKeywords_STXS();
    void declareSamplesToMerge_STXS();

};

#endif
